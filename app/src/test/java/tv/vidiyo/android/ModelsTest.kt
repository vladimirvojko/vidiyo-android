package tv.vidiyo.android

import com.google.gson.Gson
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import tv.vidiyo.android.api.model.Message
import tv.vidiyo.android.api.model.ServerResponse
import tv.vidiyo.android.api.model.user.Profile

class ModelsTest {

    lateinit var gson: Gson

    @Before
    fun setup() {
        gson = Gson()
    }

    @Test fun parsing_error() {
        val jsonStr = "{\"status\": \"error\",\"error\": {\"error_code\": 2,\"msg\": \"Login failed.\"}}"

        val error = gson.fromJson<ServerResponse<Message>>(jsonStr, ServerResponse::class.java)
        print(error.error)
    }

    @Test fun parsing_success() {
        val jsonStr = "{\"status\": \"SUCCESS\",\"data\": {\"id\": 808,\"name\": \"Jian Yang\",\"email\": \"jian.yang.v@gmail.com\",\"token\": \"91ecdb775c2559e23591b67acacae7a1\",\"user_name\": \"jian\",\"bio\": \"I eat the fish\",\"gender\": \"male\",\"birthday\": \"07/03/1993\",\"location\": \"San Francisco, California\",\"phone\": \"394354534\",\"website\": \"jianyang.com\",\"image\": \"https://vidiyo-prod.s3-us-west-2.amazonaws.com/user/r1cXFFvEZ.png\",\"is_private\": false,\"is_facebook_connected\": false,\"is_contacts_connected\": false,\"is_email_verified\": false}}"

        val response = gson.fromJson<ServerResponse<*>>(jsonStr, ServerResponse::class.java)
        val data = gson.fromJson<Profile>(gson.toJson(response.data), Profile::class.java)

        assertEquals("Jian Yang", data.name)
    }
}