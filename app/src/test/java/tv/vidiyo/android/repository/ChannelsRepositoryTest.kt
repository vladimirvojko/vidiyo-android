package tv.vidiyo.android.repository

import org.junit.Assert
import org.junit.Before
import org.junit.Test
import tv.vidiyo.android.api.ApiTestConfigurator
import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.source.channels.ChannelsRepository
import tv.vidiyo.android.wait

class ChannelsRepositoryTest {

    companion object {
        val lock = Any()
    }

    lateinit var service: VidiyoService
    lateinit var repository: ChannelsRepository

    @Before fun setup() {
        service = ApiTestConfigurator.configure("91ecdb775c2559e23591b67acacae7a1")
        repository = ChannelsRepository(service)
//        repository.refresh(object : DataSource.Callback {
//            override fun onDataLoaded(vararg args: Any) {
//                println("onDataLoaded")
//                lock.send()
//            }
//
//            override fun onDataNotAvailable(error: Message) {
//                println("onDataNotAvailable: $error")
//                lock.send()
//            }
//        })

        lock.wait()
    }

    @Test fun functionality() {
        val item = repository.getItem(0)
        Assert.assertEquals(30, item.id)
    }
}