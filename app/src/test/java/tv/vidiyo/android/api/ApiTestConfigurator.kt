package tv.vidiyo.android.api

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import tv.vidiyo.android.BuildConfig
import java.io.IOException

class ApiTestConfigurator {

    class BearerAuthorizationInterceptor(private val token: String) : Interceptor {
        @Throws(IOException::class)
        override fun intercept(chain: Interceptor.Chain): Response {
            val original = chain.request()

            val requestBuilder = original.newBuilder().apply {
                if (original.url().toString().contains(BuildConfig.SERVER_URL)) {
                    header("Authorization", "Bearer " + token)
                }
            }.method(original.method(), original.body())
            return chain.proceed(requestBuilder.build())
        }
    }

    companion object {
        fun configure(token: String?): VidiyoService {
            val httpClientBuilder = OkHttpClient.Builder()
                    .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))

            token?.let {
                httpClientBuilder.addInterceptor(BearerAuthorizationInterceptor(it))
            }

            val client = httpClientBuilder.build()
            val retrofit = Retrofit.Builder()
                    .baseUrl(BuildConfig.SERVER_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client).build()

            return retrofit.create(VidiyoService::class.java)
        }
    }
}