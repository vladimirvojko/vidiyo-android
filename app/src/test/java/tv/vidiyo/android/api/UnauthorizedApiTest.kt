package tv.vidiyo.android.api

import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import tv.vidiyo.android.api.model.user.Profile

class UnauthorizedApiTest {

    lateinit var service: VidiyoService

    @Before fun setup() {
        service = ApiTestConfigurator.configure(null)
    }

    @Test fun call_success() {
        val loginResponse = service.login("jian", "qwe").execute()
        val phoneResponse = service.createPhone("380664202020").execute()
//        val validateCodeResponse = service.validateCode("9999", "380664202020").execute()

        assertEquals(null, loginResponse.errorBody())
        assertEquals(null, phoneResponse.errorBody())
//        assertEquals(null, validateCodeResponse.errorBody())
    }

    @Test fun te() {
        val loginResponse = service.login("appleseed22", "qwe").execute()
    }

    @Test fun call2_success() {
        val validateResponse = service.validate("jian.yang.v@gmail.com").execute()
        val socialResponse = service.socialAuth("google-auth", "100244737661898619039", "eyJhbGciOiJSUzI1NiIsImtpZCI6Ijg2MmU4MDczYjQzMTk1ZDNkMTAzMmJlODdmMTNmNGM4OTE0NTUwY2YifQ.eyJhenAiOiI0MDA3Mzg2NDUxMzUtbXM5Zzk3b2RtM2htdjg2NTU0b3VwMDZkdGY4ZHMwZHMuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJhdWQiOiI0MDA3Mzg2NDUxMzUtbDdkaTU1bG42NzFyaDJqZ3AyNm1zazE2cWtpaHA4a2wuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJzdWIiOiIxMDAyNDQ3Mzc2NjE4OTg2MTkwMzkiLCJlbWFpbCI6InNseXRoZXJpbi52aWRpeW9AZ21haWwuY29tIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsImlzcyI6Imh0dHBzOi8vYWNjb3VudHMuZ29vZ2xlLmNvbSIsImlhdCI6MTUwMDQ2MTI0MywiZXhwIjoxNTAwNDY0ODQzLCJuYW1lIjoiU2FsYXphciBTbHl0aGVyaW4iLCJwaWN0dXJlIjoiaHR0cHM6Ly9saDYuZ29vZ2xldXNlcmNvbnRlbnQuY29tLy1pUXN5TURBUlBrcy9BQUFBQUFBQUFBSS9BQUFBQUFBQUFBQS9BSTZ5R1h6dFlFNUR1VEVjWmdqcTBYWnA5Y3JkUHgyRGZRL3M5Ni1jL3Bob3RvLmpwZyIsImdpdmVuX25hbWUiOiJTYWxhemFyIiwiZmFtaWx5X25hbWUiOiJTbHl0aGVyaW4iLCJsb2NhbGUiOiJlbiJ9.Tq2q9PWRmLDOHfRnv-5wG9Fb6OC3ohWJi5twpa9xrBxd_wj-6SZhp8epRq1mDW2ctDe3Se8815RDY_fYIrqm8ePOX6yFNeIqSWodjtQhpugAhuyNOBawLcA7W2ytOdASzj0Hqb61-7N7qHYYTVzVLeXH59Zye6Xl6uMNQy6WbevYeq481h-AuXZoTsNzDmAU0TRFScI_kpALvW8EAC2qvTUyfdZjVV2d3M0Pt7k5fdhq7AdFZ_hmST-t-Fa27K9njbm0Mv9ltnJ3BUqHOlSQQB_bYMl97DVE3zNvnXtjC5xSUba8b19Pcw5r49uht6VWTKDWeydbNvRydWuvOXItLA")
                .execute()

        assertEquals(null, validateResponse.body())
        assertEquals(null, socialResponse.errorBody())
    }

    @Test fun register() {
        val profile = Profile().apply {
            username = "appleseed22"
            name = "Johnny Appleseed"
            email = "johnny.appleseed@apple.com"
        }

        val password = "qwe"

        val registerResponse = service.register(profile, password).execute()
        assertEquals(null, registerResponse.errorBody())
    }

    @Test fun anon() {
        val anonResponse = service.createAnonymous().execute()

        assertEquals(null, anonResponse.errorBody())
    }
}