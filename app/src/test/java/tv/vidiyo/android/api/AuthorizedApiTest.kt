@file:Suppress("FunctionName")

package tv.vidiyo.android.api

import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotEquals
import org.junit.Before
import org.junit.Test
import tv.vidiyo.android.api.model.Message
import tv.vidiyo.android.api.model.ServerResponse
import tv.vidiyo.android.api.model.episode.ViewInfo
import tv.vidiyo.android.api.model.show.NewShowEpisode
import tv.vidiyo.android.api.model.user.Friend
import tv.vidiyo.android.extension.ServerCallback
import tv.vidiyo.android.notify
import tv.vidiyo.android.util.PriceFormat
import tv.vidiyo.android.wait

class AuthorizedApiTest {

    companion object {
        val success: String = "success"
    }

    lateinit var service: VidiyoService
    private val lock = Any()

    @Before
    fun setup() {
        service = ApiTestConfigurator.configure("91ecdb775c2559e23591b67acacae7a1")
    }

    // TODO: make more tests for each category
    @Test
    fun content_success() {
        val meResponse = service.me().execute().body()
        val homeResponse = service.home().execute().body()
        val channelsResponse = service.channels().execute().body()
        val userChannelsResponse = service.userChannels().execute().body()
        val channelInfoResponse = service.channelInfo(60).execute().body()
        val topicInfoResponse = service.topicInfo(17).execute().body()

        assertEquals(success, meResponse.status)
        assertEquals(success, homeResponse.status)

        assertEquals(success, channelsResponse.status)
        assertEquals(24, channelsResponse.data?.size) // changes are possible

        assertEquals(success, userChannelsResponse.status)
        assertEquals(success, channelInfoResponse.status)
        assertEquals(success, topicInfoResponse.status)
    }

    @Test
    fun show_success() {
        val id = 24

        val infoResponse = service.showInfo(id).execute().body()
        val castsResponse = service.showCasts(id).execute().body()
        val topicsResponse = service.showTopics(id).execute().body()
        val subscribersResponse = service.showSubscribers(id).execute().body()
        val episodesResponse = service.showEpisodes(id).execute().body()

        assertEquals(success, infoResponse.status)
        assertEquals(success, castsResponse.status)
        assertEquals(success, topicsResponse.status)
        assertEquals(success, subscribersResponse.status)
        assertEquals(success, episodesResponse.status)
    }

    @Test
    fun cast_success() {
        val id = 32

        val infoResponse = service.castInfo(id).execute().body()
        val episodesResponse = service.castEpisodes(id).execute().body()
        val showsResponse = service.castShows(id).execute().body()

        assertEquals(success, infoResponse.status)
        assertEquals(success, episodesResponse.status)
        assertEquals(success, showsResponse.status)
    }

    @Test
    fun like_success() {
        val target = "comment"
        val id = 6

        val likeResponse = service.like(target, id, true).execute().body()
        val removeResponse = service.likeRemove(target, id).execute().body()

        assertEquals(success, likeResponse.status)
        assertEquals(success, removeResponse.status)
    }

    @Test
    fun user_success() {
        val id = 808

//        val profileResponse = service.userProfile(id).execute().body()
//        val showsResponse = service.userShows(id).execute().body()
//        val episodesResponse = service.userEpisodes(id).execute().body()
//        val castsResponse = service.userCasts(id).execute().body()
//        val topicsResponse = service.userTopics(id).execute().body()
//        val confirmationResponse = service.resendEmailConfirmation().execute().body()

//        val channelId = 40
//        val showsChannelResponse = service.userShowsInChannel(id, channelId).execute().body()
//        val topicsChannelResponse = service.userTopicsInChannel(id, channelId).execute().body()
//        val castsChannelResponse = service.userCastsInChannel(id, channelId).execute().body()
//
//        val pendingStatusResponse = service.pendingStatus().execute().body()
//        val pendingRequestsResponse = service.pendingRequests().execute().body()

//        val blockedResponse = service.blocked().execute().body()
//        val followingResponse = service.userFollowing(id).execute().body()
//        val followersResponse = service.userFollowers(id).execute().body()

//        assertEquals(success, profileResponse.status)
//        assertEquals(success, showsResponse.status)
//        assertEquals(success, episodesResponse.status)
//        assertEquals(success, castsResponse.status)
//        assertEquals(success, topicsResponse.status)
//        assertEquals(success, confirmationResponse.status)
//
//        assertEquals(success, showsChannelResponse.status)
//        assertEquals(success, topicsChannelResponse.status)
//        assertEquals(success, castsChannelResponse.status)
//
//        assertEquals(success, pendingStatusResponse.status)
//        assertEquals(success, pendingRequestsResponse.status)
//
//        assertEquals(success, blockedResponse.status)
//        assertEquals(success, followingResponse.status)
//        assertEquals(success, followersResponse.status)
    }

    @Test
    fun episode_success() {
        val id = 15

        val infoResponse = service.episodeInfo(id).execute().body()
//        val castsResponse = service.episodeCasts(id).execute().body()
//        val topicsResponse = service.episodeTopics(id).execute().body()
//        val relatedResponse = service.episodeRelated(id).execute().body()

        assertEquals(infoResponse.status, success)
    }

    @Test
    fun channels_success() {
        val channelsResponse = service.channels().execute().body()
        val assignResponse = service.assignChannels(setOf(30, 32, 34)).execute().body()

        assertEquals(success, channelsResponse.status)
        assertEquals(success, assignResponse.status)
    }

    @Test
    fun subscription_success() {
        var e: Message? = null
        service.connect("655", "380930777127,380664382838", "kkxmshu@gmail.com,vladimirvojko@gmail.com").enqueue(object : ServerCallback<ServerResponse<Unit>>() {
            override fun onSuccess(response: ServerResponse<Unit>, url: String?) {
                lock.notify()
            }

            override fun onFailure(error: Message, url: String?) {
                e = error
                lock.notify()
            }
        })

        lock.wait()

        assertEquals(e, null)

        val facebookToken = "EAAEq7IrjpOwBAGEZAHbM91ZAKIgl2z4ABYChjB7YtCAyXQ9PmYndbHddZBEeptB20spEe4GGqDSm8B1wdoZA8XITRK6LMUKBWTKJBcnziaKGrOYrvlCyFzXAAJRUnb6oZBsNEqgCNQDZAjarzWdEiVdi4ZA4vYZCndfqXeEtTMbyZBqzHY6cZAaVJxn2gQhi9IZCxe2aQTmkOMQStCJ9jx8OuewKhCzdUxru7yCCMytusAP1o7utCIAqNymEdnvWUzbbIkZD"
        service.facebookFriends(facebookToken).enqueue(object : ServerCallback<ServerResponse<Array<Friend>>>() {
            override fun onSuccess(response: ServerResponse<Array<Friend>>, url: String?) {
                lock.notify()
            }

            override fun onFailure(error: Message, url: String?) {
                lock.notify()
            }
        })

        lock.wait()
    }

    @Test
    fun search_success() {
        var total = 1
        var count = 0
        while (count < total) {
            val response = service.searchPeople("t", count, 8).execute()
            val body = response.body()
            total = body.meta?.count ?: 1
            count += body.data?.size ?: 0

            println("total ->> $total, current ->> $count")
        }
    }

    @Test
    fun watching_success() {
        var newEpisodes: Array<NewShowEpisode>? = null
        service.newEpisodes().enqueue(object : ServerCallback<ServerResponse<Array<NewShowEpisode>>>() {
            override fun onSuccess(response: ServerResponse<Array<NewShowEpisode>>, url: String?) {
                newEpisodes = response.data
                lock.notify()
            }

            override fun onFailure(error: Message, url: String?) {
                lock.notify()
            }
        })
        lock.wait()

        assertNotEquals(newEpisodes, null)

        var viewed: Array<ViewInfo>? = null
        service.viewedInfo(10, null).enqueue(object : ServerCallback<ServerResponse<Array<ViewInfo>>>() {
            override fun onSuccess(response: ServerResponse<Array<ViewInfo>>, url: String?) {
                viewed = response.data
                lock.notify()
            }

            override fun onFailure(error: Message, url: String?) {
                lock.notify()
            }
        })
        lock.wait()

        assertNotEquals(viewed, null)
    }

    @Test
    fun backprop() {
        val response = service.userProfile(735).execute()
        val body = response.body()

        println(body)
    }

    //region Social

    @Test
    fun facebook_success() {
        val response = service.facebookAccessToken().execute()
        val accessToken = response.body()
        if (accessToken != null) {
            val url = Social.Facebook.URL + "thejenniferanistonpage/posts"
            val posts = service.facebookPageFeed(url, accessToken.token).execute()
            println(posts.body())
        }
    }

    @Test
    fun twitter_success() {
        val response = service.twitterAccessToken().execute()
        val accessToken = response.body()
        if (accessToken != null) {
            val screenName = "hookedonfriends"
            val tweets = service.twitterTweets(accessToken.token, screenName).execute()
            println(tweets.body())
        }
    }

    @Test
    fun instagram_success() {
        val response = service.instagramAccessToken().execute()
        val accessToken = response.body()?.data?.token
        if (accessToken != null) {
            val data = service.instagramUserSearch("vojkovladimir", accessToken).execute().body().data
            val recents = service.instagramRecents(data.firstOrNull()?.id ?: return, accessToken).execute().body()
            println("recents = $recents")
        }
    }

    @Test
    fun notifications_success() {
        val response = service.notifications().execute()
        val body = response.body()

        println(body)
    }

    @Test
    fun changeNotification_success() {
        val response = service.changeNotification(mapOf(ApiKey.newEpisodes to false)).execute()
        val body = response.body()

        println(body)
    }

    @Test
    fun price_format() {
        val id = 740
        val response = service.episodeInfo(id).execute()
        val products = response.body().data?.products

        val formatter = PriceFormat()
        products?.forEach {
            val current = it.price
            println("${it.name} ($current) ->> $" + formatter.format(current))
        }
    }
}