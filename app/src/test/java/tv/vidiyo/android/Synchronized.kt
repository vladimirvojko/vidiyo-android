@file:Suppress("PLATFORM_CLASS_MAPPED_TO_KOTLIN")

package tv.vidiyo.android

internal fun Any.wait() {
    synchronized(this) {
        (this as java.lang.Object).wait()
    }
}
internal fun Any.notify() {
    synchronized(this) {
        (this as java.lang.Object).notify()
    }
}