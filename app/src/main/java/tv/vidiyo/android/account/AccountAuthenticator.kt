package tv.vidiyo.android.account

import android.accounts.AbstractAccountAuthenticator
import android.accounts.Account
import android.accounts.AccountAuthenticatorResponse
import android.accounts.AccountManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.widget.Toast
import tv.vidiyo.android.BuildConfig
import tv.vidiyo.android.ui.boarding.BoardingActivity


class AccountAuthenticator(
        private val context: Context
) : AbstractAccountAuthenticator(context) {

    companion object {
        const val ACCOUNT_TYPE = BuildConfig.APPLICATION_ID + ".main"
        const val AUTH_TOKEN_TYPE = BuildConfig.APPLICATION_ID
        const val ERROR_CODE_ONE_ACCOUNT_ALLOWED = 101
    }

    interface Key {
        companion object {
            const val AUTH_TOKEN_TYPE = "KEY_AUTH_TOKEN_TYPE"
        }
    }

    private val handler = Handler()

    override fun getAuthTokenLabel(authTokenType: String?) = null

    override fun confirmCredentials(response: AccountAuthenticatorResponse?, account: Account?, options: Bundle?) = null

    override fun updateCredentials(response: AccountAuthenticatorResponse?, account: Account?, authTokenType: String?, options: Bundle?) = null

    override fun getAuthToken(response: AccountAuthenticatorResponse?, account: Account?, authTokenType: String?, options: Bundle?) = null

    override fun hasFeatures(response: AccountAuthenticatorResponse?, account: Account?, features: Array<out String>?) = Bundle().apply {
        putBoolean(AccountManager.KEY_BOOLEAN_RESULT, false)
    }

    override fun editProperties(response: AccountAuthenticatorResponse?, accountType: String?) = null

    override fun addAccount(response: AccountAuthenticatorResponse?, accountType: String?, authTokenType: String?, requiredFeatures: Array<out String>?, options: Bundle?): Bundle {
        return if (AccountManager.get(context).getAccountsByType(ACCOUNT_TYPE).isEmpty()) Bundle().apply {
            val intent = Intent(context, BoardingActivity::class.java)
            intent.putExtra(Key.AUTH_TOKEN_TYPE, AUTH_TOKEN_TYPE)
            intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response)
            putParcelable(AccountManager.KEY_INTENT, intent)
        } else Bundle().apply {
            val errorMessage = "Only One Account Allowed"
            putInt(AccountManager.KEY_ERROR_CODE, ERROR_CODE_ONE_ACCOUNT_ALLOWED)
            putString(AccountManager.KEY_ERROR_MESSAGE, errorMessage)
            handler.post { Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show() }
        }
    }

}