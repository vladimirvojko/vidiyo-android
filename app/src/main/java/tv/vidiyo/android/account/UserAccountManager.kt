package tv.vidiyo.android.account

import android.accounts.Account
import android.accounts.AccountManager
import android.annotation.SuppressLint
import android.content.Context
import android.os.*
import tv.vidiyo.android.api.model.user.UserAccount
import tv.vidiyo.android.extension.weak

class UserAccountManager(context: Context) {
    private val manager = AccountManager.get(context)
    private val handler = Handler(HandlerThread("AccountManager", Process.THREAD_PRIORITY_BACKGROUND).apply { start() }.looper)

    var token: String? = refreshAccount()?.token
    var account: UserAccount? = refreshAccount()
        private set
        get() = refreshAccount()?.let {
            field = it
            field
        }

    fun addAccount(newAccount: UserAccount): Boolean {
        if (account == null) {
            account = newAccount // TODO: fix nullable account
            newAccount.account = Account(newAccount.username ?: "ANONYMOUS USER", AccountAuthenticator.ACCOUNT_TYPE) // TODO: handle username

            if (manager.addAccountExplicitly(newAccount.account, newAccount.password, newAccount.bundle)) {
                manager.setAuthToken(newAccount.account, AccountAuthenticator.AUTH_TOKEN_TYPE, newAccount.token)
                token = newAccount.token
                return true
            }
        }

        return false
    }

    fun update(account: UserAccount, completion: () -> Unit, failure: (() -> Unit)? = null) {
        remove(object : AccountRemoveCallback {
            override fun onSuccess() {
                addAccount(account)
                completion()
            }

            override fun onFailure() {
                failure?.invoke()
            }
        })
    }

    fun updateAccountData(userAccount: UserAccount) {
        account?.account?.let {
            manager.setUserData(it, Key.ID, userAccount.id.toString())
            manager.setUserData(it, Key.USERNAME, userAccount.username)
            manager.setUserData(it, Key.IMAGE, userAccount.image)
            manager.setUserData(it, Key.NAME, userAccount.name)

            manager.setUserData(it, Key.GENDER, userAccount.gender)
            manager.setUserData(it, Key.EMAIL, userAccount.email)
            manager.setUserData(it, Key.BIO, userAccount.bio)
            manager.setUserData(it, Key.BIRTHDAY, userAccount.birthday)
            manager.setUserData(it, Key.LOCATION, userAccount.location)
            manager.setUserData(it, Key.PHONE, userAccount.phone)
            manager.setUserData(it, Key.WEBSITE, userAccount.website)
            manager.setUserData(it, Key.IS_PRIVATE, userAccount.isPrivate.toString())
            manager.setUserData(it, Key.IS_VERIFIED, userAccount.isEmailVerified.toString())
        }
    }

    fun remove(callback: AccountRemoveCallback? = null) {
        val weakCallback = callback?.weak()
        if (account != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                if (manager.removeAccountExplicitly(account!!.account)) {
                    account = null
                    weakCallback?.get()?.onSuccess()
                } else {
                    weakCallback?.get()?.onFailure()
                }
            } else {
                @Suppress("DEPRECATION")
                manager.removeAccount(account!!.account, {
                    try {
                        if (it.result) {
                            account = null
                            token = null
                            weakCallback?.get()?.onSuccess()
                        } else {
                            weakCallback?.get()?.onFailure()
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                        weakCallback?.get()?.onFailure()
                    }
                }, handler)
            }
        } else {
            weakCallback?.get()?.onSuccess()
        }
    }

    private fun refreshAccount(): UserAccount? {
        return manager.getAccountsByType(AccountAuthenticator.ACCOUNT_TYPE)?.elementAtOrNull(0)?.let {
            return accountFromManager(it, manager)
        }
    }

    interface AccountRemoveCallback {
        fun onSuccess()
        fun onFailure()
    }
}

private object Key {
    const val ID = "KEY_ID"
    const val USERNAME = "KEY_USERNAME"
    const val IMAGE = "KEY_IMAGE"
    const val NAME = "KEY_NAME"

    const val GENDER = "KEY_GENDER"
    const val EMAIL = "KEY_EMAIL"
    const val BIO = "KEY_BIO"
    const val BIRTHDAY = "KEY_BIRTHDAY"
    const val LOCATION = "KEY_LOCATION"
    const val PHONE = "KEY_PHONE"
    const val WEBSITE = "KEY_WEBSITE"
    const val IS_PRIVATE = "KEY_IS_PRIVATE"
    const val IS_VERIFIED = "KEY_IS_VERIFIED"
}

private fun accountFromManager(account: Account, manager: AccountManager): UserAccount? {
    val id = manager.getUserData(account, Key.ID)?.toInt() ?: return null
    val username = manager.getUserData(account, Key.USERNAME)
    val name = manager.getUserData(account, Key.NAME)
    val image = manager.getUserData(account, Key.IMAGE)
    val token = manager.peekAuthToken(account, AccountAuthenticator.AUTH_TOKEN_TYPE) ?: return null
    val gender = manager.getUserData(account, Key.GENDER)

    return UserAccount(id, username, name, image, account, gender, null, token, null).apply {
        email = manager.getUserData(account, Key.EMAIL)
        bio = manager.getUserData(account, Key.BIO)
        birthday = manager.getUserData(account, Key.BIRTHDAY)
        location = manager.getUserData(account, Key.LOCATION)
        phone = manager.getUserData(account, Key.PHONE)
        website = manager.getUserData(account, Key.WEBSITE)
        isPrivate = manager.getUserData(account, Key.IS_PRIVATE).toBoolean()
        isEmailVerified = manager.getUserData(account, Key.IS_VERIFIED).toBoolean()
    }
}

private val UserAccount.bundle: Bundle
    @SuppressLint("NewApi") get() = Bundle().apply {
        putString(Key.ID, id.toString())
        putString(Key.USERNAME, username)
        putString(Key.IMAGE, image)
        putString(Key.NAME, name)

        putString(Key.GENDER, gender)
        putString(Key.EMAIL, email)
        putString(Key.BIO, bio)
        putString(Key.BIRTHDAY, birthday)
        putString(Key.LOCATION, location)
        putString(Key.PHONE, phone)
        putString(Key.WEBSITE, website)
        putString(Key.IS_PRIVATE, isPrivate.toString())
        putString(Key.IS_VERIFIED, isEmailVerified.toString())
    }
