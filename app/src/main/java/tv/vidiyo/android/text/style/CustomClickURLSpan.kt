package tv.vidiyo.android.text.style

import android.text.style.URLSpan
import android.view.View
import tv.vidiyo.android.extension.weak
import java.lang.ref.WeakReference

open class CustomClickURLSpan(url: String, clickListener: View.OnClickListener?) : URLSpan(url) {

    private var clickListenerReference: WeakReference<View.OnClickListener>? = clickListener?.weak()

    override fun onClick(widget: View?) {
        super.onClick(widget)
        clickListenerReference?.get()?.onClick(widget)
    }
}