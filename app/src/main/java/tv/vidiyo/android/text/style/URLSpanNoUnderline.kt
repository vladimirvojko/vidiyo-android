package tv.vidiyo.android.text.style

import android.text.TextPaint
import android.view.View

class URLSpanNoUnderline(url: String, clickListener: View.OnClickListener? = null) : CustomClickURLSpan(url, clickListener) {
    override fun updateDrawState(ds: TextPaint) {
        super.updateDrawState(ds)
        ds.isUnderlineText = false
    }
}