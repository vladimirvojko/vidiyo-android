package tv.vidiyo.android.ui.adapter.delegate.settings

import android.view.ViewGroup
import tv.vidiyo.android.api.model.Header
import tv.vidiyo.android.api.model.Item
import tv.vidiyo.android.base.adapter.delegate.SectionedAdapterDelegate
import tv.vidiyo.android.base.adapter.provider.SectionedDataProvider
import tv.vidiyo.android.base.adapter.section.ItemPosition
import tv.vidiyo.android.base.view.holder.ViewHolder
import tv.vidiyo.android.ui.view.holder.SettingViewHolder
import tv.vidiyo.android.ui.view.holder.setItem

class SettingsAdapterDelegate(
        provider: SectionedDataProvider<Header, Item>,
        listener: ViewHolder.OnViewHolderClickListener?
) : SectionedAdapterDelegate<Header, Item, SectionedDataProvider<Header, Item>,
        ViewHolder.OnViewHolderClickListener, SettingViewHolder>(provider, listener) {

    override fun onCreateViewHolder(parent: ViewGroup) = SettingViewHolder(parent, listener)

    override fun onBindViewHolder(position: ItemPosition, holder: SettingViewHolder, provider: SectionedDataProvider<Header, Item>) {
        holder.setItem(provider.getItemForPosition(position) ?: return)
    }

    override fun isForViewType(itemPosition: ItemPosition): Boolean = itemPosition.isItem && itemPosition.position != 4
}