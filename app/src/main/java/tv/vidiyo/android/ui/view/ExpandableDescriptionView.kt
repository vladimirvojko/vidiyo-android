package tv.vidiyo.android.ui.view

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.episode.EpisodeInfo
import tv.vidiyo.android.api.model.show.ShowInfo
import tv.vidiyo.android.extension.DECIMAL_FORMAT
import tv.vidiyo.android.extension.assignTo
import tv.vidiyo.android.extension.changeVisibility
import tv.vidiyo.android.extension.expansion

class ExpandableDescriptionView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0
) : RelativeLayout(context, attrs, defStyleAttr), View.OnClickListener {

    val tvTitle: TextView
    val ivArrow: ImageView
    val tvViews: TextView
    val tvDescription: TextView

    init {
        inflate(context, R.layout.view_expandable_description, this)

        tvTitle = findViewById(R.id.tv_title)
        ivArrow = findViewById(R.id.iv_arrow)
        tvViews = findViewById(R.id.tv_views)
        tvDescription = findViewById(R.id.tv_description)

        assignTo(tvTitle, ivArrow)
    }

    override fun onClick(p0: View?) {
        tvDescription.expansion(0, adapter)
    }


    private val adapter = object : AnimatorListenerAdapter() {
        override fun onAnimationStart(animation: Animator?) {
            val rotateTo = if (tvDescription.maxLines == 0) 180 else 0
            val angle = rotation
            val rotation = if (rotateTo > angle) rotateTo - angle else rotateTo + angle
            val duration = tvDescription.lineCount * 10L
            ivArrow.animation?.cancel()
            ivArrow.animate().rotation(rotation).setDuration(if (duration > 300) duration else 300).start()
        }
    }
}

fun ExpandableDescriptionView.setShow(info: ShowInfo) {
    tvTitle.text = info.name
    tvDescription.text = info.description
}

fun ExpandableDescriptionView.setEpisode(info: EpisodeInfo) {
    tvTitle.text = info.name
    tvViews.text = DECIMAL_FORMAT.format(info.totalViews)
    tvViews.changeVisibility(true)
    tvDescription.text = info.description
}