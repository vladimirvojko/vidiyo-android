package tv.vidiyo.android.ui.profile

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import tv.vidiyo.android.DependencyGraph
import tv.vidiyo.android.R
import tv.vidiyo.android.base.adapter.RecyclerAdapter
import tv.vidiyo.android.base.adapter.SectionedRecyclerAdapter
import tv.vidiyo.android.base.adapter.pages.PageFragment
import tv.vidiyo.android.di.subcomponent.profile.ChildUserModule
import tv.vidiyo.android.di.subcomponent.profile.ProfileModule
import tv.vidiyo.android.domain.presenter.profile.InnerProfilePresenter
import tv.vidiyo.android.model.InnerPageType
import tv.vidiyo.android.ui.ListFragment
import tv.vidiyo.android.ui.adapter.DataRecyclerAdapter
import tv.vidiyo.android.ui.adapter.delegate.profile.*
import javax.inject.Inject

class InnerProfileFragment : BaseInnerProfileFragment(), OnViewAllClickListener {

    companion object {
        private fun newInstance(@InnerPageType type: Int) = InnerProfileFragment().apply {
            arguments = Bundle(1).also {
                it.putInt(EXTRA, type)
            }
        }

        fun showsCreator() = object : PageFragment.Creator {
            override fun newInstance(): Fragment = InnerProfileFragment.newInstance(InnerPageType.SHOWS)
        }

        fun episodeCreator() = object : PageFragment.Creator {
            override fun newInstance(): Fragment = InnerProfileFragment.newInstance(InnerPageType.EPISODES)
        }

        fun castCreator() = object : PageFragment.Creator {
            override fun newInstance(): Fragment = InnerProfileFragment.newInstance(InnerPageType.CAST)
        }

        fun topicsCreator() = object : PageFragment.Creator {
            override fun newInstance(): Fragment = InnerProfileFragment.newInstance(InnerPageType.TOPICS)
        }
    }

    override val layoutResId: Int = R.layout.fragment_state_recycler_empty
    override var type: Int = InnerPageType.SHOWS

    @Inject
    lateinit var presenter: InnerProfilePresenter

    override fun injectDependencies(graph: DependencyGraph) {
        type = arguments.getInt(EXTRA, InnerPageType.SHOWS)
        val component = (parentFragment as? ProfileFragment)?.component
        component?.plus(ChildUserModule(this))?.injectTo(this)
                ?: graph.dataComponent.plus(ProfileModule(this)).injectTo(this)
    }

    override fun onPostViewCreated() {
        presenter.start()
    }

    override fun <T> getItem(at: Int): T? = presenter.get(at)

    override fun onViewAllClick(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val channel = presenter.getSection(position) ?: return
        val userId = presenter.main.id
        startFragment(ListFragment.newInstance(userId, channel.id, channel.name, type), true)
    }

    override fun onSubscribeClick(viewHolder: RecyclerView.ViewHolder, position: Int) {
        if (refreshLayout?.isRefreshing == false)
            presenter.toggleSubscription(position)
    }

    override fun onCreateAdapter(): RecyclerAdapter {
        val repository = presenter.main
        return when (type) {
        // TODO: Header listeners
            InnerPageType.SHOWS -> repository.showsRepository.provider.let {
                SectionedRecyclerAdapter(listOf(
                        ViewAllHeaderDelegate(it, this),
                        SectionedShowAdapterDelegate(it, this, presenter.manager)
                ), it)
            }
            InnerPageType.EPISODES -> DataRecyclerAdapter.episodes(repository.episodesRepository.provider, this)
            InnerPageType.CAST -> repository.castRepository.provider.let {
                SectionedRecyclerAdapter(listOf(
                        ViewAllHeaderDelegate(it, this),
                        SectionedCastAdapterDelegate(it, this, presenter.manager)
                ), it)
            }
            InnerPageType.TOPICS -> repository.topicsRepository.provider.let {
                SectionedRecyclerAdapter(listOf(
                        ViewAllHeaderDelegate(it, this),
                        SectionedTopicAdapterDelegate(it, this, presenter.manager)
                ), it)
            }
            else -> throw IllegalArgumentException("Can't found adapter for type $type")
        }
    }

    override fun onSelected() {
        presenter.refresh()
    }

    override fun retry() {
        presenter.refresh()
    }
}