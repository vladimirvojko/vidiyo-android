package tv.vidiyo.android.ui.search

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.LinearLayout
import tv.vidiyo.android.BuildConfig
import tv.vidiyo.android.DependencyGraph
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.cast.Cast
import tv.vidiyo.android.api.model.show.SearchingShow
import tv.vidiyo.android.api.model.topic.Topic
import tv.vidiyo.android.api.model.user.User
import tv.vidiyo.android.base.adapter.ProviderRecyclerAdapter
import tv.vidiyo.android.base.adapter.RecyclerAdapter
import tv.vidiyo.android.base.adapter.pages.PageFragment
import tv.vidiyo.android.base.adapter.provider.ListDataProvider
import tv.vidiyo.android.base.fragment.StateRecyclerFragment
import tv.vidiyo.android.base.fragment.ViewFragment
import tv.vidiyo.android.di.subcomponent.search.SearchModule
import tv.vidiyo.android.domain.contract.SearchContract
import tv.vidiyo.android.domain.presenter.search.InnerSearchPresenter
import tv.vidiyo.android.extension.emptyTextView
import tv.vidiyo.android.extension.errorRetryView
import tv.vidiyo.android.model.InnerPageType
import tv.vidiyo.android.ui.adapter.delegate.UserViewAdapterDelegate
import tv.vidiyo.android.ui.adapter.delegate.search.SearchCastResultAdapterDelegate
import tv.vidiyo.android.ui.adapter.delegate.search.SearchShowResultAdapterDelegate
import tv.vidiyo.android.ui.adapter.delegate.search.SearchTopicResultAdapterDelegate
import tv.vidiyo.android.ui.cast.CastFragment
import tv.vidiyo.android.ui.profile.ProfileFragment
import tv.vidiyo.android.ui.show.ShowFragment
import tv.vidiyo.android.ui.topic.TopicFragment
import tv.vidiyo.android.ui.view.DividerItemDecoration
import tv.vidiyo.android.ui.view.searchingProgressView
import tv.vidiyo.android.ui.widget.InfiniteScrollRecyclerView
import tv.vidiyo.android.ui.widget.OnInfiniteScrollListener
import javax.inject.Inject

class InnerSearchResultFragment : StateRecyclerFragment<RecyclerAdapter>(), SearchContract.Inner.View {

    override val layoutResId get() = R.layout.fragment_search_results_page

    override val type: Int get() = arguments.getInt(EXTRA_TYPE)
    override var query: String? = null

    @Inject
    lateinit var presenter: InnerSearchPresenter

    companion object {
        private const val EXTRA_TYPE = BuildConfig.APPLICATION_ID + ".extra.TYPE"

        private fun newInstance(@InnerPageType type: Int): InnerSearchResultFragment {
            return InnerSearchResultFragment().apply { arguments = Bundle().also { it.putInt(EXTRA_TYPE, type) } }
        }

        val PEOPLE = PageFragment(PageFragmentCreator(InnerPageType.PEOPLE), R.string.people)
        val SHOWS = PageFragment(PageFragmentCreator(InnerPageType.SHOWS), R.string.shows)
        val CAST = PageFragment(PageFragmentCreator(InnerPageType.CAST), R.string.cast)
        val TOPICS = PageFragment(PageFragmentCreator(InnerPageType.TOPICS), R.string.topics)
    }

    override fun injectDependencies(graph: DependencyGraph) {
        graph.dataComponent.plus(SearchModule(this)).injectTo(this)
    }

    @Suppress("UNCHECKED_CAST")
    override fun onCreateAdapter(): RecyclerAdapter {
        val delegates = when (type) {
            InnerPageType.PEOPLE -> listOf(UserViewAdapterDelegate(presenter.repository.provider as ListDataProvider<User>, this))
            InnerPageType.SHOWS -> listOf(SearchShowResultAdapterDelegate(presenter.repository.provider as ListDataProvider<SearchingShow>, this))
            InnerPageType.CAST -> listOf(SearchCastResultAdapterDelegate(presenter.repository.provider as ListDataProvider<Cast>, this))
            InnerPageType.TOPICS -> listOf(SearchTopicResultAdapterDelegate(presenter.repository.provider as ListDataProvider<Topic>, this))
            else -> throw IllegalArgumentException()
        }
        return ProviderRecyclerAdapter(presenter.repository.provider, delegates)
    }

    override fun onViewCreated(view: View, b: Bundle?) {
        super.onViewCreated(view, b)

        val title = getString(when (type) {
            InnerPageType.PEOPLE -> R.string.people
            InnerPageType.SHOWS -> R.string.shows
            InnerPageType.CAST -> R.string.cast
            InnerPageType.TOPICS -> R.string.topics
            else -> throw IllegalArgumentException()
        })
        stateLayout.emptyTextView?.text = getString(R.string.format_search_no_s_found, title)
        stateLayout.errorRetryView?.setRetryListener(this)
        stateLayout.searchingProgressView?.setType(type)
    }

    override fun onRecyclerViewCreated(recyclerView: InfiniteScrollRecyclerView) {
        super.onRecyclerViewCreated(recyclerView)
        recyclerView.addItemDecoration(DividerItemDecoration(context, LinearLayout.VERTICAL))
    }

    override fun onPostViewCreated() {
        presenter.start()
    }

    override fun search(q: String?) {
        if (query == q) return
        if (q == null || q.isEmpty()) {
            presenter.recent()
            return
        }

        query = q

        presenter.reset()
        recyclerView.resetState()
        recyclerView.listener = infiniteScrollListener

        retry()
    }

    override fun retry() = presenter.refresh()

    override fun onViewHolderClick(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val fragment: ViewFragment = when (type) {
            InnerPageType.PEOPLE -> ProfileFragment.newInstance(presenter.get<User>(position)?.id ?: return)
            InnerPageType.SHOWS -> ShowFragment.newInstance(presenter.get(position) ?: return)
            InnerPageType.TOPICS -> TopicFragment.newInstance(presenter.get(position) ?: return)
            InnerPageType.CAST -> CastFragment.newInstance(presenter.get(position) ?: return)
            else -> return
        }
        startFragment(fragment)
    }

    private val infiniteScrollListener = object : OnInfiniteScrollListener {
        override fun onInfiniteScroll(recyclerView: InfiniteScrollRecyclerView) {
            if (recyclerView.shouldLoadMore(presenter.totalCount)) {
                presenter.loadMore()
            }
        }
    }

    private class PageFragmentCreator(@InnerPageType val type: Int) : PageFragment.Creator {
        override fun newInstance(): Fragment {
            return Companion.newInstance(type)
        }
    }
}