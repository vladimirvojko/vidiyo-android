package tv.vidiyo.android.ui.view

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import tv.vidiyo.android.R
import tv.vidiyo.android.model.InnerPageType
import tv.vidiyo.android.ui.widget.StateLayout


val StateLayout.searchingProgressView: SearchingProgressView? get() = progressView as? SearchingProgressView

class SearchingProgressView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : RelativeLayout(context, attrs, defStyleAttr) {

    private lateinit var tvMessage: TextView

    init {
        View.inflate(context, R.layout.searching_progress_view, this)
    }

    override fun onFinishInflate() {
        super.onFinishInflate()

        tvMessage = findViewById(R.id.tv_searching_message)
    }

    @SuppressLint("SwitchIntDef")
    fun setType(@InnerPageType type: Int) {
        tvMessage.text = context.getString(R.string.format_search_searching_s, when (type) {
            InnerPageType.PEOPLE -> context.getString(R.string.people)
            InnerPageType.SHOWS -> context.getString(R.string.shows)
            InnerPageType.CAST -> context.getString(R.string.cast)
            InnerPageType.TOPICS -> context.getString(R.string.topics)
            else -> throw IllegalArgumentException()
        })
    }
}