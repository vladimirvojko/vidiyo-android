package tv.vidiyo.android.ui.adapter.delegate.settings

import android.view.ViewGroup
import tv.vidiyo.android.api.model.user.FollowingUser
import tv.vidiyo.android.api.model.user.User
import tv.vidiyo.android.base.adapter.delegate.ListProviderAdapterDelegate
import tv.vidiyo.android.base.adapter.provider.ListDataProvider
import tv.vidiyo.android.domain.contract.setUser
import tv.vidiyo.android.ui.view.holder.FollowerActionViewHolder
import tv.vidiyo.android.ui.view.holder.OnFollowerActionClickListener

class FollowerActionAdapterDelegate(
        provider: ListDataProvider<User>,
        listener: OnFollowerActionClickListener?,
        private val backprop: Boolean = false
) : ListProviderAdapterDelegate<User, OnFollowerActionClickListener, FollowerActionViewHolder>(provider, listener) {

    override fun onCreateViewHolder(parent: ViewGroup) = FollowerActionViewHolder(parent, listener)
    override fun onBindViewHolder(position: Int, holder: FollowerActionViewHolder, provider: ListDataProvider<User>, item: User) {
        holder.setUser(item)
        (item as? FollowingUser)?.let {
            holder.setStatus(if (backprop) it.backprop else it.status)
        }
    }
}