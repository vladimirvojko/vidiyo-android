package tv.vidiyo.android.ui.adapter.delegate.comment

import android.view.ViewGroup
import tv.vidiyo.android.api.model.comment.Comment
import tv.vidiyo.android.base.adapter.delegate.SectionedAdapterDelegate
import tv.vidiyo.android.base.adapter.provider.SectionedDataProvider
import tv.vidiyo.android.base.adapter.section.ItemPosition
import tv.vidiyo.android.ui.view.holder.CommentViewHolder
import tv.vidiyo.android.ui.view.holder.OnCommentClickListener
import tv.vidiyo.android.ui.view.holder.setComment

class CommentAdapterDelegate(
        provider: SectionedDataProvider<Comment, Comment>,
        listener: OnCommentClickListener?
) : SectionedAdapterDelegate<Comment, Comment, SectionedDataProvider<Comment, Comment>, OnCommentClickListener, CommentViewHolder>(provider, listener) {

    override fun onCreateViewHolder(parent: ViewGroup) = CommentViewHolder(parent, listener, true).apply {
        itemView.tag = false
    }

    override fun onBindViewHolder(position: ItemPosition, holder: CommentViewHolder, provider: SectionedDataProvider<Comment, Comment>) {
        holder.setComment(provider.getItemForPosition(position) ?: return)
    }

    override fun isForViewType(itemPosition: ItemPosition): Boolean = itemPosition.isItem
}