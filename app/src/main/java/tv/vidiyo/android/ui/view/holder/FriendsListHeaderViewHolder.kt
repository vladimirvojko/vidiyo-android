package tv.vidiyo.android.ui.view.holder

import android.view.ViewGroup
import android.widget.TextView
import tv.vidiyo.android.R
import tv.vidiyo.android.base.view.holder.ViewHolder
import tv.vidiyo.android.base.view.holder.ViewHolder.OnViewHolderClickListener
import java.lang.ref.WeakReference

class FriendsListHeaderViewHolder(
        parent: ViewGroup,
        listener: WeakReference<OnViewHolderClickListener>?
) : ViewHolder<OnViewHolderClickListener>(parent, listener, R.layout.view_header_friends_list, false) {

    val tvTitle: TextView = itemView.findViewById(R.id.tv_title)
    val tvSubtitle: TextView = itemView.findViewById(R.id.tv_subtitle)
}