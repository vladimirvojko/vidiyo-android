package tv.vidiyo.android.ui.settings

import android.os.Bundle
import android.view.View
import android.widget.RadioGroup
import tv.vidiyo.android.DependencyGraph
import tv.vidiyo.android.R
import tv.vidiyo.android.base.fragment.BaseFragment
import tv.vidiyo.android.di.subcomponent.settings.SettingsModule
import tv.vidiyo.android.domain.contract.SettingsContract
import tv.vidiyo.android.domain.presenter.settings.NotificationsPresenter
import tv.vidiyo.android.extension.findViews
import javax.inject.Inject

class NotificationsFragment : BaseFragment(), SettingsContract.Notification.View, RadioGroup.OnCheckedChangeListener {

    override val toolbarTitleRes = R.string.settings_notifications
    override val layoutResId = R.layout.fragment_notifications
    override val toolbarIndicator = R.drawable.svg_back

    private lateinit var idsByIndex: Array<IntArray>
    private lateinit var radioGroups: Array<RadioGroup>
    private lateinit var radioButtons: Array<View>

    @Inject
    lateinit var presenter: NotificationsPresenter

    override fun injectDependencies(graph: DependencyGraph) {
        graph.dataComponent.plus(SettingsModule(this)).injectTo(this)
    }

    override fun onViewCreated(view: View, b: Bundle?) {
        super.onViewCreated(view, b)
        radioGroups = view.findViews(R.id.rg_follower, R.id.rg_accepted, R.id.rg_episodes, R.id.rg_friends,
                R.id.rg_topic, R.id.rg_cast, R.id.rg_comment_like, R.id.rg_comment_replied)

        idsByIndex = Array(radioGroups.size) {
            IntArray(2)
        }

        val list = mutableListOf<View>()
        radioGroups.forEachIndexed { index, radioGroup ->
            val count = radioGroup.childCount
            for (i in 0 until count) {
                val child = radioGroup.getChildAt(i) ?: continue
                idsByIndex[index][i] = child.id
                list.add(child)
            }
            radioGroup.setOnCheckedChangeListener(this)
        }
        radioButtons = list.toTypedArray()
    }

    override fun onPostViewCreated() {
        presenter.start()
    }

    override fun setRefreshing(block: Boolean) {
        super.setRefreshing(block)
        radioButtons.forEach { it.isClickable = !block }
    }

    override fun onCheckedChanged(group: RadioGroup, checkedId: Int) {
        if (presenter.ignore) return

        val index = radioGroups.indexOf(group)
        val value = idsByIndex[index].indexOf(checkedId)
        presenter.check(index, value)
    }

    override fun update() {
        val data = presenter.data ?: return
        radioGroups.forEachIndexed { index, radioGroup ->
            val value = data.valueOf(index)
            radioGroup.check(idsByIndex[index][value])
        }
        presenter.endIgnoring()
    }
}