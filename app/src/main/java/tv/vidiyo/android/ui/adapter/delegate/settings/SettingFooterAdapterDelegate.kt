package tv.vidiyo.android.ui.adapter.delegate.settings

import android.view.ViewGroup
import tv.vidiyo.android.BuildConfig
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.Header
import tv.vidiyo.android.api.model.Item
import tv.vidiyo.android.base.adapter.delegate.SectionedAdapterDelegate
import tv.vidiyo.android.base.adapter.provider.SectionedDataProvider
import tv.vidiyo.android.base.adapter.section.ItemPosition
import tv.vidiyo.android.base.view.holder.ViewHolder
import tv.vidiyo.android.ui.view.holder.SettingFooterViewHolder

class SettingFooterAdapterDelegate(
        provider: SectionedDataProvider<Header, Item>
) : SectionedAdapterDelegate<Header, Item, SectionedDataProvider<Header, Item>,
        ViewHolder.OnViewHolderClickListener, SettingFooterViewHolder>(provider, null) {

    override fun onCreateViewHolder(parent: ViewGroup) = SettingFooterViewHolder(parent)

    override fun onBindViewHolder(position: ItemPosition, holder: SettingFooterViewHolder, provider: SectionedDataProvider<Header, Item>) {
        holder.textView.text = holder.context.getString(R.string.format_copyright_d_s_s,
                2017,
                "${BuildConfig.VERSION_NAME}.${BuildConfig.VERSION_CODE}",
                BuildConfig.BUILD_TYPE
        )
    }

    override fun isForViewType(itemPosition: ItemPosition): Boolean = itemPosition.isFooter
}