package tv.vidiyo.android.ui.boarding.signup

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Button
import android.widget.TextView
import tv.vidiyo.android.DependencyGraph
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.user.Profile
import tv.vidiyo.android.base.fragment.BaseFragment
import tv.vidiyo.android.di.subcomponent.boarding.BoardingModule
import tv.vidiyo.android.domain.contract.SignUpContract
import tv.vidiyo.android.domain.presenter.boarding.CodePresenter
import tv.vidiyo.android.extension.setTextWithClickableLink
import tv.vidiyo.android.ui.view.ErrorView
import tv.vidiyo.android.ui.view.PinEntryEditText
import tv.vidiyo.android.util.Constant
import javax.inject.Inject

class CodeFragment : BaseFragment(), SignUpContract.Code.View, View.OnClickListener {

    companion object {
        private const val EXTRA = "vdyo.brdfrg.cf.extra0"//phone

        fun newInstance(phone: String): CodeFragment {
            val fragment = CodeFragment()
            val b = Bundle()
            b.putString(EXTRA, phone)
            fragment.arguments = b
            return fragment
        }
    }

    override val layoutResId = R.layout.fragment_sign_up_code
    override val toolbarTitleRes = R.string.boarding_confirmation_code

    lateinit var tvSentTo: TextView
    lateinit var etCode: PinEntryEditText
    lateinit var errorView: ErrorView
    lateinit var button: Button

    @Inject
    lateinit var presenter: CodePresenter

    var phone: String = Constant.EMPTY

    override fun injectDependencies(graph: DependencyGraph) {
        graph.applicationComponent.plus(BoardingModule(this)).injectTo(this)
    }

    override fun onCreate(b: Bundle?) {
        super.onCreate(b)
        val bundle = b ?: arguments
        phone = bundle.getString(EXTRA, Constant.EMPTY)
    }

    override fun onViewCreated(view: View, b: Bundle?) {
        super.onViewCreated(view, b)
        tvSentTo = view.findViewById(R.id.tv_sent_to)
        etCode = view.findViewById(R.id.et_code)
        errorView = view.findViewById(R.id.error_view)
        button = view.findViewById(R.id.button)

        tvSentTo.text = getString(R.string.boarding_enter_code_s, phone)

        view.findViewById<TextView>(R.id.tv_problems).setTextWithClickableLink(R.string.boarding_resend_code, this)

        etCode.addTextChangedListener(watcher)
        button.setOnClickListener(this)
    }

    private fun validate(s: CharSequence?) {
        button.isEnabled = s?.length == 4
    }

    override fun startNameEnter() {
        startFragment(NameFragment.newInstance(Profile().apply {
            phone = this@CodeFragment.phone
        }), true)
    }

    override fun showError(text: String?) {
        errorView.show(R.string.error_incorrect_digit_code)
    }

    override fun onClick(v: View?) = when (v?.id) {
        R.id.button -> presenter.verifyCode(etCode.text.toString(), phone)
        R.id.tv_problems -> presenter.resend(phone)

        else -> Unit
    }

    private val watcher = object : TextWatcher {
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            validate(s)
        }

        override fun afterTextChanged(s: Editable?) {}
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
    }
}