package tv.vidiyo.android.ui.adapter.delegate.profile

import android.view.ViewGroup
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.channel.Channel
import tv.vidiyo.android.api.model.show.Show
import tv.vidiyo.android.base.adapter.delegate.SectionedAdapterDelegate
import tv.vidiyo.android.base.adapter.provider.SectionedListDataProvider
import tv.vidiyo.android.base.adapter.section.ItemPosition
import tv.vidiyo.android.ui.view.holder.OnSubscribableViewHolderClickListener
import tv.vidiyo.android.ui.view.holder.SubscribableViewHolder
import tv.vidiyo.android.ui.view.holder.setShow
import tv.vidiyo.android.util.SubscriptionManager

class SectionedShowAdapterDelegate(
        provider: SectionedListDataProvider<Channel, Show>,
        listener: OnSubscribableViewHolderClickListener?,
        private val manager: SubscriptionManager? = null
) : SectionedAdapterDelegate<Channel, Show, SectionedListDataProvider<Channel, Show>,
        OnSubscribableViewHolderClickListener, SubscribableViewHolder>(provider, listener) {

    override fun onCreateViewHolder(parent: ViewGroup): SubscribableViewHolder {
        return SubscribableViewHolder(parent, listener, R.layout.view_item_subscribable_show)
    }

    override fun onBindViewHolder(position: ItemPosition, holder: SubscribableViewHolder, provider: SectionedListDataProvider<Channel, Show>) {
        val item = provider.getItemForPosition(position) ?: return
        val section = provider.getSection(position.section)?.section ?: return
        holder.setShow(item, section, manager)
    }

    override fun isForViewType(itemPosition: ItemPosition) = itemPosition.isItem
}