package tv.vidiyo.android.ui.adapter.delegate.channel

import android.view.ViewGroup
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.channel.Channel
import tv.vidiyo.android.api.model.show.Show
import tv.vidiyo.android.base.adapter.delegate.ListProviderAdapterDelegate
import tv.vidiyo.android.base.adapter.provider.ListDataProvider
import tv.vidiyo.android.ui.view.holder.OnSubscribableViewHolderClickListener
import tv.vidiyo.android.ui.view.holder.SubscribableViewHolder
import tv.vidiyo.android.ui.view.holder.setShow

class SubscribableShowAdapterDelegate(
        private val channel: Channel?,
        provider: ListDataProvider<Show>,
        listener: OnSubscribableViewHolderClickListener?
) : ListProviderAdapterDelegate<Show, OnSubscribableViewHolderClickListener, SubscribableViewHolder>(provider, listener) {

    override fun onCreateViewHolder(parent: ViewGroup) = SubscribableViewHolder(parent, listener, R.layout.view_item_subscribable_show)
    override fun onBindViewHolder(position: Int, holder: SubscribableViewHolder, provider: ListDataProvider<Show>, item: Show) {
        holder.setShow(item, channel)
    }
}