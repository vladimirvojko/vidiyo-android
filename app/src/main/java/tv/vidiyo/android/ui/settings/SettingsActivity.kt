package tv.vidiyo.android.ui.settings

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import tv.vidiyo.android.DependencyGraph
import tv.vidiyo.android.R
import tv.vidiyo.android.base.BaseActivity
import tv.vidiyo.android.base.fragment.BaseFragment
import tv.vidiyo.android.di.subcomponent.SocialComponent
import tv.vidiyo.android.di.subcomponent.SocialModule
import tv.vidiyo.android.util.SocialManager
import javax.inject.Inject

class SettingsActivity : BaseActivity() {

    companion object {

        private const val EXTRA = "EXTRA"

        const val SCREEN_SETTINGS = 0
        const val SCREEN_EDIT_PROFILE = 1

        fun start(activity: Activity, screen: Int = SCREEN_SETTINGS) {
            activity.startActivity(Intent(activity, SettingsActivity::class.java).also {
                it.putExtra(EXTRA, screen)
            })
        }
    }

    override val fragmentContainerId = R.id.fragment_container

    @Inject
    lateinit var socialManager: SocialManager
    var socialComponent: SocialComponent? = null

    override fun injectDependencies(graph: DependencyGraph) {
        socialComponent = graph.dataComponent.plus(SocialModule(this)).also {
            it.injectTo(this)
        }
    }

    override fun onCreate(b: Bundle?) {
        super.onCreate(b)
        val open = intent.getIntExtra(EXTRA, SCREEN_SETTINGS)
        super.setContentView(R.layout.activity_container)
        socialManager.bind(this)

        val f: BaseFragment = when (open) {
            SCREEN_SETTINGS -> SettingsFragment()
            SCREEN_EDIT_PROFILE -> EditProfileFragment()

            else -> throw IllegalArgumentException("Unknown screen to open")
        }
        startFragment(f, false)
    }

    override fun onDestroy() {
        super.onDestroy()
        socialManager.unbind(this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (!socialManager.onActivityResult(requestCode, resultCode, data))
            super.onActivityResult(requestCode, resultCode, data)
    }
}