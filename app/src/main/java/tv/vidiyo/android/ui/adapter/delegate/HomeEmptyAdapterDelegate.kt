package tv.vidiyo.android.ui.adapter.delegate

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import tv.vidiyo.android.R
import tv.vidiyo.android.base.adapter.delegate.AdapterDelegate
import tv.vidiyo.android.base.adapter.provider.ListDataProvider
import tv.vidiyo.android.extension.weak
import tv.vidiyo.android.source.DataType
import tv.vidiyo.android.ui.view.holder.empty.NoFriendsClickListener
import tv.vidiyo.android.ui.view.holder.empty.NoFriendsViewHolder
import tv.vidiyo.android.ui.view.holder.empty.NoSubscriptionsClickListener
import tv.vidiyo.android.ui.view.holder.empty.NoSubscriptionsViewHolder
import java.lang.ref.WeakReference

interface HomeEmptyClickListener : NoFriendsClickListener, NoSubscriptionsClickListener

class HomeEmptyAdapterDelegate(var type: Int, val provider: ListDataProvider<*>, listener: HomeEmptyClickListener?) : AdapterDelegate {

    val listener = listener?.weak()

    @Suppress("UNCHECKED_CAST")
    override fun onCreateViewHolder(parent: ViewGroup) = when (type) {
        DataType.TYPE_FRIENDS_WATCHING -> NoFriendsViewHolder(parent, listener as? WeakReference<NoFriendsClickListener>)
        DataType.TYPE_SUBSCRIPTION -> NoSubscriptionsViewHolder(parent, listener as? WeakReference<NoSubscriptionsClickListener>)
        DataType.TYPE_YOUR_CHANNELS -> NoSubscriptionsViewHolder(parent, listener as? WeakReference<NoSubscriptionsClickListener>)
        else -> throw IllegalStateException("Can't find empty view for $type type")
    }

    override fun onBindViewHolder(position: Int, holder: RecyclerView.ViewHolder) {
        val noSubscriptionsHolder = holder as? NoSubscriptionsViewHolder ?: return
        when (type) {
            DataType.TYPE_SUBSCRIPTION -> {
                noSubscriptionsHolder.tvDescription.setText(R.string.home_subscribe_to_shows_cast_topics)
            }
            DataType.TYPE_YOUR_CHANNELS -> {
                noSubscriptionsHolder.tvDescription.setText(R.string.home_subscribe_to_topics)
            }
        }
    }

    override fun isForViewType(position: Int) = provider.getItemCount() == 0
}