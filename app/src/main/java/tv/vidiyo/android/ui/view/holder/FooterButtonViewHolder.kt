package tv.vidiyo.android.ui.view.holder

import android.view.ViewGroup
import android.widget.Button
import tv.vidiyo.android.R
import tv.vidiyo.android.base.view.holder.ViewHolder
import tv.vidiyo.android.base.view.holder.ViewHolder.OnViewHolderClickListener
import java.lang.ref.WeakReference

class FooterButtonViewHolder(
        parent: ViewGroup,
        listener: WeakReference<OnViewHolderClickListener>?
) : ViewHolder<OnViewHolderClickListener>(parent, listener, R.layout.view_footer_button, true) {

    val button: Button = itemView.findViewById(R.id.button)
}