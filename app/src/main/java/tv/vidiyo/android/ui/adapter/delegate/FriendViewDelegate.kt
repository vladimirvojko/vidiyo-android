package tv.vidiyo.android.ui.adapter.delegate

import android.view.ViewGroup
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.user.Friend
import tv.vidiyo.android.base.adapter.delegate.SectionedAdapterDelegate
import tv.vidiyo.android.base.adapter.provider.SectionedSelectableDataProvider
import tv.vidiyo.android.base.adapter.section.ItemPosition
import tv.vidiyo.android.base.view.holder.ViewHolder.OnViewHolderClickListener
import tv.vidiyo.android.extension.changeVisibility
import tv.vidiyo.android.model.FriendsListType
import tv.vidiyo.android.ui.view.holder.FriendViewHolder

class FriendViewDelegate(
        provider: SectionedSelectableDataProvider<FriendsListType, Friend>,
        listener: OnViewHolderClickListener?
) : SectionedAdapterDelegate<FriendsListType, Friend, SectionedSelectableDataProvider<FriendsListType, Friend>, OnViewHolderClickListener, FriendViewHolder>(provider, listener) {

    override fun onCreateViewHolder(parent: ViewGroup): FriendViewHolder {
        return FriendViewHolder(parent, listener)
    }

    override fun onBindViewHolder(position: ItemPosition, holder: FriendViewHolder, provider: SectionedSelectableDataProvider<FriendsListType, Friend>) {
        provider.getItemForPosition(position)?.apply {
            holder.setAvatar(image ?: photoUri, R.drawable.ic_default_profile)
            holder.setName(name)
            holder.setUsername(username?.let { "@$it" })

            val hasAccount = username != null
            holder.tvUsername.changeVisibility(hasAccount)
            holder.changeButtonState(hasAccount, provider.selected.contains(position))
        }
    }

    override fun isForViewType(itemPosition: ItemPosition) = itemPosition.isItem
}