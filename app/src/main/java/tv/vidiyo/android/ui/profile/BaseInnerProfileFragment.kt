package tv.vidiyo.android.ui.profile

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.social.SocialConnection
import tv.vidiyo.android.base.adapter.RecyclerAdapter
import tv.vidiyo.android.base.fragment.StateRecyclerFragment
import tv.vidiyo.android.base.fragment.ViewFragment
import tv.vidiyo.android.domain.contract.ProfileContract
import tv.vidiyo.android.extension.openUrl
import tv.vidiyo.android.extension.setEmptyViewWithFormat
import tv.vidiyo.android.model.InnerPageType
import tv.vidiyo.android.ui.cast.CastFragment
import tv.vidiyo.android.ui.show.ShowFragment
import tv.vidiyo.android.ui.topic.TopicFragment
import tv.vidiyo.android.ui.view.DividerItemDecoration
import tv.vidiyo.android.ui.view.SpaceItemDecoration
import tv.vidiyo.android.ui.view.holder.OnSubscribableViewHolderClickListener
import tv.vidiyo.android.ui.widget.InfiniteScrollRecyclerView

abstract class BaseInnerProfileFragment : StateRecyclerFragment<RecyclerAdapter>(),
        ProfileContract.Inner.View, OnSubscribableViewHolderClickListener {

    companion object {
        const val EXTRA = "EXTRA"//type
    }

    override val layoutResId: Int = R.layout.fragment_state_recycler_empty
    override var type: Int = InnerPageType.SHOWS

    abstract fun <T> getItem(at: Int): T?

    override fun onCreate(b: Bundle?) {
        type = arguments.getInt(EXTRA, InnerPageType.SHOWS)
        super.onCreate(b)
    }

    override fun onViewCreated(view: View, b: Bundle?) {
        super.onViewCreated(view, b)
        val title = getString(when (type) {
            InnerPageType.SHOWS -> R.string.shows
            InnerPageType.EPISODES -> R.string.episodes
            InnerPageType.CAST -> R.string.actors
            InnerPageType.TOPICS -> R.string.topics
            InnerPageType.CONNECT_AND_SHARE -> R.string.social
            else -> throw IllegalArgumentException("Wrong type provided")
        }).toLowerCase()
        val drawableRes = when (type) {
            InnerPageType.SHOWS -> R.drawable.svg_shows
            InnerPageType.EPISODES -> R.drawable.svg_episodes
            InnerPageType.CAST -> R.drawable.svg_cast
            InnerPageType.TOPICS -> R.drawable.svg_topic
            InnerPageType.CONNECT_AND_SHARE -> R.drawable.svg_cast
            else -> throw IllegalArgumentException("Wrong type provided")
        }

        stateLayout.setEmptyViewWithFormat(title, drawableRes)
    }

    override fun onSubscribeClick(viewHolder: RecyclerView.ViewHolder, position: Int) {
        Toast.makeText(context, "not implemented", Toast.LENGTH_SHORT).show()
    }

    override fun onViewHolderClick(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val fragment: ViewFragment = when (type) {
            InnerPageType.EPISODES -> {
                mainActivity?.openEpisode(getItem(position) ?: return)
                return
            }
            InnerPageType.SHOWS -> ShowFragment.newInstance(getItem(position) ?: return)
            InnerPageType.TOPICS -> TopicFragment.newInstance(getItem(position) ?: return)
            InnerPageType.CAST -> CastFragment.newInstance(getItem(position) ?: return)
            InnerPageType.CONNECT_AND_SHARE -> {
                openUrl(getItem<SocialConnection>(position)?.url ?: return)
                return
            }
            else -> return
        }
        startFragment(fragment)
    }

    override fun onCreateLayoutManager(context: Context?) = LinearLayoutManager(context)
    override fun onRecyclerViewCreated(recyclerView: InfiniteScrollRecyclerView) {
        super.onRecyclerViewCreated(recyclerView)
        recyclerView.addItemDecoration(SpaceItemDecoration(context, true, true))
        recyclerView.addItemDecoration(DividerItemDecoration(context, LinearLayout.VERTICAL))
    }
}