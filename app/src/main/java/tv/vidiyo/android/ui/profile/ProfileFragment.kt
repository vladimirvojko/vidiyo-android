package tv.vidiyo.android.ui.profile

import android.os.Bundle
import android.support.v4.widget.NestedScrollView
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.view.View
import tv.vidiyo.android.DependencyGraph
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.user.FollowingType
import tv.vidiyo.android.api.model.user.PendingRequests
import tv.vidiyo.android.api.model.user.Profile
import tv.vidiyo.android.api.model.user.SubscriptionStatus
import tv.vidiyo.android.base.adapter.pages.PageFragment
import tv.vidiyo.android.base.adapter.pages.PagesAdapter
import tv.vidiyo.android.base.fragment.PagesMainFragment
import tv.vidiyo.android.di.subcomponent.profile.ProfileModule
import tv.vidiyo.android.di.subcomponent.profile.UserComponent
import tv.vidiyo.android.di.subcomponent.profile.UserModule
import tv.vidiyo.android.domain.contract.ProfileContract
import tv.vidiyo.android.domain.presenter.profile.ProfilePresenter
import tv.vidiyo.android.extension.assignTo
import tv.vidiyo.android.extension.changeVisibility
import tv.vidiyo.android.extension.startActivity
import tv.vidiyo.android.ui.discover.DiscoverFragment
import tv.vidiyo.android.ui.settings.SettingsActivity
import tv.vidiyo.android.ui.settings.UsersFragment
import tv.vidiyo.android.ui.view.ConfirmationView
import tv.vidiyo.android.ui.view.PendingRequestView
import tv.vidiyo.android.ui.view.ProfileInfoView
import tv.vidiyo.android.ui.view.set
import tv.vidiyo.android.util.Constant
import javax.inject.Inject

class ProfileFragment : PagesMainFragment(), ProfileContract.View, View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    companion object {
        private const val EXTRA = "EXTRA"//profileId

        fun newInstance(id: Int = Constant.NO_ID) = ProfileFragment().apply {
            arguments = Bundle(1).also {
                it.putInt(EXTRA, id)
            }
        }
    }

    override val toolbarTitleRes = R.string.navigation_profile
    override val toolbarIndicator get() = if (isBackArrow) R.drawable.svg_back else R.drawable.svg_person_add
    override val appBarLayoutResId = R.layout.app_bar_profile
    override val contentLayoutResId get() = if (presenter.isForeign) R.layout.content_profile else R.layout.view_pager
    override val optionsMenuResId get() = if (presenter.isForeign) R.menu.foreign_profile else R.menu.profile

    private lateinit var profileView: ProfileInfoView
    private var itemBlock: MenuItem? = null
    private var confirmationView: ConfirmationView? = null
    private var pendingRequestView: PendingRequestView? = null
    private var scrollView: NestedScrollView? = null

    @Inject
    lateinit var presenter: ProfilePresenter

    var component: UserComponent? = null

    override val profileId: Int get() = arguments?.getInt(EXTRA, Constant.NO_ID) ?: Constant.NO_ID
    private val isBackArrow: Boolean get() = profileId != Constant.NO_ID
    private val isForeign: Boolean get () = presenter.isForeign
    private val currentId: Int get() = if (isForeign) profileId else presenter.ownerId

    override fun injectDependencies(graph: DependencyGraph) {
        val id = profileId
        when (id) {
            Constant.NO_ID -> graph.dataComponent.plus(ProfileModule(this)).injectTo(this)
            else -> component = graph.applicationComponent.plus(UserModule(this)).also {
                it.injectTo(this)
            }
        }
    }

    override fun onToolbarCreated(toolbar: Toolbar) {
        super.onToolbarCreated(toolbar)
        itemBlock = toolbar.menu.findItem(R.id.item_block)
    }

    override fun onRefresh() {
        presenter.refresh()
    }

    override fun onViewCreated(view: View, b: Bundle?) {
        super.onViewCreated(view, b)
        (activity as? AppCompatActivity)?.supportActionBar?.hide()
        profileView = view.findViewById(R.id.profile_view)
        confirmationView = if (isBackArrow) null else view.findViewById(R.id.confirmation_view)
        pendingRequestView = view.findViewById(R.id.pending_view)
        scrollView = view.findViewById(R.id.scroll_view)
        if (presenter.isForeign) tabLayout?.changeVisibility(false)

        confirmationView?.setOnClickListener(this)
        pendingRequestView?.setOnClickListener(this)
        assignTo(profileView.llFollowers, profileView.llFollowing, profileView.btnAction)
    }

    override fun onPostViewCreated() {
        presenter.start()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }

    override fun createAdapter() = PagesAdapter(context, childFragmentManager,
            PageFragment(InnerProfileFragment.showsCreator(), R.string.shows),
            PageFragment(InnerProfileFragment.episodeCreator(), R.string.episodes),
            PageFragment(InnerProfileFragment.castCreator(), R.string.cast),
            PageFragment(InnerProfileFragment.topicsCreator(), R.string.topics)
    )

    override fun updateProfileView(profile: Profile, withPhoto: Boolean) {
        profileView.changeVisibility(true)
        profileView.set(profile, withPhoto)
        profileView.btnAction.setText(if (isForeign) when (profile.status) {
            SubscriptionStatus.accepted -> R.string.following
            SubscriptionStatus.pending -> R.string.requested
            else -> R.string.follow
        } else R.string.edit_profile)

        if (isForeign) {
            itemBlock?.setTitle(if (profile.backprop == SubscriptionStatus.blocked) R.string.unblock else R.string.block)
            val visible = !(profile.isPrivate && profile.status != SubscriptionStatus.accepted)
            tabLayout?.changeVisibility(visible)
            viewPager.changeVisibility(visible)
            scrollView?.changeVisibility(!visible)
        }
    }

    override fun dialogBlock(block: Boolean, name: String) {
        val titleRes: Int
        val messageRes: Int
        val actionRes: Int
        if (block) {
            titleRes = R.string.profile_format_block_s
            messageRes = R.string.profile_message_block
            actionRes = R.string.block
        } else {
            titleRes = R.string.profile_format_unblock_s
            messageRes = R.string.profile_message_unblock
            actionRes = R.string.unblock
        }

        AlertDialog.Builder(context, R.style.AppTheme_AlertDialog)
                .setTitle(context.getString(titleRes, name))
                .setMessage(messageRes)
                .setPositiveButton(actionRes) { _, _ ->
                    presenter.performBlockAction()
                }.setNegativeButton(R.string.Cancel, null)
                .show()
    }

    override fun dialogBlockInformation(blocked: Boolean, name: String) {
        val titleRes: Int
        val messageRes: Int
        val actionRes: Int
        if (blocked) {
            titleRes = R.string.profile_format_s_blocked
            messageRes = R.string.profile_message_blocked
            actionRes = R.string.unblock

        } else {
            titleRes = R.string.profile_format_s_unblocked
            messageRes = R.string.profile_message_unblocked
            actionRes = R.string.block
        }

        itemBlock?.setTitle(actionRes)
        AlertDialog.Builder(context, R.style.AppTheme_AlertDialog)
                .setTitle(context.getString(titleRes, name))
                .setMessage(messageRes)
                .setPositiveButton(android.R.string.ok, null)
                .show()
    }

    override fun updateConfirmationView(email: String?) {
        confirmationView?.apply {
            setEmail(email)
            changeVisibility(email != null && !isForeign)
        }
    }

    override fun showPendingRequests(requests: PendingRequests) {
        pendingRequestView?.apply {
            setAvatar(requests.last?.image)
            counter = requests.count
            changeVisibility(true)
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_action -> if (isForeign) {
                presenter.toggleSubscription()
            } else {
                SettingsActivity.start(activity, SettingsActivity.SCREEN_EDIT_PROFILE)
            }

            R.id.iv_close -> {
                presenter.closeConfirmation()
                confirmationView?.hide()
            }

            R.id.btn_send -> {
                presenter.sendConfirmation()
                confirmationView?.hide()
            }

            R.id.pending_view -> startFragment(UsersFragment.newInstance(FollowingType.pending))
            R.id.ll_followers -> startFragment(UsersFragment.newInstance(FollowingType.followers, currentId))
            R.id.ll_following -> startFragment(UsersFragment.newInstance(FollowingType.following, currentId))
        }
    }

    override fun onMenuItemClick(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.item_settings -> context.startActivity<SettingsActivity>()
            R.id.item_block -> presenter.toggleBlock()
            else -> return super.onMenuItemClick(item)
        }
        return true
    }

    override fun onNavigationIconPressed() {
        if (isBackArrow) {
            onBackPressed()
        } else {
            startFragment(DiscoverFragment())
        }
    }
}