package tv.vidiyo.android.ui.view.holder

import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import tv.vidiyo.android.R
import tv.vidiyo.android.api.graph.FBPost
import tv.vidiyo.android.api.instagram.IGMedia
import tv.vidiyo.android.api.twitter.Tweet
import tv.vidiyo.android.base.view.holder.ViewHolder
import tv.vidiyo.android.extension.MM_dd_YYYY_HH_mm
import tv.vidiyo.android.extension.changeVisibility
import tv.vidiyo.android.extension.load
import tv.vidiyo.android.ui.view.ShapeImageView
import java.lang.ref.WeakReference

open class PostViewHolder(
        parent: ViewGroup,
        listener: WeakReference<OnViewHolderClickListener>?
) : ViewHolder<ViewHolder.OnViewHolderClickListener>(parent, listener, R.layout.view_item_social, true) {

    val ivImage = itemView.findViewById<ImageView>(R.id.iv_image)
    val ivAvatar = itemView.findViewById<ShapeImageView>(R.id.iv_avatar)
    val tvName = itemView.findViewById<TextView>(R.id.tv_name)
    val tvUsername = itemView.findViewById<TextView>(R.id.tv_username)
    val tvText = itemView.findViewById<TextView>(R.id.tv_text)
    val tvDate = itemView.findViewById<TextView>(R.id.tv_date)

    private fun setText(to: TextView, text: String?) {
        to.text = text
        to.changeVisibility(text != null)
    }

    private fun setImage(to: ImageView, url: String?) {
        to.load(url)
        to.changeVisibility(url != null)
    }

    fun setPost(post: FBPost) {
        setImage(ivImage, post.picture)
        setText(tvText, post.message)

        val user = post.from ?: return
        setText(tvName, user.name)
        setText(tvUsername, post.story)

        ivAvatar.load(user.picture?.data?.url)
        with(tvDate) {
            text = MM_dd_YYYY_HH_mm.format(post.date)
            setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.svg_social_facebook, 0, 0, 0)
        }
    }

    fun setTweet(tweet: Tweet) {
        setImage(ivImage, null)
        setText(tvText, tweet.text)

        val user = tweet.user
        setText(tvName, user.name)
        setText(tvUsername, context.getString(R.string.format_at_s, user.screenName))

        ivAvatar.load(user.imageUrl)
        with(tvDate) {
            text = MM_dd_YYYY_HH_mm.format(tweet.date)
            setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.svg_social_twitter, 0, 0, 0)
        }
    }

    fun setMedia(media: IGMedia) {
        setImage(ivImage, media.images.standard.url)
        setText(tvText, media.caption?.text)

        val user = media.user
        setText(tvName, user.fullName)
        setText(tvUsername, context.getString(R.string.format_at_s, user.username))

        ivAvatar.load(user.picture)
        with(tvDate) {
            text = MM_dd_YYYY_HH_mm.format(media.date)
            setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.svg_social_instagram, 0, 0, 0)
        }
    }
}