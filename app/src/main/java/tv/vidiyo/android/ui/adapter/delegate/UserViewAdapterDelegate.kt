package tv.vidiyo.android.ui.adapter.delegate

import android.view.ViewGroup
import tv.vidiyo.android.api.model.user.User
import tv.vidiyo.android.base.adapter.delegate.ListProviderAdapterDelegate
import tv.vidiyo.android.base.adapter.provider.ListDataProvider
import tv.vidiyo.android.base.view.holder.ViewHolder.OnViewHolderClickListener
import tv.vidiyo.android.domain.contract.setUser
import tv.vidiyo.android.ui.view.holder.UserViewHolder

class UserViewAdapterDelegate(
        provider: ListDataProvider<User>,
        listener: OnViewHolderClickListener?
) : ListProviderAdapterDelegate<User, OnViewHolderClickListener, UserViewHolder<OnViewHolderClickListener>>(provider, listener) {

    override fun onCreateViewHolder(parent: ViewGroup) = UserViewHolder(parent, listener)
    override fun onBindViewHolder(position: Int, holder: UserViewHolder<OnViewHolderClickListener>, provider: ListDataProvider<User>, item: User)
            = holder.setUser(item)
}