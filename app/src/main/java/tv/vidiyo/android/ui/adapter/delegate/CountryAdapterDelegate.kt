package tv.vidiyo.android.ui.adapter.delegate

import android.view.ViewGroup
import tv.vidiyo.android.api.model.Country
import tv.vidiyo.android.base.adapter.delegate.ListProviderAdapterDelegate
import tv.vidiyo.android.base.adapter.provider.ListDataProvider
import tv.vidiyo.android.base.view.holder.ViewHolder
import tv.vidiyo.android.ui.view.holder.CountryViewHolder

class CountryAdapterDelegate(
        provider: ListDataProvider<Country>,
        listener: ViewHolder.OnViewHolderClickListener?,
        val selected: Country
) : ListProviderAdapterDelegate<Country, ViewHolder.OnViewHolderClickListener, CountryViewHolder>(provider, listener) {

    override fun onCreateViewHolder(parent: ViewGroup) = CountryViewHolder(parent, listener)
    override fun onBindViewHolder(position: Int, holder: CountryViewHolder, provider: ListDataProvider<Country>, item: Country)
            = holder.setCountry(item, selected)
}