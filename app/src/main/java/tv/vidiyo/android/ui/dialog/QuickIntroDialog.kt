package tv.vidiyo.android.ui.dialog

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import com.roughike.bottombar.BottomBar
import tv.vidiyo.android.R
import tv.vidiyo.android.base.BaseDialog
import tv.vidiyo.android.domain.contract.ChannelsContract
import tv.vidiyo.android.extension.assignTo
import tv.vidiyo.android.extension.notContains
import tv.vidiyo.android.ui.view.CircleIndicator
import tv.vidiyo.android.ui.view.SwipelessViewPager
import tv.vidiyo.android.ui.view.IntroItemView
import java.util.*

class QuickIntroDialog : BaseDialog(), View.OnClickListener, ViewPager.OnPageChangeListener {

    companion object {
        fun show(fragment: Fragment, cancelable: Boolean = false): Boolean {
            val m = fragment.childFragmentManager
            if (m.notContains(QuickIntroDialog::class.java.name)) {
                val dialog = QuickIntroDialog()
                dialog.isCancelable = cancelable
                dialog.show(m)
                return true
            }
            return false
        }
    }

    override val layoutRes = R.layout.dialog_quick_intro

    lateinit var adapter: QuickIntroAdapter

    lateinit var viewPager: SwipelessViewPager
    lateinit var indicator: CircleIndicator
    lateinit var navigation: BottomBar

    private val indexes = intArrayOf(0, 2, 3)

    override fun onCreate(b: Bundle?) {
        super.onCreate(b)
        adapter = QuickIntroAdapter()
    }

    override fun onViewCreated(view: View, b: Bundle?) {
        super.onViewCreated(view, b)
        viewPager = view.findViewById(R.id.view_pager)
        indicator = view.findViewById(R.id.indicator)
        navigation = view.findViewById(R.id.bottom_navigation)

        viewPager.adapter = adapter
        viewPager.addOnPageChangeListener(this)
        indicator.setViewPager(viewPager)

        assignTo(view.findViewById(R.id.tv_skip), view.findViewById(R.id.btn_previous), view.findViewById(R.id.btn_next))
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.tv_skip -> startMain()
            R.id.btn_previous -> viewPager.previous()
            R.id.btn_next -> if (viewPager.next()) {
                startMain()
            }
        }
    }

    private fun startMain() {
        (parentFragment as? ChannelsContract.BoardingView)?.startMain()
        dismiss()
    }

    override fun onPageSelected(position: Int) = navigation.selectTabAtPosition(indexes[position], true)
    override fun onPageScrollStateChanged(state: Int) {}
    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}

    override fun onCleanUp() {
        viewPager.clearOnPageChangeListeners()
    }
}

class QuickIntroAdapter : PagerAdapter() {

    val pool = WeakHashMap<Int, IntroItemView>()

    private val data = arrayOf(
            Intro(R.string.intro_shows_episodes_title, R.string.intro_shows_episodes_description, R.drawable.intro_episode, null),
            Intro(R.string.intro_continue_watching_title, R.string.intro_continue_watching_description, R.drawable.intro_watching, null),
            Intro(R.string.intro_channels_title, R.string.intro_channels_description, R.drawable.intro_channel1, R.drawable.intro_channel2)
    )

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = pool[position] ?: IntroItemView(container.context).also { pool.put(position, it) }
        view.setData(data[position])
        container.addView(view)
        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, any: Any?) {
        container.removeView(pool[position])
        pool.remove(position)
    }

    override fun isViewFromObject(view: View?, any: Any?): Boolean = view == any
    override fun getCount(): Int = data.size
}

class SwipelessBottomBar @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0
) : BottomBar(context, attrs, defStyleAttr) {

    override fun dispatchTouchEvent(ev: MotionEvent?) = true
}

data class Intro(
        val titleId: Int,
        val descriptionId: Int,
        val firstImage: Int,
        val secondImage: Int?
)

