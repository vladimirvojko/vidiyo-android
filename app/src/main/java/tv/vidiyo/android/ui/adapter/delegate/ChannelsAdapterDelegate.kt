package tv.vidiyo.android.ui.adapter.delegate

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import tv.vidiyo.android.api.model.channel.Channel
import tv.vidiyo.android.base.adapter.delegate.ProviderAdapterDelegate
import tv.vidiyo.android.base.view.holder.ViewHolder.OnViewHolderClickListener
import tv.vidiyo.android.domain.presenter.channels.ChannelsProvider
import tv.vidiyo.android.extension.changeVisibility
import tv.vidiyo.android.extension.load
import tv.vidiyo.android.source.channels.ChannelsMode
import tv.vidiyo.android.ui.view.holder.ChannelViewHolder

class ChannelsAdapterDelegate(
        provider: ChannelsProvider,
        listener: OnViewHolderClickListener?
) : ProviderAdapterDelegate<OnViewHolderClickListener, ChannelViewHolder, ChannelsProvider>(provider, listener) {

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return ChannelViewHolder(parent, listener)
    }

    override fun onBindViewHolder(position: Int, holder: ChannelViewHolder, provider: ChannelsProvider) {
        val item = provider.getItem(position) ?: return

        holder.tvName.text = item.name
        holder.ivPhoto.load(item.image)
        holder.checkbox.apply {
            isChecked = provider.selected.contains(item.id)
            changeVisibility(inEditMode(provider))
        }
    }

    override fun isForViewType(position: Int, provider: ChannelsProvider): Boolean = provider.repository.getItem(position) is Channel
    private fun inEditMode(provider: ChannelsProvider): Boolean = provider.mode == ChannelsMode.EDIT
}