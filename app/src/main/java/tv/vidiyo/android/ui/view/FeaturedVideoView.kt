package tv.vidiyo.android.ui.view

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import android.widget.ImageView
import android.widget.TextView
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.LoopingMediaSource
import com.google.android.exoplayer2.source.hls.DefaultHlsDataSourceFactory
import com.google.android.exoplayer2.source.hls.HlsDataSourceFactory
import com.google.android.exoplayer2.source.hls.HlsMediaSource
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.ui.SimpleExoPlayerView
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import tv.vidiyo.android.BuildConfig
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.episode.Episode
import tv.vidiyo.android.extension.MMM_dd_YY
import tv.vidiyo.android.extension.load
import tv.vidiyo.android.extension.unixTimeStampToDate

class FeaturedVideoView : ConstraintLayout {
    lateinit var player: SimpleExoPlayer

    private lateinit var hlsDataSourceFactory: HlsDataSourceFactory

    lateinit var ivShutter: ImageView
    lateinit var playerView: SimpleExoPlayerView
    lateinit var tvEpisodeTitle: TextView
    lateinit var tvEpisodeNumberDate: TextView

    var isMuted: Boolean
        get() = player.volume == 0F
        set(value) {
            player.volume = if (value) 0F else 1F
        }

    constructor (context: Context) : super(context) {
        init()
    }

    constructor (context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
    }

    constructor (context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
    }

    private fun init() {
        inflate(context, R.layout.view_featured_video, this)

        ivShutter = findViewById(R.id.exo_shutter)
        playerView = findViewById(R.id.exo_player_view)
        tvEpisodeTitle = findViewById(R.id.tv_episode_title)
        tvEpisodeNumberDate = findViewById(R.id.tv_episode_number_date)

        tvEpisodeTitle.isSelected = true

        val bandwidthMeter = DefaultBandwidthMeter()
        val videoTrackSelectionFactory = AdaptiveTrackSelection.Factory(bandwidthMeter)
        val trackSelector = DefaultTrackSelector(videoTrackSelectionFactory)
        val userAgent = Util.getUserAgent(context, BuildConfig.APPLICATION_ID)
        val dataSourceFactory = DefaultDataSourceFactory(context, userAgent, bandwidthMeter)

        hlsDataSourceFactory = DefaultHlsDataSourceFactory(dataSourceFactory)

        player = ExoPlayerFactory.newSimpleInstance(context, trackSelector)
        player.playWhenReady = true
        player.volume = 0f

        playerView.player = player
    }

    fun playEpisode(episode: Episode?) {
        player.stop()

        ivShutter.setImageDrawable(null)
        ivShutter.load(episode?.image)

        tvEpisodeTitle.text = episode?.name
        tvEpisodeNumberDate.text = episode?.let { context.getString(R.string.format_season_n_episode_m_date_s, it.seasonsNumber, it.episodesNumber, MMM_dd_YY.format(it.publishDate.unixTimeStampToDate())) }

        val uri = episode?.media?.uri
        if (uri != null) {
            val source = HlsMediaSource(uri, hlsDataSourceFactory, 1, handler, null)
            player.prepare(LoopingMediaSource(source))
        }
    }
}