package tv.vidiyo.android.ui.view

import android.annotation.SuppressLint
import android.content.Context
import android.support.v4.view.ViewPager
import android.util.AttributeSet
import android.view.MotionEvent

class SwipelessViewPager @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null
) : ViewPager(context, attrs) {

    fun previous() = moveToItem(currentItem - 1)
    fun next(): Boolean {
        val position = currentItem + 1
        moveToItem(position)
        return position == adapter.count
    }

    private fun moveToItem(position: Int) {
        if (position >= 0 && position < adapter.count) {
            setCurrentItem(position, true)
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(ev: MotionEvent?): Boolean = false
    override fun onInterceptTouchEvent(ev: MotionEvent?): Boolean = false
}