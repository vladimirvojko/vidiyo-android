package tv.vidiyo.android.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.PopupMenu
import android.support.v7.widget.RecyclerView
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import com.google.android.exoplayer2.ui.PlaybackControlView
import com.google.android.exoplayer2.ui.SimpleExoPlayerView
import tv.vidiyo.android.App
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.episode.Episode
import tv.vidiyo.android.api.model.episode.EpisodeInfo
import tv.vidiyo.android.base.BaseActivity
import tv.vidiyo.android.base.view.ShowMessagePresentationView
import tv.vidiyo.android.base.view.holder.ViewHolder
import tv.vidiyo.android.di.subcomponent.episode.FullscreenEpisodeModule
import tv.vidiyo.android.domain.contract.EpisodeContract
import tv.vidiyo.android.domain.presenter.episode.EpisodePlayerPresenter
import tv.vidiyo.android.extension.*
import tv.vidiyo.android.ui.adapter.DataRecyclerAdapter
import tv.vidiyo.android.ui.boarding.BoardingActivity
import tv.vidiyo.android.ui.dialog.ShareEpisodeSheetDialog
import tv.vidiyo.android.ui.view.SpaceItemDecoration
import tv.vidiyo.android.util.Animation
import tv.vidiyo.android.util.ExoPlayerAssistant
import javax.inject.Inject

class FullscreenActivity : BaseActivity(), View.OnClickListener, EpisodeContract.Player.View,
        PlaybackControlView.VisibilityListener, ViewHolder.OnViewHolderClickListener,
        PopupMenu.OnMenuItemClickListener {

    companion object {
        private const val EXTRA_0 = "EXTRA_0"//episode

        fun start(activity: Activity, episode: Episode) {
            activity.startActivity(Intent(activity, FullscreenActivity::class.java).also {
                it.putExtra(EXTRA_0, episode)
            })
        }
    }

    private lateinit var playerView: SimpleExoPlayerView
    private lateinit var controlsLayout: RelativeLayout
    private lateinit var tvTitle: TextView
    private lateinit var playButton: ImageView
    private lateinit var fullscreenButton: ImageView
    private lateinit var layout: View
    private lateinit var recyclerView: RecyclerView
    private lateinit var ivLike: ImageView
    private lateinit var likeView: TextView

    private lateinit var popup: PopupMenu

    @Inject lateinit var assistant: ExoPlayerAssistant
    @Inject lateinit var presenter: EpisodePlayerPresenter

    private lateinit var episode: Episode
    private var destroy = true

    override fun onCreate(b: Bundle?) {
        super.onCreate(b)
        setContentView(R.layout.activity_fullscreen)
        episode = intent.getParcelableExtra(EXTRA_0) ?: throw IllegalArgumentException("something went wrong")
        App.graph.getEpisodeComponent(episode.id).plus(FullscreenEpisodeModule(this)).injectTo(this)

        playerView = findViewById(R.id.exo_player_view)
        controlsLayout = playerView.findViewById(R.id.rl_controls)
        playButton = playerView.findViewById(R.id.btn_play)
        tvTitle = playerView.findViewById(R.id.exo_title)
        fullscreenButton = playerView.findViewById(R.id.exo_fullscreen)
        ivLike = playerView.findViewById(R.id.exo_like)
        layout = findViewById(R.id.layout)
        recyclerView = findViewById(R.id.recycler_view)
        likeView = findViewById(R.id.like_view)
        likeView.tag = false

        val ivMore = findViewById<ImageView>(R.id.exo_more)

        recyclerView.adapter = DataRecyclerAdapter.products(presenter.repository.productsProvider, this, false)
        recyclerView.addItemDecoration(SpaceItemDecoration(this, true, true))

        popup = PopupMenu(this, ivMore)
        popup.menuInflater.inflate(R.menu.episode, popup.menu)
        popup.setOnMenuItemClickListener(this)

        playerView.setControllerVisibilityListener(this)
        controlsLayout.tag = false

        layout.post {
            layout.translationY = layout.height.toFloat()
            layout.visibility = View.GONE
        }

        tvTitle.text = episode.name
        fullscreenButton.setImageResource(R.drawable.svg_fullscreen_exit)

        findViewById<View>(R.id.exo_minimize).changeVisibility(false)
        playButton.setOnClickListener(this)
        fullscreenButton.setOnClickListener(this)

        assistant.currentPresenter = presenter
        assistant.prepare(this, playerView)
        assignFrom(playerView, R.id.exo_products, R.id.exo_like, R.id.exo_more)
    }

    override fun onViewHolderClick(viewHolder: RecyclerView.ViewHolder, position: Int) {
        openUrl(presenter.getProduct(position - 1)?.url ?: return)
    }

    override fun onPostResume() {
        super.onPostResume()
        likeView.post {
            likeView.translationY = -likeView.height.toFloat() * Animation.MULTIPLIER
            likeView.visibility = View.GONE
        }
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        window.decorView.systemUiVisibility = if (hasFocus) View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or
                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or
                View.SYSTEM_UI_FLAG_FULLSCREEN or
                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        else
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or
                    View.SYSTEM_UI_FLAG_FULLSCREEN or
                    View.SYSTEM_UI_FLAG_IMMERSIVE
    }

    override fun load(info: EpisodeInfo) {
        assistant.load(info, playerView)
    }

    override fun onResume() {
        super.onResume()
        assistant.onResume()
        presenter.start()
    }

    override fun onPause() {
        super.onPause()
        assistant.onPause()
    }

    override fun onBackPressed() {
        destroy = false
        super.onBackPressed()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (destroy) {
            assistant.release()
        }
    }

    override fun showLikeViewAndUpdate(liked: Boolean, withAnimation: Boolean) {
        if (likeView.tag == false) {
            val likedRes = if (liked) R.drawable.svg_liked else R.drawable.svg_like
            likeView.setText(if (liked) R.string.episode_you_like_this_video else R.string.episode_you_dislike_this_video)
            likeView.setCompoundDrawablesRelativeWithIntrinsicBounds(likedRes, 0, 0, 0)
            ivLike.setImageResource(likedRes)

            if (!withAnimation) return

            Animation.slideToAnimator(likeView, true, false, Animation.MULTIPLIER) {
                Animation.slideToAnimator(likeView, false, false, Animation.MULTIPLIER)
                        ?.setStartDelay(Animation.DELAY)?.start()

            }?.setStartDelay(0L)?.start()
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.exo_products -> Animation.slideTo(layout, !layout.isVisible, true)
            R.id.exo_like -> if (likeView.tag == false) {
                presenter.toggleLike()
            }

            R.id.exo_fullscreen -> onBackPressed()
            R.id.btn_play -> assistant.togglePlayPause(playButton)
            R.id.exo_more -> popup.show()

            else -> return
        }
    }

    override fun onMenuItemClick(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.item_share -> {
                val episode = presenter.episode ?: return true
                ShareEpisodeSheetDialog.newInstance(episode).show(supportFragmentManager)
            }

            R.id.item_report -> Unit
            else -> return false
        }
        return true
    }


    override fun showMessage(message: ShowMessagePresentationView.Message)
            = Toast.makeText(this, message.text, Toast.LENGTH_SHORT).show()

    override fun onVisibilityChange(visibility: Int) = assistant.changeVisibility(controlsLayout, playerView, visibility)

    override fun openRegistration() {
        startActivity<BoardingActivity>(BoardingActivity.EXTRA to BoardingActivity.SIGN_UP)
    }
}