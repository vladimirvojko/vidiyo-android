package tv.vidiyo.android.ui.adapter.delegate.settings

import android.view.ViewGroup
import tv.vidiyo.android.api.model.social.SocialAccount
import tv.vidiyo.android.api.model.social.SocialStatus
import tv.vidiyo.android.base.adapter.delegate.ListProviderAdapterDelegate
import tv.vidiyo.android.base.adapter.provider.ListDataProvider
import tv.vidiyo.android.ui.view.holder.ConnectionButtonViewHolder
import tv.vidiyo.android.ui.view.holder.OnConnectionButtonClickListener

class ConnectionButtonAdapterDelegate(
        provider: ListDataProvider<SocialAccount>,
        listener: OnConnectionButtonClickListener?
) : ListProviderAdapterDelegate<SocialAccount, OnConnectionButtonClickListener,
        ConnectionButtonViewHolder>(provider, listener) {

    override fun onCreateViewHolder(parent: ViewGroup) = ConnectionButtonViewHolder(parent, listener)

    override fun onBindViewHolder(position: Int, holder: ConnectionButtonViewHolder, provider: ListDataProvider<SocialAccount>,
                                  item: SocialAccount) {
        with(holder.textView) {
            val social = item.socialType
            setText(social.title)
            setCompoundDrawablesRelativeWithIntrinsicBounds(social.icon16, 0, 0, 0)
        }
        holder.button.setActiveState(item.status == SocialStatus.connected)
    }
}