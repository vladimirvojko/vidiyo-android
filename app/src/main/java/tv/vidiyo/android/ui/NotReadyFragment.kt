package tv.vidiyo.android.ui

import tv.vidiyo.android.R
import tv.vidiyo.android.base.fragment.BaseFragment

class NotReadyFragment : BaseFragment() {
    override val layoutResId = R.layout.fragment_not_ready
}