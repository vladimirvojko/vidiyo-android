package tv.vidiyo.android.ui.boarding

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginResult
import tv.vidiyo.android.DependencyGraph
import tv.vidiyo.android.R
import tv.vidiyo.android.base.fragment.BaseFragment
import tv.vidiyo.android.di.subcomponent.boarding.FriendsModule
import tv.vidiyo.android.domain.contract.SignUpContract
import tv.vidiyo.android.domain.presenter.boarding.FindFriendsPresenter
import tv.vidiyo.android.extension.assignFrom
import tv.vidiyo.android.ui.boarding.signup.FriendsListFragment
import tv.vidiyo.android.util.ALog
import tv.vidiyo.android.util.FindFriendsHelper
import javax.inject.Inject

class FindFriendsFragment : BaseFragment(), SignUpContract.FindFriends.View, View.OnClickListener, DialogInterface.OnClickListener {

    override val layoutResId = R.layout.fragment_find_friends
    override val toolbarTitleRes = R.string.boarding_find_friends
    override val optionsMenuResId = R.menu.find_friends

    @Inject
    lateinit var presenter: FindFriendsPresenter
    private lateinit var helper: FindFriendsHelper

    override fun injectDependencies(graph: DependencyGraph) {
        graph.friendsComponent.plus(FriendsModule(this)).injectTo(this)
    }

    override fun onCreate(b: Bundle?) {
        super.onCreate(b)
        helper = FindFriendsHelper(presenter).also { it.onCreate(callback) }
    }

    override fun onViewCreated(view: View, b: Bundle?) {
        super.onViewCreated(view, b)

        assignFrom(view, R.id.btn_facebook, R.id.btn_gplus, R.id.btn_contacts)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_facebook -> helper.findFacebookFriends(this)
            R.id.btn_gplus -> presenter.findGoogleFriends()
            R.id.btn_contacts -> helper.findContacts(context, this)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (!helper.onActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.item_skip -> skip()
            else -> return super.onOptionsItemSelected(item)
        }

        return true
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (!helper.onRequestPermissionsResult(this, requestCode, permissions, grantResults)) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    override fun startFriendsList() {
        startFragment(FriendsListFragment())
    }

    override fun startSelectChannels() {
        startFragment(SelectChannelsFragment(), false)
    }

    override fun onClick(dialog: DialogInterface?, which: Int) {
        startSelectChannels()
    }

    override fun permissionNotGranted() {
        Toast.makeText(context, R.string.error_permission_contacts, Toast.LENGTH_LONG).show()
    }

    fun skip() {
        AlertDialog.Builder(context)
                .setTitle(R.string.boarding_no_friends_connected)
                .setMessage(R.string.boarding_no_friends_connected_description)
                .setPositiveButton(R.string.Continue, this)
                .setNegativeButton(R.string.go_back, null)
                .show()
    }

    override fun onDestroy() {
        presenter.destroyLoader()
        super.onDestroy()
    }

    private val callback = object : FacebookCallback<LoginResult> {
        override fun onSuccess(result: LoginResult?) {
            val accessToken = result?.accessToken ?: return
            presenter.findFacebookFriends(accessToken.token)
        }

        override fun onError(error: FacebookException?) {
            ALog.d("onError: $error")
        }

        override fun onCancel() {
            ALog.d("onCancel")
        }
    }
}