package tv.vidiyo.android.ui.boarding.signup

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

class SignUpPagerAdapter(manager: FragmentManager, val tabNames: Array<String>) : FragmentPagerAdapter(manager) {

    override fun getItem(position: Int): Fragment = when (position) {
        0 -> SocialFragment()
        1 -> PhoneFragment()
        else -> EmailFragment()
    }

    override fun getPageTitle(position: Int): CharSequence = tabNames[position]
    override fun getCount(): Int = tabNames.size
}