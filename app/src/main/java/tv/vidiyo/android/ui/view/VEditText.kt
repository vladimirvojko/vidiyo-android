package tv.vidiyo.android.ui.view

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.support.annotation.DrawableRes
import android.support.annotation.IntDef
import android.support.v7.widget.AppCompatEditText
import android.util.AttributeSet
import android.view.MotionEvent
import tv.vidiyo.android.R

class VEditText : AppCompatEditText {

    companion object {
        @IntDef(NOTHING, FAILURE, SUCCESS)
        @Retention(AnnotationRetention.SOURCE)
        annotation class IndicatorType

        const val NOTHING = 0L
        const val FAILURE = 1L
        const val SUCCESS = 2L
    }

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        parseAttributes(attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        parseAttributes(attrs, defStyleAttr)
    }

    init {
        changeClearButtonVisibility(false)
    }

    val drawableEnd: Drawable? get() = compoundDrawablesRelative[2]

    var clearButton: Int = R.drawable.svg_cancel_16
    var indicatorFailure: Int = R.drawable.svg_dot_red_16
    var indicatorSuccess: Int = R.drawable.svg_dot_green_16
    var focused: Boolean = false

    @IndicatorType
    private var current = NOTHING

    fun showIndicator(@IndicatorType type: Long) {
        val indicator = getIndicatorBy(type)
        current = type
        setDrawableEnd(indicator)
    }

    private fun parseAttributes(attrs: AttributeSet?, defStyleAttr: Int) {
        val ta = context.theme.obtainStyledAttributes(attrs, R.styleable.VEditText, defStyleAttr, 0)

        try {
            clearButton = ta.getResourceId(R.styleable.VEditText_clearButton, R.drawable.svg_cancel_16)
            indicatorFailure = ta.getResourceId(R.styleable.VEditText_indicatorFailure, R.drawable.svg_dot_red_16)
            indicatorSuccess = ta.getResourceId(R.styleable.VEditText_indicatorSuccess, R.drawable.svg_dot_green_16)

        } finally {
            ta.recycle()
        }
    }

    private fun setDrawableEnd(@DrawableRes res: Int) {
        setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, res, 0)
    }

    private fun getIndicatorBy(@IndicatorType type: Long) = when (type) {
        FAILURE -> indicatorFailure
        SUCCESS -> indicatorSuccess
        else -> 0
    }

    private fun changeClearButtonVisibility(visible: Boolean) {
        setDrawableEnd(clearButton)
        drawableEnd?.alpha = if (visible) 255 else 0
    }

    override fun onTextChanged(text: CharSequence?, start: Int, lengthBefore: Int, lengthAfter: Int) {
        changeClearButtonVisibility(text?.isNotEmpty() == true)
        super.onTextChanged(text, start, lengthBefore, lengthAfter)
    }

    private fun clear() {
        text = null
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when (event?.action) {
            MotionEvent.ACTION_UP -> drawableEnd?.let {
                if (focused && event.rawX >= (right - it.bounds.width() * 2)) {
                    clear()
                }
            }
        }

        return super.onTouchEvent(event)
    }

    override fun onFocusChanged(focused: Boolean, direction: Int, previouslyFocusedRect: Rect?) {
        this.focused = focused
        setDrawableEnd(if (focused) clearButton.also {
            changeClearButtonVisibility(text?.isNotEmpty() == true)
        } else getIndicatorBy(current))
        super.onFocusChanged(focused, direction, previouslyFocusedRect)
    }
}
