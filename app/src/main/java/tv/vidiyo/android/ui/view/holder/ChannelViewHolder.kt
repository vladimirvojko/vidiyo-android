package tv.vidiyo.android.ui.view.holder

import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import tv.vidiyo.android.R
import tv.vidiyo.android.base.view.holder.ViewHolder
import tv.vidiyo.android.ui.view.AspectRatioImageView
import java.lang.ref.WeakReference

class ChannelViewHolder(
        parent: ViewGroup,
        listener: WeakReference<OnViewHolderClickListener>?
) : ViewHolder<ViewHolder.OnViewHolderClickListener>(parent, listener, R.layout.view_item_channel, true) {

    val tvName: TextView = itemView.findViewById(R.id.tv_name)
    val ivPhoto: AspectRatioImageView = itemView.findViewById(R.id.iv_photo)
    val checkbox: CheckBox = itemView.findViewById(R.id.checkbox)
}