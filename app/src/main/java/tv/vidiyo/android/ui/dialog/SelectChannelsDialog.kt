package tv.vidiyo.android.ui.dialog

import android.os.Bundle
import android.view.View
import tv.vidiyo.android.R
import tv.vidiyo.android.base.BaseDialog
import tv.vidiyo.android.base.fragment.BaseFragment
import tv.vidiyo.android.extension.assignTo
import tv.vidiyo.android.extension.notContains
import tv.vidiyo.android.ui.boarding.SelectChannelsFragment

class SelectChannelsDialog : BaseDialog(), View.OnClickListener {

    companion object {
        fun show(fragment: BaseFragment, cancelable: Boolean = false): Boolean {
            val m = fragment.childFragmentManager
            if (m.notContains(SelectChannelsDialog::class.java.name)) {
                val dialog = SelectChannelsDialog()
                dialog.isCancelable = cancelable
                dialog.show(m)

                return true
            }
            return false
        }
    }

    override val layoutRes = R.layout.dialog_select_channels
    override val themeRes = R.style.Theme_Transparent
    override val isFullScreen = true

    override fun onViewCreated(view: View, b: Bundle?) {
        super.onViewCreated(view, b)
        assignTo(view.findViewById(R.id.btn_continue))
    }

    override fun onClick(v: View?) {
        val f = parentFragment
        if (f is SelectChannelsFragment) {
            f.changeMessageVisibility(true)
        }
        dismiss()
    }

    override fun onCleanUp() {}
}