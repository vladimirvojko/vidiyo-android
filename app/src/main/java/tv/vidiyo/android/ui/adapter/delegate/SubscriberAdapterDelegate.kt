package tv.vidiyo.android.ui.adapter.delegate

import android.view.ViewGroup
import tv.vidiyo.android.api.model.user.FollowingUser
import tv.vidiyo.android.base.adapter.delegate.ListProviderAdapterDelegate
import tv.vidiyo.android.base.adapter.provider.ListDataProvider
import tv.vidiyo.android.domain.contract.setUser
import tv.vidiyo.android.extension.changeVisibility
import tv.vidiyo.android.ui.view.holder.OnSubscribableViewHolderClickListener
import tv.vidiyo.android.ui.view.holder.SubscriberViewHolder
import tv.vidiyo.android.util.SubscriptionManager

open class SubscriberAdapterDelegate(
        provider: ListDataProvider<FollowingUser>,
        listener: OnSubscribableViewHolderClickListener?,
        protected val manager: SubscriptionManager
) : ListProviderAdapterDelegate<FollowingUser, OnSubscribableViewHolderClickListener, SubscriberViewHolder>(provider, listener) {

    override fun onCreateViewHolder(parent: ViewGroup) = SubscriberViewHolder(parent, listener)
    override fun onBindViewHolder(position: Int, holder: SubscriberViewHolder, provider: ListDataProvider<FollowingUser>, item: FollowingUser) {
        holder.setUser(item)
        with(holder.button) {
            changeVisibility(manager.canRequestUser(item))
            setState(item.status)
        }
    }
}