package tv.vidiyo.android.ui.topic

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.widget.SwipeRefreshLayout
import android.view.View
import android.widget.LinearLayout
import tv.vidiyo.android.DependencyGraph
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.channel.Channel
import tv.vidiyo.android.api.model.topic.Topic
import tv.vidiyo.android.base.adapter.pages.PageFragment
import tv.vidiyo.android.base.adapter.pages.PagesAdapter
import tv.vidiyo.android.base.fragment.PagesMainFragment
import tv.vidiyo.android.di.subcomponent.topic.TopicComponent
import tv.vidiyo.android.di.subcomponent.topic.TopicModule
import tv.vidiyo.android.domain.contract.TopicContract
import tv.vidiyo.android.domain.presenter.topic.TopicPresenter
import tv.vidiyo.android.extension.changeVisibility
import tv.vidiyo.android.ui.view.ShowView
import tv.vidiyo.android.ui.view.TopicTab
import tv.vidiyo.android.ui.view.selection
import tv.vidiyo.android.ui.view.setTopic
import javax.inject.Inject

class TopicFragment : PagesMainFragment(), TopicContract.View, TabLayout.OnTabSelectedListener, View.OnClickListener,
        SwipeRefreshLayout.OnRefreshListener {

    companion object {
        private const val EXTRA = "EXTRA" //topic

        fun newInstance(topic: Topic) = TopicFragment().apply {
            arguments = Bundle(1).also { it.putParcelable(EXTRA, topic) }
        }
    }

    override val appBarLayoutResId: Int = R.layout.app_bar_topic_tabs
    override val toolbarIndicator = R.drawable.svg_back

    private lateinit var layout: LinearLayout
    private lateinit var showView: ShowView

    lateinit var component: TopicComponent

    @Inject
    lateinit var presenter: TopicPresenter

    override fun injectDependencies(graph: DependencyGraph) {
        val topic = arguments.getParcelable<Topic>(EXTRA)
        component = graph.applicationComponent.plus(TopicModule(this, topic))
        component.injectTo(this)

        presenter.topic = topic
    }

    override fun onViewCreated(view: View, b: Bundle?) {
        super.onViewCreated(view, b)
        layout = view.findViewById(R.id.layout)
        showView = view.findViewById(R.id.show_view)
        tabLayout?.addOnTabSelectedListener(this)
        setRefreshing(true)

        showView.button.setOnClickListener(this)
    }

    override fun onPostViewCreated() {
        presenter.start()
    }

    override fun onClick(v: View) {
        if (refreshLayout?.isRefreshing == false)
            presenter.toggleSubscription()
    }

    override fun onRefresh() {
        presenter.refresh()
    }

    override fun update() {
        setRefreshing(false)

        val pages = mutableListOf<PageFragment>()
        presenter.topic.let {
            updateTopicView(it, presenter.channel)
            toolbar?.title = it.name
            pages.add(PageFragment(InnerTopicFragment.topicCreator(-1)))
        }

        presenter.related.forEachIndexed { index, _ ->
            pages.add(PageFragment(InnerTopicFragment.topicCreator(index)))
        }
        adapter.setPages(pages)
        setCustomTabs()
        layout.changeVisibility(true)
    }

    override fun updateTopicView(topic: Topic, channel: Channel?) {
        showView.setTopic(topic, channel)
    }

    private fun setCustomTabs() {
        tabLayout?.let {
            val selected = it.selectedTabPosition
            initTabAt(it, 0, presenter.topic, selected)

            presenter.related.forEachIndexed { index, topic -> initTabAt(it, index + 1, topic, selected) }
            it.changeVisibility(true)
        }

    }

    private fun initTabAt(tl: TabLayout, index: Int, topic: Topic, selected: Int) {
        (tl.getTabAt(index)?.setCustomView(TopicTab(context ?: return))?.customView as? TopicTab)?.apply {
            setTopic(topic)
            selection(index == selected)
        }
    }

    override fun createAdapter(): PagesAdapter = PagesAdapter(context, childFragmentManager)

    override fun onTabSelected(tab: TabLayout.Tab) {
        (tab.customView as? TopicTab)?.selection(true)
    }

    override fun onTabUnselected(tab: TabLayout.Tab) {
        (tab.customView as? TopicTab)?.selection(false)
    }

    override fun onTabReselected(tab: TabLayout.Tab?) {}
}