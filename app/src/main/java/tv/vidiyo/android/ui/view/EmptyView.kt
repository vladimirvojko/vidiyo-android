package tv.vidiyo.android.ui.view

import android.content.Context
import android.support.v4.widget.NestedScrollView
import android.util.AttributeSet
import android.view.View
import android.widget.TextView
import tv.vidiyo.android.R

class EmptyView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0
) : NestedScrollView(context, attrs, defStyleAttr) {

    private val textView: TextView

    init {
        View.inflate(context, R.layout.view_empty, this)
        textView = findViewById(R.id.tv_message)
    }

    fun set(text: String, drawableRes: Int) {
        textView.text = text
        textView.setCompoundDrawablesRelativeWithIntrinsicBounds(0, drawableRes, 0, 0)
    }

    fun setWithFormat(text: String, drawableRes: Int) {
        set(context.getString(R.string.format_no_s_here_yet, text), drawableRes)
    }
}