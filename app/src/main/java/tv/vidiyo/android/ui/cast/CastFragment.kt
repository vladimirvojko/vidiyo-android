package tv.vidiyo.android.ui.cast

import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.view.View
import tv.vidiyo.android.DependencyGraph
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.cast.Cast
import tv.vidiyo.android.api.model.cast.CastInfo
import tv.vidiyo.android.base.adapter.pages.PageFragment
import tv.vidiyo.android.base.adapter.pages.PagesAdapter
import tv.vidiyo.android.base.fragment.PagesMainFragment
import tv.vidiyo.android.di.subcomponent.cast.CastComponent
import tv.vidiyo.android.di.subcomponent.cast.CastModule
import tv.vidiyo.android.domain.contract.CastContract
import tv.vidiyo.android.domain.presenter.cast.CastPresenter
import tv.vidiyo.android.extension.changeVisibility
import tv.vidiyo.android.ui.social.SocialFeedFragment
import tv.vidiyo.android.ui.view.InfoView
import tv.vidiyo.android.ui.view.setCast
import javax.inject.Inject

class CastFragment : PagesMainFragment(), CastContract.View, View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    companion object {
        private const val EXTRA = "EXTRA"//cast

        fun newInstance(cast: Cast) = CastFragment().apply {
            arguments = Bundle(1).also {
                it.putParcelable(EXTRA, cast)
            }
        }
    }

    override val appBarLayoutResId = R.layout.app_bar_cast
    override val toolbarIndicator = R.drawable.svg_back
    override val toolbarTitleRes = R.string.cast

    private lateinit var infoView: InfoView

    lateinit var component: CastComponent

    @Inject
    lateinit var presenter: CastPresenter

    lateinit override var cast: Cast

    override fun injectDependencies(graph: DependencyGraph) {
        cast = arguments.getParcelable(EXTRA)
        component = graph.applicationComponent.plus(CastModule(this, cast.id))
        component.injectTo(this)
    }

    override fun onViewCreated(view: View, b: Bundle?) {
        super.onViewCreated(view, b)
        infoView = view.findViewById(R.id.info_view)
        infoView.setCast(cast)
        infoView.btnAction.setOnClickListener(this)
    }

    override fun onPostViewCreated() {
        presenter.start()
    }

    override fun onRefresh() {
        presenter.refresh()
    }

    override fun onClick(v: View) {
        if (refreshLayout?.isRefreshing == false)
            presenter.toggleSubscription()
    }

    override fun createAdapter() = PagesAdapter(context, childFragmentManager,
            PageFragment(InnerCastFragment.showsCreator(), R.string.shows),
            PageFragment(InnerCastFragment.episodeCreator(), R.string.episodes),
            PageFragment(SocialFeedFragment.creator(), R.string.social),
            PageFragment(InnerCastFragment.connectShareCreator(), R.string.connect_and_share)
    )

    override fun updateCastInfo(info: CastInfo) {
        infoView.setCastInfo(info)
        infoView.changeVisibility(true)
    }
}