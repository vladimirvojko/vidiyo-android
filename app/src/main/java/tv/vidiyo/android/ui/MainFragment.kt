package tv.vidiyo.android.ui

import android.os.Build
import android.os.Bundle
import android.support.annotation.ColorRes
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewCompat
import android.view.*
import android.widget.Toast
import tv.vidiyo.android.R
import tv.vidiyo.android.base.fragment.BaseToolbarFragment
import tv.vidiyo.android.base.view.ShowMessagePresentationView
import tv.vidiyo.android.extension.inflate
import tv.vidiyo.android.extension.statusBarHeight
import tv.vidiyo.android.util.Animation

abstract class MainFragment : BaseToolbarFragment(), ShowMessagePresentationView {
    override val layoutResId get() = R.layout.fragment_main
    open val isTranslucentStatus: Boolean = false
    open val statusBarColorId: Int = R.color.colorPrimaryDark

    open val appBarViewStubId: Int? get() = R.id.app_bar_view_stub
    open val appBarLayoutResId: Int? get() = R.layout.app_bar

    open val contentViewStubId: Int? get() = R.id.content_view_stub
    open val contentLayoutResId: Int? get() = null

    open val mainActivity: MainActivity? get() = activity as? MainActivity

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, b: Bundle?): View? {
        return super.onCreateView(inflater, container, b)?.also {
            inflateViewStub(it, appBarLayoutResId, appBarViewStubId)
            inflateViewStub(it, contentLayoutResId, contentViewStubId)
        }
    }

    private fun inflateViewStub(view: View, layoutId: Int?, stubId: Int?) {
        if (layoutId != null && stubId != null) {
            (view.findViewById<ViewStub>(stubId))?.inflate(layoutId)
        }
    }

    protected fun setStatusBarTransparency(isTransparent: Boolean) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            activity.window.apply {
                if (isTransparent) {
                    setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
                    (toolbar?.layoutParams as? ViewGroup.MarginLayoutParams)?.topMargin = activity.window.statusBarHeight
                } else {
                    clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
                    this@MainFragment.setStatusBarColor(statusBarColorId)
                }
            }
        }
    }

    override fun onStart() {
        super.onStart()
        configureActivity()
        ViewCompat.requestApplyInsets(activity.window.decorView.rootView)
    }

    open fun configureActivity() {
        setStatusBarTransparency(isTranslucentStatus)
    }

    override fun showMessage(message: ShowMessagePresentationView.Message) {
        when (message) {
            is ShowMessagePresentationView.ToastMessage -> message.apply {
                Toast.makeText(context, resId?.let { context.getString(it) } ?: text, duration).show()
            }
        }
    }

    protected fun setStatusBarColor(@ColorRes colorRes: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && context != null) {
            Animation.color = ContextCompat.getColor(context, colorRes)
            activity.window.statusBarColor = Animation.color
        }
    }
}