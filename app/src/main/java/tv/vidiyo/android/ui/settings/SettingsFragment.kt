package tv.vidiyo.android.ui.settings

import android.content.Intent
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.widget.CompoundButton
import android.widget.LinearLayout
import android.widget.Toast
import tv.vidiyo.android.DependencyGraph
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.Header
import tv.vidiyo.android.api.model.Item
import tv.vidiyo.android.api.model.user.FollowingType
import tv.vidiyo.android.base.adapter.SectionedRecyclerAdapter
import tv.vidiyo.android.base.adapter.provider.SectionedListDataProvider
import tv.vidiyo.android.base.adapter.section.Section
import tv.vidiyo.android.base.fragment.BaseRecyclerViewFragment
import tv.vidiyo.android.base.view.holder.ViewHolder
import tv.vidiyo.android.di.subcomponent.settings.SettingsModule
import tv.vidiyo.android.domain.contract.SettingsContract
import tv.vidiyo.android.domain.presenter.settings.MainSettingsPresenter
import tv.vidiyo.android.extension.startActivity
import tv.vidiyo.android.extension.weak
import tv.vidiyo.android.ui.FindFriendsActivity
import tv.vidiyo.android.ui.adapter.delegate.settings.SettingFooterAdapterDelegate
import tv.vidiyo.android.ui.adapter.delegate.settings.SettingHeaderAdapterDelegate
import tv.vidiyo.android.ui.adapter.delegate.settings.SettingsActionAdapterDelegate
import tv.vidiyo.android.ui.adapter.delegate.settings.SettingsAdapterDelegate
import tv.vidiyo.android.ui.boarding.SplashActivity
import tv.vidiyo.android.ui.view.DividerItemDecoration
import tv.vidiyo.android.ui.widget.InfiniteScrollRecyclerView
import javax.inject.Inject

class SettingsFragment : BaseRecyclerViewFragment<SectionedRecyclerAdapter<Header, Item>>(), SettingsContract.Main.View, ViewHolder.OnViewHolderClickListener,
        CompoundButton.OnCheckedChangeListener {

    override val layoutResId = R.layout.refresh_recycler_view
    override val toolbarTitleRes = R.string.settings
    override val toolbarIndicator = R.drawable.svg_back

    private val provider = SectionedListDataProvider<Header, Item>().apply {
        add(Section(Header(R.string.settings_header_invite), true), mutableListOf(
                Item(R.drawable.svg_social_facebook, R.string.settings_invite_fb_friends, 0)
        ))
        add(Section(Header(R.string.settings_header_find), true), mutableListOf(
//                Item(R.drawable.svg_social_facebook, R.string.settings_find_fb_friends, 0),
                Item(R.drawable.svg_contacts_20_blue, R.string.settings_find_contacts, 0)
        ))
        add(Section(Header(R.string.settings_header_account), true), mutableListOf(
                Item(0, R.string.settings_edit_profile, 0),
                Item(0, R.string.settings_subscription_type, 0),
                Item(0, R.string.settings_blocked_users, 0),
                Item(0, R.string.settings_change_password, 0),
                Item(0, R.string.settings_private_account, R.string.settings_private_account_description)
        ))
        add(Section(Header(R.string.settings_header_settings), true), mutableListOf(
                Item(0, R.string.settings_social_media_accounts, 0),
                Item(0, R.string.settings_streaming_settings, 0),
                Item(0, R.string.settings_broadcast_settings, 0),
                Item(0, R.string.settings_notifications, 0)
        ))
        add(Section(Header(R.string.settings_header_about), true), mutableListOf(
                Item(0, R.string.settings_privacy_policy, 0),
                Item(0, R.string.settings_terms, 0)
        ))
        add(Section(Header(0), true, true), mutableListOf(
                Item(0, R.string.settings_clear_search_history, 0),
                Item(0, R.string.settings_logout, 0)
        ))
    }

    @Inject
    lateinit var presenter: MainSettingsPresenter

    override fun injectDependencies(graph: DependencyGraph) {
        graph.dataComponent.plus(SettingsModule(this)).injectTo(this)
    }

    override fun startSplashActivity() {
        context.startActivity<SplashActivity>(flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
    }

    private fun notImplementedYet() = Toast.makeText(context, "Not implemented yet", Toast.LENGTH_SHORT).show()

    override fun onCheckedChanged(cb: CompoundButton, checked: Boolean) {
        presenter.togglePrivacy(checked)
    }

    override fun onViewHolderClick(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val item = provider.let { it.getItemForPosition(it.getItemPosition(position)) } ?: return
        when (item.titleResId) {
            R.string.settings_invite_fb_friends -> FindFriendsActivity.start(activity, FindFriendsActivity.FACEBOOK)
            R.string.settings_find_fb_friends -> notImplementedYet()
            R.string.settings_find_contacts -> FindFriendsActivity.start(activity, FindFriendsActivity.CONTACTS)
            R.string.settings_edit_profile -> startFragment(EditProfileFragment(), true)
            R.string.settings_subscription_type -> notImplementedYet()
            R.string.settings_blocked_users -> startFragment(UsersFragment.newInstance(FollowingType.blocked), true)
            R.string.settings_change_password -> startFragment(PasswordFragment(), true)
            R.string.settings_private_account -> return
            R.string.settings_social_media_accounts -> startFragment(SocialAccountsFragment(), true)
            R.string.settings_streaming_settings -> notImplementedYet()
            R.string.settings_broadcast_settings -> notImplementedYet()
            R.string.settings_notifications -> startFragment(NotificationsFragment(), true)
            R.string.settings_privacy_policy -> notImplementedYet()
            R.string.settings_terms -> notImplementedYet()
            R.string.settings_clear_search_history -> showClearSearchDialog()
            R.string.settings_logout -> presenter.logout()

            else -> return
        }
    }

    private fun showClearSearchDialog() {
        AlertDialog.Builder(context, R.style.AppTheme_AlertDialog)
                .setTitle(R.string.settings_are_you_sure)
                .setMessage(R.string.settings_this_action_cannot_be_undone)
                .setPositiveButton(R.string.confirm) { _, _ ->
                    presenter.clearSearch()
                }.setNegativeButton(R.string.Cancel, null)
                .show()
    }

    override fun onRecyclerViewCreated(recyclerView: InfiniteScrollRecyclerView) {
        super.onRecyclerViewCreated(recyclerView)
        recyclerView.addItemDecoration(DividerItemDecoration(context, LinearLayout.VERTICAL))
    }

    override fun onCreateAdapter() = SectionedRecyclerAdapter(listOf(
            SettingHeaderAdapterDelegate(provider),
            SettingsAdapterDelegate(provider, this),
            SettingFooterAdapterDelegate(provider),
            SettingsActionAdapterDelegate(provider, this.weak(), presenter.account)
    ), provider)
}