package tv.vidiyo.android.ui.adapter.delegate.profile

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import tv.vidiyo.android.api.model.channel.Channel
import tv.vidiyo.android.base.adapter.delegate.SectionedAdapterDelegate
import tv.vidiyo.android.base.adapter.provider.SectionedDataProvider
import tv.vidiyo.android.base.adapter.section.ItemPosition
import tv.vidiyo.android.base.view.holder.ViewHolder
import tv.vidiyo.android.ui.view.holder.ViewAllHeaderViewHolder

interface OnViewAllClickListener : ViewHolder.OnViewHolderClickListener {
    fun onViewAllClick(viewHolder: RecyclerView.ViewHolder, position: Int)
}

class ViewAllHeaderDelegate<I>(
        provider: SectionedDataProvider<Channel, I>,
        listener: OnViewAllClickListener?
) : SectionedAdapterDelegate<Channel, I, SectionedDataProvider<Channel, I>, OnViewAllClickListener, ViewAllHeaderViewHolder>(provider, listener) {

    override fun onCreateViewHolder(parent: ViewGroup): ViewAllHeaderViewHolder {
        return ViewAllHeaderViewHolder(parent, listener)
    }

    override fun onBindViewHolder(position: ItemPosition, holder: ViewAllHeaderViewHolder, provider: SectionedDataProvider<Channel, I>) {
        val section = provider.getSection(position.section)?.section ?: return
        holder.setTitle(section.name)
    }

    override fun isForViewType(itemPosition: ItemPosition) = itemPosition.isHeader
}