package tv.vidiyo.android.ui.view

import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import android.widget.TextView
import tv.vidiyo.android.R
import tv.vidiyo.android.extension.changeVisibility
import tv.vidiyo.android.extension.changeVisibilityAnimated
import tv.vidiyo.android.extension.toTime
import tv.vidiyo.android.util.OnAdPlayingListener

class AdvertisementView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr), OnAdPlayingListener {

    private val tvlabel: TextView

    init {
        inflate(context, R.layout.view_advertisement, this)
        tvlabel = findViewById(R.id.tv_label)
        changeVisibility(false)
    }

    override fun onCurrentProgressChanged(current: Long, duration: Long) {
        tvlabel.text = context.getString(R.string.format_ad_s, format(duration - current))
    }

    private fun format(time: Long): String {
        val t = (time / 1000f).toTime()
        return String.format("%02d:%02d", t.second, t.third)
    }

    override fun hide() {
        tvlabel.setText(R.string.ad)
        changeVisibilityAnimated(false)
    }

    override fun show() = changeVisibilityAnimated(true)
    override fun getView() = this
}