package tv.vidiyo.android.ui.view.holder

import android.view.ViewGroup
import tv.vidiyo.android.R
import tv.vidiyo.android.ui.view.StateButton
import java.lang.ref.WeakReference

class SubscriberViewHolder(
        parent: ViewGroup,
        listener: WeakReference<OnSubscribableViewHolderClickListener>?
) : UserViewHolder<OnSubscribableViewHolderClickListener>(parent, listener, R.layout.view_item_subscriber, true) {

    val button: StateButton = itemView.findViewById(R.id.button)

    init {
        button.setOnClickListener {
            listener?.get()?.onSubscribeClick(this, adapterPosition)
        }
    }
}