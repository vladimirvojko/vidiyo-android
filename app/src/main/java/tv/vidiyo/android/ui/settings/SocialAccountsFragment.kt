package tv.vidiyo.android.ui.settings

import android.support.v7.widget.RecyclerView
import android.widget.LinearLayout
import tv.vidiyo.android.DependencyGraph
import tv.vidiyo.android.R
import tv.vidiyo.android.base.fragment.BaseRecyclerViewFragment
import tv.vidiyo.android.di.subcomponent.settings.SocialAccountModule
import tv.vidiyo.android.domain.contract.SettingsContract
import tv.vidiyo.android.domain.presenter.settings.SocialAccountPresenter
import tv.vidiyo.android.ui.adapter.DataRecyclerAdapter
import tv.vidiyo.android.ui.view.DividerItemDecoration
import tv.vidiyo.android.ui.view.holder.OnConnectionButtonClickListener
import tv.vidiyo.android.ui.widget.InfiniteScrollRecyclerView
import javax.inject.Inject

class SocialAccountsFragment : BaseRecyclerViewFragment<DataRecyclerAdapter>(),
        SettingsContract.Socials.View, OnConnectionButtonClickListener {

    override val toolbarTitleRes = R.string.settings_social_media_accounts
    override val toolbarIndicator = R.drawable.svg_back

    @Inject
    lateinit var presenter: SocialAccountPresenter

    override fun injectDependencies(graph: DependencyGraph) {
        (activity as? SettingsActivity)?.socialComponent?.plus(SocialAccountModule(this))?.injectTo(this)
    }

    override fun onPostViewCreated() {
        presenter.start()
    }

    override fun onViewHolderClick(viewHolder: RecyclerView.ViewHolder, position: Int) {}

    override fun onConnectionButtonClicked(holder: RecyclerView.ViewHolder, position: Int) {
        if (refreshLayout?.isRefreshing == false)
            presenter.connect(position)
    }

    override fun onCreateAdapter() = DataRecyclerAdapter.social(presenter.provider, this)
    override fun onRecyclerViewCreated(recyclerView: InfiniteScrollRecyclerView) {
        super.onRecyclerViewCreated(recyclerView)
        recyclerView.addItemDecoration(DividerItemDecoration(context, LinearLayout.VERTICAL))
    }
}