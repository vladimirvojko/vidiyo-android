package tv.vidiyo.android.ui.view.holder.empty

import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import tv.vidiyo.android.R
import tv.vidiyo.android.base.view.holder.ViewHolder
import java.lang.ref.WeakReference

class WatchingEmptyViewHolder(
        parent: ViewGroup,
        listener: WeakReference<NoFriendsClickListener>?
) : ViewHolder<ViewHolder.OnViewHolderClickListener>(parent, null, R.layout.view_empty_watching, false) {

    val tvTitle: TextView = itemView.findViewById(R.id.tv_title)
    val tvMessage: TextView = itemView.findViewById(R.id.tv_message)
}