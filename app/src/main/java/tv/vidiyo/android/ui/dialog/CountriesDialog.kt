package tv.vidiyo.android.ui.dialog

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.LinearLayout
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.Country
import tv.vidiyo.android.base.BaseDialog
import tv.vidiyo.android.base.adapter.provider.MutableListDataProvider
import tv.vidiyo.android.base.fragment.BaseFragment
import tv.vidiyo.android.base.view.holder.ViewHolder
import tv.vidiyo.android.extension.notContains
import tv.vidiyo.android.ui.adapter.DataRecyclerAdapter
import tv.vidiyo.android.ui.boarding.signup.PhoneFragment
import tv.vidiyo.android.ui.view.DividerItemDecoration
import tv.vidiyo.android.ui.view.SpaceItemDecoration
import tv.vidiyo.android.util.AssetsLoader

class CountriesDialog : BaseDialog(), View.OnClickListener, ViewHolder.OnViewHolderClickListener {

    companion object {
        private const val EXTRA = "EXTRA"//country

        private fun newInstance(wasSelected: Country): CountriesDialog {
            val fragment = CountriesDialog()
            val b = Bundle()
            b.putParcelable(EXTRA, wasSelected)
            fragment.arguments = b
            return fragment
        }

        fun show(fragment: BaseFragment, cancelable: Boolean, wasSelected: Country): Boolean {
            val m = fragment.childFragmentManager
            if (m.notContains(CountriesDialog::class.java.name)) {
                val dialog = CountriesDialog.newInstance(wasSelected)
                dialog.isCancelable = cancelable
                dialog.show(m)

                return true
            }
            return false
        }
    }

    override val layoutRes = R.layout.fragment_toolbar_recycler
    lateinit var selected: Country
    lateinit var adapter: DataRecyclerAdapter

    override fun onCreate(b: Bundle?) {
        super.onCreate(b)
        val bundle = b ?: arguments
        selected = bundle.getParcelable(EXTRA)

        val provider = MutableListDataProvider<Country>()
        provider.addAll(AssetsLoader.loadCountries(context), true)

        adapter = DataRecyclerAdapter.countries(provider, this, selected)
    }

    override fun onViewCreated(view: View, b: Bundle?) {
        super.onViewCreated(view, b)
        val toolbar = view.findViewById<Toolbar>(R.id.toolbar)
        val recyclerView = view.findViewById<RecyclerView>(R.id.recycler_view)

        toolbar.apply {
            title = getString(R.string.boarding_select_country)
            setNavigationIcon(R.drawable.svg_back)
            setNavigationOnClickListener(this@CountriesDialog)
        }
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.addItemDecoration(SpaceItemDecoration(context, R.dimen.spacing, true, true))
        recyclerView.addItemDecoration(DividerItemDecoration(context, LinearLayout.VERTICAL))
        recyclerView.adapter = adapter
    }

    override fun onViewHolderClick(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val item = adapter.provider.items?.get(position) as? Country ?: return
        val target = parentFragment
        (target as? PhoneFragment)?.apply {
            country = item
            updateCountry()
            dismiss()
        }
    }

    override fun onClick(v: View?) {
        dismiss()
    }

    override fun onCleanUp() {}
}