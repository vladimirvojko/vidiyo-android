package tv.vidiyo.android.ui.view.holder

import android.view.ViewGroup
import android.widget.TextView
import com.bumptech.glide.load.resource.drawable.GlideDrawable
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import tv.vidiyo.android.R
import tv.vidiyo.android.base.view.holder.ViewHolder
import tv.vidiyo.android.domain.contract.UserView
import tv.vidiyo.android.extension.load
import tv.vidiyo.android.ui.view.ShapeImageView
import java.lang.Exception
import java.lang.ref.WeakReference

open class UserViewHolder<L : ViewHolder.OnViewHolderClickListener>(
        parent: ViewGroup,
        listener: WeakReference<L>?,
        layoutResId: Int = R.layout.view_item_user,
        clickable: Boolean = true
) : ViewHolder<L>(parent, listener, layoutResId, clickable), UserView {

    private val ivAvatar: ShapeImageView = itemView.findViewById(R.id.iv_avatar)
    private val tvName: TextView = itemView.findViewById(R.id.tv_name)
    val tvUsername: TextView = itemView.findViewById(R.id.tv_username)

    override fun <T> setAvatar(avatar: T?, fallbackImageResId: Int) {
        ivAvatar.setImageResource(fallbackImageResId)
        ivAvatar.load(avatar, requestListener = object : RequestListener<T?, GlideDrawable> {
            override fun onResourceReady(resource: GlideDrawable?, model: T?, target: Target<GlideDrawable>?, isFromMemoryCache: Boolean, isFirstResource: Boolean): Boolean {
                ivAvatar.setImageDrawable(resource)
                return true
            }

            override fun onException(e: Exception?, model: T?, target: Target<GlideDrawable>?, isFirstResource: Boolean): Boolean {
                ivAvatar.setImageResource(fallbackImageResId)
                return true
            }
        })
    }

    override fun setUsername(username: String?) = tvUsername.let { it.text = username.let { "@$it" } }

    override fun setName(name: String?) {
        tvName.text = name
    }
}