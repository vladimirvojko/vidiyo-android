package tv.vidiyo.android.ui.home

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.IntentFilter
import android.os.Bundle
import android.support.design.widget.AppBarLayout
import android.support.design.widget.CollapsingToolbarLayout
import android.support.v4.content.LocalBroadcastManager
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.ncapdevi.fragnav.FragNavController
import tv.vidiyo.android.DependencyGraph
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.episode.Episode
import tv.vidiyo.android.base.fragment.ViewFragment
import tv.vidiyo.android.base.interfaces.HasFeatured
import tv.vidiyo.android.base.interfaces.newReceiver
import tv.vidiyo.android.di.subcomponent.home.HomeModule
import tv.vidiyo.android.domain.contract.HomeContract
import tv.vidiyo.android.domain.presenter.home.HomePresenter
import tv.vidiyo.android.extension.*
import tv.vidiyo.android.source.DataType
import tv.vidiyo.android.ui.FindFriendsActivity
import tv.vidiyo.android.ui.MainFragment
import tv.vidiyo.android.ui.adapter.DataRecyclerAdapter
import tv.vidiyo.android.ui.adapter.delegate.HomeEmptyClickListener
import tv.vidiyo.android.ui.profile.ProfileFragment
import tv.vidiyo.android.ui.show.ShowFragment
import tv.vidiyo.android.ui.topic.TopicFragment
import tv.vidiyo.android.ui.view.ErrorRetryView
import tv.vidiyo.android.ui.view.FeaturedVideoView
import tv.vidiyo.android.ui.view.SpaceItemDecoration
import tv.vidiyo.android.ui.view.holder.OnFriendShowViewHolderClickListener
import tv.vidiyo.android.ui.widget.StateLayout
import javax.inject.Inject

class HomeFragment : MainFragment(), HomeContract.View, HomeEmptyClickListener,
        OnFriendShowViewHolderClickListener, ErrorRetryView.RetryListener,
        SwipeRefreshLayout.OnRefreshListener, HasFeatured {

    companion object {
        private const val EXTRA = "vdyo.mnact.hf.extra"//sizes
    }

    override val appBarLayoutResId get() = R.layout.app_bar_featured_video
    override val isTranslucentStatus = false
    override val statusBarColorId = R.color.black

    override val contentLayoutResId get() = R.layout.content_home

    override val toolbarLogoRes get() = R.drawable.svg_vidiyo_white
    override val toolbarIndicator get() = R.drawable.svg_broadcast

    override val optionsMenuResId get() = R.menu.home

    override lateinit var featuredVideoView: FeaturedVideoView

    private lateinit var appBarLayout: AppBarLayout
    private lateinit var collapsingToolbar: CollapsingToolbarLayout

    private lateinit var stateLayout: StateLayout
    private lateinit var menuItemToggleMute: MenuItem

    private lateinit var titles: Array<TextView>
    private lateinit var buttons: Array<Button>
    private lateinit var recycles: Array<RecyclerView>

    @Inject lateinit var manager: LocalBroadcastManager
    @Inject lateinit var presenter: HomePresenter
    private lateinit var adapters: Array<DataRecyclerAdapter>

    private var receiver: BroadcastReceiver? = null
    private var sizes = intArrayOf()

    override fun onCreate(b: Bundle?) {
        super.onCreate(b)
        adapters = DataRecyclerAdapter.createAdapters(this)
    }

    override fun injectDependencies(graph: DependencyGraph) {
        graph.dataComponent.plus(HomeModule(this)).injectTo(this)
    }

    override fun onViewCreated(view: View, b: Bundle?) {
        super.onViewCreated(view, b)

        appBarLayout = view.findViewById(R.id.app_bar)
        collapsingToolbar = view.findViewById(R.id.collapsing_toolbar)
        featuredVideoView = view.findViewById(R.id.featured_video_view)
        stateLayout = view.findViewById(R.id.state_layout)

        setFeaturedVideoViewVisible(false)
        toggleFeaturedVideoVolume(featuredVideoView.isMuted)
        stateLayout.errorRetryView?.setRetryListener(this)

        titles = view.findViews(R.id.tv_watching, R.id.tv_friends_watching, R.id.tv_top_channels,
                R.id.tv_top_picks, R.id.tv_subscription, R.id.tv_channels)
        recycles = view.findViews(R.id.rv_watching, R.id.rv_friends_watching, R.id.rv_top_channels,
                R.id.rv_top_picks, R.id.rv_subscription, R.id.rv_channels)
        buttons = view.findViews(R.id.btn_watching, R.id.btn_friends_watching, R.id.btn_top_channels,
                R.id.btn_top_picks, R.id.btn_subscription, R.id.btn_channels)

        val decoration = SpaceItemDecoration(context, R.dimen.spacing_x2, true, true)
        for ((index, item) in recycles.withIndex()) {
            item.adapter = adapters[index]
            item.addItemDecoration(decoration)
            item.isNestedScrollingEnabled = false
        }

        b?.let { update(it.getIntArray(EXTRA)) }
        toggleFeaturedVideoVolume(true)

        featuredVideoView.setOnClickListener {
            val featured = presenter.repository.featured ?: return@setOnClickListener
            mainActivity?.openEpisode(featured)
        }
    }

    override fun additionAction() {
        menuItemToggleMute.setIcon(R.drawable.svg_volume_off)
    }

    override fun onPostViewCreated() {
        presenter.start()
    }

    override fun onRefresh() {
        presenter.refresh()
    }

    override fun onResume() {
        super.onResume()
        if (receiver == null) {
            manager.registerReceiver(newReceiver().also {
                receiver = it

            }, IntentFilter().playerActions())
        }
        featuredVideoView.player.playWhenReady = true
    }

    override fun onPause() {
        super.onPause()
        receiver?.let { manager.unregisterReceiver(it) }.also { receiver = null }
        featuredVideoView.player.playWhenReady = false
    }

    override fun update(sizes: IntArray) {
        this.sizes = sizes
        for ((index, size) in sizes.withIndex()) {
            adapters[index].notifyDataSetChanged()
            changeElementState(index, size)
        }
    }

    override fun update() {}

    override fun playEpisode(episode: Episode?) {
        featuredVideoView.playEpisode(episode)

        setFeaturedVideoViewVisible(episode != null)
    }

    private fun changeElementState(index: Int, size: Int) {
        val visible = size != 0
        buttons[index].changeVisibility(false, View.INVISIBLE)
        when (index) {
            DataType.TYPE_FRIENDS_WATCHING,
            DataType.TYPE_SUBSCRIPTION,
            DataType.TYPE_YOUR_CHANNELS -> {
                buttons[index].isEnabled = visible
            }
            else -> {
                titles[index].changeVisibility(visible)
//                buttons[index].changeVisibility(visible)
                recycles[index].changeVisibility(visible)
            }
        }
    }

    override fun onToolbarCreated(toolbar: Toolbar) {
        super.onToolbarCreated(toolbar)
        menuItemToggleMute = toolbar.menu.findItem(R.id.item_toggle_mute)
    }

    override fun onMenuItemClick(item: MenuItem): Boolean {
        when (item) {
            menuItemToggleMute -> toggleFeaturedVideoVolume()

            else -> return super.onMenuItemClick(item)
        }

        return true
    }

    private fun toggleFeaturedVideoVolume(isMuted: Boolean = !featuredVideoView.isMuted) {
        featuredVideoView.isMuted = isMuted
        menuItemToggleMute.setIcon(if (isMuted) R.drawable.svg_volume_off else R.drawable.svg_volume_on)
    }

    override fun onNavigationIconPressed() {
        // TODO: Called When Broadcast pressed
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putIntArray(EXTRA, sizes)
    }

    @SuppressLint("SupportAnnotationUsage")
    override fun onViewHolderClick(viewHolder: RecyclerView.ViewHolder, position: Int) {
        @DataType val type = viewHolder.itemView.tag as? Int ?: return
        val fragment: ViewFragment = when (type) {
            DataType.TYPE_WATCHING -> {
                val episode = presenter.getWatching(position) ?: return
                mainActivity?.openEpisode(episode)
                return
            }
            DataType.TYPE_TOP_CHANNELS -> {
                val topic = presenter.getTopChannel(position) ?: return
                TopicFragment.newInstance(topic)
            }
            DataType.TYPE_TOP_PICKS -> {
                val show = presenter.getTopPick(position) ?: return
                ShowFragment.newInstance(show)
            }
            DataType.TYPE_SUBSCRIPTION -> {
                val show = presenter.getSubscribedShow(position) ?: return
                ShowFragment.newInstance(show)
            }
            DataType.TYPE_YOUR_CHANNELS -> {
                val topic = presenter.getTopic(position) ?: return
                TopicFragment.newInstance(topic)
            }

            else -> return
        }
        startFragment(fragment)
    }

    override fun onConnectContactsClicked() {
        if (presenter.isAnonymous()) {
            openRegistration()

        } else {
            activity.startActivity<FindFriendsActivity>(FindFriendsActivity.EXTRA to FindFriendsActivity.CONTACTS)
        }
    }

    override fun onFindFriendsClicked() {
        if (presenter.isAnonymous()) {
            openRegistration()

        } else {
            activity.startActivity<FindFriendsActivity>(FindFriendsActivity.EXTRA to FindFriendsActivity.FACEBOOK)
        }
    }

    override fun onExploreChannelsClicked() {
        mainActivity?.selectTab(FragNavController.TAB4)
    }

    private fun setFeaturedVideoViewVisible(visible: Boolean) {
        featuredVideoView.changeVisibility(visible)

        menuItemToggleMute.isVisible = visible
    }

    override fun retry() {
        presenter.refresh()
    }

    override fun onFriendClicked(position: Int) {
        val user = presenter.getFriend(position) ?: return
        startFragment(ProfileFragment.newInstance(user.id), true)
    }

    override fun onShowClicked(position: Int) {
        val show = presenter.getFriendShow(position) ?: return
        startFragment(ShowFragment.newInstance(show), true)
    }

    //region StatePresentationView
    override fun showProgress() {
        stateLayout.changeState(StateLayout.State.PROGRESS)
    }

    override fun showContent() {
        stateLayout.changeState(StateLayout.State.CONTENT)
    }

    override fun showError(message: String?, asToast: Boolean) {
        stateLayout.errorRetryView?.setMessage(message)
        stateLayout.changeState(StateLayout.State.ERROR)
    }

    override fun showError(resId: Int) {
        stateLayout.errorRetryView?.setMessage(resId)
        stateLayout.changeState(StateLayout.State.ERROR)
    }

    override fun showNoContent() {
        stateLayout.changeState(StateLayout.State.EMPTY)
    }
    //endregion StatePresentationView

    override fun onDestroyView() {
        super.onDestroyView()
        featuredVideoView.player.release()
    }
}