package tv.vidiyo.android.ui.view.holder

import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.load.resource.drawable.GlideDrawable
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import tv.vidiyo.android.R
import tv.vidiyo.android.base.view.holder.ViewHolder
import tv.vidiyo.android.domain.contract.SearchResultView
import tv.vidiyo.android.extension.DECIMAL_FORMAT
import tv.vidiyo.android.extension.load
import java.lang.Exception
import java.lang.ref.WeakReference

abstract class DataViewHolder<L : ViewHolder.OnViewHolderClickListener>(
        parent: ViewGroup,
        listener: WeakReference<L>?,
        layoutResId: Int,
        clickable: Boolean = true
) : ViewHolder<L>(parent, listener, layoutResId, clickable), SearchResultView {

    val ivImage: ImageView = itemView.findViewById(R.id.iv_image)
    val tvTitle: TextView = itemView.findViewById(R.id.tv_title)
    val tvSubtitle: TextView = itemView.findViewById(R.id.tv_subtitle)
    val tvSubscribers: TextView = itemView.findViewById(R.id.tv_subscribers)

    override fun <T> setImage(image: T?, fallbackResId: Int?) {
        if (fallbackResId != null) {
            ivImage.setImageResource(fallbackResId)
            ivImage.load(image, requestListener = object : RequestListener<T?, GlideDrawable> {
                override fun onResourceReady(resource: GlideDrawable?, model: T?, target: Target<GlideDrawable>?, isFromMemoryCache: Boolean, isFirstResource: Boolean): Boolean {
                    ivImage.setImageDrawable(resource)
                    return true
                }

                override fun onException(e: Exception?, model: T?, target: Target<GlideDrawable>?, isFirstResource: Boolean): Boolean {
                    ivImage.setImageResource(fallbackResId)
                    return true
                }
            })
        } else {
            ivImage.load(image)
        }
    }

    override fun setTitle(title: String?) {
        tvTitle.text = title
    }

    override fun setSubtitle(subtitle: String?) {
        tvSubtitle.text = subtitle
    }

    override fun setSubscribersCount(count: Int?) {
        tvSubscribers.text = context.getString(R.string.format_s_subscribers, DECIMAL_FORMAT.format((count ?: 0).toDouble()))
    }
}