package tv.vidiyo.android.ui.view.holder

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.user.SubscriptionStatus
import tv.vidiyo.android.base.view.holder.ViewHolder
import tv.vidiyo.android.extension.assignTo
import tv.vidiyo.android.extension.changeVisibility
import java.lang.ref.WeakReference

interface OnFollowerActionClickListener : ViewHolder.OnViewHolderClickListener {
    fun onAcceptClick(viewHolder: RecyclerView.ViewHolder, position: Int)
    fun onDeleteClick(viewHolder: RecyclerView.ViewHolder, position: Int)
}

class FollowerActionViewHolder(
        parent: ViewGroup,
        listener: WeakReference<OnFollowerActionClickListener>?,
        actionRes: Int = R.string.accept
) : UserViewHolder<OnFollowerActionClickListener>(parent, listener, R.layout.view_item_follower_action, false), View.OnClickListener {

    private val btnAccept = itemView.findViewById<Button>(R.id.btn_accept)
    private val btnDelete = itemView.findViewById<TextView>(R.id.btn_delete)

    init {
        btnAccept.setText(actionRes)
        assignTo(itemView, btnAccept, btnDelete)
    }

    fun setStatus(status: SubscriptionStatus, follow: Boolean = false) {
        when (status) {
            SubscriptionStatus.accepted -> {
                btnDelete.setText(if (follow) R.string.following else R.string.accepted)
                btnDelete.isEnabled = false
            }

            SubscriptionStatus.pending -> {
                btnDelete.setText(R.string.requested)
                btnDelete.isEnabled = false
            }

            else -> {
                btnDelete.setText(R.string.delete)
                btnDelete.isEnabled = true
            }
        }

        btnAccept.changeVisibility(btnDelete.isEnabled)
    }

    override fun onClick(v: View) {
        val l = listener?.get() ?: return
        when (v.id) {
            R.id.btn_accept -> l.onAcceptClick(this, adapterPosition)
            R.id.btn_delete -> l.onDeleteClick(this, adapterPosition)
            else -> l.onViewHolderClick(this, adapterPosition)
        }
    }
}