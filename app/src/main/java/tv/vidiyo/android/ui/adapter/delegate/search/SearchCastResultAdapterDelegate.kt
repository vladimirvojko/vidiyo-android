package tv.vidiyo.android.ui.adapter.delegate.search

import android.view.ViewGroup
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.cast.Cast
import tv.vidiyo.android.base.adapter.delegate.ListProviderAdapterDelegate
import tv.vidiyo.android.base.adapter.provider.ListDataProvider
import tv.vidiyo.android.base.view.holder.ViewHolder
import tv.vidiyo.android.domain.contract.setCast
import tv.vidiyo.android.ui.view.holder.SearchResultViewHolder

class SearchCastResultAdapterDelegate(
        provider: ListDataProvider<Cast>,
        listener: ViewHolder.OnViewHolderClickListener?
) : ListProviderAdapterDelegate<Cast, ViewHolder.OnViewHolderClickListener, SearchResultViewHolder>(provider, listener) {

    override fun onCreateViewHolder(parent: ViewGroup) = SearchResultViewHolder(parent, listener, R.layout.view_item_searching_cast)
    override fun onBindViewHolder(position: Int, holder: SearchResultViewHolder, provider: ListDataProvider<Cast>, item: Cast)
            = holder.setCast(item, holder.context)
}