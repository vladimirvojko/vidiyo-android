package tv.vidiyo.android.ui.adapter.delegate

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.episode.ViewInfo
import tv.vidiyo.android.api.model.show.NewShowEpisode
import tv.vidiyo.android.base.adapter.delegate.AdapterDelegate
import tv.vidiyo.android.base.adapter.delegate.ListProviderAdapterDelegate
import tv.vidiyo.android.base.adapter.provider.ListDataProvider
import tv.vidiyo.android.ui.view.holder.OnWatchingClickListener
import tv.vidiyo.android.ui.view.holder.WatchingViewHolder
import tv.vidiyo.android.ui.view.holder.empty.WatchingEmptyViewHolder
import tv.vidiyo.android.ui.view.holder.setNewShowEpisode
import tv.vidiyo.android.ui.view.holder.setViewInfo

class WatchingAdapterDelegate(
        provider: ListDataProvider<ViewInfo>,
        listener: OnWatchingClickListener?
) : ListProviderAdapterDelegate<ViewInfo, OnWatchingClickListener, WatchingViewHolder>(provider, listener) {

    override fun onCreateViewHolder(parent: ViewGroup) = WatchingViewHolder(parent, listener, false)
    override fun onBindViewHolder(position: Int, holder: WatchingViewHolder, provider: ListDataProvider<ViewInfo>, item: ViewInfo)
            = holder.setViewInfo(item)
}

class WatchingNewAdapterDelegate(
        provider: ListDataProvider<NewShowEpisode>,
        listener: OnWatchingClickListener?
) : ListProviderAdapterDelegate<NewShowEpisode, OnWatchingClickListener, WatchingViewHolder>(provider, listener) {

    override fun onCreateViewHolder(parent: ViewGroup) = WatchingViewHolder(parent, listener, true)
    override fun onBindViewHolder(position: Int, holder: WatchingViewHolder, provider: ListDataProvider<NewShowEpisode>, item: NewShowEpisode)
            = holder.setNewShowEpisode(item)
}

class EmptyWatchingAdapterDelegate(
        private val provider: ListDataProvider<*>,
        private val forNewEpisode: Boolean
) : AdapterDelegate {

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder = WatchingEmptyViewHolder(parent, null)
    override fun onBindViewHolder(position: Int, holder: RecyclerView.ViewHolder) {
        if (holder is WatchingEmptyViewHolder) {
            if (forNewEpisode) {
                holder.tvTitle.setText(R.string.watching_empty_new_episodes_title)
                holder.tvMessage.setText(R.string.watching_empty_new_episodes_message)

            } else {
                holder.tvTitle.setText(R.string.watching_empty_resume_title)
                holder.tvMessage.setText(R.string.watching_empty_resume_message)
            }
        }
    }

    override fun isForViewType(position: Int) = provider.getItemCount() == 0
}