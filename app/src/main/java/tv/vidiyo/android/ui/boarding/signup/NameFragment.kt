package tv.vidiyo.android.ui.boarding.signup

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.RequestManager
import tv.vidiyo.android.DependencyGraph
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.social.SocialAuth
import tv.vidiyo.android.api.model.user.Profile
import tv.vidiyo.android.base.fragment.BaseFragment
import tv.vidiyo.android.di.subcomponent.boarding.BoardingModule
import tv.vidiyo.android.domain.contract.SignUpContract
import tv.vidiyo.android.extension.assignTo
import tv.vidiyo.android.os.AsyncTaskListener
import tv.vidiyo.android.os.CropAndSave
import tv.vidiyo.android.ui.boarding.CreateUsernameAndPasswordFragment
import tv.vidiyo.android.ui.dialog.ChangePhotoSheetDialog
import tv.vidiyo.android.ui.view.VEditText
import tv.vidiyo.android.util.Constant
import javax.inject.Inject


class NameFragment : BaseFragment(), SignUpContract.Name.View, View.OnClickListener, AsyncTaskListener<String> {

    companion object {
        private const val EXTRA_0 = "vdyo.brdfrg.nf.EXTRA_0"//profile
        private const val EXTRA_1 = "vdyo.brdfrg.nf.EXTRA_1"//social

        fun newInstance(profile: Profile): NameFragment {
            val fragment = NameFragment()
            val b = Bundle()
            b.putParcelable(EXTRA_0, profile)
            fragment.arguments = b
            return fragment
        }

        fun newInstance(social: SocialAuth): NameFragment {
            val fragment = NameFragment()
            val b = Bundle()
            b.putParcelable(EXTRA_1, social)
            fragment.arguments = b
            return fragment
        }
    }

    override val layoutResId = R.layout.fragment_sign_up_name
    override val toolbarTitleRes = R.string.boarding_profile_info

    lateinit var ivPhoto: ImageView
    lateinit var etName: VEditText

    @Inject
    lateinit var glide: RequestManager

    lateinit var profile: Profile
    var social: SocialAuth? = null

    override fun injectDependencies(graph: DependencyGraph) {
        graph.applicationComponent.plus(BoardingModule(this)).injectTo(this)
    }

    override fun onCreate(b: Bundle?) {
        super.onCreate(b)
        val bundle = b ?: arguments
        profile = bundle.getParcelable(EXTRA_0) ?: Profile()
        social = bundle.getParcelable(EXTRA_1)
    }

    override fun onViewCreated(view: View, b: Bundle?) {
        super.onViewCreated(view, b)
        ivPhoto = view.findViewById(R.id.iv_photo)
        etName = view.findViewById(R.id.et_fullname)

        social?.let {
            etName.setText(it.name)
            glide.load(it.imageUrl).into(ivPhoto.apply {
                scaleType = ImageView.ScaleType.CENTER_CROP
            })
            profile.image = it.imageUrl
        }

        assignTo(ivPhoto, view.findViewById(R.id.button))
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putParcelable(EXTRA_0, profile)
        outState?.putParcelable(EXTRA_1, social)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when {
            requestCode == ChangePhotoSheetDialog.REQUEST_CODE && resultCode == Activity.RESULT_OK -> {
                data ?: return

                setRefreshing(true)
                (data.extras?.get(Constant.DATA) as? Bitmap)?.let {
                    executeAsync(it)
                    return
                }

                val bitmap = MediaStore.Images.Media.getBitmap(context.contentResolver, data.data)
                executeAsync(bitmap)
            }

            else -> super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun executeAsync(bitmap: Bitmap) {
        CropAndSave(context, this, bitmap, 512).execute()
    }

    override fun onTaskComplete(result: String?) {
        setRefreshing(false)
        glide.load(result).into(ivPhoto.apply {
            scaleType = ImageView.ScaleType.CENTER_CROP
        })
        profile.apply {
            imageFile = result
            image = null
        }
    }

    override fun startUsernamePasswordEnter() {
        startFragment(CreateUsernameAndPasswordFragment.newInstance(profile, social), true)
    }

    fun loadFromSocial(social: SocialAuth) {
        glide.load(social.imageUrl).into(ivPhoto)
        profile.imageFile = null
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.iv_photo -> ChangePhotoSheetDialog.newInstance(social).show(childFragmentManager)
            R.id.button -> startUsernamePasswordEnter()
        }
    }
}