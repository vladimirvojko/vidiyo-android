package tv.vidiyo.android.ui.adapter

import android.support.v7.widget.RecyclerView
import tv.vidiyo.android.api.Dateable
import tv.vidiyo.android.api.model.City
import tv.vidiyo.android.api.model.Country
import tv.vidiyo.android.api.model.cast.Cast
import tv.vidiyo.android.api.model.channel.Channel
import tv.vidiyo.android.api.model.episode.Episode
import tv.vidiyo.android.api.model.episode.Product
import tv.vidiyo.android.api.model.episode.ViewInfo
import tv.vidiyo.android.api.model.show.FriendShow
import tv.vidiyo.android.api.model.show.NewShowEpisode
import tv.vidiyo.android.api.model.show.Show
import tv.vidiyo.android.api.model.social.SocialAccount
import tv.vidiyo.android.api.model.social.SocialConnection
import tv.vidiyo.android.api.model.topic.Topic
import tv.vidiyo.android.api.model.user.FollowingUser
import tv.vidiyo.android.api.model.user.User
import tv.vidiyo.android.base.adapter.RecyclerAdapter
import tv.vidiyo.android.base.adapter.delegate.AdapterDelegate
import tv.vidiyo.android.base.adapter.provider.ListDataProvider
import tv.vidiyo.android.base.view.holder.ViewHolder
import tv.vidiyo.android.extension.toInt
import tv.vidiyo.android.source.DataType
import tv.vidiyo.android.ui.adapter.delegate.*
import tv.vidiyo.android.ui.adapter.delegate.channel.SubscribableShowAdapterDelegate
import tv.vidiyo.android.ui.adapter.delegate.discover.DiscoverConnectionAdapterDelegate
import tv.vidiyo.android.ui.adapter.delegate.discover.DiscoveredHeaderAdapterDelegate
import tv.vidiyo.android.ui.adapter.delegate.discover.SubscribableUserAdapterDelegate
import tv.vidiyo.android.ui.adapter.delegate.discover.SuggestedUserAdapterDelegate
import tv.vidiyo.android.ui.adapter.delegate.profile.EpisodeAdapterDelegate
import tv.vidiyo.android.ui.adapter.delegate.settings.CityAdapterDelegate
import tv.vidiyo.android.ui.adapter.delegate.settings.ConnectionButtonAdapterDelegate
import tv.vidiyo.android.ui.adapter.delegate.settings.FollowerActionAdapterDelegate
import tv.vidiyo.android.ui.adapter.delegate.socialfeed.SocialAdapterDelegate
import tv.vidiyo.android.ui.home.HomeFragment
import tv.vidiyo.android.ui.search.SearchFragment
import tv.vidiyo.android.ui.view.holder.*
import tv.vidiyo.android.util.SubscriptionManager

@Suppress("MemberVisibilityCanPrivate")
class DataRecyclerAdapter private constructor(
        val provider: ListDataProvider<*>,
        delegates: List<AdapterDelegate>,
        private val showEmptyView: Boolean = true
) : RecyclerAdapter(delegates) {

    companion object {
        fun watching(provider: ListDataProvider<ViewInfo>, listener: ViewHolder.OnViewHolderClickListener): DataRecyclerAdapter {
            return DataRecyclerAdapter(provider, listOf(WatchingEpisodeSmallAdapterDelegate(provider, listener)))
        }

        fun topPicks(provider: ListDataProvider<Show>, listener: ViewHolder.OnViewHolderClickListener): DataRecyclerAdapter {
            return DataRecyclerAdapter(provider, listOf(ShowAdapterDelegate(provider, listener, DataType.TYPE_TOP_PICKS)))
        }

        fun <L> subscription(provider: ListDataProvider<Show>, listener: L): DataRecyclerAdapter {
            return DataRecyclerAdapter(provider, mutableListOf<AdapterDelegate>().apply {
                if (listener is HomeEmptyClickListener) {
                    this.add(HomeEmptyAdapterDelegate(DataType.TYPE_SUBSCRIPTION, provider, listener))
                }
                if (listener is ViewHolder.OnViewHolderClickListener) {
                    this.add(ShowAdapterDelegate(provider, listener, DataType.TYPE_SUBSCRIPTION))
                }
            })
        }

        fun <L> friendsWatching(provider: ListDataProvider<FriendShow>, listener: L): DataRecyclerAdapter {
            return DataRecyclerAdapter(provider, mutableListOf<AdapterDelegate>().apply {
                if (listener is HomeEmptyClickListener) {
                    this.add(HomeEmptyAdapterDelegate(DataType.TYPE_FRIENDS_WATCHING, provider, listener))
                }
                if (listener is OnFriendShowViewHolderClickListener) {
                    this.add(FriendShowAdapterDelegate(provider, listener))
                }
            })
        }

        fun <L> yourChannels(provider: ListDataProvider<Topic>, listener: L): DataRecyclerAdapter {
            return DataRecyclerAdapter(provider, mutableListOf<AdapterDelegate>().apply {
                if (listener is HomeEmptyClickListener) {
                    this.add(HomeEmptyAdapterDelegate(DataType.TYPE_YOUR_CHANNELS, provider, listener))
                }
                if (listener is ViewHolder.OnViewHolderClickListener) {
                    this.add(TopicAdapterDelegate(provider, listener, DataType.TYPE_YOUR_CHANNELS))
                }
            })
        }

        fun topChannels(provider: ListDataProvider<Topic>, listener: ViewHolder.OnViewHolderClickListener): DataRecyclerAdapter {
            return DataRecyclerAdapter(provider, listOf(
                    TopicAdapterDelegate(provider, listener, DataType.TYPE_TOP_CHANNELS)
            ))
        }

        fun smallCasts(provider: ListDataProvider<Cast>, listener: ViewHolder.OnViewHolderClickListener): DataRecyclerAdapter {
            return DataRecyclerAdapter(provider, listOf(
                    CastSmallAdapterDelegate(provider, listener)
            ))
        }

        fun resume(provider: ListDataProvider<ViewInfo>, listener: OnWatchingClickListener): DataRecyclerAdapter {
            return DataRecyclerAdapter(provider, listOf(
                    EmptyWatchingAdapterDelegate(provider, false),
                    WatchingAdapterDelegate(provider, listener)
            ))
        }

        fun newEpisodes(provider: ListDataProvider<NewShowEpisode>, listener: OnWatchingClickListener): DataRecyclerAdapter {
            return DataRecyclerAdapter(provider, listOf(
                    EmptyWatchingAdapterDelegate(provider, true),
                    WatchingNewAdapterDelegate(provider, listener)
            ))
        }

        fun episodes(provider: ListDataProvider<Episode>, listener: ViewHolder.OnViewHolderClickListener? = null): DataRecyclerAdapter {
            return DataRecyclerAdapter(provider, listOf(
                    EpisodeAdapterDelegate(provider, listener)
            ))
        }

        fun topics(provider: ListDataProvider<Topic>, channel: Channel? = null, listener: OnSubscribableViewHolderClickListener? = null): DataRecyclerAdapter {
            return DataRecyclerAdapter(provider, listOf(
                    SubscribableTopicDelegate(provider, listener, channel)
            ))
        }

        fun cast(provider: ListDataProvider<Cast>, listener: OnSubscribableViewHolderClickListener? = null): DataRecyclerAdapter {
            return DataRecyclerAdapter(provider, listOf(
                    CastAdapterDelegate(provider, listener)
            ))
        }

        fun subscribers(provider: ListDataProvider<FollowingUser>, listener: OnSubscribableViewHolderClickListener? = null,
                        manager: SubscriptionManager): DataRecyclerAdapter {
            return DataRecyclerAdapter(provider, listOf(
                    SubscriberAdapterDelegate(provider, listener, manager)
            ))
        }

        fun subscribableShow(provider: ListDataProvider<Show>, channel: Channel? = null, listener: OnSubscribableViewHolderClickListener? = null)
                = DataRecyclerAdapter(provider, listOf(
                SubscribableShowAdapterDelegate(channel, provider, listener)
        ))

        fun connection(provider: ListDataProvider<SocialConnection>, listener: ViewHolder.OnViewHolderClickListener?)
                = DataRecyclerAdapter(provider, listOf(
                ConnectionAdapterDelegate(provider, listener)
        ))

        fun products(provider: ListDataProvider<Product>, listener: ViewHolder.OnViewHolderClickListener?, withHeader: Boolean)
                = DataRecyclerAdapter(provider, mutableListOf<AdapterDelegate>().apply {
            if (withHeader) {
                add(ProductsHeaderAdapterDelegate(provider, listener))
            }
            add(ProductsAdapterDelegate(provider, listener, !withHeader))
        }).also { it.hasHeaderView = withHeader.toInt() }

        fun cities(provider: ListDataProvider<City>, listener: ViewHolder.OnViewHolderClickListener?)
                = DataRecyclerAdapter(provider, listOf(
                CityAdapterDelegate(provider, listener)
        ))

        fun users(provider: ListDataProvider<User>, listener: ViewHolder.OnViewHolderClickListener?, actionable: Boolean = false, backprop: Boolean = false)
                = DataRecyclerAdapter(provider, listOf(
                if (actionable) {
                    FollowerActionAdapterDelegate(provider, listener as? OnFollowerActionClickListener, backprop)
                } else {
                    UserViewAdapterDelegate(provider, listener)
                }
        ))

        fun social(provider: ListDataProvider<SocialAccount>, listener: OnConnectionButtonClickListener?)
                = DataRecyclerAdapter(provider, listOf(
                ConnectionButtonAdapterDelegate(provider, listener)
        ))

        fun discover(provider: ListDataProvider<Any>, listener: OnFollowerActionClickListener?)
                = DataRecyclerAdapter(provider, listOf(
                DiscoverConnectionAdapterDelegate(provider, listener),
                SuggestedUserAdapterDelegate(provider, listener)
        ), false)

        fun discovered(provider: ListDataProvider<FollowingUser>, listener: OnSubscribableViewHolderClickListener?, manager: SubscriptionManager)
                = DataRecyclerAdapter(provider, listOf(
                DiscoveredHeaderAdapterDelegate(provider, listener),
                SubscribableUserAdapterDelegate(provider, listener, manager)
        ), false)

        fun socialFeed(provider: ListDataProvider<Dateable>, listener: ViewHolder.OnViewHolderClickListener?)
                = DataRecyclerAdapter(provider, listOf(
                SocialAdapterDelegate(provider, listener)
        ), false)

        fun countries(provider: ListDataProvider<Country>, listener: ViewHolder.OnViewHolderClickListener?, selected: Country)
                = DataRecyclerAdapter(provider, listOf(
                CountryAdapterDelegate(provider, listener, selected)
        ), false)

        fun createAdapters(fragment: HomeFragment, types: IntArray = DataType.HOME_VALUES) = Array(types.size, {
            when (types[it]) {
                DataType.TYPE_WATCHING -> watching(fragment.presenter.repository.watchingProvider, fragment)
                DataType.TYPE_TOP_PICKS -> topPicks(fragment.presenter.repository.showsChannelsProvider, fragment)
                DataType.TYPE_SUBSCRIPTION -> subscription(fragment.presenter.repository.showsSubscribedProvider, fragment)
                DataType.TYPE_FRIENDS_WATCHING -> friendsWatching(fragment.presenter.repository.showsFriendsProvider, fragment)
                DataType.TYPE_YOUR_CHANNELS -> yourChannels(fragment.presenter.repository.topicsProvider, fragment)
                DataType.TYPE_TOP_CHANNELS -> topChannels(fragment.presenter.repository.topicsTopProvider, fragment)
                else -> throw IllegalArgumentException()
            }
        })

        fun createAdapters(fragment: SearchFragment, types: IntArray = DataType.SEARCH_VALUES) = Array(types.size, {
            when (types[it]) {
                DataType.TYPE_CASTS_SMALL -> smallCasts(fragment.presenter.repository.castsProvider, fragment)
                DataType.TYPE_FRIENDS_WATCHING -> friendsWatching(fragment.presenter.repository.showsFriendsProvider, fragment)
                DataType.TYPE_TOP_PICKS -> topPicks(fragment.presenter.repository.topPicksProvider, fragment)
                DataType.TYPE_TOP_CHANNELS -> topChannels(fragment.presenter.repository.trendingChannelsProvider, fragment)
                else -> throw IllegalArgumentException()
            }
        })
    }

    private val hasEmptyView = showEmptyView && delegates.size > 1
    private var hasHeaderView = 0

    override fun getViewTypeForItem(position: Int) = delegatesManager.getItemViewType(position - hasHeaderView)
    override fun onBindItemViewHolder(holder: RecyclerView.ViewHolder, position: Int)
            = delegatesManager.onBindViewHolder(holder, position - hasHeaderView)

    override fun getTotalItemCount(): Int {
        val size = provider.getItemCount()
        return if (hasEmptyView && size == 0) 1 else size + hasHeaderView
    }
}