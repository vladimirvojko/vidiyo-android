package tv.vidiyo.android.ui.adapter.delegate.discover

import android.view.ViewGroup
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.user.SuggestedUser
import tv.vidiyo.android.base.adapter.delegate.ListProviderAdapterDelegate
import tv.vidiyo.android.base.adapter.provider.ListDataProvider
import tv.vidiyo.android.domain.contract.setUser
import tv.vidiyo.android.ui.view.holder.FollowerActionViewHolder
import tv.vidiyo.android.ui.view.holder.OnFollowerActionClickListener

class SuggestedUserAdapterDelegate(
        provider: ListDataProvider<Any>,
        listener: OnFollowerActionClickListener?
) : ListProviderAdapterDelegate<Any, OnFollowerActionClickListener, FollowerActionViewHolder>(provider, listener) {

    override fun onCreateViewHolder(parent: ViewGroup) = FollowerActionViewHolder(parent, listener, R.string.follow)
    override fun onBindViewHolder(position: Int, holder: FollowerActionViewHolder, provider: ListDataProvider<Any>, item: Any) {
        if (item is SuggestedUser) {
            holder.setUser(item)
            holder.setStatus(item.status, true)
        }
    }

    override fun isForViewType(position: Int, provider: ListDataProvider<Any>) = position > 1
}