package tv.vidiyo.android.ui.adapter.delegate.discover

import android.view.ViewGroup
import tv.vidiyo.android.api.model.ConnectionCount
import tv.vidiyo.android.base.adapter.delegate.ListProviderAdapterDelegate
import tv.vidiyo.android.base.adapter.provider.ListDataProvider
import tv.vidiyo.android.base.view.holder.ViewHolder
import tv.vidiyo.android.ui.view.holder.ConnectViewHolder

class DiscoverConnectionAdapterDelegate(
        provider: ListDataProvider<Any>,
        listener: ViewHolder.OnViewHolderClickListener?
) : ListProviderAdapterDelegate<Any, ViewHolder.OnViewHolderClickListener, ConnectViewHolder>(provider, listener) {

    override fun onCreateViewHolder(parent: ViewGroup) = ConnectViewHolder(parent, listener)
    override fun onBindViewHolder(position: Int, holder: ConnectViewHolder, provider: ListDataProvider<Any>, item: Any) {
        holder.set(item as? ConnectionCount ?: return, position)
    }

    override fun isForViewType(position: Int, provider: ListDataProvider<Any>) = position < 2
}