package tv.vidiyo.android.ui.adapter.delegate.channel

import android.view.ViewGroup
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.channel.Channel
import tv.vidiyo.android.api.model.show.Show
import tv.vidiyo.android.api.model.topic.Topic
import tv.vidiyo.android.base.adapter.delegate.SectionedAdapterDelegate
import tv.vidiyo.android.base.adapter.provider.SectionedListDataProvider
import tv.vidiyo.android.base.adapter.section.ItemPosition
import tv.vidiyo.android.ui.view.ShapeImageView
import tv.vidiyo.android.ui.view.holder.OnSubscribableViewHolderClickListener
import tv.vidiyo.android.ui.view.holder.SubscribableViewHolder
import tv.vidiyo.android.ui.view.holder.setTopic

class SectionedTopicHeaderAdapterDelegate(
        private val channel: Channel,
        provider: SectionedListDataProvider<Topic, Show>,
        listener: OnSubscribableViewHolderClickListener?
) : SectionedAdapterDelegate<Topic, Show, SectionedListDataProvider<Topic, Show>,
        OnSubscribableViewHolderClickListener, SubscribableViewHolder>(provider, listener) {

    override fun onCreateViewHolder(parent: ViewGroup): SubscribableViewHolder {
        return SubscribableViewHolder(parent, listener, R.layout.view_item_subscribable_topic).apply {
            (ivImage as? ShapeImageView)?.setShape(2, 0f)
        }
    }

    override fun onBindViewHolder(position: ItemPosition, holder: SubscribableViewHolder, provider: SectionedListDataProvider<Topic, Show>) {
        val topic = provider.getSection(position.section)?.section ?: return
        holder.setTopic(topic, channel)
    }

    override fun isForViewType(itemPosition: ItemPosition) = itemPosition.isHeader
}