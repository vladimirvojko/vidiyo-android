package tv.vidiyo.android.ui.episode

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import tv.vidiyo.android.App
import tv.vidiyo.android.DependencyGraph
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.comment.Comment
import tv.vidiyo.android.api.model.episode.Episode
import tv.vidiyo.android.api.model.user.FollowingUser
import tv.vidiyo.android.base.adapter.RecyclerAdapter
import tv.vidiyo.android.base.adapter.SectionedRecyclerAdapter
import tv.vidiyo.android.base.adapter.pages.PageFragment
import tv.vidiyo.android.base.fragment.StateRecyclerFragment
import tv.vidiyo.android.base.fragment.ViewFragment
import tv.vidiyo.android.di.subcomponent.episode.ChildEpisodeModule
import tv.vidiyo.android.domain.contract.EpisodeContract
import tv.vidiyo.android.domain.presenter.episode.EpisodeInnerPresenter
import tv.vidiyo.android.extension.setEmptyViewWithFormat
import tv.vidiyo.android.extension.weak
import tv.vidiyo.android.model.InnerPageType
import tv.vidiyo.android.ui.adapter.DataRecyclerAdapter
import tv.vidiyo.android.ui.adapter.delegate.comment.CommentAdapterDelegate
import tv.vidiyo.android.ui.adapter.delegate.comment.CommentFooterAdapterDelegate
import tv.vidiyo.android.ui.adapter.delegate.comment.CommentHeaderAdapterDelegate
import tv.vidiyo.android.ui.adapter.delegate.comment.WriteTextAdapterDelegate
import tv.vidiyo.android.ui.cast.CastFragment
import tv.vidiyo.android.ui.dialog.WriteTextSheetDialog
import tv.vidiyo.android.ui.profile.ProfileFragment
import tv.vidiyo.android.ui.topic.TopicFragment
import tv.vidiyo.android.ui.view.DividerItemDecoration
import tv.vidiyo.android.ui.view.SpaceItemDecoration
import tv.vidiyo.android.ui.view.holder.OnCommentClickListener
import tv.vidiyo.android.ui.view.holder.OnSubscribableViewHolderClickListener
import tv.vidiyo.android.ui.widget.InfiniteScrollRecyclerView
import tv.vidiyo.android.ui.widget.OnInfiniteScrollListener
import tv.vidiyo.android.util.ALog
import javax.inject.Inject

class InnerEpisodeInfoFragment : StateRecyclerFragment<RecyclerAdapter>(), EpisodeContract.Inner.View,
        OnSubscribableViewHolderClickListener {

    companion object {
        private const val EXTRA_0 = "EXTRA_0"//type
        private const val EXTRA_1 = "EXTRA_1"//id

        private fun newInstance(type: Int, id: Int) = InnerEpisodeInfoFragment().apply {
            arguments = Bundle(1).also {
                it.putInt(EXTRA_0, type)
                it.putInt(EXTRA_1, id)
            }
        }

        fun episodesCreator(id: Int) = object : PageFragment.Creator {
            override fun newInstance(): Fragment = InnerEpisodeInfoFragment.newInstance(InnerPageType.EPISODES, id)
        }

        fun commentsCreator(id: Int) = object : PageFragment.Creator {
            override fun newInstance(): Fragment = InnerEpisodeInfoFragment.newInstance(InnerPageType.COMMENTS, id)
        }

        fun castsCreator(id: Int) = object : PageFragment.Creator {
            override fun newInstance(): Fragment = InnerEpisodeInfoFragment.newInstance(InnerPageType.CAST, id)
        }

        fun topicsCreator(id: Int) = object : PageFragment.Creator {
            override fun newInstance(): Fragment = InnerEpisodeInfoFragment.newInstance(InnerPageType.TOPICS, id)
        }

        fun subscribersCreator(id: Int) = object : PageFragment.Creator {
            override fun newInstance(): Fragment = InnerEpisodeInfoFragment.newInstance(InnerPageType.SUBSCRIBERS, id)
        }
    }

    override val layoutResId = R.layout.fragment_state_recycler_empty

    override val type: Int get() = arguments.getInt(EXTRA_0, InnerPageType.EPISODES)
    private val episodeId: Int get() = arguments.getInt(EXTRA_1, 0)

    @Inject
    lateinit var presenter: EpisodeInnerPresenter
    private var listener: OnCommentClickListener? = null

    private val parent: EpisodeInfoFragment? get() = parentFragment as? EpisodeInfoFragment
    private val isRefreshing: Boolean get() = parent?.isRefreshing == true

    override fun injectDependencies(graph: DependencyGraph) {
        graph.getEpisodeComponent(episodeId).plus(ChildEpisodeModule(this)).injectTo(this)
    }

    override fun onViewCreated(view: View, b: Bundle?) {
        super.onViewCreated(view, b)
        view.setBackgroundColor(ContextCompat.getColor(context, R.color.lighter_gray))
        val title = getString(when (type) {
            InnerPageType.EPISODES -> R.string.episodes
            InnerPageType.CAST -> R.string.actors
            InnerPageType.TOPICS -> R.string.topics
            InnerPageType.SUBSCRIBERS -> R.string.subscribers
            InnerPageType.COMMENTS -> R.string.comments
            else -> throw IllegalArgumentException("Wrong type provided")
        }).toLowerCase()
        val drawableRes = when (type) {
            InnerPageType.EPISODES -> R.drawable.svg_episodes
            InnerPageType.CAST -> R.drawable.svg_cast
            InnerPageType.TOPICS -> R.drawable.svg_topic
            InnerPageType.SUBSCRIBERS -> R.drawable.svg_subscription
            InnerPageType.COMMENTS -> R.drawable.svg_topic
            else -> throw IllegalArgumentException("Wrong type provided")
        }

        stateLayout.setEmptyViewWithFormat(title, drawableRes)
    }

    override fun onPostViewCreated() {
        presenter.start()
    }

    override fun onSubscribeClick(viewHolder: RecyclerView.ViewHolder, position: Int) {
        if (!isRefreshing) presenter.toggleSubscription(position)
    }

    override fun onViewHolderClick(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val f: ViewFragment = when (type) {
            InnerPageType.EPISODES -> {
                presenter.changeCurrentEpisodeTo(presenter.get<Episode>(position)?.id ?: return)
                return
            }
            InnerPageType.CAST -> CastFragment.newInstance(presenter.get(position) ?: return)
            InnerPageType.TOPICS -> TopicFragment.newInstance(presenter.get(position) ?: return)
            InnerPageType.SUBSCRIBERS -> ProfileFragment.newInstance(presenter.get<FollowingUser>(position)?.id ?: return)

            else -> return
        }
        startFragment(f)
    }

    override fun onCreateLayoutManager(context: Context?) = when (type) {
        InnerPageType.CAST -> GridLayoutManager(context, 2)
        else -> LinearLayoutManager(context)
    }

    override fun setRefreshing(block: Boolean) {
        parent?.setRefreshing(block)
    }

    override fun onRecyclerViewCreated(recyclerView: InfiniteScrollRecyclerView) {
        super.onRecyclerViewCreated(recyclerView)
        recyclerView.addItemDecoration(SpaceItemDecoration(context, true, type != InnerPageType.COMMENTS))
        recyclerView.addItemDecoration(DividerItemDecoration(context, LinearLayout.VERTICAL))
        if (type == InnerPageType.COMMENTS) {
            recyclerView.listener = scrollListener()
        }
    }

    override fun onCreateAdapter(): RecyclerAdapter = when (type) {
        InnerPageType.EPISODES -> DataRecyclerAdapter.episodes(presenter.getProvider(), this)
        InnerPageType.COMMENTS -> {
            listener = createCommentListener()
            val account = App.graph.applicationComponent.provideUserAccount()
            presenter.commentsProvider.let {
                SectionedRecyclerAdapter(listOf(
                        WriteTextAdapterDelegate(it, listener, account),
                        CommentHeaderAdapterDelegate(it, listener),
                        CommentAdapterDelegate(it, listener),
                        CommentFooterAdapterDelegate(it, listener)
                ), it)
            }
        }
        InnerPageType.CAST -> DataRecyclerAdapter.cast(presenter.getProvider(), this)
        InnerPageType.TOPICS -> DataRecyclerAdapter.topics(presenter.getProvider(), null, this)
        InnerPageType.SUBSCRIBERS -> DataRecyclerAdapter.subscribers(presenter.getProvider(), this, presenter.manager)
        else -> throw IllegalArgumentException("Wrong type provided")
    }

    override fun retry() = presenter.refresh()

    private fun createCommentListener() = object : OnCommentClickListener {
        override fun onCommentLike(viewHolder: RecyclerView.ViewHolder, position: Int) {
            ALog.d("onCommentLike, position = $position")
        }

        override fun onCommentDislike(viewHolder: RecyclerView.ViewHolder, position: Int) {
            ALog.d("onCommentDislike, position = $position")
        }

        override fun onViewHolderClick(viewHolder: RecyclerView.ViewHolder, position: Int) {
            when (position) {
                0 -> if (presenter.notAnonymous()) {
                    WriteTextSheetDialog.newInstance(episodeId).also {
                        it.presenter = presenter.weak()
                    }.show(fragmentManager)
                }

                else -> Toast.makeText(context, "Not implemented yet", Toast.LENGTH_SHORT).show()
            }
        }

        override fun onCommentReply(viewHolder: RecyclerView.ViewHolder, position: Int) {
            val comment = presenter.get<Comment>(position) ?: return
            if (comment.parentId != 0) {
                Toast.makeText(context, "Not implemented yet", Toast.LENGTH_SHORT).show()

            } else if (presenter.notAnonymous()) {
                WriteTextSheetDialog.newInstance(episodeId, comment.id).also {
                    it.presenter = presenter.weak()
                }.show(fragmentManager)
            }
        }
    }

    private fun scrollListener() = object : OnInfiniteScrollListener {
        override fun onInfiniteScroll(recyclerView: InfiniteScrollRecyclerView) {
            if (presenter.commentsCount < presenter.totalCount) {
                presenter.loadMore()

            } else {
                recyclerView.finishInfiniteScroll()
                recyclerView.listener = null
            }
        }
    }
}