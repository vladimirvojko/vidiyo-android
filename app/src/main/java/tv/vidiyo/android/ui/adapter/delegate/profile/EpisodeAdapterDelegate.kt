package tv.vidiyo.android.ui.adapter.delegate.profile

import android.view.ViewGroup
import tv.vidiyo.android.api.model.episode.Episode
import tv.vidiyo.android.base.adapter.delegate.ListProviderAdapterDelegate
import tv.vidiyo.android.base.adapter.provider.ListDataProvider
import tv.vidiyo.android.base.view.holder.ViewHolder
import tv.vidiyo.android.ui.view.holder.EpisodeViewHolder
import tv.vidiyo.android.ui.view.holder.setEpisode

class EpisodeAdapterDelegate(
        provider: ListDataProvider<Episode>,
        listener: ViewHolder.OnViewHolderClickListener?
) : ListProviderAdapterDelegate<Episode, ViewHolder.OnViewHolderClickListener, EpisodeViewHolder>(provider, listener) {

    override fun onCreateViewHolder(parent: ViewGroup) = EpisodeViewHolder(parent, listener)
    override fun onBindViewHolder(position: Int, holder: EpisodeViewHolder, provider: ListDataProvider<Episode>, item: Episode) {
        holder.setEpisode(item)
    }
}