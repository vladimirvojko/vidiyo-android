package tv.vidiyo.android.ui.view.holder

import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import tv.vidiyo.android.R
import tv.vidiyo.android.base.view.holder.ViewHolder
import tv.vidiyo.android.base.view.holder.ViewHolder.OnViewHolderClickListener
import java.lang.ref.WeakReference

class WatchingEpisodeSmallViewHolder(
        parent: ViewGroup,
        listener: WeakReference<OnViewHolderClickListener>? = null,
        layoutResId: Int = R.layout.view_item_watching_episode_small
) : ViewHolder<OnViewHolderClickListener>(parent, listener, layoutResId, true) {

    val ivImage: ImageView = itemView.findViewById(R.id.iv_image)
    val tvShowName: TextView = itemView.findViewById(R.id.tv_show_name)
    val tvDuration: TextView = itemView.findViewById(R.id.tv_duration)
}