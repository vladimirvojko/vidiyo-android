package tv.vidiyo.android.ui.adapter.delegate.discover

import tv.vidiyo.android.api.model.user.FollowingUser
import tv.vidiyo.android.base.adapter.provider.ListDataProvider
import tv.vidiyo.android.ui.adapter.delegate.SubscriberAdapterDelegate
import tv.vidiyo.android.ui.view.holder.OnSubscribableViewHolderClickListener
import tv.vidiyo.android.util.SubscriptionManager

class SubscribableUserAdapterDelegate(
        provider: ListDataProvider<FollowingUser>,
        listener: OnSubscribableViewHolderClickListener?,
        manager: SubscriptionManager
) : SubscriberAdapterDelegate(provider, listener, manager) {

    override fun isForViewType(position: Int, provider: ListDataProvider<FollowingUser>) = position != 0
}