package tv.vidiyo.android.ui.boarding.signup

import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.RecyclerView
import android.widget.LinearLayout
import tv.vidiyo.android.App
import tv.vidiyo.android.DependencyGraph
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.user.Friend
import tv.vidiyo.android.base.adapter.SectionedRecyclerAdapter
import tv.vidiyo.android.base.adapter.delegate.AdapterDelegate
import tv.vidiyo.android.base.adapter.section.ItemPosition
import tv.vidiyo.android.base.fragment.SectionedBaseRecyclerViewFragment
import tv.vidiyo.android.di.subcomponent.boarding.FriendsModule
import tv.vidiyo.android.domain.contract.SignUpContract.FriendsList
import tv.vidiyo.android.domain.presenter.boarding.FriendsListPresenter
import tv.vidiyo.android.model.FriendsListType
import tv.vidiyo.android.ui.FindFriendsActivity
import tv.vidiyo.android.ui.adapter.delegate.FooterButtonDelegate
import tv.vidiyo.android.ui.adapter.delegate.FriendViewDelegate
import tv.vidiyo.android.ui.adapter.delegate.FriendsListHeaderDelegate
import tv.vidiyo.android.ui.boarding.SelectChannelsFragment
import tv.vidiyo.android.ui.view.holder.FriendViewHolder
import tv.vidiyo.android.ui.widget.InfiniteScrollRecyclerView
import javax.inject.Inject

class FriendsListFragment : SectionedBaseRecyclerViewFragment<FriendsListType, Friend>(), FriendsList.View {

    override val toolbarTitleRes = R.string.boarding_find_friends

    @Inject
    lateinit var presenter: FriendsListPresenter

    override fun injectDependencies(graph: DependencyGraph) {
        graph.friendsComponent.plus(FriendsModule(this)).injectTo(this)
    }

    override fun onCreateAdapter() = SectionedRecyclerAdapter(listOf<AdapterDelegate>(
            FriendViewDelegate(presenter.repository.provider, this),
            FriendsListHeaderDelegate(presenter.repository.provider, this),
            FooterButtonDelegate(presenter.repository.provider, this)
    ), presenter.repository.provider)

    override fun onRecyclerViewCreated(recyclerView: InfiniteScrollRecyclerView) {
        super.onRecyclerViewCreated(recyclerView)

        recyclerView.addItemDecoration(DividerItemDecoration(context, LinearLayout.VERTICAL))
    }

    override fun startNextScreen() {
        if (activity is FindFriendsActivity) {
            activity.finish()

        } else {
            App.graph.destroyFriendsComponent()
            startFragment(SelectChannelsFragment(), false)
        }
    }

    override fun onViewHolderClick(viewHolder: RecyclerView.ViewHolder, position: ItemPosition) {
        if (position.isFooter) {
            presenter.finishSelection()
        } else if (position.isItem) {
            val selected = presenter.toggleSelection(position)
            (viewHolder as? FriendViewHolder)?.apply {
                changeButtonState(presenter.isFriendHasAccount(position), selected)
            }
        }
    }
}