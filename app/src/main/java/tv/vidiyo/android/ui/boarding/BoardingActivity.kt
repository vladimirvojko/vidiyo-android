package tv.vidiyo.android.ui.boarding

import android.os.Bundle
import tv.vidiyo.android.R
import tv.vidiyo.android.account.BaseAccountAuthenticatorActivity
import tv.vidiyo.android.base.fragment.BaseFragment
import tv.vidiyo.android.ui.boarding.signin.SignInFragment
import tv.vidiyo.android.ui.boarding.signup.SignUpFragment

class BoardingActivity : BaseAccountAuthenticatorActivity() {

    override val fragmentContainerId: Int = R.id.fragment_container

    companion object {
        const val EXTRA = "EXTRA"//action

        const val SIGN_UP = 1
        const val SIGN_IN = 2
        const val SELECT_CHANNELS = 3
    }

    var action: Int = SIGN_IN

    override fun onCreate(b: Bundle?) {
        super.onCreate(b)
        super.setContentView(R.layout.activity_container)
        action = b?.getInt(EXTRA) ?: intent.getIntExtra(EXTRA, SIGN_IN)
        val f: BaseFragment = when (action) {
            SIGN_IN -> SignInFragment()
            SELECT_CHANNELS -> SelectChannelsFragment()
            else -> SignUpFragment()
        }

        startFragment(f, false)
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putInt(EXTRA, action)
    }
}