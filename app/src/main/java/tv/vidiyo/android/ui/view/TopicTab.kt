package tv.vidiyo.android.ui.view

import android.content.Context
import android.support.v7.widget.AppCompatImageView
import android.support.v7.widget.CardView
import android.util.AttributeSet
import android.widget.RelativeLayout
import android.widget.TextView
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.topic.Topic
import tv.vidiyo.android.extension.load

class TopicTab @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0
) : CardView(context, attrs, defStyleAttr) {

    val ivImage: AppCompatImageView
    val tvTitle: TextView

    init {
        RelativeLayout.inflate(context, R.layout.tab_view_topic, this)

        ivImage = findViewById(R.id.iv_image)
        tvTitle = findViewById(R.id.tv_title)
    }
}

fun TopicTab.setTopic(topic: Topic) {
    ivImage.load(topic.image)
    tvTitle.text = topic.name
}

fun TopicTab.selection(select: Boolean) {
    tvTitle.setBackgroundResource(if (select) R.drawable.gradient_transparent_blue else R.drawable.gradient_transparent_black)
}