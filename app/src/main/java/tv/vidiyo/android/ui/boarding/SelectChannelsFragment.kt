package tv.vidiyo.android.ui.boarding

import android.os.Bundle
import android.support.v7.app.ActionBar
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.TextView
import tv.vidiyo.android.DependencyGraph
import tv.vidiyo.android.R
import tv.vidiyo.android.base.view.holder.ViewHolder
import tv.vidiyo.android.di.subcomponent.channels.ChannelsModule
import tv.vidiyo.android.domain.contract.ChannelsContract
import tv.vidiyo.android.domain.contract.startMainImpl
import tv.vidiyo.android.domain.presenter.channels.BoardingChannelsPresenter
import tv.vidiyo.android.extension.assignTo
import tv.vidiyo.android.extension.changeVisibilityAnimated
import tv.vidiyo.android.extension.isVisible
import tv.vidiyo.android.source.channels.ChannelsMode
import tv.vidiyo.android.ui.channels.BaseChannelsFragment
import tv.vidiyo.android.ui.dialog.GatheringDialog
import tv.vidiyo.android.ui.dialog.QuickIntroDialog
import tv.vidiyo.android.ui.dialog.SelectChannelsDialog
import tv.vidiyo.android.ui.view.SpaceItemDecoration
import tv.vidiyo.android.ui.view.holder.ChannelViewHolder

class SelectChannelsFragment : BaseChannelsFragment<ChannelsContract.BoardingView, BoardingChannelsPresenter>(),
        ChannelsContract.BoardingView, ViewHolder.OnViewHolderClickListener, View.OnClickListener {

    override val layoutResId = R.layout.fragment_channels
    override var mode = ChannelsMode.EDIT

    lateinit var tvMessage: TextView
    lateinit var btnContinue: Button

    override fun injectDependencies(graph: DependencyGraph) {
        graph.dataComponent.plus(ChannelsModule(this)).injectTo(this)
    }

    override fun onCreate(b: Bundle?) {
        super.onCreate(b)
        SelectChannelsDialog.show(this)
        activity.window.apply {
            clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN)
            addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        }
    }

    override fun onActionBarReady(bar: ActionBar) {
        super.onActionBarReady(bar)
        bar.hide()
    }

    override fun onViewCreated(view: View, b: Bundle?) {
        super.onViewCreated(view, b)
        tvMessage = view.findViewById(R.id.tv_message)
        btnContinue = view.findViewById(R.id.btn_continue)
        recyclerView = view.findViewById(R.id.recycler_view)

        recyclerView.layoutManager = GridLayoutManager(context, 2)
        recyclerView.adapter = adapter
        recyclerView.addItemDecoration(SpaceItemDecoration(context, true, true))

        tvMessage.text = getString(R.string.channel_select_at_least_d_topics, presenter.minimumTopics)
        assignTo(btnContinue)
    }

    override fun update() {
        adapter.notifyDataSetChanged()
    }

    override fun updateMessage(count: Int) {
        animate(tvMessage, true)
        tvMessage.text = getString(R.string.channel_select_at_least_d_topics, count)
    }

    override fun changeMessageVisibility(visible: Boolean) {
        animate(tvMessage, visible)
    }

    override fun changeButtonVisibility(visible: Boolean) {
        animate(btnContinue, visible)
    }

    override fun showGathering() {
        GatheringDialog.show(this)
    }

    override fun showIntro() {
        QuickIntroDialog.show(this)
    }

    override fun startMain() {
        activity.window.apply {
            clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
            addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN)
        }

        startMainImpl()
    }

    private fun animate(view: View, visible: Boolean) {
        if (view.isVisible != visible) {
            view.changeVisibilityAnimated(visible)
        }
    }

    override fun onClick(v: View?) {
        presenter.assignChannels()
    }

    override fun onViewHolderClick(viewHolder: RecyclerView.ViewHolder, position: Int) {
        if (viewHolder is ChannelViewHolder) {
            viewHolder.checkbox.apply {
                isChecked = !isChecked
            }
        }
        presenter.changeSelection(position)
    }

    override fun showError(text: String?, code: Int) {}
}