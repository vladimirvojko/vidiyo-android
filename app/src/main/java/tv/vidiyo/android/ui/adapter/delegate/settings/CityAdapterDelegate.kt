package tv.vidiyo.android.ui.adapter.delegate.settings

import android.annotation.SuppressLint
import android.view.ViewGroup
import tv.vidiyo.android.api.model.City
import tv.vidiyo.android.base.adapter.delegate.ListProviderAdapterDelegate
import tv.vidiyo.android.base.adapter.provider.ListDataProvider
import tv.vidiyo.android.base.view.holder.ViewHolder
import tv.vidiyo.android.ui.view.holder.TextViewHolder

class CityAdapterDelegate(
        provider: ListDataProvider<City>,
        listener: ViewHolder.OnViewHolderClickListener?
) : ListProviderAdapterDelegate<City, ViewHolder.OnViewHolderClickListener, TextViewHolder>(provider, listener) {

    override fun onCreateViewHolder(parent: ViewGroup) = TextViewHolder(parent, listener)

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(position: Int, holder: TextViewHolder, provider: ListDataProvider<City>, item: City) {
        holder.textView.text = item.address
    }
}