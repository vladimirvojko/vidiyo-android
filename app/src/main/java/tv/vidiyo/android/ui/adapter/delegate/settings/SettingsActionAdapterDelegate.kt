package tv.vidiyo.android.ui.adapter.delegate.settings

import android.view.ViewGroup
import android.widget.CompoundButton
import tv.vidiyo.android.api.model.Header
import tv.vidiyo.android.api.model.Item
import tv.vidiyo.android.api.model.user.UserAccount
import tv.vidiyo.android.base.adapter.delegate.SectionedAdapterDelegate
import tv.vidiyo.android.base.adapter.provider.SectionedDataProvider
import tv.vidiyo.android.base.adapter.section.ItemPosition
import tv.vidiyo.android.base.view.holder.ViewHolder
import tv.vidiyo.android.ui.view.holder.SettingActionViewHolder
import tv.vidiyo.android.ui.view.holder.setItem
import java.lang.ref.WeakReference

class SettingsActionAdapterDelegate(
        provider: SectionedDataProvider<Header, Item>,
        private val changeListener: WeakReference<CompoundButton.OnCheckedChangeListener>?,
        private val account: UserAccount
) : SectionedAdapterDelegate<Header, Item, SectionedDataProvider<Header, Item>,
        ViewHolder.OnViewHolderClickListener, SettingActionViewHolder>(provider, null) {

    override fun onCreateViewHolder(parent: ViewGroup) = SettingActionViewHolder(parent, changeListener)

    override fun onBindViewHolder(position: ItemPosition, holder: SettingActionViewHolder, provider: SectionedDataProvider<Header, Item>) {
        holder.setItem(provider.getItemForPosition(position) ?: return, account)
    }

    override fun isForViewType(itemPosition: ItemPosition): Boolean = itemPosition.isItem
            && itemPosition.position == 4 // TODO: hardcoded index
}