package tv.vidiyo.android.ui.view.holder

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.cast.Cast
import tv.vidiyo.android.api.model.channel.Channel
import tv.vidiyo.android.api.model.show.Show
import tv.vidiyo.android.api.model.topic.Topic
import tv.vidiyo.android.base.view.holder.ViewHolder
import tv.vidiyo.android.ui.view.StateButton
import tv.vidiyo.android.util.SubscriptionManager
import java.lang.ref.WeakReference

interface OnSubscribableViewHolderClickListener : ViewHolder.OnViewHolderClickListener {
    fun onSubscribeClick(viewHolder: RecyclerView.ViewHolder, position: Int)
}

class SubscribableViewHolder(
        parent: ViewGroup,
        l: WeakReference<OnSubscribableViewHolderClickListener>?,
        layoutResId: Int
) : DataViewHolder<OnSubscribableViewHolderClickListener>(parent, l, layoutResId, true) {

    val button: StateButton = itemView.findViewById(R.id.button)

    init {
        button.setOnClickListener {
            listener?.get()?.onSubscribeClick(this, adapterPosition)
        }
    }

    fun setButtonState(subscribed: Boolean) {
        button.setActiveState(subscribed)
    }
}

fun SubscribableViewHolder.setShow(show: Show, channel: Channel? = null, manager: SubscriptionManager? = null) {
    setImage(show.image)
    setTitle(show.name)
    setSubtitle(channel?.name ?: show.category)
    setSubscribersCount(show.subscribers)
    setButtonState(manager?.isSubscribed(show) ?: show.isSubscribed)
}

fun SubscribableViewHolder.setCast(cast: Cast, manager: SubscriptionManager? = null) {
    setImage(cast.image, R.drawable.ic_default_profile)
    setTitle(cast.name)
    setSubtitle(context.getString(cast.type.stringId))
    setSubscribersCount(cast.subscribers)
    setButtonState(manager?.isSubscribed(cast) ?: cast.isSubscribed)
}

fun SubscribableViewHolder.setTopic(topic: Topic, channel: Channel? = null, manager: SubscriptionManager? = null) {
    setImage(topic.image)
    setTitle(topic.name)
    setSubtitle(channel?.name ?: topic.channel)
    setSubscribersCount(topic.subscribers)
    setButtonState(manager?.isSubscribed(topic) ?: topic.isSubscribed)
}