package tv.vidiyo.android.ui.channels

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.widget.LinearLayout
import tv.vidiyo.android.DependencyGraph
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.show.Show
import tv.vidiyo.android.api.model.topic.Topic
import tv.vidiyo.android.base.adapter.RecyclerAdapter
import tv.vidiyo.android.base.adapter.SectionedRecyclerAdapter
import tv.vidiyo.android.base.adapter.pages.PageFragment
import tv.vidiyo.android.base.adapter.provider.MutableListDataProvider
import tv.vidiyo.android.base.adapter.provider.SectionedListDataProvider
import tv.vidiyo.android.base.fragment.StateRecyclerFragment
import tv.vidiyo.android.base.fragment.ViewFragment
import tv.vidiyo.android.di.subcomponent.channels.ChildChannelModule
import tv.vidiyo.android.domain.contract.ChannelContract
import tv.vidiyo.android.domain.presenter.channels.InnerChannelPresenter
import tv.vidiyo.android.model.InnerPageType
import tv.vidiyo.android.ui.adapter.DataRecyclerAdapter
import tv.vidiyo.android.ui.adapter.delegate.channel.SectionedChannelShowAdapterDelegate
import tv.vidiyo.android.ui.adapter.delegate.channel.SectionedTopicHeaderAdapterDelegate
import tv.vidiyo.android.ui.show.ShowFragment
import tv.vidiyo.android.ui.topic.TopicFragment
import tv.vidiyo.android.ui.view.DividerItemDecoration
import tv.vidiyo.android.ui.view.SpaceItemDecoration
import tv.vidiyo.android.ui.view.holder.OnSubscribableViewHolderClickListener
import tv.vidiyo.android.ui.widget.InfiniteScrollRecyclerView
import javax.inject.Inject

class InnerChannelFragment : StateRecyclerFragment<RecyclerAdapter>(), ChannelContract.Inner.View,
        OnSubscribableViewHolderClickListener {

    companion object {
        private const val EXTRA_0 = "EXTRA_0"//type
        private const val EXTRA_1 = "EXTRA_1"//index

        private fun newInstance(@InnerPageType type: Int, index: Int? = null) = InnerChannelFragment().apply {
            arguments = Bundle().also {
                it.putInt(EXTRA_0, type)
                if (index != null) {
                    it.putInt(EXTRA_1, index)
                }
            }
        }

        object ALL_CREATOR : PageFragment.Creator {
            override fun newInstance(): Fragment = InnerChannelFragment.newInstance(InnerPageType.ALL)
        }

        fun topicCreator(index: Int) = object : PageFragment.Creator {
            override fun newInstance(): Fragment = InnerChannelFragment.newInstance(InnerPageType.TOPICS, index)
        }

        object MORE_CREATOR : PageFragment.Creator {
            override fun newInstance(): Fragment = InnerChannelFragment.newInstance(InnerPageType.MORE)
        }
    }

    override val layoutResId: Int = R.layout.fragment_state_recycler_empty
    override var type: Int = InnerPageType.ALL
    override var index: Int = 0

    @Inject
    lateinit var presenter: InnerChannelPresenter

    override fun onCreate(b: Bundle?) {
        val bundle = b ?: arguments
        type = bundle.getInt(EXTRA_0, InnerPageType.ALL)
        index = bundle.getInt(EXTRA_1, 0)
        super.onCreate(b)
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putInt(EXTRA_0, type)
        outState?.putInt(EXTRA_1, index)
    }

    override fun injectDependencies(graph: DependencyGraph) {
        (parentFragment as? ChannelFragment)?.component?.plus(ChildChannelModule(this))?.injectTo(this)
    }

    override fun onPostViewCreated() {
        presenter.start()
    }

    @Suppress("UNCHECKED_CAST")
    override fun onCreateAdapter(): RecyclerAdapter {
        val channel = presenter.channel
        return when (type) {
            InnerPageType.ALL -> {
                val provider = presenter.getProvider(type, 0) as? MutableListDataProvider<Show> ?: throw IllegalArgumentException()
                DataRecyclerAdapter.subscribableShow(provider, channel, this)
            }
            InnerPageType.TOPICS -> {
                val provider = presenter.getProvider(type, index) as? SectionedListDataProvider<Topic, Show> ?: throw IllegalArgumentException()
                SectionedRecyclerAdapter(listOf(
                        SectionedTopicHeaderAdapterDelegate(channel, provider, this),
                        SectionedChannelShowAdapterDelegate(channel, provider, this)

                ), provider)
            }
            InnerPageType.MORE -> {
                val provider = presenter.getProvider(type, 0) as? MutableListDataProvider<Topic> ?: throw IllegalArgumentException()
                DataRecyclerAdapter.topics(provider, channel, this)
            }

            else -> throw IllegalArgumentException("Unknown type")
        }
    }

    override fun onRecyclerViewCreated(recyclerView: InfiniteScrollRecyclerView) {
        super.onRecyclerViewCreated(recyclerView)
        recyclerView.addItemDecoration(SpaceItemDecoration(context, true, true))
        recyclerView.addItemDecoration(DividerItemDecoration(context, LinearLayout.VERTICAL))
    }

    override fun onViewHolderClick(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val fragment: ViewFragment = when (type) {
            InnerPageType.ALL -> {
                val show = presenter.getItem<Show>(position) ?: return
                ShowFragment.newInstance(show)
            }
            InnerPageType.TOPICS -> {
                val item = presenter.getItem<Any>(position) ?: return
                when (item) {
                    is Topic -> TopicFragment.newInstance(item)
                    is Show -> ShowFragment.newInstance(item)

                    else -> return
                }
            }
            InnerPageType.MORE -> {
                val topic = presenter.getItem<Topic>(position) ?: return
                TopicFragment.newInstance(topic)
            }

            else -> throw IllegalArgumentException("Unknown type")
        }
        startFragment(fragment)
    }

    override fun onSubscribeClick(viewHolder: RecyclerView.ViewHolder, position: Int) {
        if (refreshLayout?.isRefreshing == false)
            presenter.toggleSubscription(position)
    }

    override fun onSelected() = presenter.refresh()
    override fun retry() = presenter.refresh()
}