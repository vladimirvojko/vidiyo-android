package tv.vidiyo.android.ui.view

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import tv.vidiyo.android.R


class ConfirmationView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0
) : RelativeLayout(context, attrs, defStyleAttr) {

    val ivClose: ImageView
    val tvTitle: TextView
    val tvMessage: TextView
    val btnSend: Button

    init {
        inflate(context, R.layout.view_confirmation, this)
        setBackgroundResource(R.drawable.yellow_radius_2)
        resources.getDimensionPixelOffset(R.dimen.spacing).let {
            setPadding(it, it, it, it)
        }

        ivClose = findViewById(R.id.iv_close)
        tvTitle = findViewById(R.id.tv_title)
        tvMessage = findViewById(R.id.tv_message)
        btnSend = findViewById(R.id.btn_send)
    }

    fun setEmail(email: String?) {
        tvMessage.text = context.getString(R.string.format_confirm_s_as_your_email_address, email)
    }

    override fun setOnClickListener(l: OnClickListener?) {
        ivClose.setOnClickListener(l)
        btnSend.setOnClickListener(l)
    }

    fun hide() {
        animate()
                .translationY(-height * 1.5f)
                .alpha(0.0f)
                .setDuration(300)
                .setListener(object : AnimatorListenerAdapter() {
                    override fun onAnimationEnd(animation: Animator) {
                        super.onAnimationEnd(animation)
                        visibility = View.GONE
                    }
                })
    }
}