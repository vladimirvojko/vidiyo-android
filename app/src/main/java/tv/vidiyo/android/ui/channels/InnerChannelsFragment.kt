package tv.vidiyo.android.ui.channels

import android.os.Bundle
import android.support.v4.app.Fragment
import tv.vidiyo.android.DependencyGraph
import tv.vidiyo.android.base.adapter.pages.PageFragment
import tv.vidiyo.android.di.subcomponent.channels.ChannelsModule
import tv.vidiyo.android.domain.contract.ChannelsContract
import tv.vidiyo.android.domain.presenter.channels.ChannelsPresenter
import tv.vidiyo.android.source.channels.ChannelsMode

class InnerChannelsFragment : BaseChannelsFragment<ChannelsContract.View, ChannelsPresenter>() {

    companion object {
        private const val EXTRA = "vdyo.mnact.cf.extra"

        private fun newInstance(@ChannelsMode mode: Int): InnerChannelsFragment {
            val fragment = InnerChannelsFragment()
            val b = Bundle()
            b.putInt(EXTRA, mode)
            fragment.arguments = b
            return fragment
        }

        object ALL_CREATOR : PageFragment.Creator {
            override fun newInstance(): Fragment = InnerChannelsFragment.newInstance(ChannelsMode.ALL)
        }

        object MY_CREATOR : PageFragment.Creator {
            override fun newInstance(): Fragment = InnerChannelsFragment.newInstance(ChannelsMode.MY)
        }
    }

    override var mode = ChannelsMode.ALL

    override fun onCreate(b: Bundle?) {
        val bundle = b ?: arguments
        mode = bundle.getInt(EXTRA, ChannelsMode.ALL)
        super.onCreate(b)
    }

    override fun injectDependencies(graph: DependencyGraph) {
        graph.dataComponent.plus(ChannelsModule(this)).injectTo(this)
    }
}