package tv.vidiyo.android.ui.channels

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.view.View
import tv.vidiyo.android.DependencyGraph
import tv.vidiyo.android.R
import tv.vidiyo.android.base.PagesPresenter
import tv.vidiyo.android.base.adapter.pages.PageFragment
import tv.vidiyo.android.base.adapter.pages.PagesAdapter
import tv.vidiyo.android.base.fragment.PagesMainFragment
import tv.vidiyo.android.di.subcomponent.channels.ChannelsModule
import tv.vidiyo.android.domain.contract.PagesPresentationView
import tv.vidiyo.android.extension.changeVisibility
import tv.vidiyo.android.ui.widget.StateLayout
import javax.inject.Inject

class ChannelsFragment : PagesMainFragment(), PagesPresentationView {

    override val toolbarTitleRes = R.string.navigation_channels
    override val contentLayoutResId = R.layout.content_state_pager
    override val optionsMenuResId get() = R.menu.channels

    private lateinit var stateLayout: StateLayout
    private lateinit var menuItemEdit: MenuItem

    @Inject
    lateinit var presenter: PagesPresenter

    override fun injectDependencies(graph: DependencyGraph) {
        graph.dataComponent.plus(ChannelsModule(this)).injectTo(this)
    }

    override fun onViewCreated(view: View, b: Bundle?) {
        super.onViewCreated(view, b)
        stateLayout = view.findViewById(R.id.state_layout)
    }

    override fun onPostViewCreated() {
        presenter.start()
    }

    override fun createAdapter(): PagesAdapter = PagesAdapter(context, childFragmentManager,
            PageFragment(InnerChannelsFragment.Companion.MY_CREATOR, R.string.channel_my_channels),
            PageFragment(InnerChannelsFragment.Companion.ALL_CREATOR, R.string.channel_all_channels)
    )

    override fun onToolbarCreated(toolbar: Toolbar) {
        super.onToolbarCreated(toolbar)
        menuItemEdit = toolbar.menu.findItem(R.id.item_edit)
    }

    override fun onTabLayoutCreated(tabLayout: TabLayout) {
        super.onTabLayoutCreated(tabLayout)
        update()
    }

    override fun onMenuItemClick(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.item_edit -> {
                mainActivity?.startFragment(EditChannelsFragment(), true)
            }
            else -> return super.onMenuItemClick(item)
        }
        return true
    }

    //region StatePresentationView
    override fun showProgress() {
        stateLayout.changeState(StateLayout.State.PROGRESS)
    }

    override fun showContent() {
        stateLayout.changeState(StateLayout.State.CONTENT)
    }

    override fun showError(message: String?, asToast: Boolean) {
        stateLayout.changeState(StateLayout.State.ERROR)
    }

    override fun showError(resId: Int) {
        stateLayout.changeState(StateLayout.State.ERROR)
    }

    override fun showNoContent() {
        stateLayout.changeState(StateLayout.State.EMPTY)
    }
    //endregion StatePresentationView

    //region UpdatablePresentationView
    override fun update() {
        menuItemEdit.isVisible = !presenter.repository.isEmpty
        tabLayout?.changeVisibility(!presenter.repository.isEmpty)
    }
    //endregion UpdatablePresentationView
}