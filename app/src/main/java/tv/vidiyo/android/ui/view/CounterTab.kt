package tv.vidiyo.android.ui.view

import android.content.Context
import android.support.v4.content.ContextCompat
import android.util.AttributeSet
import android.widget.RelativeLayout
import android.widget.TextView
import tv.vidiyo.android.R
import tv.vidiyo.android.extension.changeVisibility

class CounterTab @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0
) : RelativeLayout(context, attrs, defStyleAttr) {

    val tvTitle: TextView
    val tvCounter: TextView

    init {
        inflate(context, R.layout.tab_title_counter, this)

        tvTitle = findViewById(R.id.tv_title)
        tvCounter = findViewById(R.id.tv_counter)
    }

    fun setTitle(text: String) {
        tvTitle.text = text
    }

    fun setCount(count: Int) {
        val text = if (count == 0) null else count.toString()
        tvCounter.text = text
        tvCounter.changeVisibility(text != null)
    }

    fun selection(selected: Boolean) {
        val resId = if (selected) android.R.color.white else R.color.white_40_pct
        tvTitle.setTextColor(ContextCompat.getColor(context, resId))
    }
}