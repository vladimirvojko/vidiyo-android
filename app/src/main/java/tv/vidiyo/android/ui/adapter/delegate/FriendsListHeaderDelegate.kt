package tv.vidiyo.android.ui.adapter.delegate

import android.view.ViewGroup
import tv.vidiyo.android.api.model.user.Friend
import tv.vidiyo.android.base.adapter.delegate.SectionedAdapterDelegate
import tv.vidiyo.android.base.adapter.provider.SectionedDataProvider
import tv.vidiyo.android.base.adapter.section.ItemPosition
import tv.vidiyo.android.base.view.holder.ViewHolder.OnViewHolderClickListener
import tv.vidiyo.android.model.FriendsListType
import tv.vidiyo.android.ui.view.holder.FriendsListHeaderViewHolder

class FriendsListHeaderDelegate(
        provider: SectionedDataProvider<FriendsListType, Friend>,
        listener: OnViewHolderClickListener?
) : SectionedAdapterDelegate<FriendsListType, Friend, SectionedDataProvider<FriendsListType, Friend>, OnViewHolderClickListener, FriendsListHeaderViewHolder>(provider, listener) {

    override fun onCreateViewHolder(parent: ViewGroup): FriendsListHeaderViewHolder {
        return FriendsListHeaderViewHolder(parent, listener)
    }

    override fun onBindViewHolder(position: ItemPosition, holder: FriendsListHeaderViewHolder, provider: SectionedDataProvider<FriendsListType, Friend>) {
        provider.getSection(position.section)?.section?.apply {
            holder.tvTitle.text = titleForCount(provider.getItemsCountInSection(), holder.context)
            holder.tvSubtitle.text = subtitle(holder.context)
        }
    }

    override fun isForViewType(itemPosition: ItemPosition) = itemPosition.isHeader
}