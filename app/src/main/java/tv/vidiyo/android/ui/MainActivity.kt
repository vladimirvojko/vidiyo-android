package tv.vidiyo.android.ui

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.LocalBroadcastManager
import android.widget.FrameLayout
import com.github.pedrovgs.DraggableListener
import com.ncapdevi.fragnav.FragNavController
import com.roughike.bottombar.BottomBar
import com.roughike.bottombar.OnTabSelectListener
import tv.vidiyo.android.DependencyGraph
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.episode.Episode
import tv.vidiyo.android.api.model.user.UserAccount
import tv.vidiyo.android.base.BaseActivity
import tv.vidiyo.android.base.fragment.BaseFragment
import tv.vidiyo.android.base.fragment.ViewFragment
import tv.vidiyo.android.base.interfaces.OnBackPressedListener
import tv.vidiyo.android.di.subcomponent.SocialComponent
import tv.vidiyo.android.di.subcomponent.SocialModule
import tv.vidiyo.android.extension.changeVisibility
import tv.vidiyo.android.extension.isPermissionGranted
import tv.vidiyo.android.extension.request
import tv.vidiyo.android.extension.startActivity
import tv.vidiyo.android.model.NotificationType
import tv.vidiyo.android.ui.boarding.BoardingActivity
import tv.vidiyo.android.ui.channels.ChannelsFragment
import tv.vidiyo.android.ui.episode.EpisodeFragment
import tv.vidiyo.android.ui.home.HomeFragment
import tv.vidiyo.android.ui.profile.ProfileFragment
import tv.vidiyo.android.ui.search.SearchFragment
import tv.vidiyo.android.ui.view.BadgeView
import tv.vidiyo.android.ui.watching.WatchingFragment
import tv.vidiyo.android.util.*
import javax.inject.Inject

class MainActivity : BaseActivity(), OnTabSelectListener, FragNavController.TransactionListener,
        FragNavController.RootFragmentListener, DraggableListener, NotificationsCenter.Listener {

    companion object {
        private const val EXTRA = "EXTRA"
        fun start(activity: Activity, @NotificationType type: String? = null) {
            activity.startActivity(Intent(activity, MainActivity::class.java).also {
                it.putExtra(EXTRA, type)
                it.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            })
        }
    }

    override val fragmentContainerId = R.id.fragment_container

    lateinit var root: FrameLayout

    private lateinit var navigationBar: BottomBar
    private lateinit var controller: FragNavController
    private lateinit var badge: BadgeView

    private var episodeFragment: EpisodeFragment? = null

    lateinit var socialComponent: SocialComponent

    @Inject lateinit var socialManager: SocialManager
    @Inject lateinit var center: NotificationsCenter
    @Inject lateinit var locationWatcher: LocationWatcher
    @Inject lateinit var manager: LocalBroadcastManager

    var account: UserAccount? = null

    override fun injectDependencies(graph: DependencyGraph) {
        socialComponent = graph.dataComponent.plus(SocialModule(this))
        socialComponent.injectTo(this)
    }

    override fun onCreate(b: Bundle?) {
        super.onCreate(b)
        super.setContentView(R.layout.activity_main)
        val type = intent.extras?.getString(EXTRA, null)
        socialManager.bind(this)
        center.addListener(this)
        account = accountManager.account

        controller = FragNavController.newBuilder(b, supportFragmentManager, fragmentContainerId)
                .rootFragmentListener(this, 5)
                .transactionListener(this)
                .build()

        root = findViewById(R.id.root)
        navigationBar = findViewById(R.id.bottom_navigation)
        navigationBar.setOnTabSelectListener(this)
        navigationBar.setOnTabReselectListener {
            controller.clearStack()
        }
        navigationBar.setTabSelectionInterceptor { _, newTabId ->
            val anonymous = account?.isAnonymous == true

            when (newTabId) {
                R.id.menu_profile,
                R.id.menu_watching -> {
                    if (anonymous) {
                        startActivity<BoardingActivity>(BoardingActivity.EXTRA to BoardingActivity.SIGN_UP)
                    }
                    anonymous
                }
                else -> false
            }
        }

        if (isPermissionGranted(Manifest.permission.ACCESS_FINE_LOCATION)) {
            locationWatcher.updateLocation()
        } else {
            request(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), LocationWatcher.REQUEST_CODE)
        }

        badge = findViewById(R.id.view_badge)
        navigationBar.getTabWithId(R.id.menu_watching).addOnLayoutChangeListener { _, left, _, right, _, _, _, _, _ ->
            badge.x = (left + (right - left) / 2 - badge.width / 2).toFloat()
        }

        when (type ?: return) {
            NotificationType.REQUEST_PENDING -> {
                center.refreshPendingStatus(true)
                navigationBar.selectTabAtPosition(FragNavController.TAB5)
                controller.switchTab(FragNavController.TAB5)
            }

            NotificationType.EPISODE_NEW -> {
                center.refreshEpisodesCount(true)
                navigationBar.selectTabAtPosition(FragNavController.TAB3)
                controller.switchTab(FragNavController.TAB3)
            }
        }
    }

    override fun onAction(action: Int, type: String) {
        when (type) {
            NotificationType.EPISODE_NEW -> if (navigationBar.currentTabPosition != FragNavController.TAB3)
                badge.setCount(center.newEpisodeCount)
        }
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        refreshLayout?.setProgressViewOffset(false, 0, resources.getDimensionPixelOffset(R.dimen.offset_refresh))
    }

    override fun onDestroy() {
        super.onDestroy()
        socialManager.unbind(this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (!socialManager.onActivityResult(requestCode, resultCode, data))
            super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (!locationWatcher.onRequestPermissionsResult(requestCode, permissions, grantResults))
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    private fun sendLocalBroadcast(action: String) = manager.sendBroadcast(Intent(action))

    fun minimize() {
        episodeFragment?.minimize()
    }

    fun openEpisode(episode: Episode) {
        sendLocalBroadcast(Constant.ACTION_MAXIMIZED)
        episodeFragment?.open(episode.id) ?: EpisodeFragment(this, episode.id).also {
            episodeFragment = it
            navigationBar.changeVisibility(false)
        }
    }

    fun selectTab(tabId: Int) {
        navigationBar.selectTabAtPosition(tabId, true)
        controller.switchTab(tabId)
    }

    fun clearBadge() {
        badge.setCount(0)
    }

    override fun onClosedToRight() {
        onClosedToLeft()
    }

    override fun onClosedToLeft() {
        sendLocalBroadcast(Constant.ACTION_MINIMIZED)
        episodeFragment?.remove(root)
        episodeFragment = null
        changeStatusBarColor(Color.BLACK, Animation.color)
    }

    override fun onMaximized() {
        sendLocalBroadcast(Constant.ACTION_MAXIMIZED)
        navigationBar.changeVisibility(false)
        badge.changeVisibility(false)
        changeStatusBarColor(Animation.color, Color.BLACK)
    }

    override fun onMinimized() {
        sendLocalBroadcast(Constant.ACTION_MINIMIZED)
        navigationBar.changeVisibility(true)
        badge.changeVisibility(true)
        episodeFragment?.hideController()
        changeStatusBarColor(Color.BLACK, Animation.color)
    }

    override fun onTabSelected(tabId: Int) {
        when (tabId) {
            R.id.menu_home -> controller.switchTab(FragNavController.TAB1)
            R.id.menu_search -> controller.switchTab(FragNavController.TAB2)
            R.id.menu_watching -> controller.switchTab(FragNavController.TAB3)
            R.id.menu_channels -> controller.switchTab(FragNavController.TAB4)
            R.id.menu_profile -> controller.switchTab(FragNavController.TAB5)
            else -> throw IllegalStateException("Unknown tab id")
        }
    }

    override fun startFragment(fragment: ViewFragment, addToBackStack: Boolean): Boolean {
        if (addToBackStack) {
            controller.pushFragment(fragment)
            episodeFragment?.let { if (it.isMaximized) it.minimize() }
        } else {
            controller.replaceFragment(fragment)
        }
        return true
    }

    override fun onTabTransaction(fragment: Fragment?, index: Int) {
        val indicator = (fragment as? BaseFragment)?.toolbarIndicator != 0
        supportActionBar?.setDisplayHomeAsUpEnabled(indicator || !controller.isRootFragment)
    }

    override fun onFragmentTransaction(fragment: Fragment?, type: FragNavController.TransactionType?) {
        supportActionBar?.setDisplayHomeAsUpEnabled(!controller.isRootFragment)
    }

    override fun getRootFragment(index: Int): Fragment = when (index) {
        FragNavController.TAB1 -> HomeFragment()
        FragNavController.TAB2 -> SearchFragment()
        FragNavController.TAB3 -> WatchingFragment()
        FragNavController.TAB4 -> ChannelsFragment()
        FragNavController.TAB5 -> ProfileFragment()
        else -> throw IllegalStateException("Unknown tab index")
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.let { controller.onSaveInstanceState(it) }
    }

    override fun onBackPressed() {
        when {
            episodeFragment?.isMaximized == true -> episodeFragment?.minimize()
            !controller.isRootFragment -> controller.popFragment()
            else -> {
                val fragment = controller.currentFrag
                if (fragment is OnBackPressedListener) {
                    fragment.onBackPressed()

                } else super.onBackPressed()
            }
        }
    }

    fun changeNavigationBarVisibility(visible: Boolean) {
        navigationBar.changeVisibility(visible)
    }

    private fun changeStatusBarColor(fromColor: Int, toColor: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Animation.statusBarColorTransition(this, fromColor, toColor)
        }
    }

    override fun configureRefreshLayout() = Unit
}