package tv.vidiyo.android.ui.watching

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.view.View
import tv.vidiyo.android.DependencyGraph
import tv.vidiyo.android.R
import tv.vidiyo.android.base.adapter.pages.PageFragment
import tv.vidiyo.android.base.adapter.pages.PagesAdapter
import tv.vidiyo.android.base.fragment.PagesMainFragment
import tv.vidiyo.android.model.NotificationType
import tv.vidiyo.android.ui.view.CounterTab
import tv.vidiyo.android.util.NotificationsCenter
import javax.inject.Inject

class WatchingFragment : PagesMainFragment(), TabLayout.OnTabSelectedListener, NotificationsCenter.Listener {

    override val toolbarTitleRes = R.string.navigation_watching
    override val contentLayoutResId = R.layout.view_pager
    override val appBarLayoutResId = R.layout.app_bar_tabs

    @Inject
    lateinit var center: NotificationsCenter

    override fun injectDependencies(graph: DependencyGraph) {
        graph.dataComponent.injectTo(this)
    }

    override fun onViewCreated(view: View, b: Bundle?) {
        super.onViewCreated(view, b)
        mainActivity?.clearBadge()
        center.addListener(this)

        tabLayout?.addOnTabSelectedListener(this)
        tabLayout?.getTabAt(1)?.let {
            val counterTab = CounterTab(context)
            counterTab.setTitle(adapter.getPageTitle(1).toString())
            counterTab.setCount(center.newEpisodeCount)
            it.customView = counterTab
        }
    }

    override fun onAction(action: Int, type: String) {
        if (type == NotificationType.EPISODE_NEW) {
            tabLayout?.let {
                if (it.selectedTabPosition != 1) {
                    (it.getTabAt(1)?.customView as? CounterTab)?.setCount(center.newEpisodeCount)
                }
            }
        }
    }

    override fun onTabReselected(tab: TabLayout.Tab?) {}

    override fun onTabUnselected(tab: TabLayout.Tab) {
        (tab.customView as? CounterTab)?.selection(false)
    }

    override fun onTabSelected(tab: TabLayout.Tab) {
        (tab.customView as? CounterTab)?.let {
            it.selection(true)
            it.setCount(0)
            center.newEpisodeCount = 0
        }
    }

    override fun createAdapter() = PagesAdapter(context, childFragmentManager,
            PageFragment(InnerWatchingFragment.resumeCreator(), R.string.watching_resume),
            PageFragment(InnerWatchingFragment.newEpisodesCreator(), R.string.watching_new_episodes)
    )
}