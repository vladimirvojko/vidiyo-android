package tv.vidiyo.android.ui.view.holder

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.bumptech.glide.load.resource.drawable.GlideDrawable
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.comment.Comment
import tv.vidiyo.android.base.view.holder.ViewHolder
import tv.vidiyo.android.extension.assignTo
import tv.vidiyo.android.extension.load
import tv.vidiyo.android.ui.view.ShapeImageView
import java.lang.Exception
import java.lang.ref.WeakReference

interface OnCommentClickListener : ViewHolder.OnViewHolderClickListener {
    fun onCommentLike(viewHolder: RecyclerView.ViewHolder, position: Int)
    fun onCommentDislike(viewHolder: RecyclerView.ViewHolder, position: Int)
    fun onCommentReply(viewHolder: RecyclerView.ViewHolder, position: Int)
}

class CommentViewHolder(
        parent: ViewGroup,
        listener: WeakReference<OnCommentClickListener>?,
        reply: Boolean
) : ViewHolder<OnCommentClickListener>(parent, listener, if (reply) R.layout.view_item_comment_reply else R.layout.view_item_comment, false), View.OnClickListener {

    val ivAvatar: ShapeImageView = itemView.findViewById(R.id.iv_avatar)
    val tvName: TextView = itemView.findViewById(R.id.tv_name)
    val tvText: TextView = itemView.findViewById(R.id.tv_text)

    val tvLike: TextView = itemView.findViewById(R.id.tv_like)
    val tvDislike: TextView = itemView.findViewById(R.id.tv_dislike)
    val tvReply: TextView = itemView.findViewById(R.id.tv_replies)

    init {
        assignTo(tvLike, tvDislike, tvReply)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.tv_like -> listener?.get()?.onCommentLike(this, adapterPosition)
            R.id.tv_dislike -> listener?.get()?.onCommentDislike(this, adapterPosition)
            R.id.tv_replies -> listener?.get()?.onCommentReply(this, adapterPosition)

            else -> return
        }
    }

    fun <T> setAvatar(avatar: T?, fallbackImageResId: Int) {
        ivAvatar.setImageResource(fallbackImageResId)
        ivAvatar.load(avatar, requestListener = object : RequestListener<T?, GlideDrawable> {
            override fun onResourceReady(resource: GlideDrawable?, model: T?, target: Target<GlideDrawable>?, isFromMemoryCache: Boolean, isFirstResource: Boolean): Boolean {
                ivAvatar.setImageDrawable(resource)
                return true
            }

            override fun onException(e: Exception?, model: T?, target: Target<GlideDrawable>?, isFirstResource: Boolean): Boolean {
                ivAvatar.setImageResource(fallbackImageResId)
                return true
            }
        })
    }
}

fun CommentViewHolder.setComment(comment: Comment) {
    with(comment.user) {
        tvName.text = name
        setAvatar(image, R.drawable.ic_default_profile)
    }
    tvText.text = comment.text

    val likes = comment.likes.filter { it.liked }.size
    tvLike.text = likes.toString()
    tvDislike.text = (comment.likes.size - likes).toString()
}