package tv.vidiyo.android.ui.widget

import android.content.Context
import android.support.annotation.LayoutRes
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.widget.FrameLayout
import android.widget.FrameLayout.LayoutParams
import tv.vidiyo.android.R
import tv.vidiyo.android.extension.changeVisibility
import tv.vidiyo.android.extension.getResourceId

class StateLayout @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    companion object {
        private fun stateFromAttributes(attrValue: Int) = when (attrValue) {
            1 -> State.CONTENT
            2 -> State.PROGRESS
            3 -> State.ERROR
            4 -> State.EMPTY
            else -> null
        }

        class LayoutParams(c: Context, attrs: AttributeSet) : FrameLayout.LayoutParams(c, attrs) {
            val state: State?

            init {
                val a = c.obtainStyledAttributes(attrs, R.styleable.StateLayout)
                state = stateFromAttributes(a.getInt(R.styleable.StateLayout_state, -1))
                a.recycle()
            }
        }
    }

    @LayoutRes private val contentLayoutResId: Int?
    @LayoutRes private val progressLayoutResId: Int?
    @LayoutRes private val errorLayoutResId: Int?
    @LayoutRes private val emptyLayoutResId: Int?

    var contentView: View? = null
        private set
    var progressView: View? = null
        private set
    var errorView: View? = null
        private set
    var emptyView: View? = null
        private set

    var state: State = State.PROGRESS
        private set

    init {
        val a = context.obtainStyledAttributes(attrs, R.styleable.StateLayout)

        contentLayoutResId = a.getResourceId(R.styleable.StateLayout_state_content_layout)
        progressLayoutResId = a.getResourceId(R.styleable.StateLayout_state_progress_layout)
        errorLayoutResId = a.getResourceId(R.styleable.StateLayout_state_error_layout)
        emptyLayoutResId = a.getResourceId(R.styleable.StateLayout_state_empty_layout)

        state = stateFromAttributes(a.getInt(R.styleable.StateLayout_default_state, -1)) ?: State.PROGRESS

        a.recycle()
    }

    override fun getAccessibilityClassName() = StateLayout::javaClass.name
    override fun generateLayoutParams(attrs: AttributeSet) = LayoutParams(context, attrs)
    override fun generateDefaultLayoutParams() = LayoutParams(MATCH_PARENT, MATCH_PARENT)
    override fun checkLayoutParams(p: ViewGroup.LayoutParams?) = p is LayoutParams

    override fun onFinishInflate() {
        super.onFinishInflate()
        findViewsInRange(0 until childCount)

        val currentChildCount = childCount

        if (contentView == null && contentLayoutResId != null) {
            View.inflate(context, contentLayoutResId, this)
        }

        if (progressView == null) {
            View.inflate(context, progressLayoutResId ?: R.layout.progress_state, this)
        }

        if (errorView == null) {
            View.inflate(context, errorLayoutResId ?: R.layout.error_state, this)
        }

        if (emptyView == null && emptyLayoutResId != null) {
            View.inflate(context, emptyLayoutResId, this)
        }

        if (currentChildCount < childCount) {
            findViewsInRange(currentChildCount until childCount)
        }
    }

    private fun findViewsInRange(range: IntRange) {
        for (index in range) {
            val child = getChildAt(index)
            val state = (child.layoutParams as? LayoutParams)?.state
            when (state) {
                State.CONTENT -> contentView = child
                State.PROGRESS -> progressView = child
                State.ERROR -> errorView = child
                State.EMPTY -> emptyView = child
            }
            child.changeVisibility(state == this.state)
        }
    }

    fun changeState(newState: State = this.state) {
        contentView?.changeVisibility(newState == State.CONTENT)
        progressView?.changeVisibility(newState == State.PROGRESS)
        errorView?.changeVisibility(newState == State.ERROR)
        emptyView?.changeVisibility(newState == State.EMPTY)
    }

    enum class State { CONTENT, PROGRESS, ERROR, EMPTY; }
}