package tv.vidiyo.android.ui.adapter.delegate

import android.view.ViewGroup
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.channel.Channel
import tv.vidiyo.android.api.model.topic.Topic
import tv.vidiyo.android.base.adapter.delegate.ListProviderAdapterDelegate
import tv.vidiyo.android.base.adapter.provider.ListDataProvider
import tv.vidiyo.android.ui.view.holder.OnSubscribableViewHolderClickListener
import tv.vidiyo.android.ui.view.holder.SubscribableViewHolder
import tv.vidiyo.android.ui.view.holder.setTopic

class SubscribableTopicDelegate(
        provider: ListDataProvider<Topic>,
        listener: OnSubscribableViewHolderClickListener?,
        private val channel: Channel? = null
) : ListProviderAdapterDelegate<Topic, OnSubscribableViewHolderClickListener, SubscribableViewHolder>(provider, listener) {

    override fun onCreateViewHolder(parent: ViewGroup) = SubscribableViewHolder(parent, listener, R.layout.view_item_subscribable_topic)
    override fun onBindViewHolder(position: Int, holder: SubscribableViewHolder, provider: ListDataProvider<Topic>, item: Topic)
            = holder.setTopic(item, channel)
}