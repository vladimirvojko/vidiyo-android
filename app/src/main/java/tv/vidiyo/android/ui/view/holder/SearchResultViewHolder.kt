package tv.vidiyo.android.ui.view.holder

import android.view.ViewGroup
import tv.vidiyo.android.base.view.holder.ViewHolder.OnViewHolderClickListener
import tv.vidiyo.android.domain.contract.SearchResultView
import java.lang.ref.WeakReference

class SearchResultViewHolder(
        parent: ViewGroup,
        listener: WeakReference<OnViewHolderClickListener>?,
        layoutResId: Int,
        clickable: Boolean = true
) : DataViewHolder<OnViewHolderClickListener>(parent, listener, layoutResId, clickable), SearchResultView