package tv.vidiyo.android.ui.dialog

import android.os.Bundle
import android.support.design.widget.BottomSheetDialogFragment
import android.support.v4.app.FragmentManager
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.episode.Episode
import tv.vidiyo.android.base.view.ShowMessagePresentationView
import tv.vidiyo.android.di.subcomponent.share.ShareModule
import tv.vidiyo.android.domain.contract.ShareContract
import tv.vidiyo.android.domain.presenter.SharePresenter
import tv.vidiyo.android.extension.assignTo
import tv.vidiyo.android.extension.changeVisibility
import tv.vidiyo.android.ui.MainActivity
import javax.inject.Inject

class ShareEpisodeSheetDialog : BottomSheetDialogFragment(), ShareContract.View,
        View.OnClickListener, TextWatcher {

    companion object {
        private const val EXTRA = "EXTRA"//episode

        fun newInstance(episode: Episode) = ShareEpisodeSheetDialog().apply {
            arguments = Bundle(1).also {
                it.putParcelable(EXTRA, episode)
            }
        }
    }

    private lateinit var btnFacebook: Button
    private lateinit var btnTwitter: Button
    private lateinit var editText: EditText
    private lateinit var ivSend: ImageView

    override var episode: Episode? = null

    @Inject
    lateinit var presenter: SharePresenter

    private val fbResources = arrayOf(R.drawable.ll_circle_fb_gray, R.drawable.ll_circle_fb)
    private val twiResources = arrayOf(R.drawable.ll_circle_twitter_gray, R.drawable.ll_circle_twitter)

    override fun onCreate(savedInstanceState: Bundle?) {
        (activity as? MainActivity)?.socialComponent?.plus(ShareModule(this))?.injectTo(this)
        super.onCreate(savedInstanceState)
        episode = arguments.getParcelable(EXTRA)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
            = inflater.inflate(R.layout.dialog_share_episode, container, false)

    override fun onViewCreated(view: View, b: Bundle?) {
        super.onViewCreated(view, b)
        btnFacebook = view.findViewById(R.id.action_facebook)
        btnTwitter = view.findViewById(R.id.action_twitter)
        editText = view.findViewById(R.id.edit_text)
        ivSend = view.findViewById(R.id.btn_send)

        editText.addTextChangedListener(this)

        assignTo(btnFacebook, btnTwitter, ivSend)
    }

    override fun showMessage(message: ShowMessagePresentationView.Message) {
        val text = message.text
        if (text.isNotEmpty()) {
            Toast.makeText(context, message.text, Toast.LENGTH_SHORT).show()
        }
    }

    override fun onClick(v: View) {
        val id = v.id
        when (id) {
            R.id.action_twitter -> presenter.connect(id)
            R.id.action_facebook -> presenter.connect(id)
            R.id.btn_send -> presenter.send(editText.text.trim().toString())

            else -> return
        }
    }

    override fun changeSocialState(id: Int, enabled: Boolean) {
        val index = if (enabled) 1 else 0
        val res: Array<Int>
        val button: Button
        if (id == R.id.action_facebook) {
            res = fbResources
            button = btnFacebook
        } else {
            res = twiResources
            button = btnTwitter
        }

        with(button) {
            isActivated = enabled
            setCompoundDrawablesRelativeWithIntrinsicBounds(0, res[index], 0, 0)
        }
        validate(editText.text)
    }

    private fun validate(s: CharSequence?) {
        val notEmpty = !s?.trim().isNullOrEmpty()
        ivSend.changeVisibility(notEmpty && btnFacebook.isActivated || notEmpty && btnTwitter.isActivated)
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) = validate(s)
    override fun afterTextChanged(s: Editable?) {}
    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

    fun show(manager: FragmentManager?) = super.show(manager, ShareEpisodeSheetDialog::class.java.name)
}