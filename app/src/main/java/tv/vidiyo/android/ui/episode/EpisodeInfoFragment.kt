package tv.vidiyo.android.ui.episode

import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.LinearLayout
import android.widget.ProgressBar
import tv.vidiyo.android.DependencyGraph
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.episode.EpisodeInfo
import tv.vidiyo.android.api.model.show.Show
import tv.vidiyo.android.base.adapter.pages.PageFragment
import tv.vidiyo.android.base.adapter.pages.PagesAdapter
import tv.vidiyo.android.base.fragment.PagesMainFragment
import tv.vidiyo.android.base.view.holder.ViewHolder
import tv.vidiyo.android.di.subcomponent.episode.ChildEpisodeModule
import tv.vidiyo.android.domain.contract.EpisodeContract
import tv.vidiyo.android.domain.presenter.episode.EpisodeInfoPresenter
import tv.vidiyo.android.extension.changeVisibility
import tv.vidiyo.android.extension.isVisible
import tv.vidiyo.android.extension.openUrl
import tv.vidiyo.android.ui.adapter.DataRecyclerAdapter
import tv.vidiyo.android.ui.view.*
import tv.vidiyo.android.util.Animation
import javax.inject.Inject

class EpisodeInfoFragment : PagesMainFragment(), EpisodeContract.Info.View,
        ViewHolder.OnViewHolderClickListener, View.OnClickListener {

    companion object {
        private const val EXTRA = "EXTRA"//episodeId

        fun newInstance(id: Int) = EpisodeInfoFragment().apply {
            arguments = Bundle(1).also {
                it.putInt(EXTRA, id)
            }
        }
    }

    override val layoutResId get() = R.layout.fragment_episode_info
    override val appBarLayoutResId = R.layout.app_bar_episode_info
    override val statusBarColorId = R.color.black

    val isRefreshing: Boolean get() = progressBar.isIndeterminate

    private lateinit var recyclerView: RecyclerView
    private lateinit var descriptionView: ExpandableDescriptionView
    private lateinit var showView: ShowView
    private lateinit var progressBar: ProgressBar

    @Inject
    lateinit var presenter: EpisodeInfoPresenter

    private lateinit var productsAdapter: DataRecyclerAdapter
    private val episodeId: Int get() = arguments.getInt(EXTRA, 0)
    val productsOpen: Boolean get() = recyclerView.isVisible

    override fun injectDependencies(graph: DependencyGraph) {
        graph.getEpisodeComponent(episodeId).plus(ChildEpisodeModule(this)).injectTo(this)
    }

    override fun onCreate(b: Bundle?) {
        super.onCreate(b)
        productsAdapter = DataRecyclerAdapter.products(presenter.repository.productsProvider, this, true)
    }

    override fun onViewCreated(view: View, b: Bundle?) {
        super.onViewCreated(view, b)
        descriptionView = view.findViewById(R.id.description_view)
        showView = view.findViewById(R.id.show_view)
        recyclerView = view.findViewById(R.id.recycler_view)
        progressBar = view.findViewById(R.id.progress_bar)

        with(recyclerView) {
            adapter = productsAdapter
            addItemDecoration(SpaceItemDecoration(context))
            addItemDecoration(DividerItemDecoration(context, LinearLayout.VERTICAL))
            tag = false
        }
        viewPager.setBackgroundResource(R.color.lighter_gray)

        showView.button.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        if (!isRefreshing) presenter.toggleShowSubscription()
    }

    override fun onViewHolderClick(viewHolder: RecyclerView.ViewHolder, position: Int) {
        when (position) {
            0 -> changeProductsVisibility(false)
            else -> openUrl(presenter.getProduct(position - 1)?.url ?: return)
        }
    }

    override fun onPostViewCreated() {
        presenter.start()
    }

    override fun forceUpdateInnerViews() = (0 until adapter.count) // TODO: update only -1, current, +1 fragments
            .mapNotNull { (adapter.getFragment(it) as? InnerEpisodeInfoFragment) }
            .filter { it.isViewCreated }
            .forEach { it.onPostViewCreated() }

    override fun changeProductsVisibility(visibility: Boolean)
            = Animation.slideTo(recyclerView, visibility, false)

    override fun updateDescription(info: EpisodeInfo) {
        descriptionView.setEpisode(info)
        descriptionView.changeVisibility(true)
        productsAdapter.notifyDataSetChanged()
        recyclerView.translationY = -recyclerView.height.toFloat()
        recyclerView.visibility = View.GONE
    }

    override fun updateShowView(show: Show) {
        showView.setShow(show)
        showView.changeVisibility(true)
        tabLayout?.changeVisibility(true)
    }

    override fun setRefreshing(block: Boolean) {
        progressBar.isIndeterminate = block
        progressBar.changeVisibility(block)
    }

    override fun createAdapter(): PagesAdapter {
        val episodeId = episodeId
        return PagesAdapter(context, childFragmentManager,
                PageFragment(InnerEpisodeInfoFragment.episodesCreator(episodeId), R.string.episodes),
                PageFragment(InnerEpisodeInfoFragment.commentsCreator(episodeId), R.string.comments),
                PageFragment(InnerEpisodeInfoFragment.castsCreator(episodeId), R.string.cast),
                PageFragment(InnerEpisodeInfoFragment.topicsCreator(episodeId), R.string.topics),
                PageFragment(InnerEpisodeInfoFragment.subscribersCreator(episodeId), R.string.subscribers)
        )
    }
}