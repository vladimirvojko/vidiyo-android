package tv.vidiyo.android.ui.view

import android.content.Context
import android.support.v4.widget.NestedScrollView
import android.util.AttributeSet
import android.view.View
import android.widget.Button
import android.widget.TextView
import tv.vidiyo.android.R

class ErrorRetryView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0
) : NestedScrollView(context, attrs, defStyleAttr) {

    private val tvMessage: TextView
    private val btnRetry: Button

    private var listener : RetryListener? = null

    init {
        View.inflate(context, R.layout.view_error_retry, this)

        tvMessage = findViewById(R.id.tv_message)
        btnRetry = findViewById(R.id.btn_retry)

        btnRetry.setOnClickListener { listener?.retry() }
    }

    fun setMessage(message: String?) {
        tvMessage.text = message
    }

    fun setMessage(resId: Int) = setMessage(context.getString(resId))

    fun setRetryListener(listener: RetryListener) {
        this.listener = listener
    }

    interface RetryListener {
        fun retry()
    }
}