package tv.vidiyo.android.ui.boarding.signup

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v7.app.ActionBar
import android.view.View
import tv.vidiyo.android.DependencyGraph
import tv.vidiyo.android.R
import tv.vidiyo.android.di.subcomponent.boarding.BoardingModule
import tv.vidiyo.android.domain.contract.startMainImpl
import tv.vidiyo.android.extension.assignTo
import tv.vidiyo.android.ui.boarding.BaseSocialFragment

class SocialFragment : BaseSocialFragment() {

    override val layoutResId = R.layout.fragment_sign_up_social

    override fun injectDependencies(graph: DependencyGraph) {
        graph.applicationComponent.plus(BoardingModule(this)).injectTo(this)
    }

    override fun onViewCreated(view: View, b: Bundle?) {
        super.onViewCreated(view, b)
        assignTo(view.findViewById(R.id.btn_facebook), view.findViewById(R.id.btn_gplus))
    }

    @SuppressLint("MissingSuperCall")
    override fun onActionBarReady(bar: ActionBar) {
    }

    override fun showError(text: String?, code: Int) {
        processError(text, code)
    }

    override fun startMain() = startMainImpl()
}