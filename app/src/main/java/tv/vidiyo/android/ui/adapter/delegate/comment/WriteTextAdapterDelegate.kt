package tv.vidiyo.android.ui.adapter.delegate.comment

import android.view.ViewGroup
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.comment.Comment
import tv.vidiyo.android.api.model.user.UserAccount
import tv.vidiyo.android.base.adapter.delegate.SectionedAdapterDelegate
import tv.vidiyo.android.base.adapter.provider.SectionedDataProvider
import tv.vidiyo.android.base.adapter.section.ItemPosition
import tv.vidiyo.android.extension.load
import tv.vidiyo.android.ui.view.holder.OnCommentClickListener
import tv.vidiyo.android.ui.view.holder.WriteTextViewHolder

class WriteTextAdapterDelegate(
        provider: SectionedDataProvider<Comment, Comment>,
        listener: OnCommentClickListener?,
        private val account: UserAccount?
) : SectionedAdapterDelegate<Comment, Comment, SectionedDataProvider<Comment, Comment>, OnCommentClickListener, WriteTextViewHolder>(provider, listener) {

    override fun onCreateViewHolder(parent: ViewGroup) = WriteTextViewHolder(parent, listener)
    override fun onBindViewHolder(position: ItemPosition, holder: WriteTextViewHolder, provider: SectionedDataProvider<Comment, Comment>) {
        holder.ivAvatar.load(account?.image ?: R.drawable.ic_default_profile)
    }
    override fun isForViewType(itemPosition: ItemPosition) = itemPosition.isHeader && itemPosition.section == 0
}