package tv.vidiyo.android.ui.channels

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.MenuItem
import tv.vidiyo.android.DependencyGraph
import tv.vidiyo.android.R
import tv.vidiyo.android.base.fragment.RecyclerMainFragment
import tv.vidiyo.android.di.subcomponent.channels.ChannelsModule
import tv.vidiyo.android.domain.contract.ChannelsContract
import tv.vidiyo.android.domain.presenter.channels.ChannelsAdapter
import tv.vidiyo.android.domain.presenter.channels.ChannelsPresenter
import tv.vidiyo.android.source.channels.ChannelsMode
import tv.vidiyo.android.ui.view.SpaceItemDecoration
import tv.vidiyo.android.ui.view.holder.ChannelViewHolder
import tv.vidiyo.android.ui.widget.InfiniteScrollRecyclerView
import javax.inject.Inject


class EditChannelsFragment : RecyclerMainFragment<ChannelsAdapter>(), ChannelsContract.View {

    override val toolbarTitleRes get() = R.string.channel_select_channels
    override val toolbarIndicator get() = R.drawable.svg_back

    override val optionsMenuResId get() = R.menu.edit_channels

    @Inject
    lateinit var presenter: ChannelsPresenter

    override fun injectDependencies(graph: DependencyGraph) {
        graph.dataComponent.plus(ChannelsModule(this)).injectTo(this)
    }

    override fun onPostViewCreated() {
        presenter.start()
    }

    override fun onCreateAdapter(): ChannelsAdapter = ChannelsAdapter(this, presenter.provider, ChannelsMode.EDIT)

    override fun onCreateLayoutManager(context: Context?) = GridLayoutManager(context, 2)

    override fun onRecyclerViewCreated(recyclerView: InfiniteScrollRecyclerView) {
        super.onRecyclerViewCreated(recyclerView)
        recyclerView.addItemDecoration(SpaceItemDecoration(context, true, true))
    }

    override fun onMenuItemClick(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.item_confirm -> {
                presenter.assignChannels()
            }
            else -> return super.onMenuItemClick(item)
        }
        return true
    }

    override fun onViewHolderClick(viewHolder: RecyclerView.ViewHolder, position: Int) {
        if (viewHolder is ChannelViewHolder) {
            viewHolder.checkbox.apply {
                isChecked = !isChecked
            }
        }
        presenter.changeSelection(position)
    }

    override fun closeEditing() = onNavigationIconPressed()
}