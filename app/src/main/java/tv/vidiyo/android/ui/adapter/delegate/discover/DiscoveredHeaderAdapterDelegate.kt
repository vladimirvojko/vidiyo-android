package tv.vidiyo.android.ui.adapter.delegate.discover

import android.view.ViewGroup
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.user.FollowingUser
import tv.vidiyo.android.base.adapter.delegate.ListProviderAdapterDelegate
import tv.vidiyo.android.base.adapter.provider.ListDataProvider
import tv.vidiyo.android.base.view.holder.ViewHolder
import tv.vidiyo.android.ui.view.holder.DiscoveredHeaderViewHolder

class DiscoveredHeaderAdapterDelegate(
        provider: ListDataProvider<FollowingUser>,
        listener: ViewHolder.OnViewHolderClickListener?
) : ListProviderAdapterDelegate<FollowingUser, ViewHolder.OnViewHolderClickListener, DiscoveredHeaderViewHolder>(provider, listener) {

    override fun onCreateViewHolder(parent: ViewGroup) = DiscoveredHeaderViewHolder(parent, listener)

    override fun onBindViewHolder(position: Int, holder: DiscoveredHeaderViewHolder, provider: ListDataProvider<FollowingUser>, item: FollowingUser) {
        val size = provider.getItemCount() - 1
        holder.tvTitle.text = holder.context.resources.getQuantityString(R.plurals.format_d_friends_on_vidiyo, size, size)
    }

    override fun isForViewType(position: Int, provider: ListDataProvider<FollowingUser>) = position == 0
}