package tv.vidiyo.android.ui.cast

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import tv.vidiyo.android.DependencyGraph
import tv.vidiyo.android.base.adapter.pages.PageFragment
import tv.vidiyo.android.di.subcomponent.cast.ChildCastModule
import tv.vidiyo.android.domain.contract.CastContract
import tv.vidiyo.android.domain.presenter.cast.InnerCastPresenter
import tv.vidiyo.android.model.InnerPageType
import tv.vidiyo.android.ui.adapter.DataRecyclerAdapter
import tv.vidiyo.android.ui.profile.BaseInnerProfileFragment
import tv.vidiyo.android.ui.widget.InfiniteScrollRecyclerView
import tv.vidiyo.android.ui.widget.OnInfiniteScrollListener
import javax.inject.Inject

class InnerCastFragment : BaseInnerProfileFragment(), CastContract.Inner.View {

    companion object {
        private fun newInstance(@InnerPageType type: Int) = InnerCastFragment().apply {
            arguments = Bundle(1).also {
                it.putInt(EXTRA, type)
            }
        }

        fun showsCreator() = object : PageFragment.Creator {
            override fun newInstance(): Fragment = InnerCastFragment.newInstance(InnerPageType.SHOWS)
        }

        fun episodeCreator() = object : PageFragment.Creator {
            override fun newInstance(): Fragment = InnerCastFragment.newInstance(InnerPageType.EPISODES)
        }

        fun connectShareCreator() = object : PageFragment.Creator {
            override fun newInstance(): Fragment = InnerCastFragment.newInstance(InnerPageType.CONNECT_AND_SHARE)
        }
    }

    @Inject
    lateinit var presenter: InnerCastPresenter

    override fun injectDependencies(graph: DependencyGraph) {
        type = arguments.getInt(EXTRA, InnerPageType.SHOWS)
        (parentFragment as? CastFragment)?.component?.plus(ChildCastModule(this))?.injectTo(this)
    }

    override fun onPostViewCreated() {
        presenter.start()
    }

    override fun <T> getItem(at: Int): T? = presenter.get(at)

    override fun onSubscribeClick(viewHolder: RecyclerView.ViewHolder, position: Int) {
        if (refreshLayout?.isRefreshing == false)
            presenter.toggleSubscription(position)
    }

    override fun onCreateAdapter(): DataRecyclerAdapter {
        val repository = presenter.main
        return when (type) {
            InnerPageType.EPISODES -> DataRecyclerAdapter.episodes(repository.episodesRepository.provider, this)
            InnerPageType.SHOWS -> DataRecyclerAdapter.subscribableShow(repository.showsRepository.provider, null, this)
            InnerPageType.CONNECT_AND_SHARE -> DataRecyclerAdapter.connection(repository.socialsRepository.provider, this)

            else -> throw IllegalArgumentException("Wrong type provided")
        }
    }

    override fun onCreateLayoutManager(context: Context?) = when (type) {
        InnerPageType.CONNECT_AND_SHARE -> GridLayoutManager(context, 3)
        else -> super.onCreateLayoutManager(context)
    }

    override fun onSelected() = presenter.refresh()
    override fun retry() = presenter.refresh()

    override fun onRecyclerViewCreated(recyclerView: InfiniteScrollRecyclerView) {
        super.onRecyclerViewCreated(recyclerView)
        when (type) {
            InnerPageType.EPISODES,
            InnerPageType.SHOWS -> recyclerView.listener = scrollListener()
        }
    }

    private fun scrollListener() = object : OnInfiniteScrollListener {
        override fun onInfiniteScroll(recyclerView: InfiniteScrollRecyclerView) {
            if (recyclerView.shouldLoadMore(presenter.totalCount)) {
                presenter.loadMore()
            }
        }
    }
}