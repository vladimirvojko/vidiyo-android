package tv.vidiyo.android.ui.adapter.delegate

import android.view.ViewGroup
import tv.vidiyo.android.api.model.cast.Cast
import tv.vidiyo.android.base.adapter.delegate.ListProviderAdapterDelegate
import tv.vidiyo.android.base.adapter.provider.ListDataProvider
import tv.vidiyo.android.base.view.holder.ViewHolder
import tv.vidiyo.android.extension.load
import tv.vidiyo.android.source.DataType
import tv.vidiyo.android.ui.view.holder.CastSmallViewHolder

class CastSmallAdapterDelegate(
        provider: ListDataProvider<Cast>,
        listener: ViewHolder.OnViewHolderClickListener?
) : ListProviderAdapterDelegate<Cast, ViewHolder.OnViewHolderClickListener, CastSmallViewHolder>(provider, listener) {

    override fun onCreateViewHolder(parent: ViewGroup) = CastSmallViewHolder(parent, listener).also {
        it.itemView.tag = DataType.TYPE_CASTS_SMALL
    }

    override fun onBindViewHolder(position: Int, holder: CastSmallViewHolder, provider: ListDataProvider<Cast>, item: Cast) {
        holder.tvName.text = item.name
        holder.ivPhoto.load(item.image)
    }
}