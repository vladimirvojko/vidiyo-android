package tv.vidiyo.android.ui.boarding

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.GraphRequest
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.social.SocialAuth
import tv.vidiyo.android.base.fragment.BaseFragment
import tv.vidiyo.android.domain.contract.SignUpContract
import tv.vidiyo.android.domain.presenter.boarding.SocialPresenter
import tv.vidiyo.android.ui.boarding.signup.NameFragment
import tv.vidiyo.android.util.ALog
import javax.inject.Inject

abstract class BaseSocialFragment : BaseFragment(), SignUpContract.Social.View,
        View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {

    companion object {
        const val REQUEST_CODE = 0xFFA //google
    }

    val loginManager: LoginManager = LoginManager.getInstance()
    val callbackManager: CallbackManager = CallbackManager.Factory.create()

    @Inject lateinit var apiClient: GoogleApiClient
    @Inject lateinit var socialPresenter: SocialPresenter

    override fun onStart() {
        super.onStart()
        apiClient.connect()
    }

    override fun onStop() {
        super.onStop()
        apiClient.disconnect()
    }

    override fun onCreate(b: Bundle?) {
        super.onCreate(b)
        apiClient.connect()
        loginManager.registerCallback(callbackManager, callback)
    }

    override fun withGoogle() {
        if (apiClient.isConnected) {
            Auth.GoogleSignInApi.signOut(apiClient).setResultCallback {
                val intent = Auth.GoogleSignInApi.getSignInIntent(apiClient)
                startActivityForResult(intent, REQUEST_CODE)
            }
        } else {
            apiClient.connect()
            Toast.makeText(context, R.string.error_cant_connect_google, Toast.LENGTH_SHORT).show()
        }
    }

    override fun withFacebook() {
        loginManager.logOut()
        loginManager.logInWithReadPermissions(this, arrayListOf("public_profile", "user_friends", "email"))
    }

    override fun startNameEnter(socialAuth: SocialAuth) {
        startFragment(NameFragment.newInstance(socialAuth), true)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_gplus -> withGoogle()
            R.id.btn_facebook -> withFacebook()

            else -> return
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (callbackManager.onActivityResult(requestCode, resultCode, data)) {
            return
        }

        when (requestCode) {
            REQUEST_CODE -> {
                val result = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
                if (result.isSuccess) {
                    val account = result.signInAccount
                    account?.let {
                        val social = SocialAuth(it)
                        socialPresenter.process(social)
                    }
                }
            }

            else -> super.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        ALog.d("onConnectionFailed: $p0")
    }

    private val callback = object : FacebookCallback<LoginResult> {
        override fun onSuccess(result: LoginResult?) {
            val token = result?.accessToken ?: return

            GraphRequest.newMeRequest(token) { obj, _ ->
                socialPresenter.process(SocialAuth(token, obj))

            }.apply {
                parameters = Bundle(1).also {
                    it.putString("fields", "id,name,email,about,birthday,gender,website,picture.width(480)")
                }
            }.executeAsync()
        }

        override fun onCancel() {
            ALog.d("onCancel")
        }

        override fun onError(error: FacebookException?) {
            ALog.d("onError = $error")
        }
    }
}