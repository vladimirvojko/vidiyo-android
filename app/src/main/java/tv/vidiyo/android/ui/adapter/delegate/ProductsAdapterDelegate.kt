package tv.vidiyo.android.ui.adapter.delegate

import android.view.ViewGroup
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.episode.Product
import tv.vidiyo.android.base.adapter.delegate.ListProviderAdapterDelegate
import tv.vidiyo.android.base.adapter.provider.ListDataProvider
import tv.vidiyo.android.base.view.holder.ViewHolder
import tv.vidiyo.android.ui.view.holder.ProductViewHolder
import tv.vidiyo.android.ui.view.holder.setProduct

class ProductsAdapterDelegate(
        provider: ListDataProvider<Product>,
        listener: ViewHolder.OnViewHolderClickListener?,
        private val isFullscreen: Boolean
) : ListProviderAdapterDelegate<Product, ViewHolder.OnViewHolderClickListener, ProductViewHolder>(provider, listener) {

    override fun onCreateViewHolder(parent: ViewGroup) = ProductViewHolder(parent, listener,
            if (isFullscreen) R.layout.view_item_product_white else R.layout.view_item_product
    )

    override fun onBindViewHolder(position: Int, holder: ProductViewHolder, provider: ListDataProvider<Product>, item: Product)
            = holder.setProduct(item)
}