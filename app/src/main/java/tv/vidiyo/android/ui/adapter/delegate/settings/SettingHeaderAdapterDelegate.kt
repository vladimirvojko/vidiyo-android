package tv.vidiyo.android.ui.adapter.delegate.settings

import android.view.ViewGroup
import tv.vidiyo.android.api.model.Header
import tv.vidiyo.android.api.model.Item
import tv.vidiyo.android.base.adapter.delegate.SectionedAdapterDelegate
import tv.vidiyo.android.base.adapter.provider.SectionedDataProvider
import tv.vidiyo.android.base.adapter.section.ItemPosition
import tv.vidiyo.android.base.view.holder.ViewHolder
import tv.vidiyo.android.ui.view.holder.SettingHeaderViewHolder
import tv.vidiyo.android.ui.view.holder.setHeader

class SettingHeaderAdapterDelegate(
        provider: SectionedDataProvider<Header, Item>
) : SectionedAdapterDelegate<Header, Item, SectionedDataProvider<Header, Item>,
        ViewHolder.OnViewHolderClickListener, SettingHeaderViewHolder>(provider, null) {

    override fun onCreateViewHolder(parent: ViewGroup) = SettingHeaderViewHolder(parent)

    override fun onBindViewHolder(position: ItemPosition, holder: SettingHeaderViewHolder, provider: SectionedDataProvider<Header, Item>) {
        holder.setHeader(provider.getSection(position.section)?.section ?: return)
    }

    override fun isForViewType(itemPosition: ItemPosition): Boolean = itemPosition.isHeader
}