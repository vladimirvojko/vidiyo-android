package tv.vidiyo.android.ui.view.holder

import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.ConnectionCount
import tv.vidiyo.android.base.view.holder.ViewHolder
import java.lang.ref.WeakReference

class ConnectViewHolder(
        parent: ViewGroup,
        listener: WeakReference<OnViewHolderClickListener>?
) : ViewHolder<ViewHolder.OnViewHolderClickListener>(parent, listener, R.layout.view_item_connect, true) {

    private val ivIcon = itemView.findViewById<ImageView>(R.id.iv_icon)
    private val tvTitle = itemView.findViewById<TextView>(R.id.tv_title)
    private val tvSubtitle = itemView.findViewById<TextView>(R.id.tv_subtitle)



    fun set(connection: ConnectionCount, type: Int) {
        val (active, inactive, title) = if (type == 0)
            Triple(R.drawable.svg_facebook_24, R.drawable.svg_facebook_24_gray, R.string.boarding_connect_facebook)
        else
            Triple(R.drawable.svg_contacts_24, R.drawable.svg_contacts_24_gray, R.string.boarding_connect_contacts)

        tvTitle.setText(title)
        if (connection.connected) {
            ivIcon.setImageResource(active)
            tvSubtitle.text = context.getString(R.string.format_d_friends, connection.count)
        } else {
            ivIcon.setImageResource(inactive)
            tvSubtitle.setText(R.string.to_follow_your_friends)
        }
    }
}