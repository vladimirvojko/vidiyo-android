package tv.vidiyo.android.ui.view.holder

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import android.widget.TextView
import tv.vidiyo.android.R
import tv.vidiyo.android.base.view.holder.ViewHolder
import tv.vidiyo.android.ui.view.StateButton
import java.lang.ref.WeakReference

interface OnConnectionButtonClickListener {
    fun onConnectionButtonClicked(holder: RecyclerView.ViewHolder, position: Int)
}

class ConnectionButtonViewHolder(
        parent: ViewGroup,
        listener: WeakReference<OnConnectionButtonClickListener>?
) : ViewHolder<OnConnectionButtonClickListener>(parent, null, R.layout.view_item_social_connect_button, false) {

    val textView: TextView = itemView.findViewById(R.id.text)
    val button: StateButton = itemView.findViewById(R.id.button)

    init {
        button.setOnClickListener {
            listener?.get()?.onConnectionButtonClicked(this, adapterPosition)
        }
    }
}