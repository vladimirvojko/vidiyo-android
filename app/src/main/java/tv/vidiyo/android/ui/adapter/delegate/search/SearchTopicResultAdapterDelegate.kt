package tv.vidiyo.android.ui.adapter.delegate.search

import android.view.ViewGroup
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.topic.Topic
import tv.vidiyo.android.base.adapter.delegate.ListProviderAdapterDelegate
import tv.vidiyo.android.base.adapter.provider.ListDataProvider
import tv.vidiyo.android.base.view.holder.ViewHolder
import tv.vidiyo.android.domain.contract.setTopic
import tv.vidiyo.android.ui.view.holder.SearchResultViewHolder

class SearchTopicResultAdapterDelegate(
        provider: ListDataProvider<Topic>,
        listener: ViewHolder.OnViewHolderClickListener?
) : ListProviderAdapterDelegate<Topic, ViewHolder.OnViewHolderClickListener, SearchResultViewHolder>(provider, listener) {

    override fun onCreateViewHolder(parent: ViewGroup) = SearchResultViewHolder(parent, listener, R.layout.view_item_searching_topic)
    override fun onBindViewHolder(position: Int, holder: SearchResultViewHolder, provider: ListDataProvider<Topic>, item: Topic)
            = holder.setTopic(item)
}