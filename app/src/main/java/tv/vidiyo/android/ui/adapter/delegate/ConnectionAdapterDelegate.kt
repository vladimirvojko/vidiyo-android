package tv.vidiyo.android.ui.adapter.delegate

import android.view.ViewGroup
import tv.vidiyo.android.api.model.social.SocialConnection
import tv.vidiyo.android.base.adapter.delegate.ListProviderAdapterDelegate
import tv.vidiyo.android.base.adapter.provider.ListDataProvider
import tv.vidiyo.android.base.view.holder.ViewHolder
import tv.vidiyo.android.ui.view.holder.ConnectionViewHolder
import tv.vidiyo.android.ui.view.holder.set

class ConnectionAdapterDelegate(
        provider: ListDataProvider<SocialConnection>,
        listener: ViewHolder.OnViewHolderClickListener?
) : ListProviderAdapterDelegate<SocialConnection, ViewHolder.OnViewHolderClickListener,
        ConnectionViewHolder>(provider, listener) {

    override fun onCreateViewHolder(parent: ViewGroup) = ConnectionViewHolder(parent, listener)

    override fun onBindViewHolder(position: Int, holder: ConnectionViewHolder, provider: ListDataProvider<SocialConnection>,
                                  item: SocialConnection) = holder.set(item)
}