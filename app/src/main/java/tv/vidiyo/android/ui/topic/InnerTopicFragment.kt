package tv.vidiyo.android.ui.topic

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.widget.LinearLayout
import tv.vidiyo.android.DependencyGraph
import tv.vidiyo.android.R
import tv.vidiyo.android.base.adapter.RecyclerAdapter
import tv.vidiyo.android.base.adapter.pages.PageFragment
import tv.vidiyo.android.base.fragment.StateRecyclerFragment
import tv.vidiyo.android.di.subcomponent.topic.ChildTopicModule
import tv.vidiyo.android.domain.contract.TopicContract
import tv.vidiyo.android.domain.presenter.topic.InnerTopicPresenter
import tv.vidiyo.android.ui.adapter.DataRecyclerAdapter
import tv.vidiyo.android.ui.show.ShowFragment
import tv.vidiyo.android.ui.view.DividerItemDecoration
import tv.vidiyo.android.ui.view.SpaceItemDecoration
import tv.vidiyo.android.ui.view.holder.OnSubscribableViewHolderClickListener
import tv.vidiyo.android.ui.widget.InfiniteScrollRecyclerView
import javax.inject.Inject

class InnerTopicFragment : StateRecyclerFragment<RecyclerAdapter>(), TopicContract.Inner.View,
        OnSubscribableViewHolderClickListener {

    companion object {
        private const val EXTRA_0 = "EXTRA_0"//index

        private fun newInstance(index: Int) = InnerTopicFragment().apply {
            arguments = Bundle().also {
                it.putInt(EXTRA_0, index)
            }
        }

        fun topicCreator(index: Int) = object : PageFragment.Creator {
            override fun newInstance(): Fragment = InnerTopicFragment.newInstance(index)
        }
    }

    override val layoutResId: Int = R.layout.fragment_state_recycler_empty
    override var index: Int = 0

    @Inject
    lateinit var presenter: InnerTopicPresenter

    override fun onCreate(b: Bundle?) {
        index = arguments.getInt(EXTRA_0, 0)
        super.onCreate(b)
    }


    override fun injectDependencies(graph: DependencyGraph) {
        (parentFragment as? TopicFragment)?.component?.plus(ChildTopicModule(this))?.injectTo(this)
    }

    override fun onPostViewCreated() {
        presenter.start()
    }

    override fun onCreateAdapter(): RecyclerAdapter = DataRecyclerAdapter.subscribableShow(
            presenter.getProvider(index), presenter.channel, this)

    override fun onRecyclerViewCreated(recyclerView: InfiniteScrollRecyclerView) {
        super.onRecyclerViewCreated(recyclerView)
        recyclerView.addItemDecoration(SpaceItemDecoration(context, true, true))
        recyclerView.addItemDecoration(DividerItemDecoration(context, LinearLayout.VERTICAL))
    }

    override fun onViewHolderClick(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val show = presenter.get(position) ?: return
        startFragment(ShowFragment.newInstance(show))
    }

    override fun onSubscribeClick(viewHolder: RecyclerView.ViewHolder, position: Int) {
        if (refreshLayout?.isRefreshing == false)
            presenter.toggleSubscription(position)
    }

    override fun onSelected() = presenter.refresh()
    override fun retry() = presenter.refresh()
}