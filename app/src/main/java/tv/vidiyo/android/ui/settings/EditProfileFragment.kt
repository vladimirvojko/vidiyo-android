package tv.vidiyo.android.ui.settings

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.support.v7.app.AlertDialog
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.view.View
import android.widget.DatePicker
import android.widget.EditText
import android.widget.TextView
import tv.vidiyo.android.DependencyGraph
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.user.UserAccount
import tv.vidiyo.android.base.fragment.BaseFragment
import tv.vidiyo.android.di.subcomponent.settings.SettingsModule
import tv.vidiyo.android.domain.contract.SettingsContract
import tv.vidiyo.android.domain.presenter.settings.EditProfilePresenter
import tv.vidiyo.android.extension.MM_dd_YYYY
import tv.vidiyo.android.extension.assignTo
import tv.vidiyo.android.extension.load
import tv.vidiyo.android.os.AsyncTaskListener
import tv.vidiyo.android.os.CropAndSave
import tv.vidiyo.android.ui.dialog.ChangePhotoSheetDialog
import tv.vidiyo.android.ui.dialog.LocationDialog
import tv.vidiyo.android.ui.view.ShapeImageView
import tv.vidiyo.android.util.Constant
import java.util.*
import javax.inject.Inject

class EditProfileFragment : BaseFragment(), SettingsContract.Edit.View, View.OnClickListener,
        TextWatcher, AsyncTaskListener<String>, DatePickerDialog.OnDateSetListener {

    companion object {
        private const val _120_YEARS = 3786834240000
    }

    override val toolbarIndicator = R.drawable.svg_back
    override val toolbarTitleRes = R.string.edit_profile
    override val layoutResId = R.layout.fragment_edit_profile

    override val optionsMenuResId = R.menu.edit_profile

    private lateinit var ivAvatar: ShapeImageView
    private lateinit var etName: EditText
    private lateinit var etUsername: EditText
    private lateinit var etBio: EditText

    private lateinit var tvGender: TextView
    private lateinit var tvBirthday: TextView
    private lateinit var tvLocation: TextView

    private lateinit var etEmail: EditText
    private lateinit var etPhone: EditText
    private lateinit var etWebsite: EditText

    @Inject
    lateinit var presenter: EditProfilePresenter

    private var genderArray = arrayOf<String>()
    private val currentTimeMillis = System.currentTimeMillis()
    private val calendar = Calendar.getInstance().also {
        it.timeInMillis = currentTimeMillis
    }

    override fun injectDependencies(graph: DependencyGraph) {
        genderArray = resources.getStringArray(R.array.gender_array)
        graph.dataComponent.plus(SettingsModule(this)).injectTo(this)
    }

    override fun onViewCreated(view: View, b: Bundle?) {
        super.onViewCreated(view, b)
        ivAvatar = view.findViewById(R.id.iv_avatar)
        etName = view.findViewById(R.id.et_name)
        etUsername = view.findViewById(R.id.et_username)
        etBio = view.findViewById(R.id.et_bio)

        tvGender = view.findViewById(R.id.tv_gender)
        tvBirthday = view.findViewById(R.id.tv_birthday)
        tvLocation = view.findViewById(R.id.tv_location)

        etEmail = view.findViewById(R.id.et_email)
        etPhone = view.findViewById(R.id.et_phone)
        etWebsite = view.findViewById(R.id.et_website)

        assignTo(ivAvatar, tvGender, tvBirthday, tvLocation)

        etName.addTextChangedListener(this)
        etUsername.addTextChangedListener(this)
        etBio.addTextChangedListener(this)
        etEmail.addTextChangedListener(this)
        etPhone.addTextChangedListener(this)
        etWebsite.addTextChangedListener(this)
    }

    override fun onPostViewCreated() {
        presenter.start()
    }

    override fun updateWith(account: UserAccount) {
        ivAvatar.load(account.image)
        etName.setText(account.name)
        etUsername.setText(account.username)
        etBio.setText(account.bio)

        tvBirthday.text = account.birthday
        tvLocation.text = account.location
        tvGender.text = account.gender?.capitalize()

        etEmail.setText(account.email)
        etPhone.setText(account.phone)
        etWebsite.setText(account.website)
    }

    override fun updateLocation(with: String) {
        tvLocation.text = with
        presenter.updateField(tvLocation.id, with)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.item_confirm -> presenter.save()

            else -> return super.onOptionsItemSelected(item)
        }
        return true
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.iv_avatar -> ChangePhotoSheetDialog.newInstance().show(childFragmentManager)
            R.id.tv_location -> LocationDialog.show(this, true)
            R.id.tv_birthday -> DatePickerDialog(context, R.style.AppTheme_AlertDialog, this,
                    calendar.get(Calendar.YEAR),
                    calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH)
            ).also {
                it.datePicker.maxDate = currentTimeMillis
                it.datePicker.minDate = currentTimeMillis - _120_YEARS

            }.show()
            R.id.tv_gender -> AlertDialog.Builder(context, R.style.AppTheme_AlertDialog)
                    .setTitle(R.string.settings_field_gender)
                    .setItems(genderArray) { _, i ->
                        tvGender.text = genderArray[i].also {
                            presenter.updateField(R.id.tv_gender, it)
                        }
                    }.create().show()

            else -> return
        }
    }

    override fun onDateSet(picker: DatePicker, year: Int, month: Int, day: Int) {
        calendar.set(year, month, day)
        tvBirthday.text = MM_dd_YYYY.format(calendar.timeInMillis).also {
            presenter.updateField(R.id.tv_birthday, it)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when {
            requestCode == ChangePhotoSheetDialog.REQUEST_CODE && resultCode == Activity.RESULT_OK -> {
                data ?: return

                setRefreshing(true)
                (data.extras?.get(Constant.DATA) as? Bitmap)?.let {
                    executeAsync(it)
                    return
                }

                val bitmap = MediaStore.Images.Media.getBitmap(context.contentResolver, data.data)
                executeAsync(bitmap)
            }

            else -> super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun executeAsync(bitmap: Bitmap) {
        CropAndSave(context, this, bitmap, 512).execute()
    }

    override fun onTaskComplete(result: String?) {
        setRefreshing(false)
        ivAvatar.load(result)
        presenter.updateField(ivAvatar.id, result)
    }

    private fun findId(value: Editable?, vararg views: EditText)
            = views.firstOrNull { value === it.editableText }?.id ?: 0

    override fun afterTextChanged(value: Editable?) {
        presenter.updateField(findId(value, etName, etUsername, etBio, etEmail, etPhone, etWebsite),
                value?.toString())
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
}