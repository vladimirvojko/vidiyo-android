package tv.vidiyo.android.ui.view

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import android.widget.TextView
import tv.vidiyo.android.R
import tv.vidiyo.android.extension.changeVisibility
import tv.vidiyo.android.util.Animation

class BadgeView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    private val tvCounter: TextView

    init {
        inflate(context, R.layout.view_badge, this)
        tvCounter = findViewById(R.id.tv_counter)
    }

    fun setCount(count: Int, withPlus: Boolean = true) = setText(when (count) {
        0 -> null
        else -> if (withPlus) "+$count" else count.toString()
    })


    fun setText(text: String?) {
        if (text != null) {
            tvCounter.text = text
            changeVisibility(true)
        } else {
            changeVisibility(false)
            postDelayed({ tvCounter.text = null }, Animation.DELAY)
        }
    }

    fun changeVisibility(visible: Boolean) {
        val notEmpty = tvCounter.text.isNotEmpty()
        changeVisibility(visible && notEmpty, View.INVISIBLE)
    }
}