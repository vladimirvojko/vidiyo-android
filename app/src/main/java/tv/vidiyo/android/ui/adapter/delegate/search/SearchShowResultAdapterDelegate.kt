package tv.vidiyo.android.ui.adapter.delegate.search

import android.view.ViewGroup
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.show.SearchingShow
import tv.vidiyo.android.base.adapter.delegate.ListProviderAdapterDelegate
import tv.vidiyo.android.base.adapter.provider.ListDataProvider
import tv.vidiyo.android.base.view.holder.ViewHolder.OnViewHolderClickListener
import tv.vidiyo.android.domain.contract.setSearchingShow
import tv.vidiyo.android.ui.view.holder.SearchResultViewHolder

class SearchShowResultAdapterDelegate(
        provider: ListDataProvider<SearchingShow>,
        listener: OnViewHolderClickListener?
) : ListProviderAdapterDelegate<SearchingShow, OnViewHolderClickListener, SearchResultViewHolder>(provider, listener) {

    override fun onCreateViewHolder(parent: ViewGroup) = SearchResultViewHolder(parent, listener, R.layout.view_item_searching_show)
    override fun onBindViewHolder(position: Int, holder: SearchResultViewHolder, provider: ListDataProvider<SearchingShow>, item: SearchingShow)
            = holder.setSearchingShow(item)
}