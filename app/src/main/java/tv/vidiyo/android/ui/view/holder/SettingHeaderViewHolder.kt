package tv.vidiyo.android.ui.view.holder

import android.view.ViewGroup
import android.widget.TextView
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.Header
import tv.vidiyo.android.base.view.holder.ViewHolder

class SettingHeaderViewHolder(parent: ViewGroup)
    : ViewHolder<ViewHolder.OnViewHolderClickListener>(parent, null, R.layout.view_item_settings_header, true) {

    val textView: TextView = itemView as TextView

    init {
        textView.tag = false
    }
}

fun SettingHeaderViewHolder.setHeader(header: Header) {
    if (header.textResId != 0) {
        textView.setText(header.textResId)
    } else {
        textView.text = null
    }
}