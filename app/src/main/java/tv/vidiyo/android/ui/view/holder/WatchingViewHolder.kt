package tv.vidiyo.android.ui.view.holder

import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.episode.ViewInfo
import tv.vidiyo.android.api.model.show.NewShowEpisode
import tv.vidiyo.android.base.view.holder.ViewHolder
import tv.vidiyo.android.extension.changeVisibility
import tv.vidiyo.android.extension.load
import tv.vidiyo.android.extension.toTime
import java.lang.ref.WeakReference

interface OnWatchingClickListener {
    fun onViewClicked(position: Int)
    fun onShowClicked(position: Int)
    fun onMoreClicked(position: Int)
}

class WatchingViewHolder(
        parent: ViewGroup,
        listener: WeakReference<OnWatchingClickListener>? = null,
        forNewEpisodes: Boolean = true
) : ViewHolder<OnWatchingClickListener>(parent, listener, R.layout.view_item_watching) {

    val ivImage: ImageView = itemView.findViewById(R.id.iv_image)
    val tvTitle: TextView = itemView.findViewById(R.id.tv_title)
    val tvDuration: TextView = itemView.findViewById(R.id.tv_duration)

    val ivShow: ImageView = itemView.findViewById(R.id.iv_show)
    val tvEpisodeNumber: TextView = itemView.findViewById(R.id.tv_episode_number)
    val tvShowName: TextView = itemView.findViewById(R.id.tv_show_name)
    val tvDescription: TextView = itemView.findViewById(R.id.tv_description)

    private val tvNew: TextView = itemView.findViewById(R.id.tv_new)

    init {
        val l = View.OnClickListener { v ->
            listener?.get()?.let {
                when (v.id) {
                    R.id.iv_image -> it.onViewClicked(adapterPosition)
                    R.id.iv_more -> it.onMoreClicked(adapterPosition)
                    else -> it.onShowClicked(adapterPosition)
                }
            }
        }

        ivImage.setOnClickListener(l)
        itemView.setOnClickListener(l)
        itemView.findViewById<View>(R.id.iv_more).setOnClickListener(l)

        tvNew.changeVisibility(forNewEpisodes)
        tvDuration.changeVisibility(!forNewEpisodes)
    }
}

fun WatchingViewHolder.setViewInfo(item: ViewInfo) {
    val episode = item.episode ?: return
    val show = episode.show ?: return

    ivImage.load(episode.image)
    tvTitle.text = episode.name
    tvDuration.text = item.time.toTime(context)

    ivShow.load(show.image)
    tvShowName.text = show.name
    tvDescription.text = episode.description
    tvEpisodeNumber.text = context.getString(R.string.format_season_d_episode_d_short, episode.seasonsNumber, episode.episodesNumber)
}

fun WatchingViewHolder.setNewShowEpisode(item: NewShowEpisode) {
    val episode = item.episode

    ivImage.load(episode.image)
    tvTitle.text = episode.name

    ivShow.load(item.show.image)
    tvShowName.text = item.show.name
    tvDescription.text = episode.description
    tvEpisodeNumber.text = context.getString(R.string.format_season_d_episode_d_short, episode.seasonsNumber, episode.episodesNumber)
}