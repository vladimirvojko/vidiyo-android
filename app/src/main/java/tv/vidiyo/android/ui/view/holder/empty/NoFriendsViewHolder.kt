package tv.vidiyo.android.ui.view.holder.empty

import android.view.ViewGroup
import android.widget.Button
import tv.vidiyo.android.R
import tv.vidiyo.android.base.view.holder.ViewHolder
import java.lang.ref.WeakReference

interface NoFriendsClickListener : ViewHolder.OnViewHolderClickListener {
    fun onFindFriendsClicked()
    fun onConnectContactsClicked()
}

class NoFriendsViewHolder(
        parent: ViewGroup,
        listener: WeakReference<NoFriendsClickListener>?
) : ViewHolder<NoFriendsClickListener>(parent, listener, R.layout.view_empty_friends, false) {

    val btnFacebook: Button = itemView.findViewById(R.id.btn_facebook)
    val btnContacts: Button = itemView.findViewById(R.id.btn_contacts)

    init {
        btnFacebook.setOnClickListener { listener?.get()?.onFindFriendsClicked() }
        btnContacts.setOnClickListener { listener?.get()?.onConnectContactsClicked() }
    }
}