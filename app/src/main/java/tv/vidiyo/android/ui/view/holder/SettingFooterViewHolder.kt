package tv.vidiyo.android.ui.view.holder

import android.view.ViewGroup
import android.widget.TextView
import tv.vidiyo.android.R
import tv.vidiyo.android.base.view.holder.ViewHolder

class SettingFooterViewHolder(parent: ViewGroup)
    : ViewHolder<ViewHolder.OnViewHolderClickListener>(parent, null, R.layout.view_item_settings_footer, true) {

    val textView: TextView = itemView as TextView

    init {
        textView.tag = false
    }
}