package tv.vidiyo.android.ui.view

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.Button
import android.widget.RelativeLayout
import android.widget.TextView
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.cast.Cast
import tv.vidiyo.android.api.model.cast.CastInfo
import tv.vidiyo.android.extension.expansion
import tv.vidiyo.android.extension.load



@Suppress("LeakingThis")
open class InfoView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0,
        open val layoutResId: Int = R.layout.view_info
) : RelativeLayout(context, attrs, defStyleAttr), View.OnClickListener {

    open val collapsedLines: Int = 5

    val ivAvatar: ShapeImageView
    val btnAction: Button
    val tvName: TextView
    val tvBio: TextView // FIXME: ellipsize

    init {
        inflate(context, layoutResId, this)
        resources.getDimensionPixelOffset(R.dimen.spacing).let {
            setPadding(it, it, it, it)
        }

        ivAvatar = findViewById(R.id.iv_avatar)
        btnAction = findViewById(R.id.btn_action)
        tvName = findViewById(R.id.tv_name)
        tvBio = findViewById(R.id.tv_bio)

        btnAction.setText(R.string.unsubscribe)

        tvBio.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        tvBio.expansion(collapsedLines)
    }

    fun setCastInfo(info: CastInfo) {
        tvBio.text = info.bio
        btnAction.setText(if (info.isSubscribed) R.string.unsubscribe else R.string.subscribe)
    }
}

fun InfoView.setCast(cast: Cast) {
    ivAvatar.load(cast.image)
    tvName.text = cast.name
}