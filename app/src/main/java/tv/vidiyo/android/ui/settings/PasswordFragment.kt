package tv.vidiyo.android.ui.settings

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import tv.vidiyo.android.DependencyGraph
import tv.vidiyo.android.R
import tv.vidiyo.android.base.fragment.BaseFragment
import tv.vidiyo.android.di.subcomponent.settings.SettingsModule
import tv.vidiyo.android.domain.contract.SettingsContract
import tv.vidiyo.android.domain.presenter.settings.ChangePasswordPresenter
import javax.inject.Inject

class PasswordFragment : BaseFragment(), SettingsContract.Password.View, TextWatcher {

    override val layoutResId = R.layout.fragment_password
    override val optionsMenuResId = R.menu.password
    override val toolbarTitleRes = R.string.settings_change_password
    override val toolbarIndicator = R.drawable.svg_back

    private lateinit var etCurrent: EditText
    private lateinit var etNew: EditText
    private lateinit var etRetype: EditText

    private lateinit var menuItemChange: MenuItem

    @Inject
    lateinit var presenter: ChangePasswordPresenter

    override fun injectDependencies(graph: DependencyGraph) {
        graph.dataComponent.plus(SettingsModule(this)).injectTo(this)
    }

    override fun onViewCreated(view: View, b: Bundle?) {
        super.onViewCreated(view, b)
        etCurrent = view.findViewById(R.id.et_password)
        etNew = view.findViewById(R.id.et_new_password)
        etRetype = view.findViewById(R.id.et_new_password_confirm)

        etCurrent.addTextChangedListener(this)
        etNew.addTextChangedListener(this)
        etRetype.addTextChangedListener(this)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        menuItemChange = menu.findItem(R.id.item_confirm)
        changeMenuState(false)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.item_confirm -> {
                presenter.change(
                        etCurrent.text.trim().toString(),
                        etNew.text.trim().toString(),
                        etRetype.text.trim().toString()
                )
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun changeMenuState(enabled: Boolean) {
        if (enabled) {
            menuItemChange.isEnabled = true
            menuItemChange.icon.alpha = 255
        } else {
            menuItemChange.isEnabled = false
            menuItemChange.icon.alpha = 130
        }
    }

    override fun afterTextChanged(p0: Editable?) {
        val current = etCurrent.text.trim().toString()
        val new = etNew.text.trim().toString()
        val retype = etRetype.text.trim().toString()

        changeMenuState(presenter.validate(current, new, retype))
    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }
}