package tv.vidiyo.android.ui.boarding

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.TextView
import android.widget.Toast
import tv.vidiyo.android.App
import tv.vidiyo.android.R
import tv.vidiyo.android.base.BaseActivity
import tv.vidiyo.android.base.view.ShowMessagePresentationView
import tv.vidiyo.android.di.subcomponent.SplashModule
import tv.vidiyo.android.domain.contract.SplashContract
import tv.vidiyo.android.domain.presenter.SplashPresenter
import tv.vidiyo.android.extension.assignTo
import tv.vidiyo.android.extension.setTextWithClickableLink
import tv.vidiyo.android.extension.startActivity
import javax.inject.Inject

class SplashActivity : BaseActivity(), SplashContract.View, View.OnClickListener {

    companion object {
        private const val EXTRA = "EXTRA"

        fun start(context: Context, resId: Int? = null) {
            context.startActivity(Intent(context, SplashActivity::class.java).also {
                it.putExtra(EXTRA, resId)
                it.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            })
        }
    }

    @Inject
    lateinit var presenter: SplashPresenter

    override fun onCreate(b: Bundle?) {
        super.onCreate(b)
        super.setContentView(R.layout.activity_splash)
        App.graph.applicationComponent.plus(SplashModule(this)).injectTo(this)

        findViewById<TextView>(R.id.tv_have_account)
                .setTextWithClickableLink(R.string.boarding_have_account, this)
        assignTo(findViewById(R.id.tv_skip), findViewById(R.id.btn_sign_up))

        (if (intent.hasExtra(EXTRA)) {
            intent.getIntExtra(EXTRA, R.string.error_invalid_token)

        } else null)?.let {
            Toast.makeText(this, it, Toast.LENGTH_LONG).show()
        }
    }

    override fun onClick(v: View?) {
        val action = when (v?.id) {
            R.id.tv_skip -> return presenter.registerAnonymous()
            R.id.btn_sign_up -> BoardingActivity.SIGN_UP
            else -> BoardingActivity.SIGN_IN
        }
        startActivity<BoardingActivity>(BoardingActivity.EXTRA to action)
    }

    override fun startMain() {
        startActivity<BoardingActivity>(BoardingActivity.EXTRA to BoardingActivity.SELECT_CHANNELS,
                flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
    }

    override fun showMessage(message: ShowMessagePresentationView.Message) {
        Toast.makeText(this, message.text, Toast.LENGTH_LONG).show()
    }

    override fun showError(text: String?, code: Int) {}
}