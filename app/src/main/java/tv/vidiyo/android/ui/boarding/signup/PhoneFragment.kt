package tv.vidiyo.android.ui.boarding.signup

import android.os.Bundle
import android.util.Patterns
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import tv.vidiyo.android.BuildConfig
import tv.vidiyo.android.DependencyGraph
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.Country
import tv.vidiyo.android.api.model.debug
import tv.vidiyo.android.api.model.default
import tv.vidiyo.android.di.subcomponent.boarding.BoardingModule
import tv.vidiyo.android.domain.contract.SignUpContract
import tv.vidiyo.android.domain.presenter.boarding.PhonePresenter
import tv.vidiyo.android.extension.assignTo
import tv.vidiyo.android.ui.dialog.CountriesDialog
import tv.vidiyo.android.ui.view.ErrorView
import javax.inject.Inject

class PhoneFragment : SignUpFragment.SignUpInnerFragment(), SignUpContract.Phone.View, View.OnClickListener {

    override val layoutResId = R.layout.fragment_sign_up_phone

    lateinit var tvCode: TextView
    lateinit var etPhone: EditText
    lateinit var divider: View
    lateinit var errorView: ErrorView
    lateinit var btnContinue: Button

    @Inject
    lateinit var presenter: PhonePresenter

    var country = if (BuildConfig.DEBUG) Country.debug else Country.default

    override fun injectDependencies(graph: DependencyGraph) {
        graph.applicationComponent.plus(BoardingModule(this)).injectTo(this)
    }

    override fun onViewCreated(view: View, b: Bundle?) {
        super.onViewCreated(view, b)
        tvCode = view.findViewById(R.id.tv_code)
        etPhone = view.findViewById(R.id.et_phone)
        divider = view.findViewById(R.id.divider)
        errorView = view.findViewById(R.id.error_view)
        btnContinue = view.findViewById(R.id.btn_continue)

        etPhone.apply {
            addTextChangedListener(watcher)
            onFocusChangeListener = focusListener
        }
        assignTo(tvCode, btnContinue)

        updateCountry()
    }

    override fun showError(text: String?) {
        errorView.show(R.string.error_incorrect_phone_number)
    }

    override fun startCodeEnter() {
        startFragment(CodeFragment.newInstance(country.dial + etPhone.text.toString()), true)
    }

    fun updateCountry() {
        val countryWithCode = "${country.iso2} +${country.dial}"
        tvCode.text = countryWithCode
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_continue -> presenter.sendPhone(country.dial + etPhone.text.toString())
            R.id.tv_code -> CountriesDialog.show(this, true, wasSelected = country)
        }
    }

    override fun validate(s: CharSequence?) {
        btnContinue.isEnabled = s?.let {
            it.isNotEmpty() && Patterns.PHONE.matcher(s).matches()
        } == true
    }

    private val focusListener = View.OnFocusChangeListener { _, hasFocus ->
        changeColor(divider, hasFocus, errorView.text)
    }
}