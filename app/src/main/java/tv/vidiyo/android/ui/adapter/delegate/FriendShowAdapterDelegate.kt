package tv.vidiyo.android.ui.adapter.delegate

import android.view.ViewGroup
import tv.vidiyo.android.api.model.show.FriendShow
import tv.vidiyo.android.base.adapter.delegate.ListProviderAdapterDelegate
import tv.vidiyo.android.base.adapter.provider.ListDataProvider
import tv.vidiyo.android.domain.contract.setFriendShow
import tv.vidiyo.android.ui.view.holder.FriendShowViewHolder
import tv.vidiyo.android.ui.view.holder.OnFriendShowViewHolderClickListener


class FriendShowAdapterDelegate(
        provider: ListDataProvider<FriendShow>,
        listener: OnFriendShowViewHolderClickListener?
) : ListProviderAdapterDelegate<FriendShow, OnFriendShowViewHolderClickListener, FriendShowViewHolder>(provider, listener) {

    override fun onCreateViewHolder(parent: ViewGroup) = FriendShowViewHolder(parent, listener)

    override fun onBindViewHolder(position: Int, holder: FriendShowViewHolder, provider: ListDataProvider<FriendShow>, item: FriendShow) = holder.setFriendShow(item)

}