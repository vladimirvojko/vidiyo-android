package tv.vidiyo.android.ui.adapter.delegate.socialfeed

import android.view.ViewGroup
import tv.vidiyo.android.api.Dateable
import tv.vidiyo.android.api.graph.FBPost
import tv.vidiyo.android.api.instagram.IGMedia
import tv.vidiyo.android.api.twitter.Tweet
import tv.vidiyo.android.base.adapter.delegate.ListProviderAdapterDelegate
import tv.vidiyo.android.base.adapter.provider.ListDataProvider
import tv.vidiyo.android.base.view.holder.ViewHolder
import tv.vidiyo.android.ui.view.holder.PostViewHolder

class SocialAdapterDelegate(
        provider: ListDataProvider<Dateable>,
        listener: ViewHolder.OnViewHolderClickListener?
) : ListProviderAdapterDelegate<Dateable, ViewHolder.OnViewHolderClickListener, PostViewHolder>(provider, listener) {

    override fun onCreateViewHolder(parent: ViewGroup) = PostViewHolder(parent, listener)

    override fun onBindViewHolder(position: Int, holder: PostViewHolder, provider: ListDataProvider<Dateable>, item: Dateable) {
        when (item) {
            is FBPost -> holder.setPost(item)
            is Tweet -> holder.setTweet(item)
            is IGMedia -> holder.setMedia(item)
        }
    }
}