package tv.vidiyo.android.ui.view.holder

import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.Country
import tv.vidiyo.android.base.view.holder.ViewHolder
import java.io.FileNotFoundException
import java.lang.ref.WeakReference

class CountryViewHolder(
        parent: ViewGroup,
        listener: WeakReference<OnViewHolderClickListener>?
) : ViewHolder<ViewHolder.OnViewHolderClickListener>(parent, listener, R.layout.view_item_country, true) {

    val ivFlag = itemView.findViewById<ImageView>(R.id.image)
    val tvName = itemView.findViewById<TextView>(R.id.tv_name)
    val tvCode = itemView.findViewById<TextView>(R.id.tv_code)

    fun setCountry(country: Country, selected: Country) {
        try {
            val drawable = Drawable.createFromStream(context.assets.open("flag/${country.iso2.toLowerCase()}.webp"), null)
            ivFlag.setImageDrawable(drawable)

        } catch (ignore: FileNotFoundException) {
        }

        tvName.text = country.name
        tvCode.text = context.getString(R.string.plus_s, country.dial)

        val isSelected = country.name == selected.name
        tvName.isActivated = isSelected
        tvCode.isActivated = isSelected

        tvName.typeface = if (isSelected) Typeface.DEFAULT_BOLD else Typeface.DEFAULT
    }
}