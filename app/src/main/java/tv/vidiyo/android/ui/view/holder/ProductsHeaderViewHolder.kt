package tv.vidiyo.android.ui.view.holder

import android.view.ViewGroup
import android.widget.ImageView
import tv.vidiyo.android.R
import tv.vidiyo.android.base.view.holder.ViewHolder
import java.lang.ref.WeakReference

class ProductsHeaderViewHolder(
        parent: ViewGroup,
        listener: WeakReference<OnViewHolderClickListener>?
) : ViewHolder<ViewHolder.OnViewHolderClickListener>(parent, listener, R.layout.view_item_products_header, false) {

    private val ivClose: ImageView = itemView.findViewById(R.id.iv_close)

    init {
        ivClose.setOnClickListener {
            listener?.get()?.onViewHolderClick(this, adapterPosition)
        }
    }
}