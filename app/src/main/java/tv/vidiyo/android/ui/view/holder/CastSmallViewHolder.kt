package tv.vidiyo.android.ui.view.holder

import android.view.ViewGroup
import android.widget.TextView
import tv.vidiyo.android.R
import tv.vidiyo.android.base.view.holder.ViewHolder
import tv.vidiyo.android.ui.view.ShapeImageView
import java.lang.ref.WeakReference

class CastSmallViewHolder(
        parent: ViewGroup,
        listener: WeakReference<OnViewHolderClickListener>?
) : ViewHolder<ViewHolder.OnViewHolderClickListener>(parent, listener, R.layout.view_item_cast_small, true) {

    val ivPhoto: ShapeImageView = itemView.findViewById(R.id.iv_photo)
    val tvName: TextView = itemView.findViewById(R.id.tv_name)
}