package tv.vidiyo.android.ui

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import tv.vidiyo.android.DependencyGraph
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.cast.Cast
import tv.vidiyo.android.api.model.channel.Channel
import tv.vidiyo.android.api.model.show.Show
import tv.vidiyo.android.api.model.topic.Topic
import tv.vidiyo.android.base.fragment.RecyclerMainFragment
import tv.vidiyo.android.di.subcomponent.list.ListModule
import tv.vidiyo.android.domain.contract.ListContract
import tv.vidiyo.android.domain.presenter.ListPresenter
import tv.vidiyo.android.extension.errorRetryView
import tv.vidiyo.android.model.InnerPageType
import tv.vidiyo.android.ui.adapter.DataRecyclerAdapter
import tv.vidiyo.android.ui.cast.CastFragment
import tv.vidiyo.android.ui.show.ShowFragment
import tv.vidiyo.android.ui.topic.TopicFragment
import tv.vidiyo.android.ui.view.DividerItemDecoration
import tv.vidiyo.android.ui.view.SpaceItemDecoration
import tv.vidiyo.android.ui.view.holder.OnSubscribableViewHolderClickListener
import tv.vidiyo.android.ui.widget.InfiniteScrollRecyclerView
import tv.vidiyo.android.ui.widget.StateLayout
import tv.vidiyo.android.util.Constant
import javax.inject.Inject

class ListFragment : RecyclerMainFragment<DataRecyclerAdapter>(), ListContract.View,
        OnSubscribableViewHolderClickListener {

    companion object {
        private const val EXTRA_0 = "EXTRA_0"//userId
        private const val EXTRA_1 = "EXTRA_1"//channelId
        private const val EXTRA_2 = "EXTRA_2"//channelName
        private const val EXTRA_3 = "EXTRA_3"//type

        fun newInstance(userId: Int, channelId: Int, channelName: String, @InnerPageType type: Int) = ListFragment().apply {
            arguments = Bundle(4).also {
                it.putInt(EXTRA_0, userId)
                it.putInt(EXTRA_1, channelId)
                it.putString(EXTRA_2, channelName)
                it.putInt(EXTRA_3, type)
            }
        }
    }

    override val toolbarIndicator = R.drawable.svg_back
    override val contentLayoutResId = R.layout.content_state_recycler_center

    override val userId: Int get() = arguments.getInt(EXTRA_0, Constant.NO_ID)
    override val channelId: Int get() = arguments.getInt(EXTRA_1, Constant.NO_ID)
    private val channelName: String get() = arguments.getString(EXTRA_2, Constant.EMPTY)
    @InnerPageType override val type: Int get() = arguments.getInt(EXTRA_3, InnerPageType.SHOWS)

    @Inject
    lateinit var presenter: ListPresenter

    private lateinit var stateLayout: StateLayout

    override fun injectDependencies(graph: DependencyGraph) {
        graph.applicationComponent.plus(ListModule(this)).injectTo(this)
    }

    override fun onViewCreated(view: View, b: Bundle?) {
        super.onViewCreated(view, b)
        stateLayout = view.findViewById(R.id.state_layout)

        toolbar?.let {
            it.title = getString(titleFor(type))
            it.subtitle = channelName
        }
    }

    override fun onPostViewCreated() {
        presenter.start()
    }

    private fun titleFor(type: Int): Int = when (type) {
        InnerPageType.SHOWS -> R.string.shows
        InnerPageType.CAST -> R.string.actors
        InnerPageType.TOPICS -> R.string.topics

        else -> R.string.unknown
    }

    override fun onSubscribeClick(viewHolder: RecyclerView.ViewHolder, position: Int) {
        if (refreshLayout?.isRefreshing == false)
            presenter.toggleSubscription(position)
    }

    override fun onViewHolderClick(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val item = presenter.get<Any>(position)
        startFragment(when (item) {
            is Show -> ShowFragment.newInstance(item)
            is Cast -> CastFragment.newInstance(item)
            is Topic -> TopicFragment.newInstance(item)

            else -> return
        })
    }

    override fun onCreateAdapter() = when (type) {
        InnerPageType.SHOWS -> DataRecyclerAdapter.subscribableShow(presenter.provider(), Channel(channelId, channelName), this)
        InnerPageType.CAST -> DataRecyclerAdapter.cast(presenter.provider(), this)
        InnerPageType.TOPICS -> DataRecyclerAdapter.topics(presenter.provider(), Channel(channelId, channelName), this)

        else -> throw IllegalArgumentException("Unknown adapter for type $type")
    }

    override fun onCreateLayoutManager(context: Context?) = when (type) {
        InnerPageType.CAST -> GridLayoutManager(context, 2)
        else -> LinearLayoutManager(context)
    }

    override fun onRecyclerViewCreated(recyclerView: InfiniteScrollRecyclerView) {
        super.onRecyclerViewCreated(recyclerView)
        recyclerView.addItemDecoration(SpaceItemDecoration(context, true, true))
        if (type != InnerPageType.CAST) recyclerView.addItemDecoration(DividerItemDecoration(context, LinearLayout.VERTICAL))
    }

    //region StatePresentationView

    override fun showContent() = stateLayout.changeState(StateLayout.State.CONTENT)
    override fun showProgress() = stateLayout.changeState(StateLayout.State.PROGRESS)
    override fun showNoContent() = stateLayout.changeState(StateLayout.State.EMPTY)
    override fun showError(resId: Int) = showError(getString(resId), false)

    override fun showError(message: String?, asToast: Boolean) {
        if (asToast) {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
        } else {
            stateLayout.changeState(StateLayout.State.ERROR)
            stateLayout.errorRetryView?.setMessage(message)
        }
    }

    //endregion
}