package tv.vidiyo.android.ui.boarding.signup

import android.os.Bundle
import android.util.Patterns
import android.view.View
import android.widget.Button
import android.widget.EditText
import tv.vidiyo.android.DependencyGraph
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.user.Profile
import tv.vidiyo.android.di.subcomponent.boarding.BoardingModule
import tv.vidiyo.android.domain.contract.SignUpContract
import tv.vidiyo.android.domain.presenter.boarding.EmailPresenter
import tv.vidiyo.android.ui.view.ErrorView
import tv.vidiyo.android.util.Constant
import javax.inject.Inject

class EmailFragment : SignUpFragment.SignUpInnerFragment(), SignUpContract.Email.View, View.OnClickListener {

    override val layoutResId = R.layout.fragment_sign_up_email

    lateinit var etEmail: EditText
    lateinit var divider: View
    lateinit var errorView: ErrorView
    lateinit var btnContinue: Button

    @Inject
    lateinit var presenter: EmailPresenter

    override fun injectDependencies(graph: DependencyGraph) {
        graph.applicationComponent.plus(BoardingModule(this)).injectTo(this)
    }

    override fun onViewCreated(view: View, b: Bundle?) {
        super.onViewCreated(view, b)
        etEmail = view.findViewById(R.id.et_email)
        divider = view.findViewById(R.id.divider)
        errorView = view.findViewById(R.id.error_view)
        btnContinue = view.findViewById(R.id.btn_continue)

        etEmail.apply {
            addTextChangedListener(watcher)
            onFocusChangeListener = focusListener
        }
        btnContinue.setOnClickListener(this)
    }

    override fun validate(s: CharSequence?) {
        btnContinue.isEnabled = s?.let {
            it.isNotEmpty() && Patterns.EMAIL_ADDRESS.matcher(s).matches()
        } == true
    }

    override fun showError(text: String?) {
        errorView.show(text?.split(Constant.DOT)?.get(0))
    }

    override fun startNameEnter() {
        startFragment(NameFragment.newInstance(Profile().apply {
            email = etEmail.text.toString()
        }), true)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_continue -> presenter.validate(etEmail.text.toString())
        }
    }

    private val focusListener = View.OnFocusChangeListener { _, hasFocus ->
        changeColor(divider, hasFocus, errorView.text)
    }
}