package tv.vidiyo.android.ui.boarding.signin

import android.os.Bundle
import android.support.v7.app.ActionBar
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import android.widget.TextView
import tv.vidiyo.android.DependencyGraph
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.ErrorCode
import tv.vidiyo.android.di.subcomponent.boarding.BoardingModule
import tv.vidiyo.android.domain.contract.SignInContract
import tv.vidiyo.android.domain.contract.startMainImpl
import tv.vidiyo.android.domain.presenter.boarding.SignInPresenter
import tv.vidiyo.android.extension.assignTo
import tv.vidiyo.android.extension.findViews
import tv.vidiyo.android.extension.setTextWithClickableLink
import tv.vidiyo.android.ui.boarding.BaseSocialFragment
import tv.vidiyo.android.ui.boarding.RestoreFragment
import tv.vidiyo.android.ui.boarding.signup.SignUpFragment
import tv.vidiyo.android.ui.view.ErrorView
import javax.inject.Inject

class SignInFragment : BaseSocialFragment(), SignInContract.View {

    override val layoutResId = R.layout.fragment_sign_in
    override val toolbarTitleRes = R.string.boarding_sign_in

    lateinit var etUsername: EditText
    lateinit var etPassword: EditText
    lateinit var errorViews: Array<ErrorView>
    lateinit var controls: Array<View>

    @Inject
    lateinit var presenter: SignInPresenter

    override fun onActionBarReady(bar: ActionBar) {
        super.onActionBarReady(bar)
        bar.apply {
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
        }
    }

    override fun injectDependencies(graph: DependencyGraph) {
        graph.applicationComponent.plus(BoardingModule(this)).injectTo(this)
    }

    override fun onViewCreated(view: View, b: Bundle?) {
        super.onViewCreated(view, b)
        etUsername = view.findViewById(R.id.et_username)
        etPassword = view.findViewById(R.id.et_password)
        controls = view.findViews(R.id.btn_sign_in, R.id.tv_forgot, R.id.tv_havent_account, R.id.btn_facebook, R.id.btn_gplus)
        errorViews = view.findViews(R.id.error_username, R.id.error_password)

        etUsername.addTextChangedListener(watcher)
        etPassword.addTextChangedListener(watcher)

        view.findViewById<TextView>(R.id.tv_forgot).setTextWithClickableLink(R.string.boarding_forgot_login_details, this)
        view.findViewById<TextView>(R.id.tv_havent_account).setTextWithClickableLink(R.string.boarding_havent_account, this)

        assignTo(controls[0], controls[3], controls[4])
    }

    override fun setRefreshing(block: Boolean) {
        super.setRefreshing(block)
        controls.map { it.isEnabled = !block }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_sign_in -> presenter.signIn(etUsername.text.toString(), etPassword.text.toString())
            R.id.tv_havent_account -> startFragment(SignUpFragment(), false)
            R.id.tv_forgot -> startFragment(RestoreFragment())
            else -> super.onClick(v)
        }
    }

    override fun showError(text: String?, code: Int) {
        text?.let {
            val errorCode = ErrorCode.parse(code)
            when (errorCode) {
                ErrorCode.incorrectUsername, ErrorCode.incorrectPassword -> {
                    val ordinal = errorCode.ordinal
                    errorViews[ordinal].show(errorCode.stringId)
                    errorViews[if (ordinal == 0) 1 else 0].show(null)
                }

                else -> processError(text, code)
            }
        }
    }

    override fun startMain() = startMainImpl()

    private val watcher = object : TextWatcher {
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            controls[0].isEnabled = presenter.validate(etUsername.text.toString(), etPassword.text.toString())
        }

        override fun afterTextChanged(s: Editable?) {}
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
    }
}