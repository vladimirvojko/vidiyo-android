package tv.vidiyo.android.ui.view.holder

import android.support.annotation.LayoutRes
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import tv.vidiyo.android.R
import tv.vidiyo.android.base.view.holder.ViewHolder
import tv.vidiyo.android.domain.contract.CoverView
import tv.vidiyo.android.extension.load
import tv.vidiyo.android.extension.unit
import java.lang.ref.WeakReference

open class CoverViewHolder<L>(
        parent: ViewGroup,
        listener: WeakReference<L>?,
        @LayoutRes layoutResId: Int,
        clickable: Boolean = true
) : ViewHolder<L>(parent, listener, layoutResId, clickable), CoverView {
    private val ivImage: ImageView = itemView.findViewById(R.id.iv_image)
    private val tvTitle: TextView = itemView.findViewById(R.id.tv_title)

    override fun setImage(imageUrl: String?) = ivImage.load(imageUrl).unit()

    override fun setTitle(title: String) {
        tvTitle.text = title
    }
}