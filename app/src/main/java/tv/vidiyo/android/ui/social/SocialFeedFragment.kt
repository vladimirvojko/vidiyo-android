package tv.vidiyo.android.ui.social

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.view.View
import tv.vidiyo.android.DependencyGraph
import tv.vidiyo.android.R
import tv.vidiyo.android.base.adapter.pages.PageFragment
import tv.vidiyo.android.base.fragment.StateRecyclerFragment
import tv.vidiyo.android.di.subcomponent.cast.ChildCastModule
import tv.vidiyo.android.di.subcomponent.show.ChildShowModule
import tv.vidiyo.android.domain.contract.SocialContract
import tv.vidiyo.android.domain.presenter.SocialFeedPresenter
import tv.vidiyo.android.extension.openUrl
import tv.vidiyo.android.extension.setEmptyViewWithFormat
import tv.vidiyo.android.ui.adapter.DataRecyclerAdapter
import tv.vidiyo.android.ui.cast.CastFragment
import tv.vidiyo.android.ui.show.ShowFragment
import tv.vidiyo.android.ui.view.SpaceItemDecoration
import tv.vidiyo.android.ui.widget.InfiniteScrollRecyclerView
import javax.inject.Inject

class SocialFeedFragment : StateRecyclerFragment<DataRecyclerAdapter>(), SocialContract.View {

    companion object {
        fun creator() = object : PageFragment.Creator {
            override fun newInstance(): Fragment = SocialFeedFragment()
        }
    }

    @Inject
    lateinit var presenter: SocialFeedPresenter

    override fun injectDependencies(graph: DependencyGraph) {
        val parent = parentFragment
        when (parent) {
            is CastFragment -> parent.component.plus(ChildCastModule(this)).injectTo(this)
            is ShowFragment -> parent.component.plus(ChildShowModule(this)).injectTo(this)
        }
    }

    override fun onViewCreated(view: View, b: Bundle?) {
        super.onViewCreated(view, b)
        val title = getString(R.string.social).toLowerCase()
        val drawableRes = R.drawable.svg_subscription

        stateLayout.setEmptyViewWithFormat(title, drawableRes)
    }

    override fun onPostViewCreated() {
        presenter.start()
    }

    override fun onViewHolderClick(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val openable = presenter.get(position) ?: return
        openUrl(openable.url)
    }

    override fun onCreateAdapter() = DataRecyclerAdapter.socialFeed(presenter.provider, this)

    override fun onRecyclerViewCreated(recyclerView: InfiniteScrollRecyclerView) {
        super.onRecyclerViewCreated(recyclerView)
        recyclerView.addItemDecoration(SpaceItemDecoration(context, true, true))
    }
}