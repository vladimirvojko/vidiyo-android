package tv.vidiyo.android.ui.channels

import android.content.BroadcastReceiver
import android.content.IntentFilter
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.Toolbar
import android.view.View
import tv.vidiyo.android.DependencyGraph
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.channel.Channel
import tv.vidiyo.android.api.model.episode.Episode
import tv.vidiyo.android.base.adapter.pages.PageFragment
import tv.vidiyo.android.base.adapter.pages.PagesAdapter
import tv.vidiyo.android.base.fragment.PagesMainFragment
import tv.vidiyo.android.base.interfaces.HasFeatured
import tv.vidiyo.android.base.interfaces.newReceiver
import tv.vidiyo.android.di.subcomponent.channels.ChannelComponent
import tv.vidiyo.android.di.subcomponent.channels.ChannelModule
import tv.vidiyo.android.domain.contract.ChannelContract
import tv.vidiyo.android.domain.presenter.channels.ChannelPresenter
import tv.vidiyo.android.extension.playerActions
import tv.vidiyo.android.ui.view.FeaturedVideoView
import javax.inject.Inject

class ChannelFragment : PagesMainFragment(), ChannelContract.View,
        SwipeRefreshLayout.OnRefreshListener, View.OnClickListener, HasFeatured {

    companion object {
        private const val EXTRA = "EXTRA" //channel

        fun newInstance(channel: Channel) = ChannelFragment().apply {
            arguments = Bundle(1).also { it.putParcelable(EXTRA, channel) }
        }
    }

    override val statusBarColorId = R.color.black
    override val appBarLayoutResId = R.layout.app_bar_video_tabs
    override val toolbarIndicator = R.drawable.svg_back

    override lateinit var featuredVideoView: FeaturedVideoView

    lateinit var component: ChannelComponent

    @Inject lateinit var presenter: ChannelPresenter
    @Inject lateinit var manager: LocalBroadcastManager

    private var receiver: BroadcastReceiver? = null

    override fun injectDependencies(graph: DependencyGraph) {
        val channel = arguments.getParcelable<Channel>(EXTRA)
        component = graph.applicationComponent.plus(ChannelModule(this, channel))
        component.injectTo(this)

        presenter.channel = channel
    }

    override fun playEpisode(episode: Episode?) {
        featuredVideoView.playEpisode(episode)
    }

    override fun onRefresh() {
        presenter.refresh()
    }

    override fun onViewCreated(view: View, b: Bundle?) {
        super.onViewCreated(view, b)
        featuredVideoView = view.findViewById(R.id.featured_video_view)
        setRefreshing(true)
        featuredVideoView.setOnClickListener(this)
    }

    override fun onToolbarCreated(toolbar: Toolbar) {
        super.onToolbarCreated(toolbar)
        toolbar.title = presenter.channel.name
    }

    override fun onPostViewCreated() {
        presenter.start()
    }

    override fun onResume() {
        super.onResume()
        if (receiver == null) {
            manager.registerReceiver(newReceiver().also {
                receiver = it

            }, IntentFilter().playerActions())
        }
        featuredVideoView.player.playWhenReady = true
    }

    override fun update() {
        val pages = mutableListOf(PageFragment(InnerChannelFragment.Companion.ALL_CREATOR, R.string.all))
        val topics = presenter.topics
        pages.addAll(topics
                .filterIndexed { index, _ -> index < 3 }
                .mapIndexed { index, topic ->
                    PageFragment(InnerChannelFragment.topicCreator(index), title = topic.name)
                })
        if (topics.size > 2) pages.add(PageFragment(InnerChannelFragment.Companion.MORE_CREATOR, R.string.more))
        adapter.setPages(pages, true)
        setRefreshing(false)
    }

    override fun onPause() {
        super.onPause()
        receiver?.let { manager.unregisterReceiver(it) }.also { receiver = null }
        featuredVideoView.player.playWhenReady = false
    }

    override fun onClick(v: View) {
        val episode = presenter.featured ?: return
        mainActivity?.openEpisode(episode)
    }

    override fun createAdapter() = PagesAdapter(context, childFragmentManager,
            PageFragment(PageFragment.Creator.EMPTY_FRAGMENT, R.string.all)
    )

    override fun onDestroyView() {
        super.onDestroyView()
        featuredVideoView.player.release()
    }

    override fun additionAction() = Unit
}