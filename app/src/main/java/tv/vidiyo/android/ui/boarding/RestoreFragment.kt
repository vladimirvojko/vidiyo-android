package tv.vidiyo.android.ui.boarding

import android.os.Bundle
import android.support.v7.app.ActionBar
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Button
import android.widget.EditText
import tv.vidiyo.android.DependencyGraph
import tv.vidiyo.android.R
import tv.vidiyo.android.base.fragment.BaseFragment
import tv.vidiyo.android.di.subcomponent.boarding.BoardingModule
import tv.vidiyo.android.domain.contract.RestoreContract
import tv.vidiyo.android.domain.presenter.boarding.RestorePresenter
import javax.inject.Inject

class RestoreFragment : BaseFragment(), RestoreContract.View, View.OnClickListener {

    override val layoutResId = R.layout.fragment_restore
    override val toolbarTitleRes = R.string.boarding_restore

    private lateinit var etUsername: EditText
    private lateinit var btnSend: Button

    @Inject
    lateinit var presenter: RestorePresenter

    override fun injectDependencies(graph: DependencyGraph) {
        graph.applicationComponent.plus(BoardingModule(this)).injectTo(this)
    }

    override fun onActionBarReady(bar: ActionBar) {
        super.onActionBarReady(bar)
        bar.apply {
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
        }
    }

    override fun onViewCreated(view: View, b: Bundle?) {
        super.onViewCreated(view, b)
        etUsername = view.findViewById(R.id.et_username)
        btnSend = view.findViewById(R.id.button_send_link)
        btnSend.setOnClickListener(this)
        etUsername.addTextChangedListener(watcher)
    }

    override fun setRefreshing(block: Boolean) {
        super.setRefreshing(block)
        btnSend.isEnabled = !block
    }

    override fun onClick(v: View) {
        presenter.restore(etUsername.text.toString())
    }

    private val watcher = object : TextWatcher {
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            btnSend.isEnabled = presenter.validate(etUsername.text.toString())
        }

        override fun afterTextChanged(s: Editable?) {}
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
    }

}