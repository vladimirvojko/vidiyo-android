package tv.vidiyo.android.ui.dialog

import android.os.Bundle
import android.support.design.widget.BottomSheetDialogFragment
import android.support.v4.app.FragmentManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.social.SocialAuth
import tv.vidiyo.android.api.model.social.SocialAuthType
import tv.vidiyo.android.extension.assignTo
import tv.vidiyo.android.extension.choosePhoto
import tv.vidiyo.android.extension.takePhoto
import tv.vidiyo.android.ui.boarding.signup.NameFragment

class ChangePhotoSheetDialog : BottomSheetDialogFragment(), View.OnClickListener {

    companion object {
        const val REQUEST_CODE = 0x5D0

        private const val EXTRA = "vdyo.brdfrg.cpsd.extra0"//social

        fun newInstance(social: SocialAuth? = null): ChangePhotoSheetDialog {
            val fragment = ChangePhotoSheetDialog()
            val b = Bundle()
            b.putParcelable(EXTRA, social)
            fragment.arguments = b
            return fragment
        }
    }

    lateinit var btnSocial: Button

    var social: SocialAuth? = null

    override fun onCreate(b: Bundle?) {
        super.onCreate(b)
        val bundle = b ?: arguments
        social = bundle.getParcelable(EXTRA)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_change_photo, container, false)
    }

    override fun onViewCreated(view: View, b: Bundle?) {
        super.onViewCreated(view, b)
        btnSocial = view.findViewById(R.id.action_social)
        social?.let {
            val res = when (it.type) {
                SocialAuthType.facebook -> R.string.profile_import_from_fb
                SocialAuthType.google -> R.string.profile_import_from_g
                else -> return
            }
            btnSocial.setText(res)
            btnSocial.visibility = View.VISIBLE
        }
        assignTo(btnSocial, view.findViewById(R.id.action_take), view.findViewById(R.id.action_library), view.findViewById(R.id.action_cancel))
    }

    fun show(manager: FragmentManager?) {
        super.show(manager, ChangePhotoSheetDialog::class.java.name)
    }

    override fun onClick(v: View?) {
        val parent = parentFragment

        when (v?.id) {
            R.id.action_social -> social?.let { (parent as? NameFragment)?.loadFromSocial(it) }
            R.id.action_take -> parent.takePhoto(REQUEST_CODE)
            R.id.action_library -> parent.choosePhoto(REQUEST_CODE)
        }
        dismiss()
    }
}