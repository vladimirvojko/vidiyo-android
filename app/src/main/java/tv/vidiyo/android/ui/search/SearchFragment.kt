package tv.vidiyo.android.ui.search

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.IntentFilter
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.Button
import android.widget.TextView
import tv.vidiyo.android.DependencyGraph
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.episode.Episode
import tv.vidiyo.android.base.fragment.ViewFragment
import tv.vidiyo.android.base.interfaces.HasFeatured
import tv.vidiyo.android.base.interfaces.newReceiver
import tv.vidiyo.android.base.view.holder.ViewHolder
import tv.vidiyo.android.di.subcomponent.search.SearchModule
import tv.vidiyo.android.domain.contract.SearchContract
import tv.vidiyo.android.domain.presenter.search.SearchPresenter
import tv.vidiyo.android.extension.changeVisibility
import tv.vidiyo.android.extension.errorRetryView
import tv.vidiyo.android.extension.findViews
import tv.vidiyo.android.extension.playerActions
import tv.vidiyo.android.source.DataType
import tv.vidiyo.android.ui.MainFragment
import tv.vidiyo.android.ui.adapter.DataRecyclerAdapter
import tv.vidiyo.android.ui.cast.CastFragment
import tv.vidiyo.android.ui.profile.ProfileFragment
import tv.vidiyo.android.ui.show.ShowFragment
import tv.vidiyo.android.ui.topic.TopicFragment
import tv.vidiyo.android.ui.view.FeaturedVideoView
import tv.vidiyo.android.ui.view.SpaceItemDecoration
import tv.vidiyo.android.ui.view.holder.OnFriendShowViewHolderClickListener
import tv.vidiyo.android.ui.widget.StateLayout
import javax.inject.Inject

class SearchFragment : MainFragment(), SearchContract.View, View.OnClickListener,
        ViewHolder.OnViewHolderClickListener, OnFriendShowViewHolderClickListener, HasFeatured {

    override val statusBarColorId = R.color.black
    override val contentLayoutResId = R.layout.content_search_main
    override val toolbarTitleRes = R.string.navigation_search
    override val appBarLayoutResId = R.layout.app_bar_search_main

    override lateinit var featuredVideoView: FeaturedVideoView

    private lateinit var searchView: View
    private lateinit var stateLayout: StateLayout

    private lateinit var titles: Array<TextView>
    private lateinit var buttons: Array<Button>
    private lateinit var recycles: Array<RecyclerView>

    private lateinit var adapters: Array<DataRecyclerAdapter>

    @Inject lateinit var presenter: SearchPresenter
    @Inject lateinit var manager: LocalBroadcastManager

    private var receiver: BroadcastReceiver? = null

    override fun injectDependencies(graph: DependencyGraph) {
        graph.dataComponent.plus(SearchModule(this)).injectTo(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        adapters = DataRecyclerAdapter.createAdapters(this)
    }

    override fun onViewCreated(view: View, b: Bundle?) {
        super.onViewCreated(view, b)
        searchView = view.findViewById(R.id.search_view)
        featuredVideoView = view.findViewById(R.id.featured_video_view)
        stateLayout = view.findViewById(R.id.state_layout)

        titles = view.findViews(R.id.tv_friends_watching, R.id.tv_top_picks, R.id.tv_channels)
        recycles = view.findViews(R.id.rv_cast, R.id.rv_friends_watching, R.id.rv_top_picks, R.id.rv_channels)
        buttons = view.findViews(R.id.btn_friends_watching, R.id.btn_top_picks, R.id.btn_channels)

        val decoration = SpaceItemDecoration(context, R.dimen.spacing_x2, true, true)
        for ((index, item) in recycles.withIndex()) {
            item.adapter = adapters[index]
            item.addItemDecoration(decoration)
            item.isNestedScrollingEnabled = false
        }

        searchView.setOnClickListener(this)
        featuredVideoView.setOnClickListener(this)
    }

    override fun onPostViewCreated() {
        super.onPostViewCreated()
        presenter.start()
    }

    override fun playEpisode(episode: Episode?) {
        featuredVideoView.playEpisode(episode)
        changeFeaturedVideoVisibility(episode != null)
    }

    private fun changeFeaturedVideoVisibility(visible: Boolean) {
        featuredVideoView.changeVisibility(visible)
    }

    override fun onResume() {
        super.onResume()
        if (receiver == null) {
            manager.registerReceiver(newReceiver().also {
                receiver = it

            }, IntentFilter().playerActions())
        }
        featuredVideoView.player.playWhenReady = true
    }

    override fun onPause() {
        super.onPause()
        receiver?.let { manager.unregisterReceiver(it) }.also { receiver = null }
        featuredVideoView.player.playWhenReady = false
    }

    override fun onDestroyView() {
        super.onDestroyView()
        changeFeaturedVideoVisibility(false)
        featuredVideoView.player.release()
    }

    override fun update(sizes: IntArray) {
        for ((index, size) in sizes.withIndex()) {
            adapters[index].notifyDataSetChanged()

            if (index == 0) continue
            changeElementState(index - 1, size)
        }
    }

    override fun update() {}

    private fun changeElementState(index: Int, size: Int) {
        val visible = size != 0
        titles[index].changeVisibility(visible)
//        buttons[index].changeVisibility(visible)
        recycles[index + 1].changeVisibility(visible)
    }

    @SuppressLint("SupportAnnotationUsage")
    override fun onViewHolderClick(viewHolder: RecyclerView.ViewHolder, position: Int) {
        @DataType val type = viewHolder.itemView.tag as? Int ?: return
        val fragment: ViewFragment = when (type) {
            DataType.TYPE_TOP_PICKS -> {
                val show = presenter.getTopPickShow(position) ?: return
                ShowFragment.newInstance(show)
            }

            DataType.TYPE_TOP_CHANNELS -> {
                val topic = presenter.getTrendingTopic(position) ?: return
                TopicFragment.newInstance(topic)
            }

            DataType.TYPE_CASTS_SMALL -> {
                val cast = presenter.getCast(position) ?: return
                CastFragment.newInstance(cast)
            }
            else -> return
        }
        startFragment(fragment)
    }

    override fun onFriendClicked(position: Int) {
        val friend = presenter.getFriendShow(position)?.user ?: return
        startFragment(ProfileFragment.newInstance(friend.id))
    }

    override fun onShowClicked(position: Int) {
        val show = presenter.getFriendShow(position)?.show ?: return
        startFragment(ShowFragment.newInstance(show))
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.search_view -> startFragment(SearchResultsFragment())
            else -> {
                val featured = presenter.repository.featured ?: return
                mainActivity?.openEpisode(featured)
            }
        }
    }

    override fun additionAction() = Unit

    //region StatePresentationView
    override fun showProgress() {
        stateLayout.changeState(StateLayout.State.PROGRESS)
    }

    override fun showContent() {
        stateLayout.changeState(StateLayout.State.CONTENT)
    }

    override fun showError(message: String?, asToast: Boolean) {
        stateLayout.errorRetryView?.setMessage(message)
        stateLayout.changeState(StateLayout.State.ERROR)
    }

    override fun showError(resId: Int) {
        stateLayout.errorRetryView?.setMessage(resId)
        stateLayout.changeState(StateLayout.State.ERROR)
    }

    override fun showNoContent() {
        stateLayout.changeState(StateLayout.State.EMPTY)
    }
    //endregion StatePresentationView
}