package tv.vidiyo.android.ui.settings

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v7.app.ActionBar
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.LinearLayout
import tv.vidiyo.android.DependencyGraph
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.user.FollowingType
import tv.vidiyo.android.base.fragment.BaseRecyclerViewFragment
import tv.vidiyo.android.di.subcomponent.settings.UsersModule
import tv.vidiyo.android.domain.contract.SettingsContract
import tv.vidiyo.android.domain.presenter.settings.UsersPresenter
import tv.vidiyo.android.ui.MainActivity
import tv.vidiyo.android.ui.adapter.DataRecyclerAdapter
import tv.vidiyo.android.ui.profile.ProfileFragment
import tv.vidiyo.android.ui.view.DividerItemDecoration
import tv.vidiyo.android.ui.view.holder.OnFollowerActionClickListener
import tv.vidiyo.android.ui.widget.InfiniteScrollRecyclerView
import tv.vidiyo.android.ui.widget.OnInfiniteScrollListener
import tv.vidiyo.android.util.Constant
import javax.inject.Inject

class UsersFragment : BaseRecyclerViewFragment<DataRecyclerAdapter>(), SettingsContract.Users.View,
        OnFollowerActionClickListener {

    companion object {
        private const val EXTRA_0 = "EXTRA_0"//type
        private const val EXTRA_1 = "EXTRA_1"//id

        fun newInstance(type: FollowingType, id: Int = Constant.NO_ID) = UsersFragment().apply {
            arguments = Bundle(2).also {
                it.putInt(EXTRA_0, type.ordinal)
                it.putInt(EXTRA_1, id)
            }
        }
    }

    override val toolbarTitleRes: Int? get() = type.titleRes
    override val toolbarIndicator = R.drawable.svg_back
    override val layoutResId: Int get() = if (activity is MainActivity) R.layout.fragment_users_toolbar else R.layout.recycler_view

    override val type: FollowingType get() = FollowingType.values()[arguments.getInt(EXTRA_0, 0)]
    override val userId: Int get() = arguments.getInt(EXTRA_1, Constant.NO_ID)

    @Inject
    lateinit var presenter: UsersPresenter

    override fun injectDependencies(graph: DependencyGraph) {
        graph.dataComponent.plus(UsersModule(this)).injectTo(this)
    }

    override fun onViewCreated(view: View, b: Bundle?) {
        super.onViewCreated(view, b)
        view.findViewById<Toolbar>(R.id.toolbar)?.apply {
            toolbarTitleRes?.let { setTitle(it) }
            setNavigationIcon(R.drawable.svg_back)
            setNavigationOnClickListener {
                onBackPressed()
            }
        }
    }

    @SuppressLint("RestrictedApi")
    override fun onActionBarReady(bar: ActionBar) {
        super.onActionBarReady(bar)
        bar.setShowHideAnimationEnabled(false)
        bar.show()
    }

    override fun onPostViewCreated() = presenter.start()
    override fun onAcceptClick(viewHolder: RecyclerView.ViewHolder, position: Int) = presenter.accept(position)
    override fun onDeleteClick(viewHolder: RecyclerView.ViewHolder, position: Int) = presenter.delete(position)

    override fun onViewHolderClick(viewHolder: RecyclerView.ViewHolder, position: Int) {
        startFragment(ProfileFragment.newInstance(presenter.get(position)?.id ?: return), true)
    }

    override fun onCreateAdapter(): DataRecyclerAdapter {
        val pending = type == FollowingType.pending
        return DataRecyclerAdapter.users(presenter.provider, this, pending, pending)
    }

    override fun onRecyclerViewCreated(recyclerView: InfiniteScrollRecyclerView) {
        super.onRecyclerViewCreated(recyclerView)
        recyclerView.addItemDecoration(DividerItemDecoration(context, LinearLayout.VERTICAL))
        recyclerView.listener = listener
    }

    private val listener = object : OnInfiniteScrollListener {
        override fun onInfiniteScroll(recyclerView: InfiniteScrollRecyclerView) {
            if (recyclerView.shouldLoadMore(presenter.totalCount)) {
                presenter.loadMore()
            }
        }
    }
}