package tv.vidiyo.android.ui.view

import android.content.Context
import android.support.v7.widget.AppCompatTextView
import android.util.AttributeSet
import android.view.View
import tv.vidiyo.android.R
import tv.vidiyo.android.extension.color

class ErrorView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0
) : AppCompatTextView(context, attrs, defStyleAttr) {

    init {
        setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.svg_warning_16, 0)
        setTextColor(context.color(R.color.red))
        visibility = View.GONE
    }

    fun show(textId: Int) {
        show(context.getString(textId))
    }

    fun show(text: String?) {
        this.text = text
        visibility = if (text.isNullOrEmpty()) View.GONE else View.VISIBLE
    }
}