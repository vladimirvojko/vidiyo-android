package tv.vidiyo.android.ui.dialog

import android.os.Bundle
import android.support.design.widget.BottomSheetDialogFragment
import android.support.v4.app.FragmentManager
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.EditText
import android.widget.ImageView
import tv.vidiyo.android.App
import tv.vidiyo.android.R
import tv.vidiyo.android.domain.presenter.episode.EpisodeInnerPresenter
import tv.vidiyo.android.extension.changeVisibility
import tv.vidiyo.android.extension.load
import tv.vidiyo.android.ui.view.ShapeImageView
import tv.vidiyo.android.util.Constant
import java.lang.ref.WeakReference

class WriteTextSheetDialog : BottomSheetDialogFragment(), View.OnClickListener, TextWatcher {

    companion object {
        private const val EXTRA_0 = "EXTRA_0"//episodeId
        private const val EXTRA_1 = "EXTRA_1"//parentId

        fun newInstance(episodeId: Int, parentId: Int = Constant.NO_ID) = WriteTextSheetDialog().apply {
            arguments = Bundle(2).also {
                it.putInt(EXTRA_0, episodeId)
                it.putInt(EXTRA_1, parentId)
            }
        }
    }

    private lateinit var ivAvatar: ShapeImageView
    private lateinit var editText: EditText
    private lateinit var btnSend: ImageView

    private var episodeId: Int = Constant.NO_ID
    private var parentId: Int = Constant.NO_ID

    var presenter: WeakReference<EpisodeInnerPresenter>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        episodeId = arguments.getInt(EXTRA_0, Constant.NO_ID)
        parentId = arguments.getInt(EXTRA_1, Constant.NO_ID)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
            = inflater.inflate(R.layout.dialog_write_text, container, false)

    override fun onViewCreated(view: View, b: Bundle?) {
        super.onViewCreated(view, b)
        ivAvatar = view.findViewById(R.id.iv_avatar)
        editText = view.findViewById(R.id.edit_text)
        btnSend = view.findViewById(R.id.btn_send)

        val account = App.graph.applicationComponent.provideUserAccount()
        ivAvatar.load(account?.image)

        editText.addTextChangedListener(this)
        btnSend.setOnClickListener(this)

        dialog.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)
    }

    override fun onClick(v: View) {
        presenter?.get()?.createComment(episodeId, parentId, editText.text.toString())
        dismiss()
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        val empty = s?.trim()?.isEmpty() != false
        btnSend.changeVisibility(!empty, View.INVISIBLE)
    }

    override fun afterTextChanged(s: Editable?) {}
    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

    fun show(manager: FragmentManager?) = super.show(manager, WriteTextSheetDialog::class.java.name)
}