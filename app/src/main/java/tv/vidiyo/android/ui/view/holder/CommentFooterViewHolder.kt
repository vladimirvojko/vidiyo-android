package tv.vidiyo.android.ui.view.holder

import android.view.ViewGroup
import tv.vidiyo.android.R
import tv.vidiyo.android.base.view.holder.ViewHolder
import java.lang.ref.WeakReference

class CommentFooterViewHolder(
        parent: ViewGroup,
        listener: WeakReference<OnCommentClickListener>?
) : ViewHolder<OnCommentClickListener>(parent, listener, R.layout.view_item_comment_footer, true)