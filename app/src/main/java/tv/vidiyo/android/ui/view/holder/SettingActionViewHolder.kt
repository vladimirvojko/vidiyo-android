package tv.vidiyo.android.ui.view.holder

import android.support.v7.widget.SwitchCompat
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.TextView
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.Item
import tv.vidiyo.android.api.model.user.UserAccount
import tv.vidiyo.android.base.view.holder.ViewHolder
import java.lang.ref.WeakReference

class SettingActionViewHolder(
        parent: ViewGroup,
        val changeListener: WeakReference<CompoundButton.OnCheckedChangeListener>?
) : ViewHolder<ViewHolder.OnViewHolderClickListener>(parent, null, R.layout.view_item_setting_action, false) {

    val tvText: TextView = itemView.findViewById(R.id.text)
    val tvSubtitle: TextView = itemView.findViewById(R.id.tv_subtitle)
    val scAction: SwitchCompat = itemView.findViewById(R.id.sc_action)

    init {
        scAction.setOnCheckedChangeListener(changeListener?.get())
    }
}

fun SettingActionViewHolder.setItem(item: Item, account: UserAccount) {
    tvText.setText(item.titleResId)
    tvSubtitle.setText(item.subtitleResId)
    scAction.setOnCheckedChangeListener(null)
    scAction.isChecked = account.isPrivate
    scAction.setOnCheckedChangeListener(changeListener?.get())
}