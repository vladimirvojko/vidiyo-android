package tv.vidiyo.android.ui.view.holder

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import tv.vidiyo.android.R
import tv.vidiyo.android.extension.inflateChild

class ProgressViewHolder(parent: ViewGroup, layoutResId: Int = R.layout.view_item_progress)
    : RecyclerView.ViewHolder(parent.inflateChild(layoutResId)) {

    init {
        itemView.tag = false
    }
}