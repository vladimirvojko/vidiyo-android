package tv.vidiyo.android.ui.episode

import android.graphics.Point
import android.view.View
import android.widget.FrameLayout
import com.github.pedrovgs.DraggablePanel
import tv.vidiyo.android.App
import tv.vidiyo.android.R
import tv.vidiyo.android.extension.weak
import tv.vidiyo.android.source.episode.EpisodeRepository
import tv.vidiyo.android.ui.MainActivity
import javax.inject.Inject

interface PlayerListener {
    fun onPlayerButtonClick(id: Int)
}

class EpisodeFragment(activity: MainActivity, var id: Int) : PlayerListener {

    private val playerFragment = EpisodePlayerFragment.newInstance(id).also { it.listener = this.weak() }
    private val bottomFragment = EpisodeInfoFragment.newInstance(id)

    private val panel: DraggablePanel

    val isMaximized: Boolean get() = panel.isMaximized

    @Inject
    lateinit var repository: EpisodeRepository

    init {
        val root = activity.root
        App.graph.getEpisodeComponent(id).inject(this)

        val size = Point()
        activity.windowManager.defaultDisplay.getSize(size)
        val height = size.x * 0.5625

        panel = (activity.layoutInflater.inflate(R.layout.drag_panel, root, false) as DraggablePanel).apply {
            id = View.generateViewId()
            setFragmentManager(activity.supportFragmentManager)
            setTopFragment(playerFragment)
            setBottomFragment(bottomFragment)
            setDraggableListener(activity)
            if (height > 0) setTopViewHeight(height.toInt())
        }

        panel.initializeView()
        root.addView(panel)
    }

    fun open(id: Int) {
        this.id = id
        repository.id = id
        panel.maximize()
    }

    override fun onPlayerButtonClick(id: Int) {
        when (id) {
            R.id.exo_products -> bottomFragment.changeProductsVisibility(!bottomFragment.productsOpen)
            else -> return
        }
    }

    fun hideController() {
        playerFragment.playerView.hideController()
    }

    fun minimize() {
        panel.minimize()
    }

    fun remove(root: FrameLayout) {
        bottomFragment.mainActivity?.changeNavigationBarVisibility(true)
        App.graph.destroyEpisodeComponent()
        playerFragment.release()

        panel.release()
        panel.removeAllViews()
        root.removeView(panel)
    }
}