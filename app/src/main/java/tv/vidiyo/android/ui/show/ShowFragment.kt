package tv.vidiyo.android.ui.show

import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.load.resource.drawable.GlideDrawable
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import tv.vidiyo.android.DependencyGraph
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.show.Show
import tv.vidiyo.android.api.model.show.ShowInfo
import tv.vidiyo.android.base.adapter.pages.PageFragment
import tv.vidiyo.android.base.adapter.pages.PagesAdapter
import tv.vidiyo.android.base.fragment.PagesMainFragment
import tv.vidiyo.android.di.subcomponent.show.ShowComponent
import tv.vidiyo.android.di.subcomponent.show.ShowModule
import tv.vidiyo.android.domain.contract.ShowContract
import tv.vidiyo.android.domain.presenter.show.ShowPresenter
import tv.vidiyo.android.extension.changeVisibility
import tv.vidiyo.android.extension.load
import tv.vidiyo.android.ui.social.SocialFeedFragment
import tv.vidiyo.android.ui.view.ExpandableDescriptionView
import tv.vidiyo.android.ui.view.ShowView
import tv.vidiyo.android.ui.view.setShow
import java.lang.Exception
import javax.inject.Inject

class ShowFragment : PagesMainFragment(), ShowContract.View, View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    companion object {
        private const val EXTRA = "EXTRA"//show

        fun newInstance(show: Show): ShowFragment = ShowFragment().apply {
            arguments = Bundle().also { it.putParcelable(EXTRA, show) }
        }
    }

    override val appBarLayoutResId = R.layout.app_bar_show
    override val contentLayoutResId = R.layout.view_pager
    override val toolbarIndicator = R.drawable.svg_back
    override val statusBarColorId = R.color.black

    private lateinit var imageView: ImageView
    private lateinit var descriptionView: ExpandableDescriptionView
    private lateinit var showView: ShowView

    lateinit var component: ShowComponent

    @Inject
    lateinit var presenter: ShowPresenter

    override fun injectDependencies(graph: DependencyGraph) {
        val show = arguments.getParcelable<Show>(EXTRA)
        component = graph.applicationComponent.plus(ShowModule(this, show.id))
        component.injectTo(this)

        presenter.show = show
    }

    override fun onViewCreated(view: View, b: Bundle?) {
        super.onViewCreated(view, b)

        imageView = view.findViewById(R.id.image)
        descriptionView = view.findViewById(R.id.description_view)
        showView = view.findViewById(R.id.show_view)

        showView.button.setOnClickListener(this)
    }

    override fun updateShowInfo(info: ShowInfo) {
        descriptionView.changeVisibility(true)
        descriptionView.setShow(info)
        val image = info.banner /*?: info.image */
        image?.let {
            imageView.load(it, requestListener = object : RequestListener<String?, GlideDrawable> {
                override fun onResourceReady(resource: GlideDrawable?, model: String?, target: Target<GlideDrawable>?,
                                             isFromMemoryCache: Boolean, isFirstResource: Boolean): Boolean {
                    imageView.setImageDrawable(resource)
                    imageView.scaleType = ImageView.ScaleType.CENTER_CROP
                    return true
                }

                override fun onException(e: Exception?, model: String?, target: Target<GlideDrawable>?,
                                         isFirstResource: Boolean): Boolean {
                    imageView.setImageResource(R.drawable.svg_vidiyo)
                    return true
                }
            })
        }
    }

    override fun onRefresh() {
        presenter.refresh()
    }

    override fun onClick(v: View?) {
        view?.isClickable = false
        presenter.toggleSubscription()
    }

    override fun updateShowView(show: Show) {
        showView.changeVisibility(true)
        showView.setShow(show)
    }

    override fun onPostViewCreated() {
        presenter.start()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        presenter.show?.let { outState?.putParcelable(EXTRA, it) }
    }

    override fun createAdapter() = PagesAdapter(context, childFragmentManager,
            PageFragment(InnerShowFragment.Companion.EPISODES_CREATOR, R.string.episodes),
            PageFragment(InnerShowFragment.Companion.CAST_CREATOR, R.string.cast),
            PageFragment(SocialFeedFragment.creator(), R.string.social),
            PageFragment(InnerShowFragment.Companion.TOPICS_CREATOR, R.string.topics),
            PageFragment(InnerShowFragment.Companion.SUBSCRIBERS_CREATOR, R.string.subscribers)
    )
}