package tv.vidiyo.android.ui.discover

import android.support.v7.widget.RecyclerView
import android.widget.LinearLayout
import tv.vidiyo.android.DependencyGraph
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.ConnectionCount
import tv.vidiyo.android.api.model.user.SuggestedUser
import tv.vidiyo.android.base.fragment.StateRecyclerMainFragment
import tv.vidiyo.android.di.subcomponent.discover.DiscoverModule
import tv.vidiyo.android.domain.contract.DiscoverContract
import tv.vidiyo.android.domain.presenter.discover.DiscoverPresenter
import tv.vidiyo.android.ui.adapter.DataRecyclerAdapter
import tv.vidiyo.android.ui.profile.ProfileFragment
import tv.vidiyo.android.ui.view.DividerItemDecoration
import tv.vidiyo.android.ui.view.holder.OnFollowerActionClickListener
import tv.vidiyo.android.ui.widget.InfiniteScrollRecyclerView
import javax.inject.Inject

class DiscoverFragment : StateRecyclerMainFragment<DataRecyclerAdapter>(), DiscoverContract.View,
        OnFollowerActionClickListener {

    override val toolbarIndicator = R.drawable.svg_back
    override val toolbarTitleRes = R.string.discover_new_people
    override val contentLayoutResId = R.layout.content_state_recycler_center

    @Inject
    lateinit var presenter: DiscoverPresenter

    override fun injectDependencies(graph: DependencyGraph) {
        mainActivity?.socialComponent?.plus(DiscoverModule(this))?.injectTo(this)
    }

    override fun onPostViewCreated() {
        presenter.start()
    }

    override fun onViewHolderClick(viewHolder: RecyclerView.ViewHolder, position: Int) {
        when (position) {
            0, 1 -> if (!isRefreshing) {
                val connection = presenter.get<ConnectionCount>(position) ?: return
                startFragment(DiscoveredFragment.newInstance(position, connection.connected))
            }

            else -> if (!isRefreshing) {
                val suggested = presenter.get<SuggestedUser>(position) ?: return
                startFragment(ProfileFragment.newInstance(suggested.id))
            }
        }
    }

    override fun onAcceptClick(viewHolder: RecyclerView.ViewHolder, position: Int) {
        if (!isRefreshing) presenter.follow(position)
    }

    override fun onDeleteClick(viewHolder: RecyclerView.ViewHolder, position: Int) {
        if (!isRefreshing) presenter.hide(position)
    }

    override fun onCreateAdapter() = DataRecyclerAdapter.discover(presenter.provider, this)
    override fun onRecyclerViewCreated(recyclerView: InfiniteScrollRecyclerView) {
        super.onRecyclerViewCreated(recyclerView)
        recyclerView.addItemDecoration(DividerItemDecoration(context, LinearLayout.VERTICAL))
    }
}