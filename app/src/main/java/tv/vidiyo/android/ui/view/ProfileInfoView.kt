package tv.vidiyo.android.ui.view

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.TextView
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.user.Profile
import tv.vidiyo.android.extension.load

class ProfileInfoView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0
) : InfoView(context, attrs, defStyleAttr, R.layout.view_profile_info) {

    override val collapsedLines: Int = 3

    val llFollowing: View = findViewById(R.id.ll_following)
    val tvFollowing: TextView = findViewById(R.id.tv_following)
    val llFollowers: View = findViewById(R.id.ll_followers)
    val tvFollowers: TextView = findViewById(R.id.tv_followers)

}

fun ProfileInfoView.set(profile: Profile, withPhoto: Boolean = true) {
    if (withPhoto) ivAvatar.load(profile.image)
    tvName.text = profile.name
    tvBio.text = profile.bio
    tvFollowers.text = profile.followersCount.toString()
    tvFollowing.text = profile.followingCount.toString()
}