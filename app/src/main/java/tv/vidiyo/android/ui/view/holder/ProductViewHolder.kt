package tv.vidiyo.android.ui.view.holder

import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.episode.Product
import tv.vidiyo.android.base.view.holder.ViewHolder
import tv.vidiyo.android.extension.PRICE_FORMAT
import tv.vidiyo.android.extension.load
import tv.vidiyo.android.ui.view.ShapeImageView
import java.lang.ref.WeakReference

class ProductViewHolder(
        parent: ViewGroup,
        listener: WeakReference<OnViewHolderClickListener>?,
        layoutResId: Int
) : ViewHolder<ViewHolder.OnViewHolderClickListener>(parent, listener, layoutResId, false) {

    val ivImage: ShapeImageView = itemView.findViewById(R.id.iv_image)
    val tvName: TextView = itemView.findViewById(R.id.tv_title)
    val tvSubtitle: TextView = itemView.findViewById(R.id.tv_subtitle)
    val button: Button = itemView.findViewById(R.id.button)

    init {
        button.setOnClickListener {
            listener?.get()?.onViewHolderClick(this, adapterPosition)
        }
    }
}

fun ProductViewHolder.setProduct(product: Product) {
    ivImage.load(product.productImage)
    tvName.text = product.name
    tvSubtitle.text = context.getString(R.string.format_usd_s, PRICE_FORMAT.format(product.price))
}