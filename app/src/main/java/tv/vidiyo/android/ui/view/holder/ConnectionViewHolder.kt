package tv.vidiyo.android.ui.view.holder

import android.view.ViewGroup
import android.widget.TextView
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.social.SocialConnection
import tv.vidiyo.android.base.view.holder.ViewHolder
import tv.vidiyo.android.ui.view.AspectRatioImageView
import java.lang.ref.WeakReference

class ConnectionViewHolder(
        parent: ViewGroup,
        listener: WeakReference<OnViewHolderClickListener>?
) : ViewHolder<ViewHolder.OnViewHolderClickListener>(parent, listener, R.layout.view_item_social_connect, true) {

    val imageView: AspectRatioImageView = itemView.findViewById(R.id.image)
    val tvTitle: TextView = itemView.findViewById(R.id.tv_title)

    init {
        itemView.tag = false
    }
}

fun ConnectionViewHolder.set(connection: SocialConnection) {
    connection.type.let {
        imageView.setImageResource(it.icon24)
        tvTitle.setText(it.title)
    }
}