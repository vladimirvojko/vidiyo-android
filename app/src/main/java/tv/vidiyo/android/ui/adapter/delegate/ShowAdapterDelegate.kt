package tv.vidiyo.android.ui.adapter.delegate

import android.view.ViewGroup
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.show.Show
import tv.vidiyo.android.base.adapter.delegate.ListProviderAdapterDelegate
import tv.vidiyo.android.base.adapter.provider.ListDataProvider
import tv.vidiyo.android.base.view.holder.ViewHolder.OnViewHolderClickListener
import tv.vidiyo.android.domain.contract.setShow
import tv.vidiyo.android.source.DataType
import tv.vidiyo.android.ui.view.holder.CoverViewHolder


class ShowAdapterDelegate(
        provider: ListDataProvider<Show>,
        listener: OnViewHolderClickListener?,
        @DataType private val type: Int
) : ListProviderAdapterDelegate<Show, OnViewHolderClickListener, CoverViewHolder<OnViewHolderClickListener>>(provider, listener) {

    override fun onCreateViewHolder(parent: ViewGroup) = CoverViewHolder(parent, listener, R.layout.view_item_show).also {
        it.itemView.tag = type
    }

    override fun onBindViewHolder(position: Int, holder: CoverViewHolder<OnViewHolderClickListener>, provider: ListDataProvider<Show>, item: Show) = holder.setShow(item)
}