package tv.vidiyo.android.ui.view.holder

import android.view.ViewGroup
import android.widget.TextView
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.Item
import tv.vidiyo.android.base.view.holder.ViewHolder
import java.lang.ref.WeakReference

class SettingViewHolder(
        parent: ViewGroup,
        listener: WeakReference<OnViewHolderClickListener>?
) : ViewHolder<ViewHolder.OnViewHolderClickListener>(parent, listener, R.layout.view_item_setting, true) {

    val tvText: TextView = itemView.findViewById(R.id.text)
}

fun SettingViewHolder.setItem(item: Item) {
    tvText.setCompoundDrawablesRelativeWithIntrinsicBounds(item.iconResId, 0, 0, 0)
    tvText.setText(item.titleResId)
}