package tv.vidiyo.android.ui.watching

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.Toast
import tv.vidiyo.android.DependencyGraph
import tv.vidiyo.android.R
import tv.vidiyo.android.api.ApiKey
import tv.vidiyo.android.api.model.episode.ViewInfo
import tv.vidiyo.android.base.adapter.pages.PageFragment
import tv.vidiyo.android.base.fragment.StateRecyclerFragment
import tv.vidiyo.android.di.subcomponent.watching.WatchingModule
import tv.vidiyo.android.domain.contract.WatchingContract
import tv.vidiyo.android.domain.presenter.WatchingPresenter
import tv.vidiyo.android.ui.adapter.DataRecyclerAdapter
import tv.vidiyo.android.ui.show.ShowFragment
import tv.vidiyo.android.ui.view.SpaceItemDecoration
import tv.vidiyo.android.ui.view.holder.OnWatchingClickListener
import tv.vidiyo.android.ui.widget.InfiniteScrollRecyclerView
import tv.vidiyo.android.ui.widget.OnInfiniteScrollListener
import tv.vidiyo.android.ui.widget.StateLayout
import tv.vidiyo.android.util.Constant
import javax.inject.Inject

class InnerWatchingFragment : StateRecyclerFragment<DataRecyclerAdapter>(), WatchingContract.Inner.View, OnWatchingClickListener {

    companion object {
        private const val EXTRA = "EXTRA"//is

        private fun newInstance(isResume: Boolean) = InnerWatchingFragment().apply {
            arguments = Bundle().also {
                it.putBoolean(EXTRA, isResume)
            }
        }

        fun resumeCreator() = object : PageFragment.Creator {
            override fun newInstance(): Fragment = InnerWatchingFragment.newInstance(true)
        }

        fun newEpisodesCreator() = object : PageFragment.Creator {
            override fun newInstance(): Fragment = InnerWatchingFragment.newInstance(false)
        }
    }

    override val layoutResId: Int = R.layout.content_state_recycler_center

    @Inject lateinit var presenter: WatchingPresenter
    @Inject lateinit var manager: LocalBroadcastManager

    private var receiver: BroadcastReceiver? = null

    override val isResume: Boolean get() = arguments.getBoolean(EXTRA, false)

    override fun injectDependencies(graph: DependencyGraph) {
        graph.dataComponent.plus(WatchingModule(this)).injectTo(this)
    }

    override fun onPostViewCreated() {
        presenter.start()
    }

    override fun onPause() {
        super.onPause()
        receiver?.let {
            manager.unregisterReceiver(it)
            receiver = null
        }
    }

    override fun onResume() {
        super.onResume()
        if (receiver == null) {
            receiver = newReceiver().also {
                manager.registerReceiver(it, IntentFilter(Constant.ACTION_VIEWED))
            }
        }
    }

    override fun onCreateAdapter(): DataRecyclerAdapter = if (isResume) {
        DataRecyclerAdapter.resume(presenter.main.resumeRepository.provider, this)
    } else {
        DataRecyclerAdapter.newEpisodes(presenter.main.newEpisodesRepository.provider, this)
    }

    override fun onCreateLayoutManager(context: Context?) = LinearLayoutManager(context)
    override fun onRecyclerViewCreated(recyclerView: InfiniteScrollRecyclerView) {
        super.onRecyclerViewCreated(recyclerView)
        recyclerView.addItemDecoration(SpaceItemDecoration(context, true, true))
        if (isResume) {
            recyclerView.listener = infiniteScrollListener()
        }
    }

    override fun onViewClicked(position: Int) {
        mainActivity?.openEpisode(presenter.getEpisode(position) ?: return)
    }

    override fun onShowClicked(position: Int) {
        val show = presenter.getShow(position) ?: return
        startFragment(ShowFragment.newInstance(show))
    }

    override fun onMoreClicked(position: Int) {
        // TODO
        Toast.makeText(context, "not implemented", Toast.LENGTH_SHORT).show()
    }

    override fun showNoContent() {
        stateLayout.changeState(StateLayout.State.CONTENT)
        update()
    }

    override fun onViewHolderClick(viewHolder: RecyclerView.ViewHolder, position: Int) {}

    private fun newReceiver() = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val viewInfo = intent.extras.getParcelable<ViewInfo>(ApiKey.data)
            if (viewInfo == null || presenter.repository.isEmpty) {
                presenter.refresh()
                return
            }

            val current = presenter.findInfo(viewInfo.id)
            if (current != null) {
                if (viewInfo.finished) {
                    presenter.removeById(viewInfo.id)
                } else {
                    current.update(viewInfo)
                }
                update()

            } else {
                presenter.refresh()
            }
        }
    }

    private fun infiniteScrollListener() = object : OnInfiniteScrollListener {
        override fun onInfiniteScroll(recyclerView: InfiniteScrollRecyclerView) {
            if (recyclerView.shouldLoadMore(presenter.totalCount)) {
                presenter.loadMore()
            }
        }
    }
}