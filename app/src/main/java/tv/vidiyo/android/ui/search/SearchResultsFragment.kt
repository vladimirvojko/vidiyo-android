package tv.vidiyo.android.ui.search

import android.os.Bundle
import android.support.v4.view.ViewPager
import android.view.View
import tv.vidiyo.android.DependencyGraph
import tv.vidiyo.android.R
import tv.vidiyo.android.base.adapter.pages.PagesAdapter
import tv.vidiyo.android.base.fragment.PagesMainFragment
import tv.vidiyo.android.di.subcomponent.search.SearchModule
import tv.vidiyo.android.ui.search.InnerSearchResultFragment.Companion.CAST
import tv.vidiyo.android.ui.search.InnerSearchResultFragment.Companion.PEOPLE
import tv.vidiyo.android.ui.search.InnerSearchResultFragment.Companion.SHOWS
import tv.vidiyo.android.ui.search.InnerSearchResultFragment.Companion.TOPICS
import tv.vidiyo.android.ui.view.SearchView
import tv.vidiyo.android.util.OnSearchCallback
import tv.vidiyo.android.util.SearchHelper
import javax.inject.Inject

class SearchResultsFragment : PagesMainFragment(), OnSearchCallback, SearchView.OnSearchViewListener,
        ViewPager.OnPageChangeListener, View.OnClickListener {

    override val appBarLayoutResId = R.layout.app_bar_search_result

    private lateinit var searchView: SearchView

    @Inject lateinit var helper: SearchHelper

    private val query: String? get() = searchView.text
    private val currentFragment: InnerSearchResultFragment?
        get() = adapter.getFragment(viewPager.currentItem) as? InnerSearchResultFragment

    override fun injectDependencies(graph: DependencyGraph) {
        graph.dataComponent.plus(SearchModule(this)).injectTo(this)
    }

    override fun onViewCreated(view: View, b: Bundle?) {
        super.onViewCreated(view, b)
        searchView = view.findViewById(R.id.search_view)
        searchView.setOnClickListener(this)
        searchView.listener = this

        viewPager.addOnPageChangeListener(this)

        view.post { currentFragment?.search(null) }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewPager.clearOnPageChangeListeners()
    }

    override fun search(q: String?) {
        currentFragment?.search(q)
    }

    override fun afterTextChanged(value: String) {
        if (value.isEmpty()) {
            currentFragment?.search(value)

        } else {
            helper.startSearch(value)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        helper.release()
    }

    override fun onClick(v: View?) = onBackPressed()
    override fun createAdapter() = PagesAdapter(context, childFragmentManager, PEOPLE, SHOWS, CAST, TOPICS)

    override fun onPageSelected(position: Int) {
        (adapter.getFragment(position) as? InnerSearchResultFragment)?.search(query)
    }

    override fun onPageScrollStateChanged(state: Int) {}
    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
}