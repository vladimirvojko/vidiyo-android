package tv.vidiyo.android.ui.show

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.LinearLayout
import tv.vidiyo.android.DependencyGraph
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.user.FollowingUser
import tv.vidiyo.android.base.adapter.pages.PageFragment
import tv.vidiyo.android.base.fragment.StateRecyclerFragment
import tv.vidiyo.android.base.fragment.ViewFragment
import tv.vidiyo.android.di.subcomponent.show.ChildShowModule
import tv.vidiyo.android.domain.contract.ShowContract
import tv.vidiyo.android.domain.presenter.show.InnerShowPresenter
import tv.vidiyo.android.extension.setEmptyViewWithFormat
import tv.vidiyo.android.model.InnerPageType
import tv.vidiyo.android.ui.adapter.DataRecyclerAdapter
import tv.vidiyo.android.ui.cast.CastFragment
import tv.vidiyo.android.ui.profile.ProfileFragment
import tv.vidiyo.android.ui.topic.TopicFragment
import tv.vidiyo.android.ui.view.DividerItemDecoration
import tv.vidiyo.android.ui.view.SpaceItemDecoration
import tv.vidiyo.android.ui.view.holder.OnSubscribableViewHolderClickListener
import tv.vidiyo.android.ui.widget.InfiniteScrollRecyclerView
import javax.inject.Inject

class InnerShowFragment : StateRecyclerFragment<DataRecyclerAdapter>(), ShowContract.Inner.View, OnSubscribableViewHolderClickListener {

    companion object {
        private const val EXTRA = "EXTRA"//type

        private fun newInstance(@InnerPageType type: Int) = InnerShowFragment().apply {
            arguments = Bundle().also {
                it.putInt(EXTRA, type)
            }
        }

        object EPISODES_CREATOR : PageFragment.Creator {
            override fun newInstance(): Fragment = InnerShowFragment.newInstance(InnerPageType.EPISODES)
        }

        object CAST_CREATOR : PageFragment.Creator {
            override fun newInstance(): Fragment = InnerShowFragment.newInstance(InnerPageType.CAST)
        }

        object TOPICS_CREATOR : PageFragment.Creator {
            override fun newInstance(): Fragment = InnerShowFragment.newInstance(InnerPageType.TOPICS)
        }

        object SUBSCRIBERS_CREATOR : PageFragment.Creator {
            override fun newInstance(): Fragment = InnerShowFragment.newInstance(InnerPageType.SUBSCRIBERS)
        }
    }

    override val layoutResId: Int = R.layout.fragment_state_recycler_empty
    override var type: Int = InnerPageType.EPISODES

    @Inject
    lateinit var presenter: InnerShowPresenter

    override fun injectDependencies(graph: DependencyGraph) {
        type = arguments.getInt(EXTRA, InnerPageType.SHOWS)
        (parentFragment as? ShowFragment)?.component?.plus(ChildShowModule(this))?.injectTo(this)
    }

    override fun onViewCreated(view: View, b: Bundle?) {
        super.onViewCreated(view, b)
        view.setBackgroundColor(ContextCompat.getColor(context, R.color.lighter_gray))
        val title = getString(when (type) {
            InnerPageType.EPISODES -> R.string.episodes
            InnerPageType.CAST -> R.string.actors
            InnerPageType.TOPICS -> R.string.topics
            InnerPageType.SUBSCRIBERS -> R.string.subscribers
            else -> throw IllegalArgumentException("Wrong type provided")
        }).toLowerCase()
        val drawableRes = when (type) {
            InnerPageType.EPISODES -> R.drawable.svg_episodes
            InnerPageType.CAST -> R.drawable.svg_cast
            InnerPageType.TOPICS -> R.drawable.svg_topic
            InnerPageType.SUBSCRIBERS -> R.drawable.svg_subscription
            else -> throw IllegalArgumentException("Wrong type provided")
        }

        stateLayout.setEmptyViewWithFormat(title, drawableRes)
    }

    override fun onPostViewCreated() {
        presenter.start()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putInt(EXTRA, type)
    }

    override fun onCreateAdapter(): DataRecyclerAdapter {
        return when (type) {
            InnerPageType.EPISODES -> DataRecyclerAdapter.episodes(presenter.getProvider(), this)
            InnerPageType.CAST -> DataRecyclerAdapter.cast(presenter.getProvider(), this)
            InnerPageType.TOPICS -> DataRecyclerAdapter.topics(presenter.getProvider(), null, this)
            InnerPageType.SUBSCRIBERS -> DataRecyclerAdapter.subscribers(presenter.getProvider(), this, presenter.manager)
            else -> throw IllegalArgumentException("Wrong type provided")
        }
    }

    override fun onCreateLayoutManager(context: Context?): RecyclerView.LayoutManager = when (type) {
        InnerPageType.CAST -> GridLayoutManager(context, 2)
        else -> LinearLayoutManager(context)
    }

    override fun onRecyclerViewCreated(recyclerView: InfiniteScrollRecyclerView) {
        super.onRecyclerViewCreated(recyclerView)
        recyclerView.addItemDecoration(SpaceItemDecoration(context, true, true))
        when (type) {
            InnerPageType.CAST, InnerPageType.EPISODES -> return
            else -> recyclerView.addItemDecoration(DividerItemDecoration(context, LinearLayout.VERTICAL))
        }
    }

    override fun onViewHolderClick(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val fragment: ViewFragment = when (type) {
            InnerPageType.EPISODES -> {
                mainActivity?.openEpisode(presenter.get(position) ?: return)
                return
            }
            InnerPageType.TOPICS -> TopicFragment.newInstance(presenter.get(position) ?: return)
            InnerPageType.CAST -> CastFragment.newInstance(presenter.get(position) ?: return)
            InnerPageType.SUBSCRIBERS -> ProfileFragment.newInstance(presenter.get<FollowingUser>(position)?.id ?: return)
            else -> return
        }
        startFragment(fragment)
    }

    override fun onSubscribeClick(viewHolder: RecyclerView.ViewHolder, position: Int) {
        if (refreshLayout?.isRefreshing == false)
            presenter.toggleSubscription(position)
    }

    override fun onSelected() = presenter.refresh()
    override fun retry() = presenter.refresh()
}