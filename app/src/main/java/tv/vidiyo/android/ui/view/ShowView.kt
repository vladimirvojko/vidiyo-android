package tv.vidiyo.android.ui.view

import android.content.Context
import android.util.AttributeSet
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.channel.Channel
import tv.vidiyo.android.api.model.show.Show
import tv.vidiyo.android.api.model.show.ShowInfo
import tv.vidiyo.android.api.model.topic.Topic
import tv.vidiyo.android.extension.DECIMAL_FORMAT
import tv.vidiyo.android.extension.load

class ShowView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0
) : RelativeLayout(context, attrs, defStyleAttr) {

    val ivImage: ImageView
    val tvTitle: TextView
    val tvSubtitle: TextView
    val tvSubscribers: TextView
    val button: StateButton

    init {
        inflate(context, R.layout.view_show, this)
        resources.getDimensionPixelOffset(R.dimen.spacing_x2).let {
            setPadding(it, it, it, it)
        }

        ivImage = findViewById(R.id.iv_image)
        tvTitle = findViewById(R.id.tv_title)
        tvSubtitle = findViewById(R.id.tv_subtitle)
        tvSubscribers = findViewById(R.id.tv_subscribers)
        button = findViewById(R.id.button)
    }
}

fun ShowView.setShow(show: Show) {
    ivImage.load(show.image)
    tvTitle.text = show.name
    tvSubtitle.text = show.category ?: (show as? ShowInfo)?.channel?.name
    tvSubscribers.text = context.getString(R.string.format_s_subscribers, DECIMAL_FORMAT.format((show.subscribers).toDouble()))

    button.state = if (show.isSubscribed) StateButton.STATE_ACTIVE else StateButton.STATE_DEFAULT
    button.isClickable = true
}

fun ShowView.setTopic(topic: Topic, channel: Channel? = null) {
    ivImage.load(topic.image)
    tvTitle.text = topic.name
    tvSubtitle.text = channel?.name ?: topic.channel
    tvSubscribers.text = context.getString(R.string.format_s_subscribers, DECIMAL_FORMAT.format((topic.subscribers).toDouble()))

    button.state = if (topic.isSubscribed) StateButton.STATE_ACTIVE else StateButton.STATE_DEFAULT
    button.isClickable = true
}