package tv.vidiyo.android.ui.discover

import android.Manifest
import android.os.Bundle
import android.support.v4.app.LoaderManager
import android.support.v4.content.Loader
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.view.MenuItem
import android.widget.LinearLayout
import android.widget.Toast
import tv.vidiyo.android.DependencyGraph
import tv.vidiyo.android.R
import tv.vidiyo.android.base.fragment.StateRecyclerMainFragment
import tv.vidiyo.android.di.subcomponent.discover.DiscoverModule
import tv.vidiyo.android.domain.contract.DiscoverContract
import tv.vidiyo.android.domain.presenter.discover.DiscoveredPresenter
import tv.vidiyo.android.extension.isPermissionGranted
import tv.vidiyo.android.loader.ContactsLoader
import tv.vidiyo.android.model.Contact
import tv.vidiyo.android.ui.adapter.DataRecyclerAdapter
import tv.vidiyo.android.ui.profile.ProfileFragment
import tv.vidiyo.android.ui.view.DividerItemDecoration
import tv.vidiyo.android.ui.view.holder.OnSubscribableViewHolderClickListener
import tv.vidiyo.android.ui.widget.InfiniteScrollRecyclerView
import javax.inject.Inject

class DiscoveredFragment : StateRecyclerMainFragment<DataRecyclerAdapter>(),
        DiscoverContract.Discovered.View, OnSubscribableViewHolderClickListener,
        LoaderManager.LoaderCallbacks<List<Contact>> {

    companion object {
        private const val EXTRA_0 = "EXTRA_0"//type
        private const val EXTRA_1 = "EXTRA_1"//connected

        private const val REQUEST_CODE = 1337

        fun newInstance(type: Int, connected: Boolean) = DiscoveredFragment().apply {
            arguments = Bundle(2).also {
                it.putInt(EXTRA_0, type)
                it.putBoolean(EXTRA_1, connected)
            }
        }
    }

    override val contentLayoutResId = R.layout.content_state_recycler_center
    override val optionsMenuResId = R.menu.discovered
    override val toolbarIndicator = R.drawable.svg_back
    override val toolbarTitleRes get() = if (type == 0) R.string.facebook else R.string.contacts

    override val type: Int get() = arguments.getInt(EXTRA_0, 0)
    override val connected: Boolean get() = arguments.getBoolean(EXTRA_1, false)

    @Inject
    lateinit var presenter: DiscoveredPresenter

    override fun injectDependencies(graph: DependencyGraph) {
        mainActivity?.socialComponent?.plus(DiscoverModule(this))?.injectTo(this)
    }

    override fun initLoader() {
        loaderManager.initLoader(0, null, this).forceLoad()
    }

    override fun onPostViewCreated() {
        presenter.start()
    }

    override fun onDestroy() {
        super.onDestroy()
        loaderManager.getLoader<Any>(0)?.let { loaderManager.destroyLoader(0) }
    }

    override fun onSubscribeClick(viewHolder: RecyclerView.ViewHolder, position: Int) {
        if (!isRefreshing) presenter.follow(position)
    }

    override fun onViewHolderClick(viewHolder: RecyclerView.ViewHolder, position: Int) {
        if (!isRefreshing) when (position) {
            0 -> presenter.followAll()

            else -> {
                val user = presenter.get(position) ?: return
                startFragment(ProfileFragment.newInstance(user.id))
            }
        }
    }

    override fun onMenuItemClick(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.item_settings -> if (!isRefreshing) {
                AlertDialog.Builder(context, R.style.AppTheme_AlertDialog)
                        .setTitle(R.string.discover_dialog_title)
                        .setMessage(R.string.discover_dialog_message)
                        .setPositiveButton(R.string.disconnect) { _, _ ->
                            presenter.disconnect()
                        }.setNegativeButton(R.string.Cancel, null).show()
            }

            else -> return super.onMenuItemClick(item)
        }
        return true
    }

    override fun onCreateAdapter() = DataRecyclerAdapter.discovered(presenter.provider, this, presenter.subscriptionManager)
    override fun onRecyclerViewCreated(recyclerView: InfiniteScrollRecyclerView) {
        super.onRecyclerViewCreated(recyclerView)
        recyclerView.addItemDecoration(DividerItemDecoration(context, LinearLayout.VERTICAL))
    }

    //region Loader
    override fun onLoaderReset(loader: Loader<List<Contact>>?) = Unit

    override fun onLoadFinished(loader: Loader<List<Contact>>?, data: List<Contact>?) {
        presenter.onContactsLoaded(data ?: return)
    }

    override fun onCreateLoader(id: Int, args: Bundle?): Loader<List<Contact>> {
        return ContactsLoader(context)
    }
    //endregion

    //region Permission
    override fun permissionGranted() = context.isPermissionGranted(Manifest.permission.READ_CONTACTS)

    override fun requestPermissions() = requestPermissions(arrayOf(Manifest.permission.READ_CONTACTS), REQUEST_CODE)

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_CODE -> {
                if (isPermissionGranted(Manifest.permission.READ_CONTACTS, permissions, grantResults)) {
                    initLoader()

                } else {
                    Toast.makeText(context, R.string.error_permission_contacts, Toast.LENGTH_SHORT).show()
                    onBackPressed()
                }
            }

            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }
    //endregion
}