package tv.vidiyo.android.ui.view.holder

import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.load.resource.drawable.GlideDrawable
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import tv.vidiyo.android.R
import tv.vidiyo.android.base.view.holder.ViewHolder
import tv.vidiyo.android.domain.contract.FriendShowView
import tv.vidiyo.android.extension.load
import tv.vidiyo.android.extension.unit
import tv.vidiyo.android.ui.view.ShapeImageView
import java.lang.Exception
import java.lang.ref.WeakReference

interface OnFriendShowViewHolderClickListener {
    fun onFriendClicked(position: Int)
    fun onShowClicked(position: Int)
}

class FriendShowViewHolder(parent: ViewGroup,
                           listener: WeakReference<OnFriendShowViewHolderClickListener>?
) : ViewHolder<OnFriendShowViewHolderClickListener>(parent, listener, R.layout.view_item_friend_show, false), FriendShowView {

    private val ivImage: ImageView = itemView.findViewById(R.id.iv_image)
    private val tvTitle: TextView = itemView.findViewById(R.id.tv_title)
    private val ivAvatar: ShapeImageView = itemView.findViewById(R.id.iv_avatar)
    private val tvName: TextView = itemView.findViewById(R.id.tv_name)
    private val tvUsername: TextView = itemView.findViewById(R.id.tv_username)

    init {
        itemView.findViewById<View>(R.id.rl_user).setOnClickListener { listener?.get()?.onFriendClicked(adapterPosition) }
        itemView.findViewById<View>(R.id.cl_image).setOnClickListener { listener?.get()?.onShowClicked(adapterPosition) }
    }

    override fun setImage(imageUrl: String?) = ivImage.load(imageUrl).unit()

    // region CoverView
    override fun setTitle(title: String) {
        tvTitle.text = title
    }
    // endregion

    // region UserView
    override fun <T> setAvatar(avatar: T?, fallbackImageResId: Int) {
        ivAvatar.setImageResource(fallbackImageResId)
        ivAvatar.load(avatar, requestListener = object : RequestListener<T?, GlideDrawable> {
            override fun onResourceReady(resource: GlideDrawable?, model: T?, target: Target<GlideDrawable>?, isFromMemoryCache: Boolean, isFirstResource: Boolean): Boolean {
                ivAvatar.setImageDrawable(resource)
                return true
            }

            override fun onException(e: Exception?, model: T?, target: Target<GlideDrawable>?, isFirstResource: Boolean): Boolean {
                ivAvatar.setImageResource(fallbackImageResId)
                return true
            }
        })
    }

    override fun setUsername(username: String?) {
        tvUsername.text = username
    }

    override fun setName(name: String?) {
        tvName.text = name
    }
    // endregion
}