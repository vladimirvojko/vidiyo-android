package tv.vidiyo.android.ui.view.holder

import android.view.ViewGroup
import tv.vidiyo.android.R
import tv.vidiyo.android.base.view.holder.ViewHolder
import tv.vidiyo.android.ui.view.ShapeImageView
import java.lang.ref.WeakReference

class WriteTextViewHolder(
        parent: ViewGroup,
        listener: WeakReference<OnCommentClickListener>?
) : ViewHolder<OnCommentClickListener>(parent, listener, R.layout.view_item_write_text, true) {

    val ivAvatar: ShapeImageView = itemView.findViewById(R.id.iv_avatar)
}