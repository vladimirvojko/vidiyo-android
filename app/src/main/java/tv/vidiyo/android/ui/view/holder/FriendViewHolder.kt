package tv.vidiyo.android.ui.view.holder

import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import tv.vidiyo.android.R
import tv.vidiyo.android.base.view.holder.ViewHolder.OnViewHolderClickListener
import tv.vidiyo.android.extension.changeVisibility
import java.lang.ref.WeakReference

class FriendViewHolder(
        parent: ViewGroup,
        listener: WeakReference<OnViewHolderClickListener>?
) : UserViewHolder<OnViewHolderClickListener>(parent, listener, R.layout.view_item_friend, false) {

    val btnSelect: Button = itemView.findViewById(R.id.btn_select)
    val tvSelected: TextView = itemView.findViewById(R.id.tv_selected)

    init {
        val l = View.OnClickListener {
            listener?.get()?.onViewHolderClick(this, adapterPosition)
        }

        btnSelect.setOnClickListener(l)
        tvSelected.setOnClickListener(l)
    }

    fun changeButtonState(hasAccount: Boolean, isSelected: Boolean) {
        btnSelect.setText(if (hasAccount) R.string.follow else R.string.invite)
        tvSelected.setText(if (hasAccount) R.string.following else R.string.invited)
        btnSelect.changeVisibility(!isSelected, View.INVISIBLE)
        tvSelected.changeVisibility(isSelected, View.INVISIBLE)
    }
}