package tv.vidiyo.android.ui.view.holder

import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import tv.vidiyo.android.R
import tv.vidiyo.android.base.view.holder.ViewHolder
import java.lang.ref.WeakReference

class DiscoveredHeaderViewHolder(
        parent: ViewGroup,
        listener: WeakReference<OnViewHolderClickListener>?
) : ViewHolder<ViewHolder.OnViewHolderClickListener>(parent, listener, R.layout.view_header_discovered, false) {

    val tvTitle = itemView.findViewById<TextView>(R.id.tv_title)
    private val button = itemView.findViewById<Button>(R.id.button)

    init {
        button.setOnClickListener {
            listener?.get()?.onViewHolderClick(this, adapterPosition)
        }
    }
}