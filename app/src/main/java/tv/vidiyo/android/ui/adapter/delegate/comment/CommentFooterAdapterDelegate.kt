package tv.vidiyo.android.ui.adapter.delegate.comment

import android.view.ViewGroup
import tv.vidiyo.android.api.model.comment.Comment
import tv.vidiyo.android.base.adapter.delegate.SectionedAdapterDelegate
import tv.vidiyo.android.base.adapter.provider.SectionedDataProvider
import tv.vidiyo.android.base.adapter.section.ItemPosition
import tv.vidiyo.android.ui.view.holder.CommentFooterViewHolder
import tv.vidiyo.android.ui.view.holder.OnCommentClickListener

class CommentFooterAdapterDelegate(
        provider: SectionedDataProvider<Comment, Comment>,
        listener: OnCommentClickListener?
) : SectionedAdapterDelegate<Comment, Comment, SectionedDataProvider<Comment, Comment>, OnCommentClickListener, CommentFooterViewHolder>(provider, listener) {

    override fun onCreateViewHolder(parent: ViewGroup) = CommentFooterViewHolder(parent, listener)
    override fun onBindViewHolder(position: ItemPosition, holder: CommentFooterViewHolder, provider: SectionedDataProvider<Comment, Comment>) = Unit
    override fun isForViewType(itemPosition: ItemPosition) = itemPosition.isFooter
}