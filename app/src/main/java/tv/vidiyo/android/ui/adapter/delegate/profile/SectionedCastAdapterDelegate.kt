package tv.vidiyo.android.ui.adapter.delegate.profile

import android.view.ViewGroup
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.cast.Cast
import tv.vidiyo.android.api.model.channel.Channel
import tv.vidiyo.android.base.adapter.delegate.SectionedAdapterDelegate
import tv.vidiyo.android.base.adapter.provider.SectionedListDataProvider
import tv.vidiyo.android.base.adapter.section.ItemPosition
import tv.vidiyo.android.ui.view.holder.OnSubscribableViewHolderClickListener
import tv.vidiyo.android.ui.view.holder.SubscribableViewHolder
import tv.vidiyo.android.ui.view.holder.setCast
import tv.vidiyo.android.util.SubscriptionManager

class SectionedCastAdapterDelegate(
        provider: SectionedListDataProvider<Channel, Cast>,
        listener: OnSubscribableViewHolderClickListener?,
        private val manager: SubscriptionManager? = null
) : SectionedAdapterDelegate<Channel, Cast, SectionedListDataProvider<Channel, Cast>,
        OnSubscribableViewHolderClickListener, SubscribableViewHolder>(provider, listener) {

    override fun onCreateViewHolder(parent: ViewGroup): SubscribableViewHolder {
        return SubscribableViewHolder(parent, listener, R.layout.view_item_subscribable_cast)
    }

    override fun onBindViewHolder(position: ItemPosition, holder: SubscribableViewHolder, provider: SectionedListDataProvider<Channel, Cast>) {
        holder.setCast(provider.getItemForPosition(position) ?: return, manager)
    }

    override fun isForViewType(itemPosition: ItemPosition) = itemPosition.isItem
}