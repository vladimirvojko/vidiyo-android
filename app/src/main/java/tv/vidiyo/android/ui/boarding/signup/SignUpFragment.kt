package tv.vidiyo.android.ui.boarding.signup

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.support.v7.app.ActionBar
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.TextView
import tv.vidiyo.android.R
import tv.vidiyo.android.base.BaseActivity
import tv.vidiyo.android.base.fragment.BaseFragment
import tv.vidiyo.android.extension.color
import tv.vidiyo.android.extension.setTextWithClickableLink
import tv.vidiyo.android.ui.boarding.signin.SignInFragment

class SignUpFragment : BaseFragment(), View.OnClickListener {

    abstract class SignUpInnerFragment : BaseFragment() {
        lateinit var colors: Array<Int>

        override fun onCreate(b: Bundle?) {
            super.onCreate(b)
            colors = arrayOf(
                    context.color(R.color.divider),
                    context.color(R.color.blue),
                    context.color(R.color.red)
            )
        }

        @SuppressLint("MissingSuperCall")
        override fun onActionBarReady(bar: ActionBar) {
        }

        open fun validate(s: CharSequence?) {}

        open fun changeColor(divider: View, hasFocus: Boolean, hasError: CharSequence?) {
            val index = when {
                hasError?.isNotEmpty() == true -> 2
                hasFocus -> 1
                else -> 0
            }

            divider.setBackgroundColor(colors[index])
        }

        protected open val watcher = object : TextWatcher {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                validate(s)
            }

            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
        }
    }

    override val layoutResId = R.layout.fragment_sign_up
    override val toolbarTitleRes = R.string.boarding_sign_up

    var actionBar: ActionBar? = null
    lateinit var tabLayout: TabLayout
    lateinit var viewPager: ViewPager

    lateinit var adapter: SignUpPagerAdapter

    override fun onCreate(b: Bundle?) {
        super.onCreate(b)
        adapter = SignUpPagerAdapter(childFragmentManager, context.resources.getStringArray(R.array.boarding_sign_up_tabs))
    }

    override fun onActionBarReady(bar: ActionBar) {
        super.onActionBarReady(bar)
        actionBar = bar
        actionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
            elevation = 0f
        }
    }

    private fun offsetRefreshLayout(reset: Boolean) {
        val offset = context.resources.getDimensionPixelOffset(if (reset) R.dimen.spacing_x2 else R.dimen.offset_refresh)
        (activity as? BaseActivity)?.refreshLayout?.setProgressViewOffset(false, offset, offset)
    }

    override fun onResume() {
        super.onResume()
        offsetRefreshLayout(false)
    }

    override fun onViewCreated(view: View, b: Bundle?) {
        super.onViewCreated(view, b)
        tabLayout = view.findViewById<TabLayout>(R.id.tab_layout)
        viewPager = view.findViewById<ViewPager>(R.id.view_pager)

        view.findViewById<TextView>(R.id.tv_have_account).setTextWithClickableLink(R.string.boarding_have_account, this)

        viewPager.adapter = adapter
        tabLayout.setupWithViewPager(viewPager)

        offsetRefreshLayout(false)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.tv_have_account -> startFragment(SignInFragment(), false)
        }
    }
}