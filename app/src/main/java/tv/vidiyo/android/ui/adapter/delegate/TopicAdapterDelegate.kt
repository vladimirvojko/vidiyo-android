package tv.vidiyo.android.ui.adapter.delegate

import android.view.ViewGroup
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.topic.Topic
import tv.vidiyo.android.base.adapter.delegate.ListProviderAdapterDelegate
import tv.vidiyo.android.base.adapter.provider.ListDataProvider
import tv.vidiyo.android.base.view.holder.ViewHolder.OnViewHolderClickListener
import tv.vidiyo.android.domain.contract.setTopic
import tv.vidiyo.android.source.DataType
import tv.vidiyo.android.ui.view.holder.CoverViewHolder

class TopicAdapterDelegate(
        provider: ListDataProvider<Topic>,
        listener: OnViewHolderClickListener?,
        @DataType private val type: Int
) : ListProviderAdapterDelegate<Topic, OnViewHolderClickListener, CoverViewHolder<OnViewHolderClickListener>>(provider, listener) {

    override fun onCreateViewHolder(parent: ViewGroup) = CoverViewHolder(parent, listener, R.layout.view_item_topic).also {
        it.itemView.tag = type
    }

    override fun onBindViewHolder(position: Int, holder: CoverViewHolder<OnViewHolderClickListener>, provider: ListDataProvider<Topic>, item: Topic) = holder.setTopic(item)
}