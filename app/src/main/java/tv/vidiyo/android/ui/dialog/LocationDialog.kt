package tv.vidiyo.android.ui.dialog

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import android.widget.LinearLayout
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.City
import tv.vidiyo.android.base.BaseDialog
import tv.vidiyo.android.base.adapter.provider.FilterableDataProvider
import tv.vidiyo.android.base.fragment.BaseFragment
import tv.vidiyo.android.base.view.holder.ViewHolder
import tv.vidiyo.android.extension.has
import tv.vidiyo.android.extension.notContains
import tv.vidiyo.android.ui.adapter.DataRecyclerAdapter
import tv.vidiyo.android.ui.settings.EditProfileFragment
import tv.vidiyo.android.ui.view.DividerItemDecoration
import tv.vidiyo.android.util.AssetsLoader

class LocationDialog : BaseDialog(), View.OnClickListener, ViewHolder.OnViewHolderClickListener, TextWatcher {

    companion object {
        fun show(fragment: BaseFragment, cancelable: Boolean): Boolean {
            val m = fragment.childFragmentManager
            if (m.notContains(LocationDialog::class.java.name)) {
                val dialog = LocationDialog()
                dialog.isCancelable = cancelable
                dialog.show(m)

                return true
            }
            return false
        }
    }

    override val layoutRes = R.layout.fragment_location

    private val provider = FilterableDataProvider<City>()

    private lateinit var editText: EditText
    private lateinit var adapter: DataRecyclerAdapter

    override fun onCreate(b: Bundle?) {
        super.onCreate(b)
        provider.addAll(AssetsLoader.loadCities(context), true)
        adapter = DataRecyclerAdapter.cities(provider, this)
    }

    override fun onViewCreated(view: View, b: Bundle?) {
        super.onViewCreated(view, b)
        editText = view.findViewById(R.id.search_view)
        editText.addTextChangedListener(this)

        with(view.findViewById<Toolbar>(R.id.toolbar)) {
            title = getString(R.string.settings_field_location)
            setNavigationIcon(R.drawable.svg_back)
            setNavigationOnClickListener(this@LocationDialog)
        }

        with(view.findViewById<RecyclerView>(R.id.recycler_view)) {
            layoutManager = LinearLayoutManager(context)
            adapter = this@LocationDialog.adapter

            addItemDecoration(DividerItemDecoration(context, LinearLayout.VERTICAL))
        }
    }

    override fun onViewHolderClick(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val city = provider.items[position]
        (parentFragment as? EditProfileFragment)?.updateLocation(city.address)
        dismiss()
    }

    override fun onClick(v: View?) {
        dismiss()
    }

    override fun afterTextChanged(editable: Editable?) {
        val query = editable?.toString()
        with(provider) {
            if (query != null && query.isNotBlank()) filter { city ->
                city.address.has(query)
            } else clearFilter()
        }
        adapter.notifyDataSetChanged()
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
    override fun onCleanUp() {}
}