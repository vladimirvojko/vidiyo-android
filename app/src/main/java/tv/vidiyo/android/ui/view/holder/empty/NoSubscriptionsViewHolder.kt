package tv.vidiyo.android.ui.view.holder.empty

import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import tv.vidiyo.android.R
import tv.vidiyo.android.base.view.holder.ViewHolder
import java.lang.ref.WeakReference

interface NoSubscriptionsClickListener {
    fun onExploreChannelsClicked()
}

class NoSubscriptionsViewHolder(
        parent: ViewGroup,
        listener: WeakReference<NoSubscriptionsClickListener>?
) : ViewHolder<NoSubscriptionsClickListener>(parent, null, R.layout.view_empty_subscription, false) {

    val tvDescription: TextView = itemView.findViewById(R.id.tv_description)
    val btnExplore: Button = itemView.findViewById(R.id.btn_explore)

    init {
        btnExplore.setOnClickListener { listener?.get()?.onExploreChannelsClicked() }
    }
}