package tv.vidiyo.android.ui.adapter.delegate

import android.view.ViewGroup
import tv.vidiyo.android.api.model.episode.Product
import tv.vidiyo.android.base.adapter.delegate.ListProviderAdapterDelegate
import tv.vidiyo.android.base.adapter.provider.ListDataProvider
import tv.vidiyo.android.base.view.holder.ViewHolder
import tv.vidiyo.android.ui.view.holder.ProductsHeaderViewHolder

class ProductsHeaderAdapterDelegate(
        provider: ListDataProvider<Product>,
        listener: ViewHolder.OnViewHolderClickListener?
) : ListProviderAdapterDelegate<Product, ViewHolder.OnViewHolderClickListener, ProductsHeaderViewHolder>(provider, listener) {

    override fun onCreateViewHolder(parent: ViewGroup) = ProductsHeaderViewHolder(parent, listener)
    override fun onBindViewHolder(position: Int, holder: ProductsHeaderViewHolder, provider: ListDataProvider<Product>) = Unit
    override fun onBindViewHolder(position: Int, holder: ProductsHeaderViewHolder, provider: ListDataProvider<Product>, item: Product) = Unit

    override fun isForViewType(position: Int, provider: ListDataProvider<Product>) = position < 0
}