package tv.vidiyo.android.ui.widget

import android.content.Context
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.AttributeSet
import android.view.ViewGroup
import tv.vidiyo.android.ui.view.holder.ProgressViewHolder

interface OnInfiniteScrollListener {
    fun onInfiniteScroll(recyclerView: InfiniteScrollRecyclerView)
}

class InfiniteScrollRecyclerView : RecyclerView {

    companion object {
        const val DEFAULT_VISIBLE_ITEMS_THRESHOLD: Int = 5

        abstract class Adapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

            companion object {
                private const val TYPE_PROGRESS = -2
            }

            private var isLoading: Boolean = false

            abstract fun getTotalItemCount(): Int
            abstract fun onCreateItemViewHolder(parent: ViewGroup, viewType: Int): ViewHolder
            abstract fun onBindItemViewHolder(holder: ViewHolder, position: Int)
            abstract fun getViewTypeForItem(position: Int): Int

            override final fun getItemCount(): Int {
                return getTotalItemCount() + if (isLoading) 1 else 0
            }

            override final fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
                if (viewType == TYPE_PROGRESS) {
                    return ProgressViewHolder(parent)
                }
                return onCreateItemViewHolder(parent, viewType)
            }

            override final fun onBindViewHolder(holder: ViewHolder, position: Int) {
                if (holder.itemViewType != TYPE_PROGRESS) {
                    onBindItemViewHolder(holder, position)
                }
            }

            override final fun getItemViewType(position: Int): Int {
                if (isLoading && getTotalItemCount() == position) {
                    return TYPE_PROGRESS
                }
                return getViewTypeForItem(position)
            }

            internal fun setIsProgressShown(isShown: Boolean) {
                isLoading = isShown
                if (isShown) {
                    notifyItemInserted(getTotalItemCount())
                } else {
                    notifyItemRemoved(getTotalItemCount())
                }
            }
        }
    }

    constructor (context: Context) : super(context)
    constructor (context: Context, attrs: AttributeSet) : super(context, attrs)
    constructor (context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    var listener: OnInfiniteScrollListener? = null

    private var visibleItemsThreshold = DEFAULT_VISIBLE_ITEMS_THRESHOLD
    private var previousTotalItemCount = 0
    private var isLoading = false
    private var finishedLoading = true

    override fun setAdapter(adapter: RecyclerView.Adapter<*>?) {
        if (adapter == null || adapter is Adapter) {
            super.setAdapter(adapter)
        } else {
            throw IllegalArgumentException("Only RecyclerAdapter supported")
        }
    }

    override fun onScrolled(dx: Int, dy: Int) {
        super.onScrolled(dx, dy)
        val listener = listener ?: return
        val adapter = adapter as? Adapter ?: return
        val layoutManager = layoutManager
        val totalItemCount: Int
        val lastVisibleItem: Int

        when (layoutManager) {
            is LinearLayoutManager -> {
                totalItemCount = layoutManager.itemCount
                lastVisibleItem = layoutManager.findLastVisibleItemPosition()
            }
            is GridLayoutManager -> {
                totalItemCount = layoutManager.itemCount
                lastVisibleItem = layoutManager.findLastVisibleItemPosition()
            }
            else -> return
        }

        val oldLoading = isLoading
        when {
            totalItemCount < previousTotalItemCount -> {
                previousTotalItemCount = totalItemCount
                if (totalItemCount == 0) {
                    isLoading = true
                }
                return
            }

            finishedLoading && isLoading && totalItemCount > previousTotalItemCount -> {
                isLoading = false
                previousTotalItemCount = totalItemCount
            }

            finishedLoading && !isLoading && lastVisibleItem + visibleItemsThreshold > totalItemCount -> {
                finishedLoading = false
                isLoading = true
                listener.onInfiniteScroll(this)
            }
        }

        if (oldLoading != isLoading) {
            updateProgress(adapter, isLoading)
        }
    }

    fun shouldLoadMore(total: Int): Boolean {
        return if (adapter.itemCount >= total) {
            finishInfiniteScroll()
            listener = null
            false

        } else true
    }

    fun resetState() {
        previousTotalItemCount = 0
        isLoading = true
        finishedLoading = true
    }

    fun finishLoading() {
        finishedLoading = true
    }

    fun finishInfiniteScroll() {
        finishedLoading = true
        isLoading = false
        updateProgress(adapter as? Adapter ?: return, isLoading)
    }

    private fun updateProgress(adapter: Adapter, shown: Boolean) {
        post { adapter.setIsProgressShown(shown) }
    }
}