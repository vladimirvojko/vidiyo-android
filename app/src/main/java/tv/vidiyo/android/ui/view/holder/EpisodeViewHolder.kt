package tv.vidiyo.android.ui.view.holder

import android.support.v7.widget.AppCompatImageView
import android.view.ViewGroup
import android.widget.TextView
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.episode.Episode
import tv.vidiyo.android.base.view.holder.ViewHolder
import tv.vidiyo.android.extension.MMM_dd_YY
import tv.vidiyo.android.extension.fromHtml
import tv.vidiyo.android.extension.load
import java.lang.ref.WeakReference

class EpisodeViewHolder(
        parent: ViewGroup,
        listener: WeakReference<OnViewHolderClickListener>?
) : ViewHolder<ViewHolder.OnViewHolderClickListener>(parent, listener, R.layout.view_item_episode, true) {

    val ivPhoto: AppCompatImageView = itemView.findViewById(R.id.iv_image)
    val tvTitle: TextView = itemView.findViewById(R.id.tv_title)
    val tvSubtitle: TextView = itemView.findViewById(R.id.tv_subtitle)
    val tvDate: TextView = itemView.findViewById(R.id.tv_date)

    init {
        itemView.tag = false
    }
}

fun EpisodeViewHolder.setEpisode(episode: Episode) {
    ivPhoto.load(episode.image)
    tvTitle.text = episode.showName
    tvSubtitle.fromHtml(context.getString(R.string.format_season_d_episode_d, episode.seasonsNumber, episode.episodesNumber))
    tvDate.text = MMM_dd_YY.format(episode.publishDate * 1000)
}