package tv.vidiyo.android.ui.view.holder

import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import tv.vidiyo.android.R
import tv.vidiyo.android.base.view.holder.ViewHolder
import tv.vidiyo.android.ui.adapter.delegate.profile.OnViewAllClickListener
import java.lang.ref.WeakReference

class ViewAllHeaderViewHolder(
        parent: ViewGroup,
        listener: WeakReference<OnViewAllClickListener>?
) : ViewHolder<OnViewAllClickListener>(parent, listener, R.layout.view_header_view_all, false) {

    val tvTitle: TextView = itemView.findViewById(R.id.tv_title)
    val button: Button = itemView.findViewById(R.id.button)

    init {
        listener?.let {
            button.setOnClickListener {
                listener.get()?.onViewAllClick(this, adapterPosition)
            }
        }
        itemView.tag = false // divider visibility
    }

    fun setTitle(where: String?) {
        tvTitle.text = context.getString(R.string.format_subscribed_in_s, where)
    }
}