package tv.vidiyo.android.ui.view

import android.content.Context
import android.support.v7.widget.CardView
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import tv.vidiyo.android.R

/**
 * @author Maxim Ambroskin kkxmshu@gmail.com
 * @since 07/09/17
 */
class SearchView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0
) : CardView(context, attrs, defStyleAttr) {

    interface OnSearchViewListener {
        fun afterTextChanged(value: String)
    }

    private val ivIcon: ImageView
    private val etSearch: EditText

    var listener: OnSearchViewListener? = null
    val text: String? get() = etSearch.text?.toString()

    init {
        View.inflate(context, R.layout.view_search_edit, this)

        ivIcon = findViewById(R.id.iv_icon)
        etSearch = findViewById(R.id.et_search)
    }

    override fun setOnClickListener(listener: OnClickListener?) {
        ivIcon.setOnClickListener(listener)
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        etSearch.addTextChangedListener(watcher)
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        etSearch.removeTextChangedListener(watcher)
    }

    private val watcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            s?.let { listener?.afterTextChanged(it.toString()) }
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) = Unit
    }
}