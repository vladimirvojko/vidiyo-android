package tv.vidiyo.android.ui.adapter.delegate

import android.view.ViewGroup
import tv.vidiyo.android.base.adapter.delegate.SectionedAdapterDelegate
import tv.vidiyo.android.base.adapter.provider.SectionedDataProvider
import tv.vidiyo.android.base.adapter.section.ItemPosition
import tv.vidiyo.android.base.view.holder.ViewHolder.OnViewHolderClickListener
import tv.vidiyo.android.model.FriendsListType
import tv.vidiyo.android.ui.view.holder.FooterButtonViewHolder


class FooterButtonDelegate<S, I>(
        provider: SectionedDataProvider<S, I>,
        listener: OnViewHolderClickListener?
) : SectionedAdapterDelegate<S, I, SectionedDataProvider<S, I>, OnViewHolderClickListener, FooterButtonViewHolder>(provider, listener) {

    override fun onCreateViewHolder(parent: ViewGroup): FooterButtonViewHolder {
        return FooterButtonViewHolder(parent, listener)
    }

    override fun onBindViewHolder(position: ItemPosition, holder: FooterButtonViewHolder, provider: SectionedDataProvider<S, I>) {
        (provider.getSection()?.section as? FriendsListType)?.let {
            holder.button.text = it.buttonTitle(holder.context)
        }
    }

    override fun isForViewType(itemPosition: ItemPosition) = itemPosition.isFooter
}