package tv.vidiyo.android.ui.view.holder

import android.view.ViewGroup
import android.widget.TextView
import tv.vidiyo.android.R
import tv.vidiyo.android.base.view.holder.ViewHolder
import tv.vidiyo.android.ui.view.AspectRatioImageView
import tv.vidiyo.android.ui.view.StateButton
import java.lang.ref.WeakReference

class CastViewHolder(
        parent: ViewGroup,
        listener: WeakReference<OnSubscribableViewHolderClickListener>?
) : ViewHolder<OnSubscribableViewHolderClickListener>(parent, listener, R.layout.view_item_cast, true) {

    val ivPhoto: AspectRatioImageView = itemView.findViewById(R.id.iv_photo)
    val tvName: TextView = itemView.findViewById(R.id.tv_name)
    val tvTitle: TextView = itemView.findViewById(R.id.tv_title)
    val button: StateButton = itemView.findViewById(R.id.button)

    init {
        button.setOnClickListener {
            listener?.get()?.onSubscribeClick(this, adapterPosition)
        }
    }
}