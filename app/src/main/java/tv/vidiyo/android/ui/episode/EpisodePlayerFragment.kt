package tv.vidiyo.android.ui.episode

import android.os.Bundle
import android.support.v7.widget.PopupMenu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.ui.PlaybackControlView
import com.google.android.exoplayer2.ui.SimpleExoPlayerView
import tv.vidiyo.android.DependencyGraph
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.episode.EpisodeInfo
import tv.vidiyo.android.base.fragment.BaseFragment
import tv.vidiyo.android.di.subcomponent.episode.ChildEpisodeModule
import tv.vidiyo.android.domain.contract.EpisodeContract
import tv.vidiyo.android.domain.presenter.episode.EpisodePlayerPresenter
import tv.vidiyo.android.extension.assignFrom
import tv.vidiyo.android.ui.FullscreenActivity
import tv.vidiyo.android.ui.dialog.ShareEpisodeSheetDialog
import tv.vidiyo.android.util.Animation
import tv.vidiyo.android.util.ExoPlayerAssistant
import java.lang.ref.WeakReference
import javax.inject.Inject

class EpisodePlayerFragment : BaseFragment(), EpisodeContract.Player.View, View.OnClickListener,
        PlaybackControlView.VisibilityListener, PopupMenu.OnMenuItemClickListener {

    companion object {
        private const val EXTRA = "EXTRA"//episodeId

        fun newInstance(id: Int) = EpisodePlayerFragment().apply {
            arguments = Bundle(1).also {
                it.putInt(EXTRA, id)
            }
        }
    }

    override val layoutResId = R.layout.episode_player

    lateinit var playerView: SimpleExoPlayerView

    private lateinit var controlsLayout: RelativeLayout
    private lateinit var playButton: ImageView
    private lateinit var popup: PopupMenu
    private lateinit var likeView: TextView
    private lateinit var ivLike: ImageView

    @Inject lateinit var presenter: EpisodePlayerPresenter
    @Inject lateinit var assistant: ExoPlayerAssistant

    private val episodeId: Int get() = arguments.getInt(EXTRA, 0)

    var listener: WeakReference<PlayerListener>? = null

    override fun injectDependencies(graph: DependencyGraph) {
        graph.getEpisodeComponent(episodeId).plus(ChildEpisodeModule(this)).injectTo(this)
    }

    override fun onViewCreated(view: View, b: Bundle?) {
        super.onViewCreated(view, b)
        playerView = view.findViewById(R.id.exo_player_view)
        controlsLayout = playerView.findViewById(R.id.rl_controls)
        playButton = playerView.findViewById(R.id.btn_play)
        ivLike = playerView.findViewById(R.id.exo_like)

        likeView = view.findViewById(R.id.like_view)
        likeView.tag = false
        likeView.post {
            likeView.translationY = likeView.height * Animation.MULTIPLIER
            likeView.visibility = View.GONE
        }

        val ivMore = playerView.findViewById<ImageView>(R.id.exo_more)

        popup = PopupMenu(context, ivMore)
        popup.menuInflater.inflate(R.menu.episode, popup.menu)
        popup.setOnMenuItemClickListener(this)

        playerView.setControllerVisibilityListener(this)
        controlsLayout.tag = false

        playButton.setOnClickListener(this)
        assignFrom(playerView, R.id.exo_minimize, R.id.exo_products,
                R.id.exo_like, R.id.exo_more, R.id.exo_fullscreen)
    }

    override fun onMenuItemClick(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.item_share -> if (presenter.canShare()) {
                ShareEpisodeSheetDialog.newInstance(presenter.episode ?: return true).show(fragmentManager)
            }

            R.id.item_report -> Unit

            else -> return false
        }
        return true
    }

    override fun showLikeViewAndUpdate(liked: Boolean, withAnimation: Boolean) {
        val likedRes = if (liked) R.drawable.svg_liked else R.drawable.svg_like
        likeView.setText(if (liked) R.string.episode_you_like_this_video else R.string.episode_you_dislike_this_video)
        likeView.setCompoundDrawablesRelativeWithIntrinsicBounds(likedRes, 0, 0, 0)
        ivLike.setImageResource(likedRes)

        if (!withAnimation) return

        Animation.slideToAnimator(likeView, true, true, Animation.MULTIPLIER) {
            Animation.slideToAnimator(likeView, false, true, Animation.MULTIPLIER)
                    ?.setStartDelay(Animation.DELAY)?.start()

        }?.setStartDelay(0L)?.start()
    }

    override fun onClick(v: View) {
        val it = v.id
        when (it) {
            R.id.exo_minimize -> mainActivity?.minimize()
            R.id.exo_products -> listener?.get()?.onPlayerButtonClick(it)
            R.id.exo_like -> if (likeView.tag == false) {
                presenter.toggleLike()
            }

            R.id.exo_fullscreen -> FullscreenActivity.start(activity, presenter.episode ?: return)
            R.id.btn_play -> assistant.togglePlayPause(playButton)
            R.id.exo_more -> popup.show()

            else -> return
        }
    }

    override fun onPostViewCreated() {
        presenter.start()
    }

    override fun onResume() {
        super.onResume()
        assistant.currentPresenter = presenter
        assistant.onResume()
        assistant.prepare(context, playerView)
        assistant.load(presenter.episode ?: return, playerView)
        presenter.updateWithActualState()
    }

    override fun onPause() {
        super.onPause()
        assistant.onPause()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        release()
    }

    override fun load(info: EpisodeInfo) {
        assistant.load(info, playerView, true)
    }

    fun release() {
        assistant.onPlayerStateChanged(false, Player.STATE_ENDED)
        assistant.release()
    }

    override fun onVisibilityChange(visibility: Int) = assistant.changeVisibility(controlsLayout,
            playerView, visibility)
}