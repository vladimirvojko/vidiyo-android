package tv.vidiyo.android.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.ProgressBar
import android.widget.Toast
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginResult
import tv.vidiyo.android.App
import tv.vidiyo.android.R
import tv.vidiyo.android.base.BaseActivity
import tv.vidiyo.android.base.view.ShowMessagePresentationView
import tv.vidiyo.android.di.subcomponent.boarding.FriendsModule
import tv.vidiyo.android.domain.contract.SignUpContract
import tv.vidiyo.android.domain.presenter.boarding.FindFriendsPresenter
import tv.vidiyo.android.extension.changeVisibility
import tv.vidiyo.android.ui.boarding.BoardingActivity
import tv.vidiyo.android.ui.boarding.signup.FriendsListFragment
import tv.vidiyo.android.util.ALog
import tv.vidiyo.android.util.FindFriendsHelper
import javax.inject.Inject

class FindFriendsActivity : BaseActivity(), SignUpContract.FindFriends.View {

    override val fragmentContainerId = R.id.fragment_container

    companion object {
        const val EXTRA = "EXTRA"//type

        const val FACEBOOK = 1
        const val CONTACTS = 2

        fun start(activity: Activity, type: Int = FACEBOOK) {
            activity.startActivity(Intent(activity, FindFriendsActivity::class.java).also {
                it.putExtra(FindFriendsActivity.EXTRA, type)
            })
        }
    }

    lateinit var progressBar: ProgressBar

    @Inject
    lateinit var presenter: FindFriendsPresenter
    lateinit var helper: FindFriendsHelper

    private var type = FACEBOOK

    override fun onCreate(b: Bundle?) {
        super.onCreate(b)
        super.setContentView(R.layout.activity_find_friends)
        App.graph.friendsComponent.plus(FriendsModule(this)).injectTo(this)
        supportActionBar?.setTitle(R.string.boarding_find_friends)
        progressBar = findViewById(R.id.progress_bar)
        helper = FindFriendsHelper(presenter).also { it.onCreate(callback) }
        type = b?.getInt(BoardingActivity.EXTRA) ?: intent.getIntExtra(EXTRA, FACEBOOK)

        setRefreshing(true)
        when (type) {
            FACEBOOK -> helper.findFacebookFriends(this)
            CONTACTS -> helper.findContacts(this, this)
        }
    }

    override fun setRefreshing(block: Boolean) {
        progressBar.changeVisibility(block)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (!helper.onActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (!helper.onRequestPermissionsResult(this, requestCode, permissions, grantResults)) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    override fun startFriendsList() {
        startFragment(FriendsListFragment(), false)
    }

    override fun showMessage(message: ShowMessagePresentationView.Message) {
        when (message) {
            is ShowMessagePresentationView.ToastMessage -> message.apply {
                Toast.makeText(this@FindFriendsActivity, resId?.let {
                    getString(it)
                } ?: text, duration).show()
            }
        }
    }

    override fun permissionNotGranted() {
        Toast.makeText(this, R.string.error_permission_contacts, Toast.LENGTH_SHORT).show()
        finish()
    }

    override fun onBackPressed() {
        finish()
    }

    override fun startSelectChannels() {}

    override fun finish() {
        App.graph.destroyFriendsComponent()
        super.finish()
    }

    private val callback = object : FacebookCallback<LoginResult> {
        override fun onSuccess(result: LoginResult?) {
            val accessToken = result?.accessToken ?: return
            presenter.findFacebookFriends(accessToken.token)
        }

        override fun onError(error: FacebookException?) {
            ALog.d("onError: $error")
            finish()
        }

        override fun onCancel() {
            ALog.d("onCancel")
            finish()
        }
    }
}