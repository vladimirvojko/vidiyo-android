package tv.vidiyo.android.ui.adapter.delegate

import android.view.ViewGroup
import tv.vidiyo.android.api.model.episode.ViewInfo
import tv.vidiyo.android.base.adapter.delegate.ListProviderAdapterDelegate
import tv.vidiyo.android.base.adapter.provider.ListDataProvider
import tv.vidiyo.android.base.view.holder.ViewHolder.OnViewHolderClickListener
import tv.vidiyo.android.extension.load
import tv.vidiyo.android.extension.toTime
import tv.vidiyo.android.source.DataType
import tv.vidiyo.android.ui.view.holder.WatchingEpisodeSmallViewHolder


class WatchingEpisodeSmallAdapterDelegate(
        provider: ListDataProvider<ViewInfo>,
        listener: OnViewHolderClickListener?
) : ListProviderAdapterDelegate<ViewInfo, OnViewHolderClickListener, WatchingEpisodeSmallViewHolder>(provider, listener) {

    override fun onCreateViewHolder(parent: ViewGroup) = WatchingEpisodeSmallViewHolder(parent, listener).also {
        it.itemView.tag = DataType.TYPE_WATCHING
    }

    override fun onBindViewHolder(position: Int, holder: WatchingEpisodeSmallViewHolder, provider: ListDataProvider<ViewInfo>, item: ViewInfo) {
        holder.ivImage.load(item.episode?.image)
        holder.tvShowName.text = item.episode?.show?.name
        holder.tvDuration.text = item.time.toTime(holder.context)
    }
}