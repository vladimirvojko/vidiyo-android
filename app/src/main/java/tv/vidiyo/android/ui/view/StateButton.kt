package tv.vidiyo.android.ui.view

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.AppCompatTextView
import android.util.AttributeSet
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.user.SubscriptionStatus

class StateButton : AppCompatTextView {

    companion object {
        const val STATE_DEFAULT = 0
        const val STATE_ACTIVE = 1
        const val STATE_REQUEST = 2
    }

    private var array: Array<CharSequence> = emptyArray()
    private var backgroundIds = intArrayOf(0, 0, 0)
    private var textColorIds = intArrayOf(0, 0, 0)
    private var drawableIds = intArrayOf(0, 0, 0)
    private var position: Int = 0

    var state: Int = STATE_DEFAULT
        set(value) {
            field = value
            update()
        }

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        parseAttributes(attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        parseAttributes(attrs, defStyleAttr)
    }

    fun setActiveState(value: Boolean) {
        state = if (value) STATE_ACTIVE else STATE_DEFAULT
    }

    fun setState(status: SubscriptionStatus) {
        state = when (status) {
            SubscriptionStatus.accepted -> STATE_ACTIVE
            SubscriptionStatus.pending -> STATE_REQUEST
            else -> STATE_DEFAULT
        }
    }

    private fun parseAttributes(attrs: AttributeSet?, defStyleAttr: Int) {
        val ta = context.theme.obtainStyledAttributes(attrs, R.styleable.StateButton, defStyleAttr, 0)
        val res = context.resources

        try {
            array = ta.getTextArray(R.styleable.StateButton_text_array) ?: res.getTextArray(R.array.subscription_array)
            backgroundIds[0] = ta.getResourceId(R.styleable.StateButton_background_default_state, R.drawable.corner_radius_solid_blue)
            backgroundIds[1] = ta.getResourceId(R.styleable.StateButton_background_active_state, android.R.color.transparent)
            backgroundIds[2] = ta.getResourceId(R.styleable.StateButton_background_request_state, android.R.color.transparent)

            textColorIds[0] = ta.getResourceId(R.styleable.StateButton_color_default_state, R.drawable.button_text_color)
            textColorIds[1] = ta.getResourceId(R.styleable.StateButton_color_active_state, R.drawable.button_text_color_blue)
            textColorIds[2] = ta.getResourceId(R.styleable.StateButton_color_request_state, R.drawable.button_text_color_gray)

            drawableIds[0] = ta.getResourceId(R.styleable.StateButton_drawable_default_state, 0)
            drawableIds[1] = ta.getResourceId(R.styleable.StateButton_drawable_active_state, R.drawable.svg_done_blue)
            drawableIds[2] = ta.getResourceId(R.styleable.StateButton_drawable_request_state, 0)

            position = ta.getInt(R.styleable.StateButton_drawable_position, 0)
            update()

        } finally {
            ta.recycle()
        }
    }

    private fun update() {
        text = array[state]
        setBackgroundResource(backgroundIds[state])
        setTextColor(ContextCompat.getColorStateList(context, textColorIds[state]))
        // TODO: position
        setCompoundDrawablesRelativeWithIntrinsicBounds(drawableIds[state], 0, 0, 0)
    }
}