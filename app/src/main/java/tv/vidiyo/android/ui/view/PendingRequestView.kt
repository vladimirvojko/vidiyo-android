package tv.vidiyo.android.ui.view

import android.content.Context
import android.support.v7.widget.CardView
import android.util.AttributeSet
import android.view.View
import android.widget.TextView
import com.bumptech.glide.load.resource.drawable.GlideDrawable
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import tv.vidiyo.android.R
import tv.vidiyo.android.extension.changeVisibility
import tv.vidiyo.android.extension.load
import java.lang.Exception

class PendingRequestView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0
) : CardView(context, attrs, defStyleAttr) {

    private val ivAvatar: ShapeImageView
    private val tvCounter: TextView

    var counter: Int = 0
        set(value) {
            field = value
            setText(value)
        }

    init {
        View.inflate(context, R.layout.view_pending_request, this)

        ivAvatar = findViewById(R.id.iv_avatar)
        tvCounter = findViewById(R.id.tv_counter)
    }

    fun <T> setAvatar(avatar: T?) {
        if (avatar != null) {
            ivAvatar.load(avatar, requestListener = object : RequestListener<T?, GlideDrawable> {
                override fun onResourceReady(resource: GlideDrawable?, model: T?, target: Target<GlideDrawable>?,
                                             isFromMemoryCache: Boolean, isFirstResource: Boolean): Boolean {
                    ivAvatar.setImageDrawable(resource)
                    return true
                }

                override fun onException(e: Exception?, model: T?, target: Target<GlideDrawable>?,
                                         isFirstResource: Boolean): Boolean {
                    ivAvatar.setImageResource(R.drawable.ic_default_profile)
                    return true
                }
            })
        } else {
            ivAvatar.load(R.drawable.ic_default_profile)
        }
    }

    private fun setText(value: Int) {
        tvCounter.text = value.toString()
        tvCounter.changeVisibility(value > 0)
    }
}