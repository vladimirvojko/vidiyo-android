package tv.vidiyo.android.ui.channels

import android.content.Context
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import tv.vidiyo.android.base.BasePresenter
import tv.vidiyo.android.base.fragment.BaseRecyclerViewFragment
import tv.vidiyo.android.domain.contract.ChannelsContract
import tv.vidiyo.android.domain.presenter.channels.ChannelsAdapter
import tv.vidiyo.android.source.channels.ChannelsMode
import tv.vidiyo.android.ui.view.SpaceItemDecoration
import tv.vidiyo.android.ui.widget.InfiniteScrollRecyclerView
import javax.inject.Inject

abstract class BaseChannelsFragment<out V, P> : BaseRecyclerViewFragment<ChannelsAdapter>(), ChannelsContract.View
        where V : ChannelsContract.View, P : BasePresenter<V>, P : ChannelsContract.Presenter {

    @Inject
    lateinit var presenter: P
    open var mode = ChannelsMode.ALL

    override fun onPostViewCreated() {
        presenter.start()
    }

    override fun onCreateAdapter(): ChannelsAdapter = ChannelsAdapter(this, presenter.provider, mode)
    override fun onCreateLayoutManager(context: Context?) = GridLayoutManager(context, 2)
    override fun onRecyclerViewCreated(recyclerView: InfiniteScrollRecyclerView) {
        super.onRecyclerViewCreated(recyclerView)
        recyclerView.addItemDecoration(SpaceItemDecoration(context, true, true))
    }

    override fun onViewHolderClick(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val channel = presenter.provider.getItem(position) ?: return
        startFragment(ChannelFragment.newInstance(channel))
    }
}