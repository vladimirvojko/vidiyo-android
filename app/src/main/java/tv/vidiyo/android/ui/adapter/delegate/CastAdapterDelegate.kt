package tv.vidiyo.android.ui.adapter.delegate

import android.view.ViewGroup
import tv.vidiyo.android.api.model.cast.Cast
import tv.vidiyo.android.base.adapter.delegate.ListProviderAdapterDelegate
import tv.vidiyo.android.base.adapter.provider.ListDataProvider
import tv.vidiyo.android.extension.load
import tv.vidiyo.android.ui.view.holder.CastViewHolder
import tv.vidiyo.android.ui.view.holder.OnSubscribableViewHolderClickListener

class CastAdapterDelegate(
        provider: ListDataProvider<Cast>,
        listener: OnSubscribableViewHolderClickListener?
) : ListProviderAdapterDelegate<Cast, OnSubscribableViewHolderClickListener, CastViewHolder>(provider, listener) {

    override fun onCreateViewHolder(parent: ViewGroup) = CastViewHolder(parent, listener)

    override fun onBindViewHolder(position: Int, holder: CastViewHolder, provider: ListDataProvider<Cast>, item: Cast) {
        holder.tvName.text = item.name
        holder.tvTitle.text = holder.context.getString(item.type.stringId)
        holder.ivPhoto.load(item.image)
        holder.button.setActiveState(item.isSubscribed)
    }
}