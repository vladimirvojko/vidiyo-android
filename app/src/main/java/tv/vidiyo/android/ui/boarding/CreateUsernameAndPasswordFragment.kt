package tv.vidiyo.android.ui.boarding

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.ActionBar
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import tv.vidiyo.android.App
import tv.vidiyo.android.DependencyGraph
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.social.SocialAuth
import tv.vidiyo.android.api.model.user.Profile
import tv.vidiyo.android.base.fragment.BaseFragment
import tv.vidiyo.android.di.subcomponent.boarding.BoardingModule
import tv.vidiyo.android.domain.contract.SignUpContract
import tv.vidiyo.android.domain.presenter.boarding.CreateUsernameAndPasswordPresenter
import tv.vidiyo.android.extension.setTextWithClickableLink
import tv.vidiyo.android.extension.startActivity
import tv.vidiyo.android.ui.MainActivity
import tv.vidiyo.android.ui.boarding.signin.SignInFragment
import javax.inject.Inject

class CreateUsernameAndPasswordFragment : BaseFragment(), SignUpContract.UsernameAndPassword.View, View.OnClickListener {

    companion object {
        private const val EXTRA_0 = "EXTRA_0"//profile
        private const val EXTRA_1 = "EXTRA_1"//social

        fun newInstance(profile: Profile, social: SocialAuth?): CreateUsernameAndPasswordFragment {
            val fragment = CreateUsernameAndPasswordFragment()
            val b = Bundle()
            b.putParcelable(EXTRA_0, profile)
            b.putParcelable(EXTRA_1, social)
            fragment.arguments = b
            return fragment
        }
    }

    override val layoutResId: Int = R.layout.fragment_create_username_and_password
    override val toolbarTitleRes: Int = R.string.boarding_user_name_and_password

    lateinit var etUsername: EditText
    lateinit var etPassword: EditText
    lateinit var loginButton: Button

    @Inject
    lateinit var presenter: CreateUsernameAndPasswordPresenter

    override fun injectDependencies(graph: DependencyGraph) {
        graph.applicationComponent.plus(BoardingModule(this)).injectTo(this)
    }

    override fun onCreate(b: Bundle?) {
        super.onCreate(b)
        val bundle = b ?: arguments
        presenter.apply {
            profile = bundle.getParcelable(EXTRA_0)
            social = bundle.getParcelable(EXTRA_1)
        }
    }

    override fun onActionBarReady(bar: ActionBar) {
        super.onActionBarReady(bar)
        bar.apply {
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
        }
    }

    override fun onViewCreated(view: View, b: Bundle?) {
        super.onViewCreated(view, b)
        etUsername = view.findViewById(R.id.et_username)
        etPassword = view.findViewById(R.id.et_password)

        loginButton = view.findViewById(R.id.btn_log_in)

        view.findViewById<TextView>(R.id.tv_terms_and_privacy_policy_prompt)
                .setTextWithClickableLink(R.string.boarding_terms_and_privacy_policy_agreement_prompt, this)
        view.findViewById<TextView>(R.id.tv_have_account)
                .setTextWithClickableLink(R.string.boarding_have_account, this)

        etUsername.addTextChangedListener(watcher)
        etPassword.addTextChangedListener(watcher)

        loginButton.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.tv_terms_and_privacy_policy_prompt -> return // TODO
            R.id.tv_have_account -> startFragment(SignInFragment(), false)
            R.id.btn_log_in -> presenter.create(etUsername.text?.toString(), etPassword.text?.toString())
        }
    }

    override fun showError(text: String?, code: Int) {
        processError(text, code)
    }

    override fun startFindFriends() {
        startFragment(FindFriendsFragment(), false)
    }

    override fun startMainAfterAnonymousRegister() {
        App.graph.destroyDataComponent()
        App.graph.applicationComponent.provideSubscriptionManager().refreshUser()
        activity.startActivity<MainActivity>(flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
    }

    override fun startMain() = startFindFriends()

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putParcelable(EXTRA_0, presenter.profile)
        outState?.putParcelable(EXTRA_1, presenter.social)
    }

    private val watcher = object : TextWatcher {
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            loginButton.isEnabled = presenter.validate(etUsername.text?.toString(), etPassword.text?.toString())
        }

        override fun afterTextChanged(s: Editable?) {}
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
    }
}