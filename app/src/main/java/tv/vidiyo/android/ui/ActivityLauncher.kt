package tv.vidiyo.android.ui

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.Toast
import tv.vidiyo.android.App
import tv.vidiyo.android.DependencyGraph
import tv.vidiyo.android.api.ApiKey
import tv.vidiyo.android.base.BaseActivity
import tv.vidiyo.android.di.subcomponent.launcher.LauncherModule
import tv.vidiyo.android.domain.contract.LauncherContract
import tv.vidiyo.android.domain.presenter.LauncherPresenter
import tv.vidiyo.android.extension.startActivity
import tv.vidiyo.android.ui.boarding.SplashActivity
import javax.inject.Inject

class ActivityLauncher : BaseActivity(), LauncherContract.View {

    @Inject
    lateinit var presenter: LauncherPresenter

    override fun injectDependencies(graph: DependencyGraph) {
        graph.applicationComponent.plus(LauncherModule(this)).inject(this)
    }

    override fun onCreate(b: Bundle?) {
        super.onCreate(b)
        val withoutAccount = accountManager.account == null
        when {
            withoutAccount && intent.action == Intent.ACTION_VIEW && handleIntent(intent.data) -> return
            withoutAccount -> startSplash()
            else -> MainActivity.start(this, intent.extras?.getString(ApiKey.type, null))
        }
        finish()
    }

    private fun handleIntent(uri: Uri?): Boolean {
        val token = uri?.getQueryParameter(ApiKey.token)
        return if (token != null && token.isNotEmpty()) {
            presenter.loginWith(token)
            true

        } else false
    }

    override fun showError(text: String?, code: Int) {
        Toast.makeText(this, code, Toast.LENGTH_SHORT).show()
        startSplash()
        finish()
    }

    override fun startSplash() {
        startActivity<SplashActivity>(flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
    }

    override fun startMain() {
        App.graph.applicationComponent.provideSubscriptionManager().refreshUser()
        MainActivity.start(this)
        finish()
    }
}