package tv.vidiyo.android.ui.view

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import tv.vidiyo.android.R
import tv.vidiyo.android.base.interfaces.DataEntity
import tv.vidiyo.android.extension.changeVisibility
import tv.vidiyo.android.ui.dialog.Intro

class IntroItemView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0
) : RelativeLayout(context, attrs, defStyleAttr), DataEntity<IntroItemView, Intro> {

    override val self = this

    val tvTitle: TextView
    val tvDescription: TextView
    val ivFirst: ImageView
    val ivSecond: ImageView

    init {
        View.inflate(context, R.layout.view_item_intro, this)

        tvTitle = findViewById(R.id.tv_title)
        tvDescription = findViewById(R.id.tv_description)
        ivFirst = findViewById(R.id.image)
        ivSecond = findViewById(R.id.image2)
    }

    override fun setData(data: Intro, vararg any: Any) {
        tvTitle.setText(data.titleId)
        tvDescription.setText(data.descriptionId)
        ivFirst.setImageResource(data.firstImage)
        data.secondImage?.let {
            ivSecond.changeVisibility(true)
            ivSecond.setImageResource(it)
        }
    }
}