package tv.vidiyo.android.loader

import android.content.ContentUris
import android.content.Context
import android.net.Uri
import android.provider.ContactsContract.CommonDataKinds.Email
import android.provider.ContactsContract.CommonDataKinds.Phone
import android.provider.ContactsContract.Contacts
import android.provider.ContactsContract.Contacts.Photo
import android.support.v4.content.AsyncTaskLoader
import tv.vidiyo.android.model.Contact
import java.io.IOException

class ContactsLoader(context: Context?) : AsyncTaskLoader<List<Contact>>(context) {

    companion object {
        private val PROJECTION = arrayOf(
                Contacts._ID,
                Contacts.LOOKUP_KEY,
                Contacts.DISPLAY_NAME_PRIMARY
        )

        private const val CONTACT_ID_COLUMN_INDEX = 0
        private const val CONTACT_NAME_COLUMN_INDEX = 2

        private val EMAIL_PROJECTION = arrayOf(Email.DATA)
        private const val EMAIL_SELECTION = "${Email.CONTACT_ID} = ?"
        private const val EMAIL_COLUMN_INDEX = 0

        private val PHONE_PROJECTION = arrayOf(Phone.DATA)
        private const val PHONE_SELECTION = "${Phone.CONTACT_ID} = ?"
        private const val PHONE_COLUMN_INDEX = 0
    }

    override fun loadInBackground(): List<Contact> {
        val contacts = mutableListOf<Contact>()

        context.contentResolver.query(Contacts.CONTENT_URI, PROJECTION, null, null, Contacts.DISPLAY_NAME_PRIMARY)?.use { cursor ->
            if (!cursor.moveToFirst()) return@use

            do {
                val contactId = cursor.getLong(CONTACT_ID_COLUMN_INDEX)

                val emails = mutableListOf<String>()
                val phones = mutableListOf<String>()
                var thumbnailPhotoUri = Uri.withAppendedPath(ContentUris.withAppendedId(Contacts.CONTENT_URI, contactId), Photo.CONTENT_DIRECTORY)
                var displayPhotoUri = Uri.withAppendedPath(ContentUris.withAppendedId(Contacts.CONTENT_URI, contactId), Photo.DISPLAY_PHOTO)

                context.contentResolver.query(Email.CONTENT_URI, EMAIL_PROJECTION, EMAIL_SELECTION, arrayOf(contactId.toString()), null)?.apply {
                    if (!moveToFirst()) return@apply

                    do {
                        getString(EMAIL_COLUMN_INDEX)?.let { emails.add(it) }
                    } while (moveToNext())

                    close()
                }

                context.contentResolver.query(Phone.CONTENT_URI, PHONE_PROJECTION, PHONE_SELECTION, arrayOf(contactId.toString()), null)?.apply {
                    if (!moveToFirst()) return@apply

                    do {
                        getString(PHONE_COLUMN_INDEX)?.let { phones.add(it) }
                    } while (moveToNext())

                    close()
                }

                context.contentResolver.query(thumbnailPhotoUri, null, null, null, null, null)?.apply {
                    if (!moveToFirst()) thumbnailPhotoUri = null

                    close()
                }

                try {
                    if (context.contentResolver.openAssetFileDescriptor(displayPhotoUri, "r") == null) {
                        displayPhotoUri = null
                    }
                } catch (e: IOException) {
                    displayPhotoUri = null
                }

                if (!(emails.isEmpty() && phones.isEmpty())) {
                    contacts.add(Contact(cursor.getString(CONTACT_NAME_COLUMN_INDEX), emails, phones, thumbnailPhotoUri, displayPhotoUri))
                }
            } while (cursor.moveToNext())
        }

        return contacts
    }
}