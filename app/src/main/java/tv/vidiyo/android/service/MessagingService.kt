package tv.vidiyo.android.service

import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import tv.vidiyo.android.App
import tv.vidiyo.android.util.ALog

class MessagingService : FirebaseMessagingService() {

    override fun onMessageReceived(message: RemoteMessage) {
        ALog.d("onMessageReceived : $message")
        val center = App.graph.let {
            it.applicationComponent.provideUserAccount() ?: return
            it.dataComponent.provideNotificationsCenter()
        }

        center.handleForegroundMessage(message)
    }
}