package tv.vidiyo.android.service

import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.FirebaseInstanceIdService
import tv.vidiyo.android.App
import tv.vidiyo.android.util.ALog

class InstanceIdService : FirebaseInstanceIdService() {

    override fun onTokenRefresh() {
        if (App.graph.applicationComponent.provideUserAccount() != null) {
            val center = App.graph.dataComponent.provideNotificationsCenter()
            FirebaseInstanceId.getInstance().token?.let {
                ALog.d("Firebase refreshed token")
                center.registerCloudMessaging(it)
            }
        }
    }
}