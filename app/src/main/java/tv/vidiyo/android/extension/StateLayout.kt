package tv.vidiyo.android.extension

import android.widget.TextView
import tv.vidiyo.android.ui.view.EmptyView
import tv.vidiyo.android.ui.view.ErrorRetryView
import tv.vidiyo.android.ui.widget.StateLayout

val StateLayout.errorRetryView: ErrorRetryView? get() = errorView as? ErrorRetryView
val StateLayout.emptyMessageView: EmptyView? get() = emptyView as? EmptyView
val StateLayout.emptyTextView: TextView? get() = emptyView as? TextView

fun StateLayout.setEmptyView(text: String, drawableRes: Int) = emptyMessageView?.set(text, drawableRes)
fun StateLayout.setEmptyViewWithFormat(text: String, drawableRes: Int) = emptyMessageView?.setWithFormat(text, drawableRes)
