package tv.vidiyo.android.extension

import android.content.res.TypedArray


fun TypedArray.getResourceId(index: Int): Int? {
    val resId = getResourceId(index, -1)
    return if (resId != -1) resId else null
}