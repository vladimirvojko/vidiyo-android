package tv.vidiyo.android.extension

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.view.View

fun View.changeVisibility(visible: Boolean, invisibleState: Int = View.GONE) {
    visibility = if (visible) View.VISIBLE else invisibleState
}

val View.isVisible: Boolean get() = visibility == View.VISIBLE

fun View.changeVisibilityAnimated(visible: Boolean) {
    val a = if (visible) 1f else 0f
    changeVisibility(true)

    val weak = weak()
    animate().alpha(a).setListener(object : AnimatorListenerAdapter() {
        override fun onAnimationCancel(animation: Animator?) {
            val v = weak.get() ?: return
            v.alpha = a
            v.changeVisibility(visible)
        }

        override fun onAnimationEnd(animation: Animator?) {
            val v = weak.get() ?: return
            v.alpha = a
            v.changeVisibility(visible)
        }
    }).start()
}