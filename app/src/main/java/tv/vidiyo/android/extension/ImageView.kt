package tv.vidiyo.android.extension

import android.widget.ImageView
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.GlideDrawable
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import tv.vidiyo.android.App


@Suppress("unused")
val ImageView.glide
    get() = App.graph.applicationComponent.provideGlide()

fun <T> ImageView.load(model: T?, strategy: DiskCacheStrategy = DiskCacheStrategy.ALL, requestListener: RequestListener<T?, GlideDrawable>? = null): Target<GlideDrawable> {
    return glide.load(model).diskCacheStrategy(strategy).listener(requestListener).into(this)
}

@Suppress("unused")
fun Target<*>.unit() {}