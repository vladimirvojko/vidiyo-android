package tv.vidiyo.android.extension

import android.graphics.Color
import android.support.v4.widget.SwipeRefreshLayout

fun SwipeRefreshLayout.style() {
    this.setColorSchemeColors(Color.RED, Color.BLUE, Color.GRAY)
}