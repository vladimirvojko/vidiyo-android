package tv.vidiyo.android.extension

import android.text.SpannableString
import android.view.View
import tv.vidiyo.android.text.style.URLSpanNoUnderline

fun CharSequence.stripUnderlines(clickListener: View.OnClickListener? = null): CharSequence {
    val spannable = SpannableString(this)
    val spans = spannable.getSpans(0, spannable.length, android.text.style.URLSpan::class.java)
    for (span in spans) {
        val start = spannable.getSpanStart(span)
        val end = spannable.getSpanEnd(span)
        spannable.removeSpan(span)
        spannable.setSpan(URLSpanNoUnderline(span.url, clickListener), start, end, 0)
    }
    return spannable
}