package tv.vidiyo.android.extension

import android.support.annotation.LayoutRes
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

fun ViewGroup.inflateChild(@LayoutRes resource: Int): View = LayoutInflater.from(context).inflate(resource, this, false)