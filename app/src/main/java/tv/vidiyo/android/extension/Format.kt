package tv.vidiyo.android.extension


import tv.vidiyo.android.util.PriceFormat
import java.text.DateFormat
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*

val MMM_dd_YY: DateFormat by lazy { SimpleDateFormat("MMM d',' ''yy", Locale.US) }
val MM_dd_YYYY: DateFormat by lazy { SimpleDateFormat("MM/dd/YYYY", Locale.US) }
val CREATED_TIME_FORMAT_FACEBOOK: DateFormat by lazy { SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZZZZ", Locale.US) }
val CREATED_TIME_FORMAT_TWITTER: DateFormat by lazy { SimpleDateFormat("EEE MMM dd HH:mm:ss ZZZZ yyyy", Locale.US) }
val MM_dd_YYYY_HH_mm: DateFormat by lazy { SimpleDateFormat("EEE MMM d',' yyyy HH:mm", Locale.US) }
val DECIMAL_FORMAT: DecimalFormat by lazy { DecimalFormat("#,###") }
val PRICE_FORMAT: PriceFormat by lazy { PriceFormat() }