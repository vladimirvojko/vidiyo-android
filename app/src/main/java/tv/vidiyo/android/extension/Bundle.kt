package tv.vidiyo.android.extension

import android.os.Bundle
import android.os.Parcelable

fun <T : Parcelable> Bundle.putParcelableList(key: String, list: List<T>) {
    putParcelableArrayList(key, list as? ArrayList<T> ?: ArrayList(list))
}