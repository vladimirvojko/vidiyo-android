package tv.vidiyo.android.extension

import android.view.Window

val Window.statusBarHeight: Int get() {
    val resources = context.resources
    val resourceId = resources.getIdentifier("status_bar_height", "dimen", "android")
    return if (resourceId > 0) resources.getDimensionPixelSize(resourceId) else 0
}