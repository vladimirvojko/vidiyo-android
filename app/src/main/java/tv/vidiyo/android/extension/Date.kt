package tv.vidiyo.android.extension

import java.util.*

fun Long.toDate() = Date(this)

fun Long.unixTimeStampToMillis() = this * 1000L

fun Long.unixTimeStampToDate() = unixTimeStampToMillis().toDate()
