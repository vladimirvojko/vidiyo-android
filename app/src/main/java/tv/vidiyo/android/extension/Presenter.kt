package tv.vidiyo.android.extension

import tv.vidiyo.android.api.model.Subscribable
import tv.vidiyo.android.api.model.user.FollowingUser
import tv.vidiyo.android.api.model.user.SubscriptionRequest
import tv.vidiyo.android.api.model.user.SubscriptionStatus
import tv.vidiyo.android.base.BasePresenter
import tv.vidiyo.android.base.view.*
import tv.vidiyo.android.util.SubscriptionManager

fun <V> BasePresenter<V>.toggleSubscriptionInternal(v: V?, item: Any, manager: SubscriptionManager)
        where V : PresentationView, V : RefreshablePresentationView, V : ShowMessagePresentationView,
              V : UpdatablePresentationView {

    v?.setRefreshing(true)
    when (item) {
        is Subscribable -> manager.setSubscription(item, !manager.isSubscribed(item)) { _, subscribed, error ->
            v?.setRefreshing(false)
            if (error != null) {
                v?.showMessage(ShowMessagePresentationView.ToastMessage(error))
            } else {
                item.isSubscribed = subscribed
                v?.update()
            }
        }

        is FollowingUser -> {
            val status = manager.subscriptionStatus(item)
            manager.setUserSubscription(if (status != SubscriptionStatus.accepted && status != SubscriptionStatus.pending) {
                SubscriptionRequest.follow
            } else {
                SubscriptionRequest.unfollow
            }, item) { subscriptions, error ->
                v?.setRefreshing(false)
                if (error != null) {
                    v?.showMessage(ShowMessagePresentationView.ToastMessage(error))

                } else if (subscriptions != null && subscriptions.isNotEmpty()) {
                    val first = subscriptions.firstOrNull() ?: return@setUserSubscription
                    item.status = first.status
                    v?.update()
                }
            }
        }
    }
}

fun <V> BasePresenter<V>.accountIsAnonumous(v: V?, manager: SubscriptionManager): Boolean
        where V : PresentationView, V : NeedRegistrationView = if (manager.isAnonymous) {
    v?.openRegistration()
    true

} else false