package tv.vidiyo.android.extension

import android.content.res.Resources
import android.graphics.Color
import android.support.annotation.AttrRes
import android.support.annotation.ColorInt
import android.util.TypedValue


@ColorInt
fun Resources.Theme.getColor(@AttrRes attrRes: Int, @ColorInt fallBackColor: Int = Color.BLACK): Int {
    val typedValue = TypedValue()
    if (resolveAttribute(attrRes, typedValue, true)) {
        return typedValue.data
    } else {
        return fallBackColor
    }
}