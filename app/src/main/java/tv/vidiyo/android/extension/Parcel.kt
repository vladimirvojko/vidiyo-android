package tv.vidiyo.android.extension

import android.os.Parcel

//region Boolean
fun Parcel.readBoolean(): Boolean = readByte() != 0.toByte()

fun Parcel.writeBoolean(b: Boolean) = writeByte(if (b) 1 else 0)
//endregion