package tv.vidiyo.android.extension

import android.support.annotation.LayoutRes
import android.view.View
import android.view.ViewStub


fun ViewStub.inflate(@LayoutRes layoutResId: Int): View {
    layoutResource = layoutResId
    return inflate()
}