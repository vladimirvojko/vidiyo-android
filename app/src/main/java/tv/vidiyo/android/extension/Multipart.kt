package tv.vidiyo.android.extension

import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import tv.vidiyo.android.api.ApiKey
import tv.vidiyo.android.api.model.user.Profile
import tv.vidiyo.android.api.model.user.UserAccount
import tv.vidiyo.android.util.Constant
import java.io.File

fun Profile.toMultipartMap(): MutableMap<String, RequestBody> {
    val map = linkedMapOf<String, RequestBody>()

    val keys = arrayOf(ApiKey.username, ApiKey.fullName, ApiKey.email, ApiKey.phone, ApiKey.image)
    val values = arrayOf(username, name, email, phone, image)

    values.forEachIndexed { index, s -> s?.let { map.put(keys[index], it.requestBody) } }
    return map
}

fun UserAccount.toMultipartMap(): MutableMap<String, RequestBody> {
    val map = linkedMapOf<String, RequestBody>()

    val keys = arrayOf(ApiKey.username, ApiKey.name, ApiKey.email, ApiKey.phone, ApiKey.bio,
            ApiKey.birthday, ApiKey.location, ApiKey.website, ApiKey.gender)
    val values = arrayOf(username, name, email, phone, bio, birthday, location, website, gender)

    values.forEachIndexed { index, s ->
        s?.let {
            if (it.isNotBlank()) map.put(keys[index], it.requestBody)
        }
    }
    return map
}

val String.requestBody: RequestBody get() = RequestBody.create(MediaType.parse(Constant.PLAINTEXT), this)

val File.imagePartBody: MultipartBody.Part
    get() = MultipartBody.Part
            .createFormData(ApiKey.imageFile, this.name, RequestBody.create(MediaType.parse(Constant.IMAGE_PNG), this))