package tv.vidiyo.android.extension

import android.graphics.Color
import android.support.annotation.ColorInt


@ColorInt
fun argb(alpha: Float, red: Float = 0f, green: Float = 0f, blue: Float = 0f): Int {
    return Color.argb((255 * alpha).toInt(), (255 * red).toInt(), (255 * green).toInt(), (255 * blue).toInt())
}