package tv.vidiyo.android.extension

import com.google.gson.Gson
import com.google.gson.JsonParseException
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import tv.vidiyo.android.api.ApiKey
import tv.vidiyo.android.api.model.Message
import tv.vidiyo.android.api.model.ServerResponse

val gson: Gson by lazy { Gson() }

val <T> Call<T>.urlString get() = request().url().toString()

open class ServerCallback<T> : Callback<T> {

    override fun onResponse(call: Call<T>?, response: Response<T>?) {
        response?.let {
            if (it.isSuccessful) {
                onSuccess(it.body(), call?.urlString)
            } else {
                val str = it.errorBody().string()
                val error = try {
                    // TODO: rethink logic mb
                    if (str.contains(ApiKey.status)) {
                        val r = gson.fromJson<ServerResponse<*>>(str, ServerResponse::class.java)
                        r.error ?: Message(null, null, 0)

                    } else {
                        gson.fromJson<Message>(str, Message::class.java)
                    }

                } catch (e: JsonParseException) {
                    Message(str, null, 0)
                }

                onFailure(error, call?.urlString)
            }
        }
    }

    override fun onFailure(call: Call<T>?, t: Throwable?) {
        t?.let {
            onFailure(Message(it.localizedMessage, null, 0), call?.urlString)
        }
    }

    open fun onSuccess(response: T, url: String?) {
    }

    open fun onFailure(error: Message, url: String?) {
    }
}

class EmptyCallback<T> : Callback<T> {
    override fun onResponse(call: Call<T>?, response: Response<T>?) = Unit
    override fun onFailure(call: Call<T>?, t: Throwable?) = Unit
}