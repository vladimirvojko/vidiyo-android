package tv.vidiyo.android.extension

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.Parcelable
import android.provider.MediaStore
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.content.ContextCompat
import android.view.View
import tv.vidiyo.android.R
import tv.vidiyo.android.base.BaseActivity
import tv.vidiyo.android.util.Constant
import java.io.File
import java.io.FileOutputStream
import java.io.Serializable
import java.lang.ref.WeakReference

fun <T> T.weak() = WeakReference(this)
val String.lastSegment: String get() = replaceFirst(".*/([^/?]+).*".toRegex(), "$1")

fun View.OnClickListener.assignFrom(parent: View, vararg childrenIds: Int) {
    for (id in childrenIds) {
        parent.findViewById<View>(id).setOnClickListener(this)
    }
}

inline fun <reified T : View> View.findViews(vararg ids: Int): Array<T> = Array(ids.size, {
    findViewById<T>(ids[it])
})

inline fun <reified A : BaseActivity> Context.startActivity(vararg params: Pair<String, Any?>, flags: Int? = null) {
    val intent = Intent(this, A::class.java)
    if (params.isNotEmpty()) {
        Internal.fillIntentParameters(intent, params)
    }
    flags?.let { intent.setFlags(it) }
    startActivity(intent)
}

fun Boolean.toInt() = if (this) 1 else 0
fun Context.color(id: Int) = ContextCompat.getColor(this, id)

fun bundleOf(vararg params: Pair<String, Any>): Bundle {
    val b = Bundle()
    for ((k, v) in params) {
        when (v) {
            is Boolean -> b.putBoolean(k, v)
            is Byte -> b.putByte(k, v)
            is Char -> b.putChar(k, v)
            is Short -> b.putShort(k, v)
            is Int -> b.putInt(k, v)
            is Long -> b.putLong(k, v)
            is Float -> b.putFloat(k, v)
            is Double -> b.putDouble(k, v)
            is String -> b.putString(k, v)
            is CharSequence -> b.putCharSequence(k, v)
            is Parcelable -> b.putParcelable(k, v)
            is Serializable -> b.putSerializable(k, v)
            is BooleanArray -> b.putBooleanArray(k, v)
            is ByteArray -> b.putByteArray(k, v)
            is CharArray -> b.putCharArray(k, v)
            is DoubleArray -> b.putDoubleArray(k, v)
            is FloatArray -> b.putFloatArray(k, v)
            is IntArray -> b.putIntArray(k, v)
            is LongArray -> b.putLongArray(k, v)
            is ShortArray -> b.putShortArray(k, v)
            is Bundle -> b.putBundle(k, v)
        }
    }

    return b
}

val Any.className: String get() = this.javaClass.simpleName

object Internal {

    @JvmStatic
    fun fillIntentParameters(intent: Intent, params: Array<out Pair<String, Any?>>) {
        for ((key, value) in params) {
            when (value) {
                null -> intent.putExtra(key, null as Serializable?)
                is Int -> intent.putExtra(key, value)
                is Long -> intent.putExtra(key, value)
                is CharSequence -> intent.putExtra(key, value)
                is String -> intent.putExtra(key, value)
                is Float -> intent.putExtra(key, value)
                is Double -> intent.putExtra(key, value)
                is Char -> intent.putExtra(key, value)
                is Short -> intent.putExtra(key, value)
                is Boolean -> intent.putExtra(key, value)
                is Bundle -> intent.putExtra(key, value)
                is Parcelable -> intent.putExtra(key, value)
            // TODO
            }
        }
    }
}

fun View.OnClickListener.assignTo(vararg views: View) {
    for (view in views) {
        view.setOnClickListener(this)
    }
}

fun FragmentManager.notContains(tag: String) = findFragmentByTag(tag) == null

fun Fragment.takePhoto(requestCode: Int) {
    startActivityForResult(Intent(MediaStore.ACTION_IMAGE_CAPTURE), requestCode)
}

fun Fragment.choosePhoto(requestCode: Int) {
    startActivityForResult(Intent(Intent.ACTION_GET_CONTENT).apply {
        type = Constant.IMAGE_SLASH
    }, requestCode)
}

fun Fragment.openUrl(url: String) {
    Intent(Intent.ACTION_VIEW, Uri.parse(url)).let {
        if (it.resolveActivity(context.packageManager) != null) {
            startActivity(it)
        }
    }
}

fun Activity.openUrl(url: String) {
    Intent(Intent.ACTION_VIEW, Uri.parse(url)).let {
        if (it.resolveActivity(packageManager) != null) {
            startActivity(it)
        }
    }
}

fun CharSequence?.has(other: CharSequence, ignoreCase: Boolean = true): Boolean
        = this?.contains(other, ignoreCase) == true

fun Context.cacheBitmap(bitmap: Bitmap): String? {
    val file = File.createTempFile("img", null, cacheDir)
    if (file.exists()) {
        val fos = FileOutputStream(file)
        try {
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos)

        } finally {
            fos.flush()
            fos.close()
        }
        return file.path
    }
    return null
}

fun StringBuilder.ifNotEmpty(): StringBuilder? {
    return if (length > 0) this else null
}

fun StringBuilder?.deleteLastCharacter(): StringBuilder? {
    return this?.deleteCharAt(this.lastIndex)
}

fun IntentFilter.playerActions(): IntentFilter = this.apply {
    addAction(Constant.ACTION_MAXIMIZED)
    addAction(Constant.ACTION_MINIMIZED)
}

fun Float.toTime() = Triple((this / 3600).toInt(), ((this % 3600) / 60).toInt(), (this % 60).toInt())
fun Float.toTime(context: Context): String {
    val t = toTime()
    return if (t.first > 0) {
        context.getString(R.string.format_h_m_s, t.first, t.second, t.third)
    } else {
        context.getString(R.string.format_m_s, t.second, t.third)
    }
}