package tv.vidiyo.android.extension

import android.animation.AnimatorListenerAdapter
import android.animation.ObjectAnimator
import android.os.Build
import android.support.annotation.StringRes
import android.text.Html
import android.text.method.LinkMovementMethod
import android.view.View
import android.widget.TextView

fun TextView.setTextWithClickableLink(@StringRes resId: Int, clickListener: View.OnClickListener?) {
    setTextWithClickableLink(context.getString(resId), clickListener)
}

fun TextView.setTextWithClickableLink(text: String, clickListener: View.OnClickListener?) {
    movementMethod = LinkMovementMethod.getInstance()
    @Suppress("DEPRECATION")
    this.text = Html.fromHtml(text).stripUnderlines(clickListener)
}

fun TextView.expansion(lines: Int = 3, adapter: AnimatorListenerAdapter? = null) {
    (tag as? ObjectAnimator)?.let {
        it.removeAllListeners()
        it.cancel()
        tag = null
    }

    val animator = ObjectAnimator.ofInt(this, "maxLines", if (maxLines == lines) lineCount else lines)
    val duration = (lineCount - lines) * 10L
    animator.duration = if (duration < 0) duration * -1 else duration
    adapter?.let { animator.addListener(adapter) }
    tag = animator
    animator.start()
}

@Suppress("DEPRECATION")
fun TextView.fromHtml(source: String) {
    text = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        Html.fromHtml(source, Html.FROM_HTML_MODE_LEGACY)
    } else {
        Html.fromHtml(source)
    }
}