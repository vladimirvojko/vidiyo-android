package tv.vidiyo.android.extension

import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager.PERMISSION_GRANTED
import android.os.Build
import android.support.v4.content.ContextCompat.checkSelfPermission

fun Context.isPermissionsGranted(vararg permissions: String) = permissions.any { isPermissionGranted(it) }
fun Context.isPermissionGranted(permission: String) = checkSelfPermission(this, permission) == PERMISSION_GRANTED

fun Activity.request(permissions: Array<out String>, requestCode: Int) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        requestPermissions(permissions, requestCode)
    }
}

fun isPermissionGranted(permission: String, permissions: Array<out String>, grantResults: IntArray): Boolean {
    return grantResults.elementAtOrNull(permissions.indexOf(permission))?.let { it == PERMISSION_GRANTED } == true
}