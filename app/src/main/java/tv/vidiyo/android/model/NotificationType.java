package tv.vidiyo.android.model;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
@StringDef({NotificationType.EPISODE_NEW, NotificationType.REQUEST_PENDING, NotificationType.REQUEST_ACCEPTED,
        NotificationType.FRIENDS_ON, NotificationType.TOPIC_NEW_VIDEO, NotificationType.CAST_NEW_VIDEO,
        NotificationType.COMMENT_LIKED, NotificationType.COMMENT_REPLIED})
public @interface NotificationType {
    String EPISODE_NEW = "1";
    String REQUEST_PENDING = "2";
    String REQUEST_ACCEPTED = "3";
    String FRIENDS_ON = "4";
    String TOPIC_NEW_VIDEO = "5";
    String CAST_NEW_VIDEO = "6";
    String COMMENT_LIKED = "7";
    String COMMENT_REPLIED = "8";
}
