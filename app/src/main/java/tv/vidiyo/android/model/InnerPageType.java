package tv.vidiyo.android.model;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
@IntDef({InnerPageType.SHOWS, InnerPageType.EPISODES, InnerPageType.CAST, InnerPageType.TOPICS,
        InnerPageType.SOCIAL, InnerPageType.SUBSCRIBERS, InnerPageType.CONNECT_AND_SHARE,
        InnerPageType.PEOPLE, InnerPageType.COMMENTS, InnerPageType.ALL, InnerPageType.MORE})
public @interface InnerPageType {
    int SHOWS = 10;
    int EPISODES = 20;
    int CAST = 30;
    int TOPICS = 40;
    int SOCIAL = 50;
    int SUBSCRIBERS = 60;
    int CONNECT_AND_SHARE = 70;
    int PEOPLE = 80;
    int COMMENTS = 90;

    int ALL = 100;
    int MORE = 110;
}
