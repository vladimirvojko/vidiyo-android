package tv.vidiyo.android.model

import android.net.Uri
import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.api.model.user.ContactFriends
import tv.vidiyo.android.api.model.user.Friend
import tv.vidiyo.android.api.model.user.User
import tv.vidiyo.android.util.Constant

data class Contact(
        val name: String?,
        val emails: List<String>,
        val phones: List<String>,
        val thumbnailPhotoUri: Uri? = null,
        val displayPhotoUri: Uri? = null,
        var user: User? = null
)

fun groupContacts(contacts: List<Contact>, contactFriends: ContactFriends): List<Friend> {
    val list = mutableListOf<Friend>()
    for (contact in contacts) {
        val friends = contactFriends.users.filter { !contactFriends.followingIds.contains(it.id) }
        val contactFriend = Friend(contact)
        for (friend in friends) {
            if (contact.isEqualsToFriend(friend)) {
                contactFriend.id = friend.id
                contactFriend.username = friend.username
                contactFriend.name = friend.name
                contactFriend.image = friend.image
            }
        }

        if (contactFriend.id == Constant.NO_ID && contactFriend.email == null && contactFriend.phone == null) {
            continue
        }

        list.add(contactFriend)
    }
    return list
}

fun VidiyoService.findContacts(contacts: List<Contact>) = findContacts(
        contacts.emails.joinToString(separator = Constant.COMMA),
        contacts.phones.joinToString(separator = Constant.COMMA, transform = { it.replace("[^0-9]".toRegex(), Constant.EMPTY) })
)

fun VidiyoService.uploadContacts(contacts: List<Contact>) = uploadContacts(
        contacts.emails.joinToString(separator = Constant.COMMA),
        contacts.phones.joinToString(separator = Constant.COMMA, transform = { it.replace("[^0-9]".toRegex(), Constant.EMPTY) })
)

private fun Contact.isEqualsToFriend(friend: Friend): Boolean {
    friend.email?.let { if (emails.contains(it)) return true }
    friend.phone?.let { if (phones.contains(it)) return true }
    return false
}

private val List<Contact>.emails get() = flatMap { it.emails }.toSet()
private val List<Contact>.phones get() = flatMap { it.phones }.toSet()