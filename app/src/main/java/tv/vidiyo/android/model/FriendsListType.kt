package tv.vidiyo.android.model

import android.content.Context
import android.os.Parcel
import android.os.Parcelable
import tv.vidiyo.android.R


enum class FriendsListType : Parcelable {
    CONNECT_CONTACTS, FIND_FACEBOOK_FRIENDS; // TODO: add followFacebookFriends, findFacebookFriends, discoverPeople

    fun titleForCount(count: Int, context: Context): String = when (this) {
        CONNECT_CONTACTS -> context.getString(R.string.boarding_connect_contacts)
        FIND_FACEBOOK_FRIENDS -> context.getString(R.string.boarding_d_fb_friends_on, count)
    }

    fun subtitle(context: Context): String = when (this) {
        CONNECT_CONTACTS, FIND_FACEBOOK_FRIENDS -> context.getString(R.string.boarding_find_people_that_share_similar_interests)
    }

    fun buttonTitle(context: Context): String = when (this) {
        CONNECT_CONTACTS, FIND_FACEBOOK_FRIENDS -> context.getString(R.string.done)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(toString())
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<FriendsListType> {
        override fun createFromParcel(parcel: Parcel): FriendsListType {
            return FriendsListType.valueOf(parcel.readString())
        }

        override fun newArray(size: Int): Array<FriendsListType?> {
            return arrayOfNulls(size)
        }
    }
}