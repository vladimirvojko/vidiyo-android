package tv.vidiyo.android

import android.annotation.SuppressLint
import android.app.Application
import android.os.Build.*
import android.os.Build.VERSION.CODENAME
import android.os.Build.VERSION.SDK_INT
import android.provider.Settings
import android.text.TextUtils
import com.crashlytics.android.Crashlytics
import com.squareup.leakcanary.LeakCanary
import io.fabric.sdk.android.Fabric
import tv.vidiyo.android.di.ApplicationComponent
import tv.vidiyo.android.di.ApplicationModule
import tv.vidiyo.android.di.DaggerApplicationComponent
import tv.vidiyo.android.di.subcomponent.DataComponent
import tv.vidiyo.android.di.subcomponent.DataModule
import tv.vidiyo.android.di.subcomponent.StoredFriendsComponent
import tv.vidiyo.android.di.subcomponent.StoredFriendsModule
import tv.vidiyo.android.di.subcomponent.episode.EpisodeComponent
import tv.vidiyo.android.di.subcomponent.episode.EpisodeModule
import java.util.*

open class App : Application() {

    /**
     * TODO: replace with Dagger-android. Delete useless components
     */

    companion object {
        lateinit var graph: DependencyGraph
            private set

        lateinit var deviceId: String
            private set
    }

    val deviceId: String
        @SuppressLint("HardwareIds")
        get() {
            val androidId = Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)
            if (TextUtils.isEmpty(androidId)) {
                val id = MANUFACTURER + DEVICE + MODEL + BRAND + DISPLAY + CODENAME + SDK_INT
                return UUID(id.hashCode().toLong(), -id.hashCode().toLong()).toString()
            }

            return androidId
        }


    override fun onCreate() {
        super.onCreate()
        graph = DependencyGraph(this)
        App.deviceId = deviceId

        if (!BuildConfig.DEBUG) Fabric.with(this, Crashlytics())
        if (!LeakCanary.isInAnalyzerProcess(this)) LeakCanary.install(this)
    }
}

class DependencyGraph(app: App) {
    val applicationComponent: ApplicationComponent = DaggerApplicationComponent.builder()
            .applicationModule(ApplicationModule(app))
            .build()

    private var fc: StoredFriendsComponent? = null
    val friendsComponent: StoredFriendsComponent
        get() {
            if (fc == null) {
                fc = applicationComponent.plus(StoredFriendsModule())
            }
            return fc!!
        }

    fun destroyFriendsComponent() {
        fc = null
    }

    private var dc: DataComponent? = null
    val dataComponent: DataComponent
        get() {
            if (dc == null) {
                dc = applicationComponent.plus(DataModule()).also {
                    it.provideProfileRepository()
                    it.provideNotificationsCenter()
                }
            }
            return dc!!
        }

    fun destroyDataComponent() {
        dc = null
    }

    private var ec: EpisodeComponent? = null
    fun getEpisodeComponent(id: Int): EpisodeComponent {
        if (ec == null) {
            ec = applicationComponent.plus(EpisodeModule(id))
        } else if (ec!!.provideId() != id) {
            destroyEpisodeComponent()
            ec = applicationComponent.plus(EpisodeModule(id))
        }
        return ec!!
    }

    fun destroyEpisodeComponent() {
        ec = null
    }
}