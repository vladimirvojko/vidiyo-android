package tv.vidiyo.android.util

import android.animation.ValueAnimator
import android.app.Activity
import android.graphics.Color
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v4.view.animation.FastOutSlowInInterpolator
import android.view.View
import android.view.ViewPropertyAnimator

/**
 * For animation View's tag must be set to Boolean
 */
object Animation {

    const val MULTIPLIER = 2f
    const val DELAY = 2000L

    private const val DURATION = 340L
    var color: Int = Color.BLACK

    fun slideTo(view: View, visibility: Boolean, toDown: Boolean, multiplier: Float = 1f,
                withEndAction: (() -> Unit)? = null) {
        slideToAnimator(view, visibility, toDown, multiplier, withEndAction)?.start()
    }


    fun slideToAnimator(view: View, visibility: Boolean, toDown: Boolean, multiplier: Float = 1f,
                        withEndAction: (() -> Unit)? = null): ViewPropertyAnimator? {
        if (view.tag as? Boolean == true) return null
        val direction = if (toDown) 1 else -1

        return if (!visibility) {
            view.tag = true
            view.visibility = View.VISIBLE
            view.animate().translationY(view.height.toFloat() * direction * multiplier).setDuration(DURATION)
                    .setInterpolator(FastOutSlowInInterpolator()).withEndAction {
                view.visibility = View.GONE
                view.tag = false
                withEndAction?.invoke()
            }

        } else {
            view.visibility = View.VISIBLE
            view.animate().translationY(0f).setDuration(DURATION)
                    .setInterpolator(FastOutSlowInInterpolator()).withEndAction(withEndAction)
        }
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun statusBarColorTransition(activity: Activity, fromColor: Int, toColor: Int) {
        val currentColor = activity.window.statusBarColor
        if (currentColor == toColor) return

        val animation = ValueAnimator.ofArgb(fromColor, toColor)
        animation.duration = Animation.DURATION
        animation.addUpdateListener { valueAnimator -> activity.window.statusBarColor = (valueAnimator.animatedValue as Int) }
        return animation.start()
    }
}