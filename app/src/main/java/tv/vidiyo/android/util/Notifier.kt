package tv.vidiyo.android.util

import android.content.Intent
import android.support.v4.content.LocalBroadcastManager
import tv.vidiyo.android.api.ApiKey
import tv.vidiyo.android.api.model.episode.ViewInfo
import tv.vidiyo.android.domain.contract.EpisodeContract

class Notifier(private val manager: LocalBroadcastManager) : EpisodeContract.Notifier {
    override fun send(viewInfo: ViewInfo) {
        manager.sendBroadcast(Intent(Constant.ACTION_VIEWED).putExtra(ApiKey.data, viewInfo))
    }
}