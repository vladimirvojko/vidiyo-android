package tv.vidiyo.android.util

import com.google.firebase.messaging.RemoteMessage
import retrofit2.Call
import tv.vidiyo.android.api.ApiKey
import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.api.model.Count
import tv.vidiyo.android.api.model.ServerResponse
import tv.vidiyo.android.api.model.user.PendingRequests
import tv.vidiyo.android.api.newEpisodesCount
import tv.vidiyo.android.extension.ServerCallback
import tv.vidiyo.android.model.NotificationType
import java.util.*

class NotificationsCenter(private val service: VidiyoService) {

    companion object {
        const val ACTION_LOADED = 0xAA
        const val ACTION_UPDATED = 0xBB
        const val ACTION_CHANGED = 0xCC
    }

    interface Listener {
        fun onAction(action: Int, @NotificationType type: String)
    }

    var newEpisodeCount: Int = 0
    var pendingRequests: PendingRequests? = null
    val pendingsCount: Int get() = pendingRequests?.count ?: 0

    private var pendingCall: Call<ServerResponse<PendingRequests>>? = null
    private var episodesCall: Call<ServerResponse<Count>>? = null

    private var pendingLoaded = false
    private var episodesLoaded = false

    private val listeners = WeakHashMap<Listener, Boolean>(1)

    fun addListener(listener: Listener) = listeners.put(listener, true)
    fun removeListener(listener: Listener) = listeners.remove(listener)

    fun registerCloudMessaging(token: String?) {
        if (token != null) {
            service.registerFcmToken(token).enqueue(registerCallback)
        }
    }

    fun handleForegroundMessage(message: RemoteMessage) {
        when (message.data[ApiKey.type]) {
            NotificationType.REQUEST_PENDING -> refreshPendingStatus(true)
            NotificationType.EPISODE_NEW,
            NotificationType.TOPIC_NEW_VIDEO,
            NotificationType.CAST_NEW_VIDEO -> refreshEpisodesCount(true)

            else -> return
        }
    }

    fun refresh() {
        refreshPendingStatus()
        refreshEpisodesCount()
    }

    fun refreshPendingStatus(force: Boolean = false) {
        if (force || pendingCall == null && !pendingLoaded) {
            pendingCall?.cancel()
            pendingCall = service.pendingStatus().also {
                it.enqueue(pendingStatusCallback)
            }

        } else {
            notify(ACTION_LOADED, NotificationType.REQUEST_PENDING)
        }
    }

    fun refreshEpisodesCount(force: Boolean = false) {
        if (force || episodesCall == null && !episodesLoaded) {
            episodesCall?.cancel()
            episodesCall = service.newEpisodesCount().also {
                it.enqueue(episodesCountCallback)
            }

        } else {
            notify(ACTION_LOADED, NotificationType.EPISODE_NEW)
        }
    }

    fun notify(action: Int, @NotificationType type: String) {
        listeners.keys.forEach { it.onAction(action, type) }
    }

    private val pendingStatusCallback = object : ServerCallback<ServerResponse<PendingRequests>>() {
        override fun onSuccess(response: ServerResponse<PendingRequests>, url: String?) {
            pendingLoaded = true
            pendingRequests = response.data
            notify(ACTION_LOADED, NotificationType.REQUEST_PENDING)
            pendingCall = null
        }
    }

    private val episodesCountCallback = object : ServerCallback<ServerResponse<Count>>() {
        override fun onSuccess(response: ServerResponse<Count>, url: String?) {
            episodesLoaded = true
            newEpisodeCount = response.data?.count ?: 0
            notify(ACTION_LOADED, NotificationType.EPISODE_NEW)
            episodesCall = null
        }
    }

    private val registerCallback = object : ServerCallback<ServerResponse<Unit>>() {}
}