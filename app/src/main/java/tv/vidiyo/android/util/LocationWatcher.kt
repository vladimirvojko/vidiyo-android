package tv.vidiyo.android.util

import android.Manifest
import android.annotation.SuppressLint
import android.location.Criteria
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.api.model.IpInfo
import tv.vidiyo.android.api.model.LatLng
import tv.vidiyo.android.extension.isPermissionGranted

@SuppressLint("MissingPermission")
class LocationWatcher(
        private val manager: LocationManager,
        private val service: VidiyoService
) : LocationListener {

    companion object {
        const val REQUEST_CODE = 0xAAA

        private var internalLatLng: LatLng? = null
        val latLng: LatLng get() = internalLatLng ?: LatLng(30.2672, -97.7431)
    }

    fun updateLocation() {
        val isNetworkEnabled = manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
        val provider = if (isNetworkEnabled) LocationManager.NETWORK_PROVIDER else LocationManager.GPS_PROVIDER

        val lastKnown = manager.getLastKnownLocation(provider)
        if (lastKnown != null) {
            internalLatLng = LatLng(lastKnown.latitude, lastKnown.longitude)

        } else {
            val criteria = Criteria().also {
                it.accuracy = if (isNetworkEnabled) Criteria.ACCURACY_COARSE else Criteria.ACCURACY_FINE
            }
            manager.requestSingleUpdate(criteria, this, null)
        }
    }

    private fun updateLocationInternet() = service.ipInfo().enqueue(callback)

    fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>,
                                   grantResults: IntArray): Boolean = when (requestCode) {
        REQUEST_CODE -> {
            if (isPermissionGranted(Manifest.permission.ACCESS_FINE_LOCATION, permissions, grantResults)) {
                updateLocation()
            } else {
                updateLocationInternet()
            }
            true
        }
        else -> false
    }

    // region LocationListener
    override fun onLocationChanged(location: Location?) {
        if (location != null) {
            internalLatLng = LatLng(location.latitude, location.longitude)
            manager.removeUpdates(this)
        }
    }

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) = Unit
    override fun onProviderEnabled(provider: String?) = Unit
    override fun onProviderDisabled(provider: String?) = Unit
    //endregion

    private val callback = object : Callback<IpInfo> {
        override fun onResponse(call: Call<IpInfo>, response: Response<IpInfo>) {
            val result = response.body() ?: return
            internalLatLng = result.latLng
        }

        override fun onFailure(call: Call<IpInfo>, t: Throwable) = Unit
    }
}