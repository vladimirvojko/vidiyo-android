package tv.vidiyo.android.util

import android.util.Log
import tv.vidiyo.android.BuildConfig

class ALog {
    companion object {
        private const val TAG = "VDYO.LOG"

        fun d(s: String) {
            if (BuildConfig.DEBUG) Log.d(TAG, s)
        }
    }
}