package tv.vidiyo.android.util

object Constant {
    const val EMPTY = ""
    const val DATA = "data"
    const val DOT = "."
    const val COMMA = ","
    const val NEW_LINE = "\n"
    const val QUOTES = "\""

    const val IMAGE_SLASH = "image/*"
    const val IMAGE_PNG = "image/png"
    const val PLAINTEXT = "text/plain"

    const val NO_ID = -1

    const val ACTION_VIEWED = "ACTION_0"
    const val ACTION_MINIMIZED = "ACTION_1"
    const val ACTION_MAXIMIZED = "ACTION_2"

    const val LIMIT = 10
}