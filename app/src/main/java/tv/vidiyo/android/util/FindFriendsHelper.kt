package tv.vidiyo.android.util

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v4.app.Fragment
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import tv.vidiyo.android.domain.contract.SignUpContract
import tv.vidiyo.android.domain.presenter.boarding.FindFriendsPresenter
import tv.vidiyo.android.extension.isPermissionGranted

class FindFriendsHelper(val presenter: FindFriendsPresenter) {

    companion object {
        const val REQUEST_CODE = 1
    }

    private val loginManager = LoginManager.getInstance()
    private val callbackManager = CallbackManager.Factory.create()

    fun onCreate(callback: FacebookCallback<LoginResult>) {
        loginManager.registerCallback(callbackManager, callback)
    }

    fun findFacebookFriends(view: SignUpContract.FindFriends.View) {
        loginManager.apply {
            logOut()
            val list = arrayListOf("user_friends")
            when (view) {
                is Activity -> logInWithReadPermissions(view, list)
                is Fragment -> logInWithReadPermissions(view, list)
            }
        }
    }

    fun findContacts(context: Context, view: SignUpContract.FindFriends.View) {
        if (context.isPermissionGranted(Manifest.permission.READ_CONTACTS)) {
            presenter.findContactFriends()
        } else {
            val array = arrayOf(Manifest.permission.READ_CONTACTS)
            @RequiresApi(Build.VERSION_CODES.M)
            when (view) {
                is Activity -> view.requestPermissions(array, REQUEST_CODE)
                is Fragment -> view.requestPermissions(array, REQUEST_CODE)
            }
        }
    }

    fun onRequestPermissionsResult(view: SignUpContract.FindFriends.View, requestCode: Int, permissions: Array<out String>,
                                   grantResults: IntArray): Boolean = when (requestCode) {
        REQUEST_CODE -> {
            if (isPermissionGranted(Manifest.permission.READ_CONTACTS, permissions, grantResults)) {
                presenter.findContactFriends()
            } else {
                view.permissionNotGranted()
            }
            true
        }
        else -> false
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?): Boolean
            = callbackManager.onActivityResult(requestCode, resultCode, data)
}