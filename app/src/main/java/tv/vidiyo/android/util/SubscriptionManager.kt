package tv.vidiyo.android.util

import android.annotation.SuppressLint
import tv.vidiyo.android.account.UserAccountManager
import tv.vidiyo.android.api.*
import tv.vidiyo.android.api.model.Message
import tv.vidiyo.android.api.model.ServerResponse
import tv.vidiyo.android.api.model.SimpleSubscription
import tv.vidiyo.android.api.model.Subscribable
import tv.vidiyo.android.api.model.cast.Cast
import tv.vidiyo.android.api.model.episode.EpisodeInfo
import tv.vidiyo.android.api.model.show.Show
import tv.vidiyo.android.api.model.topic.Topic
import tv.vidiyo.android.api.model.user.*
import tv.vidiyo.android.extension.ServerCallback
import tv.vidiyo.android.model.InnerPageType

typealias SubscriptionCompletion = (ids: IntArray?, subscribed: Boolean, error: String?) -> Unit
typealias UserSubscriptionCompletion = (subscriptions: Array<Subscription>?, error: String?) -> Unit

class SubscriptionManager(
        private val service: VidiyoService,
        private val manager: UserAccountManager
) {

    private var current: UserAccount? = manager.account

    private val users = hashSetOf<Subscription>()
    private val casts = hashSetOf<SimpleSubscription>()
    private val topics = hashSetOf<SimpleSubscription>()
    private val shows = hashSetOf<SimpleSubscription>()
    private val episodes = hashSetOf<SimpleSubscription>()

    private var subscriptionCompletion: SubscriptionCompletion? = null
    private var userSubscriptionCompletion: UserSubscriptionCompletion? = null

    val isAnonymous: Boolean get() = current?.isAnonymous != false

    fun refreshUser() {
        current = manager.account
    }

    fun canRequestUser(user: User): Boolean {
        if (current == null) {
            current = manager.account
        }
        return user.id != current?.id
    }

    private fun contains(set: MutableSet<SimpleSubscription>, id: Int) = find(set, id) != null
    private fun find(set: MutableSet<SimpleSubscription>, id: Int) = set.find { it.id == id }

    fun setSubscription(subscribable: Subscribable, to: Boolean, completion: SubscriptionCompletion) {
        val set = getSetFor(subscribable)
        if (find(set, subscribable.id)?.subscribed == to) {
            completion(intArrayOf(subscribable.id), to, null)
            return
        }

        subscriptionCompletion = completion
        service.subscribe(subscribable, to).enqueue(subscribedCallback)
    }

    fun favorite(episode: EpisodeInfo, favorite: Boolean, completion: SubscriptionCompletion) {
        if (find(episodes, episode.id)?.subscribed == favorite) {
            completion(intArrayOf(episode.id), favorite, null)
            return
        }

        subscriptionCompletion = completion
        service.episodeFavorite(favorite, episode).enqueue(subscribedCallback)
    }

    fun setUsersSubscription(to: SubscriptionRequest, users: Array<FollowingUser>, completion: UserSubscriptionCompletion) {
        userSubscriptionCompletion = completion
        service.subscription(to, users).enqueue(userSubscriptionCallback)
    }

    fun setUserSubscription(to: SubscriptionRequest, user: FollowingUser, completion: UserSubscriptionCompletion) {
        userSubscriptionCompletion = completion
        service.subscription(to, user).enqueue(userSubscriptionCallback)
    }

    fun subscriptionStatus(user: FollowingUser) = users.find {
        it.subscriberId == user.id
    }?.status ?: user.status

    fun isLiked(episode: EpisodeInfo): Boolean {
        val found = find(episodes, episode.id)
        return found?.subscribed ?: episode.isFavorite
    }

    fun isSubscribed(subscribable: Subscribable): Boolean {
        val set = getSetFor(subscribable)
        val found = find(set, subscribable.id)
        return found?.subscribed ?: subscribable.isSubscribed
    }

    @SuppressLint("SwitchIntDef")
    fun getSetFor(@InnerPageType type: Int) = when (type) {
        InnerPageType.SHOWS -> shows
        InnerPageType.EPISODES -> episodes
        InnerPageType.CAST -> casts
        InnerPageType.TOPICS -> topics
        else -> throw IllegalArgumentException("Unknown set for type: $type")
    }

    private fun getSetFor(subscribable: Subscribable) = when (subscribable) {
        is Cast -> casts
        is Topic -> topics
        is Show -> shows
        else -> throw IllegalArgumentException("Unknown set for this type")
    }

    fun reset() {
        users.clear(); casts.clear(); topics.clear()
        shows.clear(); episodes.clear()
    }

    private val userSubscriptionCallback = object : ServerCallback<ServerResponse<Array<Subscription>>>() {
        override fun onSuccess(response: ServerResponse<Array<Subscription>>, url: String?) {
            val data = response.data
            userSubscriptionCompletion?.invoke(data, null)
            userSubscriptionCompletion = null

            users.addAll(data?.toSet() ?: return)
        }

        override fun onFailure(error: Message, url: String?) {
            userSubscriptionCompletion?.invoke(null, error.msg)
            userSubscriptionCompletion = null
        }
    }

    private val subscribedCallback = object : ServerCallback<ServerResponse<IntArray>>() {
        override fun onSuccess(response: ServerResponse<IntArray>, url: String?) {
            url ?: return

            val assign = url.contains(ApiKey.assign) || url.contains("/favorite")
            val data = response.data

            val set = when {
                url.contains("casts") -> casts
                url.contains("topics") -> topics
                url.contains("shows") -> shows
                url.contains("episodes") -> episodes
                else -> throw IllegalArgumentException("Unknown set for this type")
            }

            subscriptionCompletion?.invoke(data, assign, null)
            subscriptionCompletion = null

            val transformed = data?.map { SimpleSubscription(it, assign) } ?: return
            set.removeAll(transformed)
            set.addAll(transformed)
        }

        override fun onFailure(error: Message, url: String?) {
            subscriptionCompletion?.invoke(null, false, error.msg)
            subscriptionCompletion = null
        }
    }
}