package tv.vidiyo.android.util

import android.content.Context
import tv.vidiyo.android.api.model.City
import tv.vidiyo.android.api.model.Country
import java.nio.charset.Charset

object AssetsLoader {

    private fun loadFile(context: Context, filename: String): String {
        return context.assets.open(filename).use {
            it.readBytes().toString(Charset.defaultCharset())
        }
    }

    fun loadCountries(context: Context): List<Country> {
        val data = loadFile(context, "country-codes.txt").split(Constant.NEW_LINE)
        val countries = mutableListOf<Country>()
        data.map { it.split(Constant.COMMA) }.mapTo(countries) {
            Country(it[0], it[1], it[2], it[3])
        }
        return countries
    }

    fun loadCities(context: Context): List<City> {
        val data = loadFile(context, "us-cities-states.txt").split(Constant.NEW_LINE)
        val cities = mutableListOf<City>()
        data.map {
            it.replace(Constant.QUOTES, Constant.EMPTY).split(Constant.COMMA)
        }.mapTo(cities) {
            City(it[0], it[1])
        }
        return cities
    }
}
