package tv.vidiyo.android.util;

import android.view.View;

public interface OnAdPlayingListener {
    void show();
    void hide();
    void onCurrentProgressChanged(long current, long duration);
    View getView();
}
