package tv.vidiyo.android.util

import android.content.Context
import android.net.Uri
import android.view.TextureView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.ads.AdsMediaSource
import com.google.android.exoplayer2.source.hls.DefaultHlsDataSourceFactory
import com.google.android.exoplayer2.source.hls.HlsDataSourceFactory
import com.google.android.exoplayer2.source.hls.HlsMediaSource
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.ui.SimpleExoPlayerView
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import tv.vidiyo.android.BuildConfig
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.episode.EpisodeInfo
import tv.vidiyo.android.domain.presenter.episode.EpisodePlayerPresenter
import tv.vidiyo.android.extension.weak
import tv.vidiyo.android.ui.view.AdvertisementView
import java.lang.ref.WeakReference

class ExoPlayerAssistant : Player.DefaultEventListener() {

    companion object {
        private const val DURATION = 250L
    }

    var currentPresenter: EpisodePlayerPresenter? = null

    private var defaultDataSourceFactory: DefaultDataSourceFactory? = null
    private var hlsDataSourceFactory: HlsDataSourceFactory? = null
    private var player: SimpleExoPlayer? = null
    private var currentUri: Uri? = null
    private var loader: VidiyoAdsLoader? = null
    private var adViewReference: WeakReference<AdvertisementView>? = null

    private var lastSendTime = 0L
    private var isPlayerPlaying = true

    fun prepare(context: Context, view: SimpleExoPlayerView) {
        val p = player ?: createPlayer(context).also { player = it }
        view.player = p
        adViewReference?.get()?.let { (it.parent as? ViewGroup)?.removeView(it) }
        val adView = AdvertisementView(context).also { loader?.assign(it) }
        adViewReference = adView.weak()
        view.overlayFrameLayout.addView(adView)
    }

    fun load(info: EpisodeInfo, view: SimpleExoPlayerView, initial: Boolean = false) {
        val p = player ?: return
        val uri = info.media?.uri ?: return
        if (currentUri != uri) {
            currentUri = uri

            val contentSource = HlsMediaSource(uri, hlsDataSourceFactory, 1, null, null)

            p.stop()
            p.prepare(info.adsUri?.let {
                loader = VidiyoAdsLoader(it, info.media?.duration ?: 0f)
                AdsMediaSource(contentSource, defaultDataSourceFactory, loader, adViewReference?.get())
            } ?: contentSource)
        }

        val viewTime = info.view

        p.clearVideoSurface()
        p.setVideoTextureView(view.videoSurfaceView as TextureView)
        p.seekTo(if (initial && !viewTime.finished) (viewTime.time * 1000).toLong() else p.currentPosition)
        p.playWhenReady = isPlayerPlaying
        view.player = p
    }

    fun changeVisibility(layout: View, view: SimpleExoPlayerView, visibility: Int) {
        val animated = layout.tag as? Boolean
        if (animated == false) {
            when (visibility) {
                View.GONE -> {
                    layout.tag = true
                    view.showController()
                    layout.animate().alpha(0F).setDuration(DURATION).withEndAction {
                        view.hideController()
                        layout.tag = false
                    }.start()
                }

                View.VISIBLE -> layout.animate().alpha(1F).setDuration(DURATION).start()
            }
        }
    }

    fun togglePlayPause(playButton: ImageView) {
        val p = player ?: return
        val playing = p.playWhenReady
        p.playWhenReady = !playing
        playButton.setImageResource(if (playing) R.drawable.svg_play_103 else R.drawable.svg_pause)
    }

    fun release() {
        player?.apply { stop(); release() }
        player = null
        currentPresenter = null
    }

    fun onPause() {
        player?.let {
            isPlayerPlaying = it.playWhenReady
            it.playWhenReady = false
            it.removeListener(this)
        }
    }

    fun onResume() {
        player?.let {
            it.addListener(this)
            it.playWhenReady = isPlayerPlaying
        }
    }

    private fun createPlayer(context: Context): SimpleExoPlayer {
        val bandwidthMeter = DefaultBandwidthMeter()
        val selectionFactory = AdaptiveTrackSelection.Factory(bandwidthMeter)
        val trackSelector = DefaultTrackSelector(selectionFactory)
        val userAgent = Util.getUserAgent(context, BuildConfig.APPLICATION_ID)

        defaultDataSourceFactory = DefaultDataSourceFactory(context, userAgent, bandwidthMeter)
        hlsDataSourceFactory = DefaultHlsDataSourceFactory(defaultDataSourceFactory)

        return ExoPlayerFactory.newSimpleInstance(context, trackSelector).also {
            it.addListener(this)
        }
    }

    //region Player.EventListener

    override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
        val p = player ?: return
        val current = p.contentPosition
        if (lastSendTime != current && !p.isPlayingAd && !playWhenReady || playbackState == Player.STATE_ENDED) {
            lastSendTime = current
            currentPresenter?.setViewTime(current)
        }
    }

    //endregion
}