package tv.vidiyo.android.util

class PriceFormat {
    private val values = longArrayOf(
            1_000L,
            1_000_000L,
            1_000_000_000L,
            1_000_000_000_000L
    )
    private val keys = charArrayOf('k', 'M', 'G', 'T')

    fun format(float: Float): String {
        if (float <= values[0]) return float.toString()

        val (divided, suffix) = search(float)
        val rem = float.rem(divided).toLong()
        val div = float.div(divided).toLong()

        return div.toString() + Constant.DOT + rem.toString().subSequence(0, 2) + suffix
    }

    private fun search(float: Float): Pair<Long, Char> {
        var prev: Pair<Long, Char> = Pair(values[0], keys[0])
        var current: Pair<Long, Char>
        for (i in 1 until values.size) {
            current = Pair(values[i], keys[i])
            if (float <= current.first) {
                return prev
            }
            prev = current
        }
        return prev
    }
}