package tv.vidiyo.android.util

import android.app.Activity
import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.OnLifecycleEvent
import android.content.Intent
import android.widget.Toast
import com.facebook.AccessToken
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.twitter.sdk.android.core.*
import com.twitter.sdk.android.core.identity.TwitterAuthClient
import tv.vidiyo.android.R
import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.api.connectSocial
import tv.vidiyo.android.api.disconnectSocial
import tv.vidiyo.android.api.model.Message
import tv.vidiyo.android.api.model.ServerResponse
import tv.vidiyo.android.api.model.social.SocialAccount
import tv.vidiyo.android.api.model.social.SocialToken
import tv.vidiyo.android.api.model.social.SocialType
import tv.vidiyo.android.extension.ServerCallback
import tv.vidiyo.android.extension.weak
import tv.vidiyo.android.ui.boarding.BaseSocialFragment

typealias TokenCallback = (account: SocialAccount?, token: String) -> Unit
typealias SocialFailure = (error: String?) -> Unit
typealias SocialCallback = (social: SocialAccount?, Message?) -> Unit
typealias SocialTokenCompletion = (token: SocialToken) -> Unit

class SocialManager(
        activity: Activity,
        private val socials: Array<SocialAccount>?,
        private val googleApiClient: GoogleApiClient,
        private val twitter: Twitter,
        private val service: VidiyoService
) : LifecycleObserver, GoogleApiClient.OnConnectionFailedListener {

    companion object {
        const val REQUEST_CODE_GOOGLE = 0xFFA //google

        private const val PUBLIC_PROFILE = "public_profile"
        private const val PUBLISH_ACTIONS = "publish_actions"
        private const val USER_FRIENDS = "user_friends"
    }

    private val reference = activity.weak()

    private val twitterAuth = TwitterAuthClient()
    private val loginManager = LoginManager.getInstance()
    private val callbackManager = CallbackManager.Factory.create()

    private var account: SocialAccount? = null
    private var completion: SocialCallback? = null
    private var tokenCompletion: TokenCallback? = null
    private var tokenFailure: SocialFailure? = null

    val facebookToken: String? get() = AccessToken.getCurrentAccessToken()?.token

    fun bind(owner: LifecycleOwner) {
        owner.lifecycle.addObserver(this)
        googleApiClient.registerConnectionFailedListener(this)
        loginManager.registerCallback(callbackManager, loginCallback)
    }

    fun unbind(owner: LifecycleOwner) {
        owner.lifecycle.removeObserver(this)
        googleApiClient.disconnect()
        googleApiClient.unregisterConnectionFailedListener(this)
        loginManager.unregisterCallback(callbackManager)
    }

    //region Request token
    fun requestSocialToken(type: SocialType,
                           completion: SocialTokenCompletion,
                           failure: SocialFailure) {
        requestToken(SocialAccount(type), true, completion = { _, t ->
            val (token, secret) = if (type == SocialType.twitter) {
                val split = t.split(":")
                Pair(split[0], split[1])
            } else Pair(t, null)

            completion(SocialToken(type, token, secret))
        }, failure = failure)
    }

    fun requestToken(account: SocialAccount, withPublishAccess: Boolean,
                     completion: TokenCallback, failure: SocialFailure) {
        val activity = reference.get() ?: return

        this.account = account
        tokenCompletion = completion
        tokenFailure = failure

        when (account.socialType) {
            SocialType.facebook -> {
                loginManager.logOut()
                if (withPublishAccess) {
                    loginManager.logInWithPublishPermissions(activity, listOf(PUBLISH_ACTIONS))
                } else {
                    loginManager.logInWithReadPermissions(activity, listOf(PUBLIC_PROFILE, USER_FRIENDS))
                }
            }

            SocialType.google -> {
                if (googleApiClient.isConnected) {
                    Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback {
                        val intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient)
                        activity.startActivityForResult(intent, BaseSocialFragment.REQUEST_CODE)
                    }
                } else {
                    googleApiClient.connect()
                    Toast.makeText(activity, R.string.error_cant_connect_google, Toast.LENGTH_SHORT).show()
                }
            }

            SocialType.twitter -> twitterAuth.authorize(activity, twitterCallback)

            else -> throw IllegalArgumentException("unknown SocialType. Can't request token")
        }
    }
    //endregion

    fun connect(account: SocialAccount, connecting: Boolean, completion: SocialCallback, failure: SocialFailure) {
        this.account = account
        this.completion = completion

        if (connecting) {
            requestToken(account, false, { a, t ->
                service.connectSocial(a ?: return@requestToken, t).enqueue(vidiyoCallback)
            }, failure)

        } else {
            service.disconnectSocial(account).enqueue(vidiyoCallback)
        }
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?): Boolean {
        when {
            callbackManager.onActivityResult(requestCode, resultCode, data) -> Unit
            requestCode == REQUEST_CODE_GOOGLE -> {
                val result = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
                if (result.isSuccess) {
                    val token = result.signInAccount?.idToken ?: return true
                    account?.let { tokenCompletion?.invoke(it, token) }

                } else {
                    tokenFailure?.invoke(null)
                    completion = null
                }
                clearTokenCallbacks()
            }

            else -> {
                twitterAuth.onActivityResult(requestCode, resultCode, data)
                return false
            }
        }
        return true
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onStop() {
        googleApiClient.disconnect()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onStart() {
        googleApiClient.connect()
    }

    override fun onConnectionFailed(result: ConnectionResult) {
        ALog.d("onConnectionFailed: $result")
    }

    private fun clearTokenCallbacks() {
        account = null
        tokenCompletion = null
        tokenFailure = null
    }

    //region Callbacks
    private val vidiyoCallback = object : ServerCallback<ServerResponse<SocialAccount>>() {
        override fun onSuccess(response: ServerResponse<SocialAccount>, url: String?) {
            completion?.invoke(response.data, null)
            completion = null
        }

        override fun onFailure(error: Message, url: String?) {
            completion?.invoke(null, error)
            completion = null
        }
    }

    private val twitterCallback = object : Callback<TwitterSession>() {
        override fun success(result: Result<TwitterSession>?) {
            val authToken = result?.data?.authToken ?: return
            val token = "${authToken.token}:${authToken.secret}"
            account?.let { tokenCompletion?.invoke(it, token) }
            clearTokenCallbacks()
        }

        override fun failure(exception: TwitterException?) {
            tokenFailure?.invoke(exception?.message)
            clearTokenCallbacks()
            completion = null
        }
    }

    private val loginCallback = object : FacebookCallback<LoginResult> {
        override fun onSuccess(result: LoginResult?) {
            val token = result?.accessToken ?: return
            account?.let { tokenCompletion?.invoke(it, token.token) }
            clearTokenCallbacks()
        }

        override fun onError(error: FacebookException?) {
            tokenFailure?.invoke(error?.message)
            clearTokenCallbacks()
            completion = null
        }

        override fun onCancel() {
            tokenFailure?.invoke(null)
            clearTokenCallbacks()
            completion = null
        }
    }
    //endregion
}