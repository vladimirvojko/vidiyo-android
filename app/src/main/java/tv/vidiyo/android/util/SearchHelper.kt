package tv.vidiyo.android.util

import android.os.Handler
import android.os.HandlerThread
import android.os.Message
import android.view.View
import tv.vidiyo.android.extension.weak

interface OnSearchCallback {
    fun search(q: String?)
    fun getView(): View?
}

class SearchHelper(
        callback: OnSearchCallback
) : Handler(HandlerThread(SearchHelper::class.java.simpleName, Thread.MIN_PRIORITY).apply { start() }.looper) {

    companion object {
        private const val DELAY_SEARCH = 500L
        private const val WHAT_SEARCH = 0x10
    }

    private var query: String? = null
    private val reference = callback.weak()
    private val runnable = Runnable {
        reference.get()?.search(query)
    }

    fun startSearch(q: String?) {
        removeMessages(WHAT_SEARCH)
        sendMessageDelayed(obtainMessage().apply {
            what = WHAT_SEARCH
            obj = q
        }, DELAY_SEARCH)
    }

    override fun handleMessage(msg: Message) {
        when (msg.what) {
            WHAT_SEARCH -> {
                reference.get()?.getView()?.let {
                    it.removeCallbacks(runnable)
                    query = msg.obj as? String
                    it.post(runnable)
                }
            }
            else -> super.handleMessage(msg)
        }
    }

    fun release() {
        reference.clear()
        looper.quit()
    }
}