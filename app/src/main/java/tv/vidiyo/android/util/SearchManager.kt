package tv.vidiyo.android.util

import android.annotation.SuppressLint
import android.content.Context
import android.preference.PreferenceManager
import tv.vidiyo.android.api.model.cast.Cast
import tv.vidiyo.android.api.model.show.SearchingShow
import tv.vidiyo.android.api.model.topic.Topic
import tv.vidiyo.android.api.model.user.User
import tv.vidiyo.android.extension.gson
import tv.vidiyo.android.model.InnerPageType

@SuppressLint("SwitchIntDef")
@Suppress("UNCHECKED_CAST")
class SearchManager(context: Context) {

    companion object {
        const val KEY_0 = "KEY_0"//people
        const val KEY_1 = "KEY_1"//shows
        const val KEY_2 = "KEY_2"//cast
        const val KEY_3 = "KEY_3"//topics
    }

    private val preferences = PreferenceManager.getDefaultSharedPreferences(context)

    var people = emptyArray<User>()
    var shows = emptyArray<SearchingShow>()
    var casts = emptyArray<Cast>()
    var topics = emptyArray<Topic>()

    init {
        val g = gson
        preferences.getString(KEY_0, null)?.let { people = g.fromJson(it, Array<User>::class.java) }
        preferences.getString(KEY_1, null)?.let { shows = g.fromJson(it, Array<SearchingShow>::class.java) }
        preferences.getString(KEY_2, null)?.let { casts = g.fromJson(it, Array<Cast>::class.java) }
        preferences.getString(KEY_3, null)?.let { topics = g.fromJson(it, Array<Topic>::class.java) }
    }

    fun <T> set(@InnerPageType type: Int, array: Array<T>) {
        when (type) {
            InnerPageType.PEOPLE -> people = write(type, array as Array<User>)
            InnerPageType.SHOWS -> shows = write(type, array as Array<SearchingShow>)
            InnerPageType.CAST -> casts = write(type, array as Array<Cast>)
            InnerPageType.TOPICS -> topics = write(type, array as Array<Topic>)

            else -> throw IllegalArgumentException()
        }
    }

    fun clear() {
        preferences.edit().clear().apply()

        people = emptyArray()
        shows = emptyArray()
        casts = emptyArray()
        topics = emptyArray()
    }

    private fun <T> write(@InnerPageType type: Int, array: Array<T>): Array<T> {
        val edit = preferences.edit()
        edit.putString(getKeyFor(type), gson.toJson(array))
        edit.apply()
        return array
    }

    private fun getKeyFor(@InnerPageType type: Int) = when (type) {
        InnerPageType.PEOPLE -> KEY_0
        InnerPageType.SHOWS -> KEY_1
        InnerPageType.CAST -> KEY_2
        InnerPageType.TOPICS -> KEY_3

        else -> throw IllegalArgumentException()
    }
}