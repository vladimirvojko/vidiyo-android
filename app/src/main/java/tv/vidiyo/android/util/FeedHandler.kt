package tv.vidiyo.android.util

import android.os.Handler
import android.os.Looper
import android.os.Message
import tv.vidiyo.android.extension.weak
import tv.vidiyo.android.source.social.SocialFeedRepository

class FeedHandler(repository: SocialFeedRepository) : Handler(Looper.getMainLooper()) {

    companion object {
        const val WHAT_PREPARATION = 0x10
        const val WHAT_PREPARATION_FINISHED = 0x100

        const val WHAT_REFRESHING = 0x20
        const val WHAT_REFRESHING_FINISHED = 0x200
    }

    private val reference = repository.weak()
    private var preparing = 0
    private var refreshing = 0

    fun preparation() = send(WHAT_PREPARATION)
    fun preparationEnd() = send(WHAT_PREPARATION_FINISHED)
    fun refreshing() = send(WHAT_REFRESHING)
    fun refreshingEnd() = send(WHAT_REFRESHING_FINISHED)

    override fun handleMessage(msg: Message) {
        when (msg.what) {
            WHAT_PREPARATION -> preparing++
            WHAT_PREPARATION_FINISHED -> {
                preparing--
                if (preparing == 0) reference.get()?.load()
            }

            WHAT_REFRESHING -> refreshing++
            WHAT_REFRESHING_FINISHED -> {
                refreshing--
                if (refreshing == 0) reference.get()?.notifyDataLoaded()
            }

            else -> super.handleMessage(msg)
        }
    }

    private fun send(what: Int) {
        sendMessage(obtainMessage().apply {
            this.what = what
        })
    }
}