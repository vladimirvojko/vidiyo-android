package tv.vidiyo.android.util;

import android.net.Uri;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.ViewGroup;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.Player.DefaultEventListener;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.Timeline.Period;
import com.google.android.exoplayer2.source.ads.AdPlaybackState;
import com.google.android.exoplayer2.source.ads.AdsLoader;
import com.google.android.exoplayer2.util.Assertions;

import java.lang.ref.WeakReference;

@SuppressWarnings("WeakerAccess")
public class VidiyoAdsLoader extends DefaultEventListener implements AdsLoader {

    private static final long END_OF_CONTENT_POSITION_THRESHOLD_MS = 5000;
    private static final long END_OF_ADVT_POSITION_THRESHOLD_MS = 1000;
    private static final float MINIMUM_DURATION_FOR_MID_ROLL = 100f;

    @Nullable
    private final Uri[] adUris;
    private final Period period;
    private final float duration;
    private ComponentHandler handler;

    private EventListener eventListener;
    private Player player;

    private Timeline timeline;
    private long contentDurationMs;
    private AdPlaybackState adPlaybackState;
    private OnAdPlayingListener adListener;

    private boolean playingAd;
    private boolean sentContentComplete;
    private long pendingContentPositionMs;

    public int playingAdIndexInAdGroup;
    public int adGroupIndex;

    public VidiyoAdsLoader(@Nullable Uri[] uris, float duration) {
        period = new Period();
        adUris = uris;
        this.duration = duration;
    }

    public void assign(@NonNull OnAdPlayingListener listener) {
        adListener = listener;
        handler.assign(listener);
    }

    //region AdsLoader implementation

    @Override
    public void attachPlayer(ExoPlayer player, EventListener eventListener, ViewGroup adUiViewGroup) {
        this.player = player;
        this.eventListener = eventListener;
        adListener = adUiViewGroup instanceof OnAdPlayingListener
                ? (OnAdPlayingListener) adUiViewGroup : null;

        player.addListener(this);
        handler = new ComponentHandler(adListener, player);

        if (adPlaybackState != null) {
            eventListener.onAdPlaybackState(adPlaybackState);

        } else {
            loadAd();
        }
    }

    @Override
    public void detachPlayer() {
        if (playingAd) {
            adPlaybackState.setAdResumePositionUs(C.msToUs(player.getCurrentPosition()));
        }
        player.removeListener(this);
        player = null;
        eventListener = null;
        adListener = null;
    }

    @Override
    public void release() {
        handler.release();
    }

    //endregion

    //region Player.EventListener implementation

    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest) {
        if (timeline.isEmpty()) {
            // The player is being re-prepared and this source will be released.
            return;
        }
        Assertions.checkArgument(timeline.getPeriodCount() == 1);
        this.timeline = timeline;
        contentDurationMs = C.usToMs(timeline.getPeriod(0, period).durationUs);
        updateAdStateForPlayerState();
    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        if (!playingAd && playbackState == Player.STATE_BUFFERING && playWhenReady) {
            checkForContentComplete();
        } else if (playingAd) {
            if (playbackState == Player.STATE_ENDED) {
                onAdPlayed();

            } else {
                final long duration = player.getDuration();
                final long current = player.getCurrentPosition();

                if (duration - current <= END_OF_ADVT_POSITION_THRESHOLD_MS) {
                    onAdPlayed();
                }
            }
        }
    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {
        ALog.Companion.d("onPlayerError: " + playingAd);
    }

    @Override
    public void onPositionDiscontinuity(@Player.DiscontinuityReason int reason) {
        if (!playingAd && !player.isPlayingAd()) {
            checkForContentComplete();
            if (sentContentComplete) {
                for (int i = 0; i < adPlaybackState.adGroupCount; i++) {
                    if (adPlaybackState.adGroupTimesUs[i] != C.TIME_END_OF_SOURCE) {
                        adPlaybackState.playedAdGroup(i);
                    }
                }
                updateAdPlaybackState();
            } else if (timeline != null) {
                long positionMs = player.getCurrentPosition();
                timeline.getPeriod(0, period);
                if (period.getAdGroupIndexForPositionUs(C.msToUs(positionMs)) != C.INDEX_UNSET) {
                    pendingContentPositionMs = positionMs;
                }
            }
        } else {
            updateAdStateForPlayerState();
        }
    }

    //endregion

    //region Internal

    private void onAdPlayed() {
        adGroupIndex = player.getCurrentAdGroupIndex();
        playingAdIndexInAdGroup = player.getCurrentAdIndexInAdGroup();
        adPlaybackState.playedAdGroup(adGroupIndex);
        updateAdPlaybackState();
    }

    private void loadAd() {
        final float[] cuePoints;
        if (adUris != null && adUris.length > 0) {
            if (duration >= MINIMUM_DURATION_FOR_MID_ROLL) {
                cuePoints = new float[]{0, duration / 2, C.TIME_END_OF_SOURCE};
            } else {
                cuePoints = new float[]{0, C.TIME_END_OF_SOURCE};
            }

        } else {
            adPlaybackState = new AdPlaybackState(new long[0]);
            updateAdPlaybackState();
            return;
        }

        adPlaybackState = new AdPlaybackState(getAdGroupTimesUs(cuePoints));
        for (int i = 0; i < cuePoints.length; i++) {
            adPlaybackState.addAdUri(i, adUris[i]);
        }

        updateAdPlaybackState();
    }

    private void checkForContentComplete() {
        if (contentDurationMs != C.TIME_UNSET && pendingContentPositionMs == C.TIME_UNSET
                && player.getContentPosition() + END_OF_CONTENT_POSITION_THRESHOLD_MS >= contentDurationMs
                && !sentContentComplete) {
            sentContentComplete = true;
        }
    }

    private void updateAdStateForPlayerState() {
        playingAd = player.isPlayingAd();
        playingAdIndexInAdGroup = playingAd ? player.getCurrentAdIndexInAdGroup() : C.INDEX_UNSET;

        if (adListener == null) return;
        if (playingAd) {
            adListener.show();
            handler.start();
        } else {
            handler.stop();
            adListener.hide();
        }
    }

    private void updateAdPlaybackState() {
        // Ignore updates while detached. When a player is attached it will receive the latest state.
        if (eventListener != null) {
            eventListener.onAdPlaybackState(adPlaybackState.copy());
        }
    }

    private static long[] getAdGroupTimesUs(float[] cuePoints) {
        if (cuePoints.length == 0) {
            // If no cue points are specified, there is a preroll ad.
            return new long[]{0};
        }

        int count = cuePoints.length;
        long[] adGroupTimesUs = new long[count];
        for (int i = 0; i < count; i++) {
            double cuePoint = cuePoints[i];
            adGroupTimesUs[i] = cuePoint == -1.0 ? C.TIME_END_OF_SOURCE
                    : (long) (C.MICROS_PER_SECOND * cuePoint);
        }

        return adGroupTimesUs;
    }

    @NonNull
    private static Looper newLooper(@NonNull String name, int priority) {
        final HandlerThread thread = new HandlerThread(name);
        thread.setPriority(priority);
        thread.start();

        return thread.getLooper();
    }

    //endregion

    private static final class ComponentHandler extends Handler {

        private static final long DELAY_MS = 1000;
        private static final int WHAT_TICK = 0x10;

        @Nullable
        private WeakReference<OnAdPlayingListener> reference;
        private final WeakReference<ExoPlayer> playerRef;

        public ComponentHandler(@Nullable OnAdPlayingListener listener, ExoPlayer player) {
            super(newLooper(ComponentHandler.class.getSimpleName(), Thread.NORM_PRIORITY));
            reference = listener != null ? new WeakReference<>(listener) : null;
            playerRef = new WeakReference<>(player);
        }

        public void start() {
            removeMessages(WHAT_TICK);
            final Message message = obtainMessage();
            message.what = WHAT_TICK;
            sendMessageDelayed(message, DELAY_MS);
        }

        public void stop() {
            removeMessages(WHAT_TICK);
        }

        public void release() {
            getLooper().quit();
        }

        public void assign(@NonNull OnAdPlayingListener listener) {
            reference = new WeakReference<>(listener);
        }

        @Override
        public void handleMessage(Message msg) {
            if (msg.what == WHAT_TICK) {
                if (reference != null) {
                    final OnAdPlayingListener listener = reference.get();
                    final ExoPlayer player = playerRef.get();

                    if (listener != null && player != null) {
                        final long duration = player.getDuration();
                        if (duration != C.TIME_UNSET) {
                            listener.getView().post(new Runnable() {
                                @Override
                                public void run() {
                                    listener.onCurrentProgressChanged(player.getCurrentPosition(), duration);
                                }
                            });
                        }
                    }
                }
                start();
            }
        }
    }
}
