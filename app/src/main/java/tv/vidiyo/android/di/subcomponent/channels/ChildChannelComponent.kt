package tv.vidiyo.android.di.subcomponent.channels

import dagger.Subcomponent
import tv.vidiyo.android.di.scope.ChildFragmentScope
import tv.vidiyo.android.ui.channels.InnerChannelFragment

@[ChildFragmentScope Subcomponent(modules = arrayOf(
        ChildChannelModule::class
))]
interface ChildChannelComponent {
    fun injectTo(fragment: InnerChannelFragment)
}