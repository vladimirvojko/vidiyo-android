package tv.vidiyo.android.di.subcomponent.boarding

import android.content.Context
import android.support.v4.app.LoaderManager
import dagger.Module
import dagger.Provides
import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.base.BaseActivity
import tv.vidiyo.android.base.fragment.BaseFragment
import tv.vidiyo.android.di.scope.FragmentScope
import tv.vidiyo.android.domain.contract.SignUpContract
import tv.vidiyo.android.domain.presenter.boarding.FindFriendsPresenter
import tv.vidiyo.android.domain.presenter.boarding.FriendsListPresenter
import tv.vidiyo.android.source.FriendsRepository

@Module
class FriendsModule(private val view: SignUpContract.FindFriends.BaseView) {

    //region Find Friends
    @[Provides FragmentScope]
    fun providesFindFriendsView(): SignUpContract.FindFriends.View = view as SignUpContract.FindFriends.View

    @[Provides FragmentScope]
    fun providesFindFriendsPresenter(view: SignUpContract.FindFriends.View, repository: FriendsRepository, service: VidiyoService): FindFriendsPresenter {
        val loaderManager: LoaderManager; val context: Context
        when (view) {
            is BaseFragment -> {
                loaderManager = view.loaderManager
                context = view.context.applicationContext
            }
            is BaseActivity -> {
                loaderManager = view.supportLoaderManager
                context = view.applicationContext
            }
            else -> throw IllegalStateException("view must be Activity or Fragment")
        }

        return FindFriendsPresenter(view, repository, service, loaderManager, context)
    }
    //endregion

    //region Friends List
    @[Provides FragmentScope]
    fun providesFriendsListView(): SignUpContract.FriendsList.View = view as SignUpContract.FriendsList.View

    @[Provides FragmentScope]
    fun providesFriendsListPresenter(view: SignUpContract.FriendsList.View, repository: FriendsRepository, service: VidiyoService)
            = FriendsListPresenter(view, repository, service)
    //endregion
}