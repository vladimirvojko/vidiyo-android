package tv.vidiyo.android.di.subcomponent.launcher

import dagger.Module
import dagger.Provides
import tv.vidiyo.android.account.UserAccountManager
import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.di.ActivityModule
import tv.vidiyo.android.di.scope.ActivityScope
import tv.vidiyo.android.domain.contract.LauncherContract
import tv.vidiyo.android.domain.presenter.LauncherPresenter
import tv.vidiyo.android.ui.ActivityLauncher

@Module
class LauncherModule(activity: ActivityLauncher) : ActivityModule(activity) {

    @[Provides ActivityScope]
    fun provideView() = activity as LauncherContract.View

    @[Provides ActivityScope]
    fun providePresenter(view: LauncherContract.View, service: VidiyoService, manager: UserAccountManager)
            = LauncherPresenter(view, service, manager)
}