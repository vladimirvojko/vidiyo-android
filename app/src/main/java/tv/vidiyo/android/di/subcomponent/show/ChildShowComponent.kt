package tv.vidiyo.android.di.subcomponent.show

import dagger.Subcomponent
import tv.vidiyo.android.di.scope.ChildFragmentScope
import tv.vidiyo.android.ui.show.InnerShowFragment
import tv.vidiyo.android.ui.social.SocialFeedFragment

@[ChildFragmentScope Subcomponent(modules = arrayOf(
        ChildShowModule::class
))]
interface ChildShowComponent {
    fun injectTo(fragment: InnerShowFragment)
    fun injectTo(fragment: SocialFeedFragment)
}