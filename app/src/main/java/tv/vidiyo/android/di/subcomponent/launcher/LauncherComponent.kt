package tv.vidiyo.android.di.subcomponent.launcher

import dagger.Subcomponent
import tv.vidiyo.android.di.scope.ActivityScope
import tv.vidiyo.android.ui.ActivityLauncher

@[ActivityScope Subcomponent(modules = arrayOf(
        LauncherModule::class
))]
interface LauncherComponent {
    fun inject(activity: ActivityLauncher)
}