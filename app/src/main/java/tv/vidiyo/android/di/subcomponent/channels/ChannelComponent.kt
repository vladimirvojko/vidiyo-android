package tv.vidiyo.android.di.subcomponent.channels

import dagger.Subcomponent
import tv.vidiyo.android.di.scope.FragmentScope
import tv.vidiyo.android.ui.channels.ChannelFragment

@[FragmentScope Subcomponent(modules = arrayOf(
        ChannelModule::class
))]
interface ChannelComponent {
    fun injectTo(fragment: ChannelFragment)
    fun plus(module: ChildChannelModule): ChildChannelComponent
}