package tv.vidiyo.android.di.subcomponent.cast

import dagger.Subcomponent
import tv.vidiyo.android.di.scope.ChildFragmentScope
import tv.vidiyo.android.ui.cast.InnerCastFragment
import tv.vidiyo.android.ui.social.SocialFeedFragment

@[ChildFragmentScope Subcomponent(modules = arrayOf(
        ChildCastModule::class
))]
interface ChildCastComponent {
    fun injectTo(fragment: InnerCastFragment)
    fun injectTo(fragment: SocialFeedFragment)
}