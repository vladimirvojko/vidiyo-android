package tv.vidiyo.android.di.subcomponent.settings

import dagger.Module
import dagger.Provides
import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.di.FragmentModule
import tv.vidiyo.android.di.scope.FragmentScope
import tv.vidiyo.android.domain.contract.SettingsContract
import tv.vidiyo.android.domain.presenter.settings.SocialAccountPresenter
import tv.vidiyo.android.source.profile.ProfileRepository
import tv.vidiyo.android.ui.settings.SocialAccountsFragment
import tv.vidiyo.android.util.SocialManager

@Module
class SocialAccountModule(fragment: SocialAccountsFragment) : FragmentModule(fragment) {

    @[FragmentScope Provides]
    fun provideView() = fragment as SettingsContract.Socials.View

    @[FragmentScope Provides]
    fun providePresenter(view: SettingsContract.Socials.View, manager: SocialManager, service: VidiyoService,
                         repository: ProfileRepository) = SocialAccountPresenter(view, manager, service, repository)
}