package tv.vidiyo.android.di.subcomponent.cast

import dagger.Module
import dagger.Provides
import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.di.FragmentModule
import tv.vidiyo.android.di.scope.FragmentScope
import tv.vidiyo.android.domain.contract.CastContract
import tv.vidiyo.android.domain.presenter.cast.CastPresenter
import tv.vidiyo.android.source.cast.CastRepository
import tv.vidiyo.android.source.social.SocialFeedRepository
import tv.vidiyo.android.ui.cast.CastFragment
import tv.vidiyo.android.util.SubscriptionManager

@Module
class CastModule(fragment: CastFragment, private val id: Int) : FragmentModule(fragment) {

    @[FragmentScope Provides]
    fun provideView() = fragment as CastContract.View

    @[FragmentScope Provides]
    fun providePresenter(view: CastContract.View, repository: CastRepository, manager: SubscriptionManager)
            = CastPresenter(view, repository, manager)

    @[FragmentScope Provides]
    fun provideRepository(service: VidiyoService) = CastRepository(id, service)

    @[FragmentScope Provides]
    fun provideSocialRepository(repository: CastRepository, service: VidiyoService)
            = SocialFeedRepository(repository.data?.socials, service)
}