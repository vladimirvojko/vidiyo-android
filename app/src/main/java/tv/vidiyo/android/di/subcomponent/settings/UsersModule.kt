package tv.vidiyo.android.di.subcomponent.settings

import dagger.Module
import dagger.Provides
import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.api.model.user.FollowingType
import tv.vidiyo.android.di.FragmentModule
import tv.vidiyo.android.di.scope.FragmentScope
import tv.vidiyo.android.domain.contract.SettingsContract
import tv.vidiyo.android.domain.presenter.settings.UsersPresenter
import tv.vidiyo.android.source.settings.UsersRepository
import tv.vidiyo.android.ui.settings.UsersFragment
import tv.vidiyo.android.util.NotificationsCenter
import tv.vidiyo.android.util.SubscriptionManager

@Module
class UsersModule(fragment: UsersFragment) : FragmentModule(fragment) {

    @[FragmentScope Provides]
    fun provideView() = fragment as SettingsContract.Users.View

    @[FragmentScope Provides]
    fun provideType(view: SettingsContract.Users.View) = view.type

    @[FragmentScope Provides]
    fun provideUserId(view: SettingsContract.Users.View) = view.userId

    @[FragmentScope Provides]
    fun provideRepository(service: VidiyoService, type: FollowingType, id: Int) = UsersRepository(service, type, id)

    @[FragmentScope Provides]
    fun providePresenter(view: SettingsContract.Users.View, repository: UsersRepository, manager: SubscriptionManager, center: NotificationsCenter)
            = UsersPresenter(view, repository, manager, center)
}