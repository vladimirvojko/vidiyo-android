package tv.vidiyo.android.di.subcomponent

import dagger.Subcomponent
import tv.vidiyo.android.di.scope.ActivityScope
import tv.vidiyo.android.ui.boarding.SplashActivity

@[ActivityScope Subcomponent(modules = arrayOf(
        SplashModule::class
))]
interface SplashComponent {
    fun injectTo(activity: SplashActivity)
}