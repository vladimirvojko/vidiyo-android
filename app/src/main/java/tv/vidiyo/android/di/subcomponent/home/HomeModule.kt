package tv.vidiyo.android.di.subcomponent.home

import dagger.Module
import dagger.Provides
import tv.vidiyo.android.di.FragmentModule
import tv.vidiyo.android.di.scope.FragmentScope
import tv.vidiyo.android.domain.contract.HomeContract
import tv.vidiyo.android.domain.presenter.home.HomePresenter
import tv.vidiyo.android.source.home.HomeRepository
import tv.vidiyo.android.ui.home.HomeFragment
import tv.vidiyo.android.util.SubscriptionManager

@Module
class HomeModule(fragment: HomeFragment) : FragmentModule(fragment) {

    @[FragmentScope Provides]
    fun provideView() = fragment as HomeContract.View

    @[FragmentScope Provides]
    fun providePresenter(view: HomeContract.View, repository: HomeRepository, manager: SubscriptionManager)
            = HomePresenter(view, repository, manager)
}