package tv.vidiyo.android.di.subcomponent.episode

import dagger.Module
import dagger.Provides
import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.source.episode.EpisodeRepository
import tv.vidiyo.android.util.ExoPlayerAssistant
import javax.inject.Singleton

@Module
class EpisodeModule(private val id: Int) {

    @[Provides Singleton]
    fun provideId(repository: EpisodeRepository) = repository.id

    @[Provides Singleton]
    fun provideEpisodeRepository(service: VidiyoService) = EpisodeRepository(id, service)

    @[Provides Singleton]
    fun provideExoPlayerAssistant() = ExoPlayerAssistant()
}