package tv.vidiyo.android.di.subcomponent.profile

import android.support.v4.app.Fragment
import dagger.Module
import dagger.Provides
import tv.vidiyo.android.di.FragmentModule
import tv.vidiyo.android.di.scope.FragmentScope
import tv.vidiyo.android.domain.contract.ProfileContract

@Module
abstract class BaseProfileModule(fragment: Fragment) : FragmentModule(fragment) {

    @[FragmentScope Provides]
    fun provideView() = fragment as ProfileContract.View
}