package tv.vidiyo.android.di.subcomponent

import dagger.Module
import dagger.Provides
import tv.vidiyo.android.App
import tv.vidiyo.android.account.UserAccountManager
import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.di.ActivityModule
import tv.vidiyo.android.di.scope.ActivityScope
import tv.vidiyo.android.domain.contract.SplashContract
import tv.vidiyo.android.domain.presenter.SplashPresenter
import tv.vidiyo.android.ui.boarding.SplashActivity

@Module
class SplashModule(activity: SplashActivity) : ActivityModule(activity) {

    @[Provides ActivityScope]
    fun provideSplashView() = activity as SplashContract.View

    @[Provides ActivityScope]
    fun provideSplashPresenter(view: SplashContract.View, manager: UserAccountManager, service: VidiyoService)
            = SplashPresenter(view, App.deviceId, manager, service)
}