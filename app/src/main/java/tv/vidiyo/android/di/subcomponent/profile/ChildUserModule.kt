package tv.vidiyo.android.di.subcomponent.profile

import dagger.Module
import dagger.Provides
import tv.vidiyo.android.di.scope.ChildFragmentScope
import tv.vidiyo.android.domain.contract.ProfileContract
import tv.vidiyo.android.domain.presenter.profile.InnerProfilePresenter
import tv.vidiyo.android.source.profile.ProfileRepository
import tv.vidiyo.android.ui.profile.InnerProfileFragment
import tv.vidiyo.android.util.SubscriptionManager

@Module
class ChildUserModule(private val fragment: InnerProfileFragment) {

    @[ChildFragmentScope Provides]
    fun provideInnerView(): ProfileContract.Inner.View = fragment

    @[ChildFragmentScope Provides]
    fun provideInnerPresenter(view: ProfileContract.Inner.View, repository: ProfileRepository, manager: SubscriptionManager)
            = InnerProfilePresenter(view, repository, manager)
}