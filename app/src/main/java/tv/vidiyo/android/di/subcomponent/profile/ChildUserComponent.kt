package tv.vidiyo.android.di.subcomponent.profile

import dagger.Subcomponent
import tv.vidiyo.android.di.scope.ChildFragmentScope
import tv.vidiyo.android.ui.profile.InnerProfileFragment

@[ChildFragmentScope Subcomponent(modules = arrayOf(
        ChildUserModule::class
))]
interface ChildUserComponent {
    fun injectTo(fragment: InnerProfileFragment)
}