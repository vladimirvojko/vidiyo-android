package tv.vidiyo.android.di.subcomponent.profile

import dagger.Subcomponent
import tv.vidiyo.android.di.scope.FragmentScope
import tv.vidiyo.android.ui.profile.ProfileFragment

@[FragmentScope Subcomponent(modules = arrayOf(
        UserModule::class
))]
interface UserComponent {
    fun injectTo(fragment: ProfileFragment)
    fun plus(module: ChildUserModule): ChildUserComponent
}