package tv.vidiyo.android.di.qualifier

import javax.inject.Qualifier

@Qualifier
annotation class ApplicationQualifier