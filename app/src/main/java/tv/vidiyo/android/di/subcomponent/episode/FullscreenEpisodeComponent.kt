package tv.vidiyo.android.di.subcomponent.episode

import dagger.Subcomponent
import tv.vidiyo.android.di.scope.ActivityScope
import tv.vidiyo.android.ui.FullscreenActivity

@[ActivityScope Subcomponent(modules = arrayOf(
        FullscreenEpisodeModule::class
))]
interface FullscreenEpisodeComponent {
    fun injectTo(activity: FullscreenActivity)
}