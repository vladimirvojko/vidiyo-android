package tv.vidiyo.android.di.subcomponent.discover

import dagger.Module
import dagger.Provides
import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.di.FragmentModule
import tv.vidiyo.android.di.scope.FragmentScope
import tv.vidiyo.android.domain.contract.DiscoverContract
import tv.vidiyo.android.domain.presenter.discover.DiscoverPresenter
import tv.vidiyo.android.domain.presenter.discover.DiscoveredPresenter
import tv.vidiyo.android.ui.discover.DiscoverFragment
import tv.vidiyo.android.ui.discover.DiscoveredFragment
import tv.vidiyo.android.util.SocialManager
import tv.vidiyo.android.util.SubscriptionManager

@Module
class DiscoverModule : FragmentModule {

    constructor(fragment: DiscoverFragment) : super(fragment)
    constructor(fragment: DiscoveredFragment) : super(fragment)

    @[FragmentScope Provides]
    fun provideView() = fragment as DiscoverContract.View

    @[FragmentScope Provides]
    fun providePresenter(view: DiscoverContract.View, service: VidiyoService, subscriptionManager: SubscriptionManager,
                         socialManager: SocialManager) = DiscoverPresenter(view, service, subscriptionManager, socialManager)

    @[FragmentScope Provides]
    fun provideDiscoveredView() = fragment as DiscoverContract.Discovered.View

    @[FragmentScope Provides]
    fun provideDiscoveredPresenter(view: DiscoverContract.Discovered.View, service: VidiyoService, subscriptionManager: SubscriptionManager,
                                   socialManager: SocialManager) = DiscoveredPresenter(view, service, subscriptionManager, socialManager)
}