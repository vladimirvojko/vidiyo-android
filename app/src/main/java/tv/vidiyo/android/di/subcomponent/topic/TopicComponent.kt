package tv.vidiyo.android.di.subcomponent.topic

import dagger.Subcomponent
import tv.vidiyo.android.di.scope.FragmentScope
import tv.vidiyo.android.ui.topic.TopicFragment

@[FragmentScope Subcomponent(modules = arrayOf(
        TopicModule::class
))]
interface TopicComponent {
    fun injectTo(fragment: TopicFragment)
    fun plus(module: ChildTopicModule): ChildTopicComponent
}