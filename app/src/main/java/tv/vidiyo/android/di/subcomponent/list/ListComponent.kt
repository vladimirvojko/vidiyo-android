package tv.vidiyo.android.di.subcomponent.list

import dagger.Subcomponent
import tv.vidiyo.android.di.scope.FragmentScope
import tv.vidiyo.android.ui.ListFragment

@[FragmentScope Subcomponent(modules = arrayOf(
        ListModule::class
))]
interface ListComponent {
    fun injectTo(fragment: ListFragment)
}