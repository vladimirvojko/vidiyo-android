package tv.vidiyo.android.di.subcomponent.share

import dagger.Module
import dagger.Provides
import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.di.FragmentModule
import tv.vidiyo.android.di.scope.FragmentScope
import tv.vidiyo.android.domain.contract.ShareContract
import tv.vidiyo.android.domain.presenter.SharePresenter
import tv.vidiyo.android.ui.dialog.ShareEpisodeSheetDialog
import tv.vidiyo.android.util.SocialManager

@Module
class ShareModule(dialog: ShareEpisodeSheetDialog) : FragmentModule(dialog) {

    @[FragmentScope Provides]
    fun provideView() = fragment as ShareContract.View

    @[FragmentScope Provides]
    fun providePresenter(view: ShareContract.View, service: VidiyoService, manager: SocialManager)
            = SharePresenter(view, service, manager)
}