package tv.vidiyo.android.di.subcomponent

import android.support.v7.app.AppCompatActivity
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.GoogleApiClient
import com.twitter.sdk.android.core.Twitter
import dagger.Module
import dagger.Provides
import tv.vidiyo.android.R
import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.api.model.social.SocialAccount
import tv.vidiyo.android.di.ActivityModule
import tv.vidiyo.android.di.scope.ActivityScope
import tv.vidiyo.android.ui.MainActivity
import tv.vidiyo.android.ui.settings.SettingsActivity
import tv.vidiyo.android.util.SocialManager

@Module
class SocialModule : ActivityModule {

    constructor(activity: MainActivity) : super(activity)
    constructor(activity: SettingsActivity) : super(activity)

    @[Provides ActivityScope]
    fun provideGoogleApiClient(activity: AppCompatActivity): GoogleApiClient {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(activity.getString(R.string.default_web_client_id))
                .requestEmail()
                .build()

        val builder = GoogleApiClient.Builder(activity).addApi(Auth.GOOGLE_SIGN_IN_API, gso)
        return builder.build()
    }

    @[Provides ActivityScope]
    fun provideManager(activity: AppCompatActivity, socials: Array<SocialAccount>?, googleApiClient: GoogleApiClient, twitter: Twitter,
                       service: VidiyoService) = SocialManager(activity, socials, googleApiClient, twitter, service)
}