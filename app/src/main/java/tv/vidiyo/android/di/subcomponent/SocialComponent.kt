package tv.vidiyo.android.di.subcomponent

import dagger.Subcomponent
import tv.vidiyo.android.di.scope.ActivityScope
import tv.vidiyo.android.di.subcomponent.discover.DiscoverComponent
import tv.vidiyo.android.di.subcomponent.discover.DiscoverModule
import tv.vidiyo.android.di.subcomponent.settings.SocialAccountComponent
import tv.vidiyo.android.di.subcomponent.settings.SocialAccountModule
import tv.vidiyo.android.di.subcomponent.share.ShareComponent
import tv.vidiyo.android.di.subcomponent.share.ShareModule
import tv.vidiyo.android.ui.MainActivity
import tv.vidiyo.android.ui.settings.SettingsActivity

@[ActivityScope Subcomponent(modules = arrayOf(
        SocialModule::class
))]
interface SocialComponent {
    fun plus(module: SocialAccountModule): SocialAccountComponent
    fun plus(module: DiscoverModule): DiscoverComponent
    fun plus(module: ShareModule): ShareComponent

    fun injectTo(activity: MainActivity)
    fun injectTo(activity: SettingsActivity)
}