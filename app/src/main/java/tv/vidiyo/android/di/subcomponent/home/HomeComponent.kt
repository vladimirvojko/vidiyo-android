package tv.vidiyo.android.di.subcomponent.home

import dagger.Subcomponent
import tv.vidiyo.android.di.scope.FragmentScope
import tv.vidiyo.android.ui.home.HomeFragment

@[FragmentScope Subcomponent(modules = arrayOf(
        HomeModule::class
))]
interface HomeComponent {
    fun injectTo(fragment: HomeFragment)
}