package tv.vidiyo.android.di

import android.content.Context
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import tv.vidiyo.android.BuildConfig
import tv.vidiyo.android.account.UserAccountManager
import tv.vidiyo.android.api.AuthInterceptor
import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.di.qualifier.ApplicationQualifier
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class NetworkModule {

    @[Provides Singleton]
    fun provideOkHttpClient(interceptor: AuthInterceptor): OkHttpClient
            = OkHttpClient().newBuilder()
            .readTimeout(30, TimeUnit.SECONDS)
            .addInterceptor(interceptor)
            .addInterceptor(HttpLoggingInterceptor().apply {
                level = if (BuildConfig.DEBUG) Level.BODY else Level.NONE
            }).build()

    @[Provides Singleton]
    fun provideAuthInterceptor(@ApplicationQualifier context: Context, manager: UserAccountManager)
            = AuthInterceptor(context, manager)

    @[Provides Singleton]
    fun provideRestAdapter(client: OkHttpClient): Retrofit = Retrofit.Builder()
            .baseUrl(BuildConfig.SERVER_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    @[Provides Singleton]
    fun provideVidiyoService(retrofit: Retrofit): VidiyoService = retrofit.create(VidiyoService::class.java)
}