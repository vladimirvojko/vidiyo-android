package tv.vidiyo.android.di

import android.content.Context
import android.location.LocationManager
import android.support.v4.content.LocalBroadcastManager
import android.view.inputmethod.InputMethodManager
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import com.twitter.sdk.android.core.Twitter
import dagger.Module
import dagger.Provides
import tv.vidiyo.android.App
import tv.vidiyo.android.account.UserAccountManager
import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.api.model.user.UserAccount
import tv.vidiyo.android.di.qualifier.ApplicationQualifier
import tv.vidiyo.android.util.SubscriptionManager
import javax.inject.Singleton

@Module
class ApplicationModule(private val app: App) {

    @[Provides Singleton]
    fun provideApp(): App = app

    @[Provides Singleton ApplicationQualifier]
    fun provideAppContext(): Context = app

    @[Provides Singleton]
    fun provideGlide(@ApplicationQualifier context: Context): RequestManager = Glide.with(context)

    @[Provides Singleton]
    fun provideUserAccountManager(@ApplicationQualifier context: Context) = UserAccountManager(context)

    @[Provides Singleton]
    fun provideLocalBroadcastManager(@ApplicationQualifier context: Context): LocalBroadcastManager
            = LocalBroadcastManager.getInstance(context)

    @[Provides Singleton]
    fun provideInputMethodManager(@ApplicationQualifier context: Context): InputMethodManager
            = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

    @[Provides Singleton]
    fun provideLocationManager(@ApplicationQualifier context: Context) = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager

    @Provides
    fun provideUserAccount(manager: UserAccountManager): UserAccount? = manager.account

    @[Provides Singleton]
    fun provideSubscriptionManager(service: VidiyoService, manager: UserAccountManager)
            = SubscriptionManager(service, manager)

    @[Provides Singleton]
    fun provideTwitter(@ApplicationQualifier context: Context): Twitter {
        Twitter.initialize(context)
        return Twitter.getInstance()
    }
}