package tv.vidiyo.android.di.subcomponent.profile

import dagger.Module
import dagger.Provides
import tv.vidiyo.android.account.UserAccountManager
import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.di.scope.FragmentScope
import tv.vidiyo.android.domain.contract.ProfileContract
import tv.vidiyo.android.domain.presenter.profile.ProfilePresenter
import tv.vidiyo.android.source.profile.ProfileRepository
import tv.vidiyo.android.ui.profile.ProfileFragment
import tv.vidiyo.android.util.SubscriptionManager

@Module
class UserModule(fragment: ProfileFragment) : BaseProfileModule(fragment) {

    @[FragmentScope Provides]
    fun providePresenter(view: ProfileContract.View, repository: ProfileRepository, service: VidiyoService,
                         manager: UserAccountManager, subscriptionManager: SubscriptionManager)
            = ProfilePresenter(view, repository, service, manager, subscriptionManager)

    @[FragmentScope Provides]
    fun provideProfileId(view: ProfileContract.View) = view.profileId

    @[FragmentScope Provides]
    fun provideRepository(id: Int, service: VidiyoService) = ProfileRepository(id, service)
}