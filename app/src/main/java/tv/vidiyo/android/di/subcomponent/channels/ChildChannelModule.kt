package tv.vidiyo.android.di.subcomponent.channels

import dagger.Module
import dagger.Provides
import tv.vidiyo.android.di.scope.ChildFragmentScope
import tv.vidiyo.android.domain.contract.ChannelContract
import tv.vidiyo.android.domain.presenter.channels.InnerChannelPresenter
import tv.vidiyo.android.source.channels.ChannelRepository
import tv.vidiyo.android.ui.channels.InnerChannelFragment
import tv.vidiyo.android.util.SubscriptionManager

@Module
class ChildChannelModule(private val fragment: InnerChannelFragment) {

    @[ChildFragmentScope Provides]
    fun provideView(): ChannelContract.Inner.View = fragment

    @[ChildFragmentScope Provides]
    fun providePresenter(view: ChannelContract.Inner.View, repository: ChannelRepository, manager: SubscriptionManager)
            = InnerChannelPresenter(view, repository, manager)
}