package tv.vidiyo.android.di.subcomponent.show

import dagger.Module
import dagger.Provides
import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.di.FragmentModule
import tv.vidiyo.android.di.scope.FragmentScope
import tv.vidiyo.android.domain.contract.ShowContract
import tv.vidiyo.android.domain.presenter.show.ShowPresenter
import tv.vidiyo.android.source.show.ShowRepository
import tv.vidiyo.android.source.social.SocialFeedRepository
import tv.vidiyo.android.ui.show.ShowFragment
import tv.vidiyo.android.util.SubscriptionManager

@Module
class ShowModule(fragment: ShowFragment, private val id: Int) : FragmentModule(fragment) {

    @[FragmentScope Provides]
    fun provideView() = fragment as ShowContract.View

    @[FragmentScope Provides]
    fun providePresenter(view: ShowContract.View, repository: ShowRepository, manager: SubscriptionManager)
            = ShowPresenter(view, repository, manager)

    @[FragmentScope Provides]
    fun provideRepository(service: VidiyoService) = ShowRepository(id, service)

    @[FragmentScope Provides]
    fun provideSocialRepository(repository: ShowRepository, service: VidiyoService)
            = SocialFeedRepository(repository.data?.socials, service)
}