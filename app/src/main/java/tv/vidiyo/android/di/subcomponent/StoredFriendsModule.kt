package tv.vidiyo.android.di.subcomponent

import dagger.Module
import dagger.Provides
import tv.vidiyo.android.source.FriendsRepository
import javax.inject.Singleton

@Module
class StoredFriendsModule {

    @[Provides Singleton]
    fun provideFriendsRepository() = FriendsRepository()
}