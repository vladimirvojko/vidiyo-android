package tv.vidiyo.android.di.subcomponent.search

import dagger.Module
import dagger.Provides
import tv.vidiyo.android.di.FragmentModule
import tv.vidiyo.android.di.scope.FragmentScope
import tv.vidiyo.android.domain.contract.SearchContract
import tv.vidiyo.android.domain.presenter.search.InnerSearchPresenter
import tv.vidiyo.android.domain.presenter.search.SearchPresenter
import tv.vidiyo.android.source.search.SearchMainRepository
import tv.vidiyo.android.source.search.SearchResultsRepository
import tv.vidiyo.android.ui.search.InnerSearchResultFragment
import tv.vidiyo.android.ui.search.SearchFragment
import tv.vidiyo.android.ui.search.SearchResultsFragment
import tv.vidiyo.android.util.OnSearchCallback
import tv.vidiyo.android.util.SearchHelper

@Module
class SearchModule : FragmentModule {
    constructor(fragment: SearchFragment) : super(fragment)
    constructor(fragment: SearchResultsFragment) : super(fragment)
    constructor(fragment: InnerSearchResultFragment) : super(fragment)

    @[FragmentScope Provides]
    fun provideView() = fragment as SearchContract.View

    @[FragmentScope Provides]
    fun providePresenter(view: SearchContract.View, repository: SearchMainRepository) = SearchPresenter(view, repository)

    @[FragmentScope Provides]
    fun provideInnerView() = fragment as SearchContract.Inner.View

    @[FragmentScope Provides]
    fun provideInnerPresenter(view: SearchContract.Inner.View, repository: SearchResultsRepository)
            = InnerSearchPresenter(view, repository)

    @[FragmentScope Provides]
    fun provideSearchHelper(callback: OnSearchCallback) = SearchHelper(callback)

    @[FragmentScope Provides]
    fun provideSearchCallback() = fragment as OnSearchCallback
}