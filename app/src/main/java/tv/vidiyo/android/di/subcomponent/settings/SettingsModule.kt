package tv.vidiyo.android.di.subcomponent.settings

import dagger.Module
import dagger.Provides
import tv.vidiyo.android.account.UserAccountManager
import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.di.FragmentModule
import tv.vidiyo.android.di.scope.FragmentScope
import tv.vidiyo.android.domain.contract.SettingsContract
import tv.vidiyo.android.domain.presenter.settings.ChangePasswordPresenter
import tv.vidiyo.android.domain.presenter.settings.EditProfilePresenter
import tv.vidiyo.android.domain.presenter.settings.MainSettingsPresenter
import tv.vidiyo.android.domain.presenter.settings.NotificationsPresenter
import tv.vidiyo.android.source.profile.ProfileRepository
import tv.vidiyo.android.ui.settings.EditProfileFragment
import tv.vidiyo.android.ui.settings.NotificationsFragment
import tv.vidiyo.android.ui.settings.PasswordFragment
import tv.vidiyo.android.ui.settings.SettingsFragment
import tv.vidiyo.android.util.SearchManager

@Module
class SettingsModule : FragmentModule {

    constructor(fragment: SettingsFragment) : super(fragment)
    constructor(fragment: EditProfileFragment) : super(fragment)
    constructor(fragment: PasswordFragment) : super(fragment)
    constructor(fragment: NotificationsFragment) : super(fragment)

    @[FragmentScope Provides]
    fun provideMainView() = fragment as SettingsContract.Main.View

    @[FragmentScope Provides]
    fun provideMainPresenter(view: SettingsContract.Main.View, manager: UserAccountManager, service: VidiyoService, searchManager: SearchManager)
            = MainSettingsPresenter(view, manager, service, searchManager)

    @[FragmentScope Provides]
    fun provideEditView() = fragment as SettingsContract.Edit.View

    @[FragmentScope Provides]
    fun provideEditPresenter(view: SettingsContract.Edit.View, repository: ProfileRepository, service: VidiyoService)
            = EditProfilePresenter(view, repository, service)

    @[FragmentScope Provides]
    fun providePasswordView() = fragment as SettingsContract.Password.View

    @[FragmentScope Provides]
    fun providePasswordPresenter(view: SettingsContract.Password.View, service: VidiyoService)
            = ChangePasswordPresenter(view, service)

    @[FragmentScope Provides]
    fun provideNotificationsView() = fragment as SettingsContract.Notification.View

    @[FragmentScope Provides]
    fun provideNotificationsPresenter(view: SettingsContract.Notification.View, service: VidiyoService)
            = NotificationsPresenter(view, service)
}