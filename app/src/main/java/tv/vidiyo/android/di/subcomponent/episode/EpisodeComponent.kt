package tv.vidiyo.android.di.subcomponent.episode

import dagger.Subcomponent
import tv.vidiyo.android.ui.episode.EpisodeFragment
import javax.inject.Singleton

@[Singleton Subcomponent(modules = arrayOf(
        EpisodeModule::class
))]
interface EpisodeComponent {
    fun inject(fragment: EpisodeFragment)
    fun plus(module: ChildEpisodeModule): ChildEpisodeComponent
    fun plus(module: FullscreenEpisodeModule): FullscreenEpisodeComponent

    fun provideId(): Int
}