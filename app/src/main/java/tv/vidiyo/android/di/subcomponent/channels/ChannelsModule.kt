package tv.vidiyo.android.di.subcomponent.channels

import dagger.Module
import dagger.Provides
import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.base.PagesPresenter
import tv.vidiyo.android.di.FragmentModule
import tv.vidiyo.android.di.scope.FragmentScope
import tv.vidiyo.android.domain.contract.ChannelsContract
import tv.vidiyo.android.domain.contract.PagesPresentationView
import tv.vidiyo.android.domain.presenter.channels.BoardingChannelsPresenter
import tv.vidiyo.android.domain.presenter.channels.ChannelsPresenter
import tv.vidiyo.android.domain.presenter.channels.ChannelsProvider
import tv.vidiyo.android.source.channels.ChannelsRepository
import tv.vidiyo.android.ui.boarding.SelectChannelsFragment
import tv.vidiyo.android.ui.channels.ChannelsFragment
import tv.vidiyo.android.ui.channels.EditChannelsFragment
import tv.vidiyo.android.ui.channels.InnerChannelsFragment

@Module
class ChannelsModule : FragmentModule {

    constructor(fragment: SelectChannelsFragment) : super(fragment)
    constructor(fragment: InnerChannelsFragment) : super(fragment)
    constructor(fragment: ChannelsFragment) : super(fragment)
    constructor(fragment: EditChannelsFragment) : super(fragment)

    @[FragmentScope Provides]
    fun provideChannelsProvider(repository: ChannelsRepository) = ChannelsProvider(repository)

    @[FragmentScope Provides]
    fun provideView() = fragment as ChannelsContract.View

    @[FragmentScope Provides]
    fun provideBoardingView() = fragment as ChannelsContract.BoardingView

    @[FragmentScope Provides]
    fun providePresenter(view: ChannelsContract.View, repository: ChannelsRepository, provider: ChannelsProvider, service: VidiyoService)
            = ChannelsPresenter(view, repository, provider, service)

    @[FragmentScope Provides]
    fun provideBoardingPresenter(view: ChannelsContract.BoardingView, repository: ChannelsRepository, provider: ChannelsProvider, service: VidiyoService)
            = BoardingChannelsPresenter(view, repository, provider, service)

    @[FragmentScope Provides]
    fun providePagesView() = fragment as PagesPresentationView

    @[FragmentScope Provides]
    fun providePagesPresenter(view: PagesPresentationView, repository: ChannelsRepository) = PagesPresenter(view, repository)
}