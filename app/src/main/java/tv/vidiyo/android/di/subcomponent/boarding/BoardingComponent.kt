package tv.vidiyo.android.di.subcomponent.boarding

import dagger.Subcomponent
import tv.vidiyo.android.di.scope.FragmentScope
import tv.vidiyo.android.ui.boarding.CreateUsernameAndPasswordFragment
import tv.vidiyo.android.ui.boarding.RestoreFragment
import tv.vidiyo.android.ui.boarding.signin.SignInFragment
import tv.vidiyo.android.ui.boarding.signup.*

@[FragmentScope Subcomponent(modules = arrayOf(
        BoardingModule::class
))]
interface BoardingComponent {
    fun injectTo(fragment: SignInFragment)
    fun injectTo(fragment: CodeFragment)
    fun injectTo(fragment: NameFragment)
    fun injectTo(fragment: PhoneFragment)
    fun injectTo(fragment: EmailFragment)
    fun injectTo(fragment: SocialFragment)
    fun injectTo(fragment: CreateUsernameAndPasswordFragment)
    fun injectTo(fragment: RestoreFragment)
}