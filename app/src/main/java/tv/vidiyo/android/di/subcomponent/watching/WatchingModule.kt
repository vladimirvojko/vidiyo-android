package tv.vidiyo.android.di.subcomponent.watching

import dagger.Module
import dagger.Provides
import tv.vidiyo.android.di.FragmentModule
import tv.vidiyo.android.di.scope.FragmentScope
import tv.vidiyo.android.domain.contract.WatchingContract
import tv.vidiyo.android.domain.presenter.WatchingPresenter
import tv.vidiyo.android.source.watching.WatchingRepository
import tv.vidiyo.android.ui.watching.InnerWatchingFragment

@Module
class WatchingModule(fragment: InnerWatchingFragment) : FragmentModule(fragment) {

    @[FragmentScope Provides]
    fun provideView() = fragment as WatchingContract.Inner.View

    @[FragmentScope Provides]
    fun providePresenter(view: WatchingContract.Inner.View, repository: WatchingRepository) = WatchingPresenter(view, repository)
}