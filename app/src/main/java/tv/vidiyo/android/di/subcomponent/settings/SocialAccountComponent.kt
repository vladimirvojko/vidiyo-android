package tv.vidiyo.android.di.subcomponent.settings

import dagger.Subcomponent
import tv.vidiyo.android.di.scope.FragmentScope
import tv.vidiyo.android.ui.settings.SocialAccountsFragment

@[FragmentScope Subcomponent(modules = arrayOf(
        SocialAccountModule::class
))]
interface SocialAccountComponent {
    fun injectTo(fragment: SocialAccountsFragment)
}