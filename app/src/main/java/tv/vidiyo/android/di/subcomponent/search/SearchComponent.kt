package tv.vidiyo.android.di.subcomponent.search

import dagger.Subcomponent
import tv.vidiyo.android.di.scope.FragmentScope
import tv.vidiyo.android.ui.search.InnerSearchResultFragment
import tv.vidiyo.android.ui.search.SearchFragment
import tv.vidiyo.android.ui.search.SearchResultsFragment

@[FragmentScope Subcomponent(modules = arrayOf(
        SearchModule::class
))]
interface SearchComponent {
    fun injectTo(fragment: SearchFragment)
    fun injectTo(fragment: SearchResultsFragment)
    fun injectTo(fragment: InnerSearchResultFragment)
}