package tv.vidiyo.android.di.scope

import javax.inject.Scope

@Scope
annotation class ChildFragmentScope