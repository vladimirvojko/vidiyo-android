package tv.vidiyo.android.di.subcomponent.channels

import dagger.Subcomponent
import tv.vidiyo.android.di.scope.FragmentScope
import tv.vidiyo.android.ui.boarding.SelectChannelsFragment
import tv.vidiyo.android.ui.channels.ChannelsFragment
import tv.vidiyo.android.ui.channels.EditChannelsFragment
import tv.vidiyo.android.ui.channels.InnerChannelsFragment

@[FragmentScope Subcomponent(modules = arrayOf(
        ChannelsModule::class
))]
interface ChannelsComponent {
    fun injectTo(fragment: SelectChannelsFragment)
    fun injectTo(fragment: InnerChannelsFragment)
    fun injectTo(fragment: ChannelsFragment)
    fun injectTo(fragment: EditChannelsFragment)
}