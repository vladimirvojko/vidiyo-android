package tv.vidiyo.android.di.subcomponent

import android.location.LocationManager
import com.google.firebase.iid.FirebaseInstanceId
import dagger.Module
import dagger.Provides
import tv.vidiyo.android.App
import tv.vidiyo.android.account.UserAccountManager
import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.api.model.social.SocialAccount
import tv.vidiyo.android.api.model.user.UserAccount
import tv.vidiyo.android.source.channels.ChannelsRepository
import tv.vidiyo.android.source.home.HomeRepository
import tv.vidiyo.android.source.profile.ProfileRepository
import tv.vidiyo.android.source.search.SearchMainRepository
import tv.vidiyo.android.source.search.SearchResultsRepository
import tv.vidiyo.android.source.watching.WatchingRepository
import tv.vidiyo.android.util.LocationWatcher
import tv.vidiyo.android.util.NotificationsCenter
import tv.vidiyo.android.util.SearchManager
import javax.inject.Singleton

@Module
class DataModule {

    @[Provides Singleton]
    fun provideHomeRepository(service: VidiyoService) = HomeRepository(service)

    @[Provides Singleton]
    fun provideChannelsRepository(service: VidiyoService) = ChannelsRepository(service)

    @[Provides Singleton]
    fun provideSearchMainRepository(service: VidiyoService) = SearchMainRepository(service)

    @[Provides Singleton]
    fun provideSearchResultsRepository(service: VidiyoService, manager: SearchManager)
            = SearchResultsRepository(service, manager)

    @[Provides Singleton]
    fun provideWatchingRepository(service: VidiyoService, center: NotificationsCenter) = WatchingRepository(service, center)

    @[Provides Singleton]
    fun provideProfileRepository(manager: UserAccountManager, service: VidiyoService): ProfileRepository {
        val account = manager.account ?: throw IllegalArgumentException("No account")
        return ProfileRepository(account.id, service, manager).also { it.refresh() }
    }

    @[Provides Singleton]
    fun provideNotificationsCenter(service: VidiyoService) = NotificationsCenter(service).also {
        it.registerCloudMessaging(FirebaseInstanceId.getInstance().token)
        it.refresh()
    }

    @Provides
    fun provideUserAccount(repository: ProfileRepository): UserAccount? = repository.data

    @Provides
    fun provideSocials(repository: ProfileRepository): Array<SocialAccount>? = repository.data?.socials

    @[Singleton Provides]
    fun provideSearchManager(app: App) = SearchManager(app)

    @Provides
    fun provideLocationWatcher(manager: LocationManager, service: VidiyoService)
            = LocationWatcher(manager, service)
}