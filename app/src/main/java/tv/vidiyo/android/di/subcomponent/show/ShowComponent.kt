package tv.vidiyo.android.di.subcomponent.show

import dagger.Subcomponent
import tv.vidiyo.android.di.scope.FragmentScope
import tv.vidiyo.android.ui.show.ShowFragment

@[FragmentScope Subcomponent(modules = arrayOf(
        ShowModule::class
))]
interface ShowComponent {
    fun injectTo(fragment: ShowFragment)
    fun plus(module: ChildShowModule): ChildShowComponent
}