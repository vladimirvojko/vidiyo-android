package tv.vidiyo.android.di.subcomponent.episode

import android.support.v4.content.LocalBroadcastManager
import dagger.Module
import dagger.Provides
import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.di.FragmentModule
import tv.vidiyo.android.di.scope.FragmentScope
import tv.vidiyo.android.domain.contract.EpisodeContract
import tv.vidiyo.android.domain.presenter.episode.EpisodeInfoPresenter
import tv.vidiyo.android.domain.presenter.episode.EpisodeInnerPresenter
import tv.vidiyo.android.domain.presenter.episode.EpisodePlayerPresenter
import tv.vidiyo.android.source.episode.EpisodeRepository
import tv.vidiyo.android.ui.episode.EpisodeInfoFragment
import tv.vidiyo.android.ui.episode.EpisodePlayerFragment
import tv.vidiyo.android.ui.episode.InnerEpisodeInfoFragment
import tv.vidiyo.android.util.Notifier
import tv.vidiyo.android.util.SubscriptionManager

@Module
class ChildEpisodeModule : FragmentModule {

    constructor(fragment: EpisodePlayerFragment) : super(fragment)
    constructor(fragment: EpisodeInfoFragment) : super(fragment)
    constructor(fragment: InnerEpisodeInfoFragment) : super(fragment)

    @[Provides FragmentScope]
    fun providePlayerView() = fragment as EpisodeContract.Player.View

    @[Provides FragmentScope]
    fun provideInfoView() = fragment as EpisodeContract.Info.View

    @[Provides FragmentScope]
    fun provideInnerInfoView() = fragment as EpisodeContract.Inner.View

    @[Provides FragmentScope]
    fun provideNotifier(manager: LocalBroadcastManager): EpisodeContract.Notifier = Notifier(manager)

    @[Provides FragmentScope]
    fun providePlayerPresenter(view: EpisodeContract.Player.View, repository: EpisodeRepository,
                               service: VidiyoService, manager: SubscriptionManager,
                               notifier: EpisodeContract.Notifier)
            = EpisodePlayerPresenter(view, repository, service, manager, notifier)

    @[Provides FragmentScope]
    fun provideInfoPresenter(view: EpisodeContract.Info.View, repository: EpisodeRepository, manager: SubscriptionManager)
            = EpisodeInfoPresenter(view, repository, manager)

    @[Provides FragmentScope]
    fun provideInnerPresenter(view: EpisodeContract.Inner.View, repository: EpisodeRepository, service: VidiyoService,
                              manager: SubscriptionManager) = EpisodeInnerPresenter(view, repository, service, manager)
}