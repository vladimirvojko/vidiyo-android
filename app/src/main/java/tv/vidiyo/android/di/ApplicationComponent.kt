package tv.vidiyo.android.di

import android.support.v4.content.LocalBroadcastManager
import com.bumptech.glide.RequestManager
import dagger.Component
import tv.vidiyo.android.api.model.user.UserAccount
import tv.vidiyo.android.base.BaseActivity
import tv.vidiyo.android.di.subcomponent.*
import tv.vidiyo.android.di.subcomponent.boarding.BoardingComponent
import tv.vidiyo.android.di.subcomponent.boarding.BoardingModule
import tv.vidiyo.android.di.subcomponent.cast.CastComponent
import tv.vidiyo.android.di.subcomponent.cast.CastModule
import tv.vidiyo.android.di.subcomponent.channels.ChannelComponent
import tv.vidiyo.android.di.subcomponent.channels.ChannelModule
import tv.vidiyo.android.di.subcomponent.episode.EpisodeComponent
import tv.vidiyo.android.di.subcomponent.episode.EpisodeModule
import tv.vidiyo.android.di.subcomponent.launcher.LauncherComponent
import tv.vidiyo.android.di.subcomponent.launcher.LauncherModule
import tv.vidiyo.android.di.subcomponent.list.ListComponent
import tv.vidiyo.android.di.subcomponent.list.ListModule
import tv.vidiyo.android.di.subcomponent.profile.UserComponent
import tv.vidiyo.android.di.subcomponent.profile.UserModule
import tv.vidiyo.android.di.subcomponent.show.ShowComponent
import tv.vidiyo.android.di.subcomponent.show.ShowModule
import tv.vidiyo.android.di.subcomponent.topic.TopicComponent
import tv.vidiyo.android.di.subcomponent.topic.TopicModule
import tv.vidiyo.android.ui.dialog.WriteTextSheetDialog
import tv.vidiyo.android.util.SubscriptionManager
import javax.inject.Singleton

@[Singleton Component(modules = arrayOf(
        ApplicationModule::class,
        NetworkModule::class
))]
interface ApplicationComponent {
    fun plus(module: SplashModule): SplashComponent
    fun plus(module: BoardingModule): BoardingComponent
    fun plus(module: ShowModule): ShowComponent
    fun plus(module: ChannelModule): ChannelComponent
    fun plus(module: TopicModule): TopicComponent
    fun plus(module: CastModule): CastComponent
    fun plus(module: UserModule): UserComponent
    fun plus(module: ListModule): ListComponent
    fun plus(module: LauncherModule): LauncherComponent

    fun plus(module: StoredFriendsModule): StoredFriendsComponent
    fun plus(module: DataModule): DataComponent
    fun plus(module: EpisodeModule): EpisodeComponent

    fun inject(activity: BaseActivity)
    fun inject(dialog: WriteTextSheetDialog)

    fun provideGlide(): RequestManager
    fun provideLocalBroadcastManager(): LocalBroadcastManager
    fun provideUserAccount(): UserAccount?
    fun provideSubscriptionManager(): SubscriptionManager
}