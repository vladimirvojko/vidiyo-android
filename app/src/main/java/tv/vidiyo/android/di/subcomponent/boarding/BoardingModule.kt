package tv.vidiyo.android.di.subcomponent.boarding

import android.support.v7.app.AppCompatActivity
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.GoogleApiClient
import dagger.Module
import dagger.Provides
import tv.vidiyo.android.R
import tv.vidiyo.android.account.UserAccountManager
import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.di.FragmentModule
import tv.vidiyo.android.di.scope.FragmentScope
import tv.vidiyo.android.domain.contract.RestoreContract
import tv.vidiyo.android.domain.contract.SignInContract
import tv.vidiyo.android.domain.contract.SignUpContract.*
import tv.vidiyo.android.domain.presenter.boarding.*
import tv.vidiyo.android.ui.boarding.CreateUsernameAndPasswordFragment
import tv.vidiyo.android.ui.boarding.RestoreFragment
import tv.vidiyo.android.ui.boarding.signin.SignInFragment
import tv.vidiyo.android.ui.boarding.signup.*

@Module
class BoardingModule : FragmentModule {

    constructor(fragment: SignInFragment) : super(fragment)
    constructor(fragment: SocialFragment) : super(fragment)
    constructor(fragment: PhoneFragment) : super(fragment)
    constructor(fragment: EmailFragment) : super(fragment)
    constructor(fragment: NameFragment) : super(fragment)
    constructor(fragment: CodeFragment) : super(fragment)
    constructor(fragment: CreateUsernameAndPasswordFragment) : super(fragment)
    constructor(fragment: RestoreFragment) : super(fragment)

    @[Provides FragmentScope]
    fun provideGoogleApiClient(activity: AppCompatActivity): GoogleApiClient {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(activity.getString(R.string.default_web_client_id))
                .requestEmail()
                .build()

        val builder = GoogleApiClient.Builder(activity).addApi(Auth.GOOGLE_SIGN_IN_API, gso)
        if (fragment is GoogleApiClient.OnConnectionFailedListener) {
            builder.addOnConnectionFailedListener(fragment)
        }
        return builder.build()
    }

    //region Sign in
    @[Provides FragmentScope]
    fun provideSignInView(): SignInContract.View = fragment as SignInContract.View

    @[Provides FragmentScope]
    fun provideSocialView(): Social.View = fragment as Social.View

    @[Provides FragmentScope]
    fun provideSignInPresenter(view: SignInContract.View, service: VidiyoService, manager: UserAccountManager) = SignInPresenter(view, service, manager)

    @[Provides FragmentScope]
    fun provideSocialPresenter(view: Social.View, service: VidiyoService, manager: UserAccountManager) = SocialPresenter(view, service, manager)
    //endregion

    //region Phone
    @[Provides FragmentScope]
    fun providePhoneView(): Phone.View = fragment as Phone.View

    @[Provides FragmentScope]
    fun providePhonePresenter(view: Phone.View, service: VidiyoService) = PhonePresenter(view, service)
    //endregion

    //region Email
    @[Provides FragmentScope]
    fun provideView(): Email.View = fragment as Email.View

    @[Provides FragmentScope]
    fun provideEmailPresenter(view: Email.View, service: VidiyoService) = EmailPresenter(view, service)
    //endregion

    //region Code
    @[Provides FragmentScope]
    fun provideCodeView(): Code.View = fragment as Code.View

    @[Provides FragmentScope]
    fun provideCodePresenter(view: Code.View, service: VidiyoService) = CodePresenter(view, service)
    //endregion

    //region Username & Password
    @[Provides FragmentScope]
    fun provideUsernamePasswordView(): UsernameAndPassword.View = fragment as UsernameAndPassword.View

    @[Provides FragmentScope]
    fun provideUsernamePasswordPresenter(view: UsernameAndPassword.View, service: VidiyoService, manager: UserAccountManager)
            = CreateUsernameAndPasswordPresenter(view, service, manager)
    //endregion

    //region Restore
    @[Provides FragmentScope]
    fun proviewRestoreView() = fragment as RestoreContract.View

    @[Provides FragmentScope]
    fun provideRestorePresenter(view: RestoreContract.View, service: VidiyoService)
            = RestorePresenter(view, service)
    //endregion
}