package tv.vidiyo.android.di.subcomponent.profile

import dagger.Module
import dagger.Provides
import tv.vidiyo.android.account.UserAccountManager
import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.di.scope.FragmentScope
import tv.vidiyo.android.domain.contract.ProfileContract
import tv.vidiyo.android.domain.presenter.profile.InnerProfilePresenter
import tv.vidiyo.android.domain.presenter.profile.ProfilePresenter
import tv.vidiyo.android.source.profile.ProfileRepository
import tv.vidiyo.android.ui.profile.InnerProfileFragment
import tv.vidiyo.android.ui.profile.ProfileFragment
import tv.vidiyo.android.util.NotificationsCenter
import tv.vidiyo.android.util.SubscriptionManager

@Module
class ProfileModule : BaseProfileModule {
    constructor(fragment: ProfileFragment) : super(fragment)
    constructor(fragment: InnerProfileFragment) : super(fragment)

    @[FragmentScope Provides]
    fun providePresenter(view: ProfileContract.View, repository: ProfileRepository, service: VidiyoService,
                         manager: UserAccountManager, subscriptionManager: SubscriptionManager, center: NotificationsCenter)
            = ProfilePresenter(view, repository, service, manager, subscriptionManager, center)

    @[FragmentScope Provides]
    fun provideInnerView() = fragment as ProfileContract.Inner.View

    @[FragmentScope Provides]
    fun provideInnerPresenter(view: ProfileContract.Inner.View, repository: ProfileRepository,
                              manager: SubscriptionManager) = InnerProfilePresenter(view, repository, manager)
}