package tv.vidiyo.android.di.subcomponent.settings

import dagger.Subcomponent
import tv.vidiyo.android.di.scope.FragmentScope
import tv.vidiyo.android.ui.settings.EditProfileFragment
import tv.vidiyo.android.ui.settings.NotificationsFragment
import tv.vidiyo.android.ui.settings.PasswordFragment
import tv.vidiyo.android.ui.settings.SettingsFragment

@[FragmentScope Subcomponent(modules = arrayOf(
        SettingsModule::class
))]
interface SettingsComponent {
    fun injectTo(fragment: SettingsFragment)
    fun injectTo(fragment: EditProfileFragment)
    fun injectTo(fragment: PasswordFragment)
    fun injectTo(fragment: NotificationsFragment)
}