package tv.vidiyo.android.di.subcomponent.topic

import dagger.Module
import dagger.Provides
import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.api.model.topic.Topic
import tv.vidiyo.android.di.FragmentModule
import tv.vidiyo.android.di.scope.FragmentScope
import tv.vidiyo.android.domain.contract.TopicContract
import tv.vidiyo.android.domain.presenter.topic.TopicPresenter
import tv.vidiyo.android.source.topic.TopicRepository
import tv.vidiyo.android.ui.topic.TopicFragment
import tv.vidiyo.android.util.SubscriptionManager

@Module
class TopicModule(
        fragment: TopicFragment,
        private val topic: Topic
) : FragmentModule(fragment) {

    @[FragmentScope Provides]
    fun provideView() = fragment as TopicContract.View

    @[FragmentScope Provides]
    fun providePresenter(view: TopicContract.View, repository: TopicRepository,
                         manager: SubscriptionManager) = TopicPresenter(view, repository, manager)

    @[FragmentScope Provides]
    fun provideRepository(service: VidiyoService) = TopicRepository(topic, service)
}