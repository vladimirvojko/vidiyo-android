package tv.vidiyo.android.di.subcomponent.show

import dagger.Module
import dagger.Provides
import tv.vidiyo.android.base.fragment.BaseFragment
import tv.vidiyo.android.di.scope.ChildFragmentScope
import tv.vidiyo.android.domain.contract.ShowContract
import tv.vidiyo.android.domain.contract.SocialContract
import tv.vidiyo.android.domain.presenter.SocialFeedPresenter
import tv.vidiyo.android.domain.presenter.show.InnerShowPresenter
import tv.vidiyo.android.source.show.ShowRepository
import tv.vidiyo.android.source.social.SocialFeedRepository
import tv.vidiyo.android.util.SubscriptionManager

@Module
class ChildShowModule(private val fragment: BaseFragment) {

    @[ChildFragmentScope Provides]
    fun provideView(): ShowContract.Inner.View = fragment as ShowContract.Inner.View

    @[ChildFragmentScope Provides]
    fun providePresenter(view: ShowContract.Inner.View, repository: ShowRepository, manager: SubscriptionManager)
            = InnerShowPresenter(view, repository, manager)

    @[ChildFragmentScope Provides]
    fun provideSocialView(): SocialContract.View = fragment as SocialContract.View

    @[ChildFragmentScope Provides]
    fun provideSocialPresenter(view: SocialContract.View, repository: SocialFeedRepository)
            = SocialFeedPresenter(view, repository)
}