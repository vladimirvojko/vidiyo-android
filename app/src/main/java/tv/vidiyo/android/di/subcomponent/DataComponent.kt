package tv.vidiyo.android.di.subcomponent

import dagger.Subcomponent
import tv.vidiyo.android.di.subcomponent.channels.ChannelsComponent
import tv.vidiyo.android.di.subcomponent.channels.ChannelsModule
import tv.vidiyo.android.di.subcomponent.home.HomeComponent
import tv.vidiyo.android.di.subcomponent.home.HomeModule
import tv.vidiyo.android.di.subcomponent.profile.ProfileComponent
import tv.vidiyo.android.di.subcomponent.profile.ProfileModule
import tv.vidiyo.android.di.subcomponent.search.SearchComponent
import tv.vidiyo.android.di.subcomponent.search.SearchModule
import tv.vidiyo.android.di.subcomponent.settings.SettingsComponent
import tv.vidiyo.android.di.subcomponent.settings.SettingsModule
import tv.vidiyo.android.di.subcomponent.settings.UsersComponent
import tv.vidiyo.android.di.subcomponent.settings.UsersModule
import tv.vidiyo.android.di.subcomponent.watching.WatchingComponent
import tv.vidiyo.android.di.subcomponent.watching.WatchingModule
import tv.vidiyo.android.source.profile.ProfileRepository
import tv.vidiyo.android.ui.watching.WatchingFragment
import tv.vidiyo.android.util.NotificationsCenter
import javax.inject.Singleton

@[Singleton Subcomponent(modules = arrayOf(
        DataModule::class
))]
interface DataComponent {
    fun plus(module: HomeModule): HomeComponent
    fun plus(module: ChannelsModule): ChannelsComponent
    fun plus(module: SearchModule): SearchComponent
    fun plus(module: WatchingModule): WatchingComponent
    fun plus(module: ProfileModule): ProfileComponent
    fun plus(module: UsersModule): UsersComponent

    fun plus(module: SocialModule): SocialComponent
    fun plus(module: SettingsModule): SettingsComponent

    fun injectTo(fragment: WatchingFragment)

    fun provideProfileRepository(): ProfileRepository
    fun provideNotificationsCenter(): NotificationsCenter
}