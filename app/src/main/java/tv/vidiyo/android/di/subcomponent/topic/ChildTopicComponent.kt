package tv.vidiyo.android.di.subcomponent.topic

import dagger.Subcomponent
import tv.vidiyo.android.di.scope.ChildFragmentScope
import tv.vidiyo.android.ui.topic.InnerTopicFragment

@[ChildFragmentScope Subcomponent(modules = arrayOf(
        ChildTopicModule::class
))]
interface ChildTopicComponent {
    fun injectTo(fragment: InnerTopicFragment)
}