package tv.vidiyo.android.di.subcomponent

import dagger.Subcomponent
import tv.vidiyo.android.di.subcomponent.boarding.FriendsComponent
import tv.vidiyo.android.di.subcomponent.boarding.FriendsModule
import javax.inject.Singleton

@[Singleton Subcomponent(modules = arrayOf(
        StoredFriendsModule::class
))]
interface StoredFriendsComponent {
    fun plus(module: FriendsModule): FriendsComponent
}