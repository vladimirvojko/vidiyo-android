package tv.vidiyo.android.di.subcomponent.list

import dagger.Module
import dagger.Provides
import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.di.FragmentModule
import tv.vidiyo.android.di.scope.FragmentScope
import tv.vidiyo.android.domain.contract.ListContract
import tv.vidiyo.android.domain.presenter.ListPresenter
import tv.vidiyo.android.ui.ListFragment
import tv.vidiyo.android.util.SubscriptionManager

@Module
class ListModule(fragment: ListFragment) : FragmentModule(fragment) {

    @[FragmentScope Provides]
    fun provideView() = fragment as ListContract.View

    @[FragmentScope Provides]
    fun providePresenter(view: ListContract.View, service: VidiyoService, manager: SubscriptionManager)
            = ListPresenter(view, service, manager)
}