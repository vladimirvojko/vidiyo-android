package tv.vidiyo.android.di.subcomponent.cast

import dagger.Module
import dagger.Provides
import tv.vidiyo.android.base.fragment.BaseFragment
import tv.vidiyo.android.di.scope.ChildFragmentScope
import tv.vidiyo.android.domain.contract.CastContract
import tv.vidiyo.android.domain.contract.SocialContract
import tv.vidiyo.android.domain.presenter.SocialFeedPresenter
import tv.vidiyo.android.domain.presenter.cast.InnerCastPresenter
import tv.vidiyo.android.source.cast.CastRepository
import tv.vidiyo.android.source.social.SocialFeedRepository
import tv.vidiyo.android.util.SubscriptionManager

@Module
class ChildCastModule(private val fragment: BaseFragment) {

    @[ChildFragmentScope Provides]
    fun provideView(): CastContract.Inner.View = fragment as CastContract.Inner.View

    @[ChildFragmentScope Provides]
    fun providePresenter(view: CastContract.Inner.View, repository: CastRepository, manager: SubscriptionManager)
            = InnerCastPresenter(view, repository, manager)

    @[ChildFragmentScope Provides]
    fun provideSocialView(): SocialContract.View = fragment as SocialContract.View

    @[ChildFragmentScope Provides]
    fun provideSocialPresenter(view: SocialContract.View, repository: SocialFeedRepository)
            = SocialFeedPresenter(view, repository)
}