package tv.vidiyo.android.di.subcomponent.profile

import dagger.Subcomponent
import tv.vidiyo.android.di.scope.FragmentScope
import tv.vidiyo.android.ui.profile.InnerProfileFragment
import tv.vidiyo.android.ui.profile.ProfileFragment

@[FragmentScope Subcomponent(modules = arrayOf(
        ProfileModule::class
))]
interface ProfileComponent {
    fun injectTo(fragment: ProfileFragment)
    fun injectTo(fragment: InnerProfileFragment)
}