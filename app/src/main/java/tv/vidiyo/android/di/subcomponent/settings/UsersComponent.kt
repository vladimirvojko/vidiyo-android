package tv.vidiyo.android.di.subcomponent.settings

import dagger.Subcomponent
import tv.vidiyo.android.di.scope.FragmentScope
import tv.vidiyo.android.ui.settings.UsersFragment

@[FragmentScope Subcomponent(modules = arrayOf(
        UsersModule::class
))]
interface UsersComponent {
    fun injectTo(fragment: UsersFragment)
}