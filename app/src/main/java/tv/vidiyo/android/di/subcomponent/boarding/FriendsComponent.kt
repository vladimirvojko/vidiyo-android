package tv.vidiyo.android.di.subcomponent.boarding

import dagger.Subcomponent
import tv.vidiyo.android.di.scope.FragmentScope
import tv.vidiyo.android.ui.FindFriendsActivity
import tv.vidiyo.android.ui.boarding.FindFriendsFragment
import tv.vidiyo.android.ui.boarding.signup.FriendsListFragment

@[FragmentScope Subcomponent(modules = arrayOf(
        FriendsModule::class
))]
interface FriendsComponent {
    fun injectTo(activity: FindFriendsActivity)
    fun injectTo(fragment: FindFriendsFragment)
    fun injectTo(fragment: FriendsListFragment)
}