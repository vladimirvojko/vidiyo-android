package tv.vidiyo.android.di

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import dagger.Module
import dagger.Provides
import tv.vidiyo.android.di.scope.FragmentScope

@Module
abstract class FragmentModule(protected val fragment: Fragment) {

    @[Provides FragmentScope]
    fun provideActivity(): AppCompatActivity = fragment.activity as AppCompatActivity

    @[Provides FragmentScope]
    fun provideContext(): Context = fragment.context
}