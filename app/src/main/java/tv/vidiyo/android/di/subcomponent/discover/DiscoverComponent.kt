package tv.vidiyo.android.di.subcomponent.discover

import dagger.Subcomponent
import tv.vidiyo.android.di.scope.FragmentScope
import tv.vidiyo.android.ui.discover.DiscoverFragment
import tv.vidiyo.android.ui.discover.DiscoveredFragment

@[FragmentScope Subcomponent(modules = arrayOf(
        DiscoverModule::class
))]
interface DiscoverComponent {
    fun injectTo(fragment: DiscoverFragment)
    fun injectTo(fragment: DiscoveredFragment)
}