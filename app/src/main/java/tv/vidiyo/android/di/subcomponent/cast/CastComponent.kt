package tv.vidiyo.android.di.subcomponent.cast

import dagger.Subcomponent
import tv.vidiyo.android.di.scope.FragmentScope
import tv.vidiyo.android.ui.cast.CastFragment

@[FragmentScope Subcomponent(modules = arrayOf(
        CastModule::class
))]
interface CastComponent {
    fun injectTo(fragment: CastFragment)
    fun plus(module: ChildCastModule): ChildCastComponent
}