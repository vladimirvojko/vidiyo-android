package tv.vidiyo.android.di.subcomponent.episode

import dagger.Subcomponent
import tv.vidiyo.android.di.scope.FragmentScope
import tv.vidiyo.android.ui.episode.EpisodeInfoFragment
import tv.vidiyo.android.ui.episode.EpisodePlayerFragment
import tv.vidiyo.android.ui.episode.InnerEpisodeInfoFragment

@[FragmentScope Subcomponent(modules = arrayOf(
        ChildEpisodeModule::class
))]
interface ChildEpisodeComponent {
    fun injectTo(fragment: EpisodePlayerFragment)
    fun injectTo(fragment: EpisodeInfoFragment)
    fun injectTo(fragment: InnerEpisodeInfoFragment)
}