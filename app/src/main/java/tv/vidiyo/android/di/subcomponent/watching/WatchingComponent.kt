package tv.vidiyo.android.di.subcomponent.watching

import dagger.Subcomponent
import tv.vidiyo.android.di.scope.FragmentScope
import tv.vidiyo.android.ui.watching.InnerWatchingFragment

@[FragmentScope Subcomponent(modules = arrayOf(
        WatchingModule::class
))]
interface WatchingComponent {
    fun injectTo(fragment: InnerWatchingFragment)
}