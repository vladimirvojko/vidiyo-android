package tv.vidiyo.android.di.subcomponent.share

import dagger.Subcomponent
import tv.vidiyo.android.di.scope.FragmentScope
import tv.vidiyo.android.ui.dialog.ShareEpisodeSheetDialog

@[FragmentScope Subcomponent(modules = arrayOf(
        ShareModule::class
))]
interface ShareComponent {
    fun injectTo(dialog: ShareEpisodeSheetDialog)
}