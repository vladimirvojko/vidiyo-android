package tv.vidiyo.android.di.subcomponent.channels

import dagger.Module
import dagger.Provides
import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.api.model.channel.Channel
import tv.vidiyo.android.di.FragmentModule
import tv.vidiyo.android.di.scope.FragmentScope
import tv.vidiyo.android.domain.contract.ChannelContract
import tv.vidiyo.android.domain.presenter.channels.ChannelPresenter
import tv.vidiyo.android.source.channels.ChannelRepository
import tv.vidiyo.android.ui.channels.ChannelFragment

@Module
class ChannelModule(
        fragment: ChannelFragment,
        private val channel: Channel
) : FragmentModule(fragment) {

    @[FragmentScope Provides]
    fun provideView() = fragment as ChannelContract.View

    @[FragmentScope Provides]
    fun providePresenter(view: ChannelContract.View, repository: ChannelRepository)
            = ChannelPresenter(view, repository)

    @[FragmentScope Provides]
    fun provideRepository(service: VidiyoService) = ChannelRepository(channel, service)
}