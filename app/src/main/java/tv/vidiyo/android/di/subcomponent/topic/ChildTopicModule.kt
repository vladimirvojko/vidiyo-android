package tv.vidiyo.android.di.subcomponent.topic

import dagger.Module
import dagger.Provides
import tv.vidiyo.android.di.scope.ChildFragmentScope
import tv.vidiyo.android.domain.contract.TopicContract
import tv.vidiyo.android.domain.presenter.topic.InnerTopicPresenter
import tv.vidiyo.android.source.topic.TopicRepository
import tv.vidiyo.android.ui.topic.InnerTopicFragment
import tv.vidiyo.android.util.SubscriptionManager

@Module
class ChildTopicModule(private val fragment: InnerTopicFragment) {

    @[ChildFragmentScope Provides]
    fun provideView(): TopicContract.Inner.View = fragment

    @[ChildFragmentScope Provides]
    fun providePresenter(view: TopicContract.Inner.View, repository: TopicRepository, manager: SubscriptionManager)
            = InnerTopicPresenter(view, repository, manager)
}