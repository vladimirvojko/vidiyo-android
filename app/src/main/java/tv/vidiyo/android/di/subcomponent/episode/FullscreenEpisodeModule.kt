package tv.vidiyo.android.di.subcomponent.episode

import android.support.v4.content.LocalBroadcastManager
import dagger.Module
import dagger.Provides
import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.di.ActivityModule
import tv.vidiyo.android.di.scope.ActivityScope
import tv.vidiyo.android.domain.contract.EpisodeContract
import tv.vidiyo.android.domain.presenter.episode.EpisodePlayerPresenter
import tv.vidiyo.android.source.episode.EpisodeRepository
import tv.vidiyo.android.ui.FullscreenActivity
import tv.vidiyo.android.util.Notifier
import tv.vidiyo.android.util.SubscriptionManager

@Module
class FullscreenEpisodeModule(activity: FullscreenActivity) : ActivityModule(activity) {

    @[Provides ActivityScope]
    fun provideView() = activity as EpisodeContract.Player.View

    @[Provides ActivityScope]
    fun provideNotifier(manager: LocalBroadcastManager): EpisodeContract.Notifier = Notifier(manager)

    @[Provides ActivityScope]
    fun providePlayerPresenter(view: EpisodeContract.Player.View, repository: EpisodeRepository,
                               service: VidiyoService, manager: SubscriptionManager,
                               notifier: EpisodeContract.Notifier)
            = EpisodePlayerPresenter(view, repository, service, manager, notifier)
}