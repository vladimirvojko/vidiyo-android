package tv.vidiyo.android.base.adapter.delegate

import tv.vidiyo.android.base.adapter.provider.SectionedDataProvider
import tv.vidiyo.android.base.adapter.section.ItemPosition
import tv.vidiyo.android.base.view.holder.ViewHolder


abstract class SectionedAdapterDelegate<S, I, P: SectionedDataProvider<S, I>, L : ViewHolder.OnViewHolderClickListener, in V : ViewHolder<L>>(
        provider: P,
        listener: L?
) : ProviderAdapterDelegate<L, V, P>(provider, listener) {

    override fun isForViewType(position: Int, provider: P): Boolean {
        return isForViewType(provider.getItemPosition(position))
    }

    override fun onBindViewHolder(position: Int, holder: V, provider: P) {
        onBindViewHolder(provider.getItemPosition(position), holder, provider)
    }

    abstract fun onBindViewHolder(position: ItemPosition, holder: V, provider: P)

    abstract fun isForViewType(itemPosition: ItemPosition): Boolean
}