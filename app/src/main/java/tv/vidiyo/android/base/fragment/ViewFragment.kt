package tv.vidiyo.android.base.fragment

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import tv.vidiyo.android.base.BaseActivity
import tv.vidiyo.android.base.view.NeedRegistrationView
import tv.vidiyo.android.extension.startActivity
import tv.vidiyo.android.ui.boarding.BoardingActivity

abstract class ViewFragment : DaggerFragment(), NeedRegistrationView {
    abstract val layoutResId: Int
    var isViewCreated = false

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, b: Bundle?): View? {
        isViewCreated = false
        return inflater?.inflate(layoutResId, container, false)
    }

    override fun onResume() {
        super.onResume()
        if (!isViewCreated) {
            isViewCreated = true
            onPostViewCreated()
        }
    }

    open fun onPostViewCreated() {}

    open fun hideKeyboard(view: View?) = (context as? BaseActivity)?.hideKeyboard(view)
    open fun onBackPressed() {
        (context as? Activity)?.onBackPressed()
    }

    open fun startFragment(fragment: ViewFragment, addToBackStack: Boolean = true) {
        (context as? BaseActivity)?.startFragment(fragment, addToBackStack)
    }

    override fun openRegistration() {
        activity.startActivity<BoardingActivity>(BoardingActivity.EXTRA to BoardingActivity.SIGN_UP)
    }
}