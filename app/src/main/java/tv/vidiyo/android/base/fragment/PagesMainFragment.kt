package tv.vidiyo.android.base.fragment

import android.os.Bundle
import android.support.annotation.CallSuper
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.view.View
import tv.vidiyo.android.R
import tv.vidiyo.android.base.adapter.pages.PagesAdapter
import tv.vidiyo.android.base.view.HasPages
import tv.vidiyo.android.base.view.SelectableView
import tv.vidiyo.android.ui.MainFragment

abstract class PagesMainFragment : MainFragment(), HasPages {

    override val appBarLayoutResId = R.layout.app_bar_tabs
    override val contentLayoutResId = R.layout.view_pager

    open val viewPagerId: Int get() = R.id.view_pager
    open val tabLayoutId: Int? get() = R.id.tab_layout

    lateinit var viewPager: ViewPager
    var tabLayout: TabLayout? = null

    lateinit var adapter: PagesAdapter

    override fun onCreate(b: Bundle?) {
        super.onCreate(b)

        adapter = createAdapter()
    }

    abstract fun createAdapter(): PagesAdapter

    override fun onViewCreated(view: View, b: Bundle?) {
        super.onViewCreated(view, b)

        viewPager = view.findViewById(viewPagerId)
        tabLayout = tabLayoutId?.let { view.findViewById(it) }
        if (refreshLayout != null) {
            viewPager.addOnPageChangeListener(listener)
        }

        viewPager.adapter = adapter
        tabLayout?.let { onTabLayoutCreated(it) }
    }

    @CallSuper
    open fun onTabLayoutCreated(tabLayout: TabLayout) {
        tabLayout.setupWithViewPager(viewPager)
    }

    override fun refreshCurrentPage() {
        (adapter.getFragment(viewPager.currentItem) as? SelectableView)?.onSelected()
    }

    private val listener = object : ViewPager.OnPageChangeListener {
        override fun onPageScrollStateChanged(state: Int) {
            refreshLayout?.let {
                it.isEnabled = it.isRefreshing || state == ViewPager.SCROLL_STATE_IDLE && isRefreshable
            }
        }

        override fun onPageSelected(position: Int) {
            val fragment = adapter.getFragment(position) as? SelectableView ?: return
            fragment.onSelected()
        }

        override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
    }
}