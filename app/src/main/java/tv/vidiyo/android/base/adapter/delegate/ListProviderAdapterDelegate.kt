package tv.vidiyo.android.base.adapter.delegate

import tv.vidiyo.android.base.adapter.provider.ListDataProvider
import tv.vidiyo.android.base.view.holder.ViewHolder

abstract class ListProviderAdapterDelegate<T, L, in V : ViewHolder<L>>(
        provider: ListDataProvider<T>,
        listener: L?
) : ProviderAdapterDelegate<L, V, ListDataProvider<T>>(provider, listener) {

    override fun onBindViewHolder(position: Int, holder: V, provider: ListDataProvider<T>) {
        onBindViewHolder(position, holder, provider, provider.items?.get(position) ?: return)
    }

    abstract fun onBindViewHolder(position: Int, holder: V, provider: ListDataProvider<T>, item: T)

    override fun isForViewType(position: Int, provider: ListDataProvider<T>) = provider.getItemCount() != 0
}