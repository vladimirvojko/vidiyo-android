package tv.vidiyo.android.base

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.inputmethod.InputMethodManager
import tv.vidiyo.android.App
import tv.vidiyo.android.DependencyGraph
import tv.vidiyo.android.R
import tv.vidiyo.android.account.UserAccountManager
import tv.vidiyo.android.base.fragment.ViewFragment
import tv.vidiyo.android.extension.className
import tv.vidiyo.android.extension.style
import javax.inject.Inject

abstract class BaseActivity : AppCompatActivity() {

    open val fragmentContainerId: Int = 0
    var refreshLayout: SwipeRefreshLayout? = null

    @Inject lateinit var accountManager: UserAccountManager
    @Inject lateinit var inputMethodManager: InputMethodManager

    open fun injectDependencies(graph: DependencyGraph) {
        graph.applicationComponent.inject(this)
    }

    override fun onCreate(b: Bundle?) {
        super.onCreate(b)
        injectDependencies(App.graph)
    }

    open protected fun configureRefreshLayout() {
        refreshLayout = findViewById(R.id.refresh_layout)
        refreshLayout?.let {
            it.style()
            if (this@BaseActivity is SwipeRefreshLayout.OnRefreshListener) {
                it.setOnRefreshListener(this@BaseActivity)
            } else {
                it.isEnabled = false
            }
        }
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        configureRefreshLayout()
    }

    open fun startFragment(fragment: ViewFragment, addToBackStack: Boolean = true): Boolean {
        return try {
            startFragmentSimple(fragmentContainerId, fragment, addToBackStack)

        } catch (e: Throwable) {
            false
        }
    }

    open fun setRefreshing(block: Boolean) {
        refreshLayout?.isRefreshing = block
    }

    protected fun startFragmentSimple(containerId: Int, fragment: Fragment, addToBackStack: Boolean): Boolean {
        val manager = supportFragmentManager
        if (!addToBackStack) manager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)

        val transaction = manager.beginTransaction()
        val tag = fragment.className
        if (addToBackStack) {
            transaction.addToBackStack(tag)
        }

        transaction
                .replace(containerId, fragment, tag)
                .commit()

        return true
    }

    fun hideKeyboard(view: View?) {
        view?.let { inputMethodManager.hideSoftInputFromWindow(it.windowToken, 0) }
    }
}