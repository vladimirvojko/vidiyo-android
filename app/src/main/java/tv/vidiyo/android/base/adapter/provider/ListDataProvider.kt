package tv.vidiyo.android.base.adapter.provider

interface ListDataProvider<out T> : DataProvider {
    val items: List<T>?

    override fun getItemCount() = items?.size ?: 0
}