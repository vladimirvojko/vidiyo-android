package tv.vidiyo.android.base.fragment

import android.os.Bundle
import android.support.annotation.CallSuper
import android.support.design.widget.AppBarLayout
import android.support.v4.content.ContextCompat
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.view.View
import tv.vidiyo.android.R
import tv.vidiyo.android.base.BaseActivity
import tv.vidiyo.android.base.view.RefreshablePresentationView
import tv.vidiyo.android.extension.style


abstract class BaseToolbarFragment : ToolbarFragment(), Toolbar.OnMenuItemClickListener, RefreshablePresentationView {
    open val toolbarId: Int get() = R.id.toolbar
    open val refreshLayoutId: Int get() = R.id.refresh_layout

    var toolbar: Toolbar? = null
    var refreshLayout: SwipeRefreshLayout? = null
    protected var isRefreshable = false

    @CallSuper
    override fun onViewCreated(view: View, b: Bundle?) {
        super.onViewCreated(view, b)

        toolbar = view.findViewById(toolbarId)
        refreshLayout = view.findViewById(refreshLayoutId) ?: (context as? BaseActivity)?.refreshLayout

        toolbar?.let { onToolbarCreated(it) }
        refreshLayout?.apply {
            style()
            val height = resources.getDimensionPixelOffset(R.dimen.offset_refresh)
            setProgressViewOffset(false, -height, height)

            if (this@BaseToolbarFragment is SwipeRefreshLayout.OnRefreshListener) {
                isEnabled = true
                setOnRefreshListener(this@BaseToolbarFragment)
                view.findViewById<AppBarLayout>(R.id.app_bar).addOnOffsetChangedListener { _, verticalOffset ->
                    isRefreshable = verticalOffset == 0 || refreshLayout?.isRefreshing == true
                    refreshLayout?.isEnabled = isRefreshable
                }

            } else {
                isEnabled = false
                setOnRefreshListener(null)
            }
        }
    }

    @CallSuper
    protected open fun onToolbarCreated(toolbar: Toolbar) {
        toolbar.logo = toolbarLogo
        toolbar.title = toolbarTitle

        val navigationIcon = toolbarIndicator?.let { return@let ContextCompat.getDrawable(context, it) }
        if (navigationIcon != null) {
            toolbar.navigationIcon = navigationIcon
            toolbar.setNavigationOnClickListener { onNavigationIconPressed() }
        }

        val optionsMenu = optionsMenuResId
        if (optionsMenu != null) {
            toolbar.inflateMenu(optionsMenu)
            toolbar.setOnMenuItemClickListener(this)
        }
    }

    override fun onMenuItemClick(item: MenuItem) = true

    open fun onNavigationIconPressed() {
        onBackPressed()
    }

    override fun setRefreshing(block: Boolean) {
        refreshLayout?.isRefreshing = block
    }
}