package tv.vidiyo.android.base.adapter.provider

class FilterableDataProvider<T> : MutableListDataProvider<T>() {

    private var filtered: List<T>? = null
    override val items: List<T> get() = filtered ?: super.items

    fun filter(predicate: ((T) -> Boolean)?) {
        filtered = predicate?.let {
            mutableList.filter(it)
        }
    }

    fun remove(item: T) {
        mutableList.remove(item)
    }

    fun clearFilter() {
        filtered = null
    }
}