package tv.vidiyo.android.base

import tv.vidiyo.android.base.view.PresentationView
import tv.vidiyo.android.extension.weak
import java.lang.ref.WeakReference

abstract class BasePresenter<out V : PresentationView>(view: V) {

    private var viewRef: WeakReference<V>? = view.weak()
    protected val view: V? get() = viewRef?.get()

    open fun start() {}
    open fun onDestroy() {}
}