package tv.vidiyo.android.base.view

interface SelectableView {
    fun onSelected()
}