package tv.vidiyo.android.base.adapter.delegate

import android.support.v7.widget.RecyclerView
import tv.vidiyo.android.base.view.holder.ViewHolder
import tv.vidiyo.android.extension.weak
import java.lang.ref.WeakReference


abstract class ProviderAdapterDelegate<L, in V : ViewHolder<L>, P>(
        val provider: P,
        listener: L? = null
) : AdapterDelegate {

    protected val listener: WeakReference<L>? = listener?.weak()

    override final fun onBindViewHolder(position: Int, holder: RecyclerView.ViewHolder) {
        @Suppress("UNCHECKED_CAST")
        onBindViewHolder(position, holder as? V ?: return, provider)
    }

    abstract fun onBindViewHolder(position: Int, holder: V, provider: P)

    override final fun isForViewType(position: Int): Boolean {
        return isForViewType(position, provider)
    }

    open fun isForViewType(position: Int, provider: P): Boolean = true
}