package tv.vidiyo.android.base.fragment

import android.content.Context
import android.os.Bundle
import android.support.annotation.CallSuper
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import tv.vidiyo.android.R
import tv.vidiyo.android.base.adapter.RecyclerAdapter
import tv.vidiyo.android.base.view.UpdatablePresentationView
import tv.vidiyo.android.base.view.holder.ViewHolder
import tv.vidiyo.android.ui.MainFragment
import tv.vidiyo.android.ui.widget.InfiniteScrollRecyclerView


abstract class RecyclerMainFragment<A : RecyclerAdapter> : MainFragment(), UpdatablePresentationView, ViewHolder.OnViewHolderClickListener {
    override val contentLayoutResId get() = R.layout.recycler_view

    open val recyclerViewId: Int get() = R.id.recycler_view

    lateinit var recyclerView: InfiniteScrollRecyclerView
    lateinit var adapter: A

    override fun onCreate(b: Bundle?) {
        super.onCreate(b)
        adapter = onCreateAdapter()
    }

    abstract fun onCreateAdapter(): A

    override fun onViewCreated(view: View, b: Bundle?) {
        super.onViewCreated(view, b)

        recyclerView = view.findViewById(recyclerViewId)

        onRecyclerViewCreated(recyclerView)
    }

    @CallSuper
    protected open fun onRecyclerViewCreated(recyclerView: InfiniteScrollRecyclerView) {
        recyclerView.layoutManager = onCreateLayoutManager(context)
        recyclerView.adapter = adapter
    }

    protected open fun onCreateLayoutManager(context: Context?): RecyclerView.LayoutManager = LinearLayoutManager(context)

    //region UpdatablePresentationView

    override fun update() {
        adapter.notifyDataSetChanged()
    }

    //endregion
}