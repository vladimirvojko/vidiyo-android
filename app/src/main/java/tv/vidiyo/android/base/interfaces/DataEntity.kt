package tv.vidiyo.android.base.interfaces

interface DataEntity<out SELF, in DATA> {

    fun setData(data: DATA, vararg any: Any)
    val self: SELF
}