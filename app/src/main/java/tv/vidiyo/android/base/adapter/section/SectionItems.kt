package tv.vidiyo.android.base.adapter.section


data class SectionItems<S, I>(
        var section: Section<S> = Section(),
        val items: MutableList<I> = mutableListOf()
)