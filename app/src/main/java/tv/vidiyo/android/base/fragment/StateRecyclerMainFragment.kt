package tv.vidiyo.android.base.fragment

import android.os.Bundle
import android.view.View
import android.widget.Toast
import tv.vidiyo.android.R
import tv.vidiyo.android.base.adapter.RecyclerAdapter
import tv.vidiyo.android.base.view.StatePresentationView
import tv.vidiyo.android.extension.errorRetryView
import tv.vidiyo.android.ui.widget.StateLayout

abstract class StateRecyclerMainFragment<A : RecyclerAdapter> : RecyclerMainFragment<A>(), StatePresentationView {

    override val contentLayoutResId = R.layout.content_state_recycler

    lateinit var stateLayout: StateLayout

    protected val isRefreshing: Boolean
        get() = refreshLayout?.isRefreshing == true

    override fun onViewCreated(view: View, b: Bundle?) {
        super.onViewCreated(view, b)
        stateLayout = view.findViewById(R.id.state_layout)
    }

    //region StatePresentationView

    override fun showContent() = stateLayout.changeState(StateLayout.State.CONTENT)
    override fun showProgress() = stateLayout.changeState(StateLayout.State.PROGRESS)
    override fun showNoContent() = stateLayout.changeState(StateLayout.State.EMPTY)
    override fun showError(resId: Int) = showError(getString(resId), false)

    override fun showError(message: String?, asToast: Boolean) {
        if (asToast) {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
        } else {
            stateLayout.changeState(StateLayout.State.ERROR)
            stateLayout.errorRetryView?.setMessage(message)
        }
    }

    //endregion
}