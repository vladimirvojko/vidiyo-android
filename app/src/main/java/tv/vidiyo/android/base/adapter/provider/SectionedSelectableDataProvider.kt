package tv.vidiyo.android.base.adapter.provider

import tv.vidiyo.android.base.adapter.section.ItemPosition

class SectionedSelectableDataProvider<S, I> : SectionedListDataProvider<S, I>() {
    val selected = mutableSetOf<ItemPosition>()

    val selectedItems: List<I>
        get() {
            return selected.flatMap {
                val item = getItemForPosition(it)
                return if (item != null) listOf(item) else emptyList()
            }
        }

    fun toggleSelection(position: ItemPosition): Boolean {
        if (!position.isItem) return false

        return if (selected.contains(position)) {
            selected.remove(position)
            true
        } else {
            selected.add(position)
            false
        }
    }
}