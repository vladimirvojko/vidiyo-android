package tv.vidiyo.android.base.adapter.provider


interface DataProvider {
    fun getItemCount(): Int
}