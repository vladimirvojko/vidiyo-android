package tv.vidiyo.android.base

import android.content.Context
import android.graphics.Color
import android.graphics.Point
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.annotation.CallSuper
import android.support.v4.app.DialogFragment
import android.support.v4.app.FragmentManager
import android.view.*
import tv.vidiyo.android.R

abstract class BaseDialog : DialogFragment() {

    protected open val isFullScreen = false
    protected open val themeRes: Int = R.style.AppTheme

    abstract val layoutRes: Int

    override fun onCreate(b: Bundle?) {
        super.onCreate(b)
        super.setStyle(DialogFragment.STYLE_NORMAL, themeRes)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val window = dialog.window
        window?.attributes?.windowAnimations = R.style.AppTheme_DialogAnimation

        if (isFullScreen && window != null) {
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, b: Bundle?): View? {
        val v = inflater?.inflate(layoutRes, container, false)
        if (isFullScreen) {
            val screenSize = Point()

            val windowManager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
            windowManager.defaultDisplay.getSize(screenSize)

            v?.apply {
                minimumWidth = screenSize.x
                minimumHeight = screenSize.y
            }
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        }
        return v
    }

    override fun onDestroyView() {
        super.onDestroyView()
        try {
            onCleanUp()

        } catch (ignore: Exception) {
        } finally {
            System.gc()
        }
    }

    @Throws(Exception::class)
    protected abstract fun onCleanUp()

    @CallSuper
    fun show(manager: FragmentManager): Boolean {
        val tag = javaClass.name

        if (manager.findFragmentByTag(tag) == null) {
            super.show(manager, tag)
            return true
        }
        return false
    }
}