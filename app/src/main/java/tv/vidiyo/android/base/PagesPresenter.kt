package tv.vidiyo.android.base

import tv.vidiyo.android.domain.contract.PagesPresentationView
import tv.vidiyo.android.domain.presenter.DataSourcePresenter
import tv.vidiyo.android.source.DataSource

class PagesPresenter(v: PagesPresentationView, repository: DataSource<*>)
    : DataSourcePresenter<PagesPresentationView, DataSource<*>>(v, repository) {

    init {
        repository.subscribe(this)
    }
}