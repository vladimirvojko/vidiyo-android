package tv.vidiyo.android.base.fragment

import android.os.Bundle
import android.view.View
import android.widget.Toast
import tv.vidiyo.android.R
import tv.vidiyo.android.base.adapter.RecyclerAdapter
import tv.vidiyo.android.base.view.StatePresentationView
import tv.vidiyo.android.extension.errorRetryView
import tv.vidiyo.android.ui.view.ErrorRetryView
import tv.vidiyo.android.ui.widget.StateLayout


abstract class StateRecyclerFragment<A : RecyclerAdapter> : BaseRecyclerViewFragment<A>(),
        StatePresentationView, ErrorRetryView.RetryListener {

    override val layoutResId = R.layout.fragment_state_recycler_empty
    open val stateLayoutId get() = R.id.state_layout

    lateinit var stateLayout: StateLayout

    override fun onViewCreated(view: View, b: Bundle?) {
        super.onViewCreated(view, b)

        stateLayout = view.findViewById(stateLayoutId)
        stateLayout.errorRetryView?.setRetryListener(this)
    }

    override fun retry() = Unit

    // region StatePresentationView

    override fun showProgress() {
        stateLayout.changeState(StateLayout.State.PROGRESS)
    }

    override fun showContent() {
        stateLayout.changeState(StateLayout.State.CONTENT)
    }

    override fun showError(message: String?, asToast: Boolean) {
        if (asToast) {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
        } else {
            stateLayout.changeState(StateLayout.State.ERROR)
            stateLayout.errorRetryView?.setMessage(message)
        }
    }

    override fun showError(resId: Int) = showError(getString(resId), false)

    override fun showNoContent() {
        stateLayout.changeState(StateLayout.State.EMPTY)
    }

    // endregion StatePresentationView
}