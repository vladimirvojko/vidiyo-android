package tv.vidiyo.android.base.view

interface UpdatablePresentationView {
    fun update()
}