package tv.vidiyo.android.base.fragment

import android.graphics.drawable.Drawable
import android.support.v4.content.ContextCompat

abstract class ToolbarFragment : ViewFragment() {
    open val toolbarTitleRes: Int? get() = null
    open val toolbarTitle: CharSequence? get() = toolbarTitleRes?.let { return@let getText(it) }

    open val toolbarLogoRes: Int? get() = null
    open val toolbarLogo: Drawable? get() = toolbarLogoRes?.let { return@let ContextCompat.getDrawable(context, it) }

    open val toolbarIndicator: Int? get() = null

    open val optionsMenuResId: Int? get() = null
}