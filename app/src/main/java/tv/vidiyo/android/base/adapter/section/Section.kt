package tv.vidiyo.android.base.adapter.section


data class Section<out T>(
        val section: T? = null,
        var hasHeader: Boolean = false,
        var hasFooter: Boolean = false
)