package tv.vidiyo.android.base.adapter.section


data class ItemPosition(val section: Int, val position: Int) {
    val isItem: Boolean get() = position >= 0
    val isHeader: Boolean get() = position == POSITION_HEADER
    val isFooter: Boolean get() = position == POSITION_FOOTER

    companion object {
        val POSITION_HEADER = -1
        val POSITION_FOOTER = -2
    }

    override fun toString(): String {
        val position = when (position) {
            POSITION_HEADER -> "HEADER"
            POSITION_FOOTER -> "FOOTER"
            else -> "position=$position"
        }

        return "ItemPosition(section=$section, $position)"
    }
}