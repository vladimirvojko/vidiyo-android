package tv.vidiyo.android.base.adapter.provider

import tv.vidiyo.android.base.adapter.section.ItemPosition
import tv.vidiyo.android.base.adapter.section.Section
import tv.vidiyo.android.base.adapter.section.SectionItems

interface SectionedDataProvider<S, T> : DataProvider {
    fun getSectionsCount(): Int
    fun getItemsCountInSection(section: Int = 0): Int

    fun hasHeaderForSection(section: Int = 0): Boolean
    fun hasFooterForSection(section: Int = 0): Boolean

    fun getItemPosition(absolutePosition: Int): ItemPosition

    fun getSection(section: Int = 0): Section<S>?
    fun setSection(index: Int = 0, section: Section<S>)
    fun insertSection(index: Int = 0, section: Section<S>)

    fun getSectionItems(section: Int = 0): SectionItems<S, T>?
    fun setSectionItems(section: Int = 0, items: List<T>)

    fun add(section: Section<S>, items: MutableList<T>)

    fun getItemForPosition(position: ItemPosition): T?

    override fun getItemCount(): Int {
        var count = 0
        for (section in 0 until getSectionsCount()) {
            count += getItemsCountInSection(section)

            if (hasHeaderForSection(section)) count++
            if (hasFooterForSection(section)) count++
        }
        return count
    }
}