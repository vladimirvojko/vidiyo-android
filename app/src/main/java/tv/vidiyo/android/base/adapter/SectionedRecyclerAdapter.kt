package tv.vidiyo.android.base.adapter

import tv.vidiyo.android.base.adapter.delegate.AdapterDelegate
import tv.vidiyo.android.base.adapter.provider.SectionedDataProvider
import tv.vidiyo.android.base.adapter.provider.SectionedListDataProvider

open class SectionedRecyclerAdapter<S, I>(
        delegates: List<AdapterDelegate>,
        val provider: SectionedDataProvider<S, I> = SectionedListDataProvider()
) : RecyclerAdapter(delegates) {

    override fun getTotalItemCount(): Int {
        var count = 0
        for (section in 0 until provider.getSectionsCount()) {
            count += provider.getItemsCountInSection(section)

            if (provider.hasHeaderForSection(section)) count++
            if (provider.hasFooterForSection(section)) count++
        }
        return count
    }
}