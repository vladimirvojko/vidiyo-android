package tv.vidiyo.android.base.view


interface RefreshablePresentationView {
    fun setRefreshing(block: Boolean)
}