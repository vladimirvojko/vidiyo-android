package tv.vidiyo.android.base.fragment

import android.os.Bundle
import android.support.annotation.CallSuper
import android.support.annotation.DrawableRes
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.ActionBar
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import tv.vidiyo.android.R
import tv.vidiyo.android.base.BaseActivity
import tv.vidiyo.android.base.view.RefreshablePresentationView
import tv.vidiyo.android.base.view.ShowMessagePresentationView
import tv.vidiyo.android.base.view.ShowMessagePresentationView.ToastMessage
import tv.vidiyo.android.extension.style
import tv.vidiyo.android.ui.MainActivity

abstract class BaseFragment : ToolbarFragment(), RefreshablePresentationView, ShowMessagePresentationView {
    protected var refreshLayout: SwipeRefreshLayout? = null
    protected val mainActivity: MainActivity? get() = activity as? MainActivity

    override fun onCreate(b: Bundle?) {
        super.onCreate(b)
        super.setHasOptionsMenu(true)
    }

    override fun onStart() {
        super.onStart()
        val bar = (context as? AppCompatActivity)?.supportActionBar

        if (bar != null) {
            onActionBarReady(bar)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(optionsMenuResId ?: return, menu)
    }

    override fun onViewCreated(view: View, b: Bundle?) {
        super.onViewCreated(view, b)
        refreshLayout = view.findViewById(R.id.refresh_layout)
                ?: (context as? BaseActivity)?.refreshLayout

        refreshLayout?.let {
            it.style()

            if (this@BaseFragment is SwipeRefreshLayout.OnRefreshListener) {
                it.isEnabled = true
                it.setOnRefreshListener(this@BaseFragment)
            } else {
                it.isEnabled = false
                it.setOnRefreshListener(null)
            }
        } ?: (parentFragment as? BaseToolbarFragment)?.refreshLayout?.also {
            refreshLayout = it
        }
    }

    @CallSuper
    open fun onActionBarReady(bar: ActionBar) {
        toolbarTitle?.let { bar.title = it }

        @DrawableRes val indicator = toolbarIndicator
        if (indicator == null) {
            bar.setDisplayHomeAsUpEnabled(false)
            bar.setDisplayShowHomeEnabled(false)

        } else {
            bar.setDisplayHomeAsUpEnabled(true)
            bar.setDisplayShowHomeEnabled(true)
            bar.setHomeAsUpIndicator(indicator)
        }
    }

    protected fun processError(text: String?, code: Int) {
        Toast.makeText(context, text ?: getString(code), Toast.LENGTH_SHORT).show()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onHomePressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    open fun onHomePressed() = onBackPressed()

    override fun setRefreshing(block: Boolean) {
        refreshLayout?.let {
            it.isRefreshing = block
        } ?: (context as? BaseActivity)?.setRefreshing(block)
    }

    override fun showMessage(message: ShowMessagePresentationView.Message) {
        when (message) {
            is ToastMessage -> message.apply {
                Toast.makeText(context, resId?.let { context.getString(it) } ?: text, duration).show()
            }
        }
    }
}