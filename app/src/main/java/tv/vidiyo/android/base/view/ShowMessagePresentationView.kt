package tv.vidiyo.android.base.view

import android.widget.Toast
import tv.vidiyo.android.util.Constant


interface ShowMessagePresentationView {

    fun showMessage(message: Message)

    interface Message {
        val text: String
    }

    class ToastMessage private constructor(val resId: Int?, text: String?, val duration: Int) : Message {
        constructor(text: String?, duration: Int = Toast.LENGTH_LONG) : this(null, text, duration)
        constructor(resId: Int?, duration: Int = Toast.LENGTH_LONG) : this(resId, null, duration)

        override val text: String = text ?: Constant.EMPTY
    }
}