package tv.vidiyo.android.base.adapter

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import tv.vidiyo.android.base.adapter.delegate.AdapterDelegate
import tv.vidiyo.android.base.adapter.delegate.AdapterDelegatesManager
import tv.vidiyo.android.ui.widget.InfiniteScrollRecyclerView


abstract class RecyclerAdapter(
        delegates: List<AdapterDelegate>
) : InfiniteScrollRecyclerView.Companion.Adapter() {

    open val delegatesManager: AdapterDelegatesManager = AdapterDelegatesManager(delegates)

    override fun onCreateItemViewHolder(parent: ViewGroup, viewType: Int)
            = delegatesManager.onCreateViewHolder(parent, viewType)

    override fun onBindItemViewHolder(holder: RecyclerView.ViewHolder, position: Int)
            = delegatesManager.onBindViewHolder(holder, position)

    override fun getViewTypeForItem(position: Int) = delegatesManager.getItemViewType(position)
}