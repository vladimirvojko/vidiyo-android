package tv.vidiyo.android.base.fragment

import android.content.Context
import android.support.v4.app.Fragment
import tv.vidiyo.android.App
import tv.vidiyo.android.DependencyGraph


abstract class DaggerFragment : Fragment() {

    override fun onAttach(context: Context?) {
        injectDependencies(App.graph)
        super.onAttach(context)
    }

    protected open fun injectDependencies(graph: DependencyGraph) {
    }
}