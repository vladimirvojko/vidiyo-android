package tv.vidiyo.android.base.view

import android.support.annotation.StringRes


interface StatePresentationView : PresentationView {
    fun showProgress()
    fun showContent()
    fun showError(message: String? = null, asToast: Boolean = false)
    fun showError(@StringRes resId: Int)
    fun showNoContent()
}