package tv.vidiyo.android.base.interfaces

interface OnBackPressedListener {
    fun onBackPressed()
}