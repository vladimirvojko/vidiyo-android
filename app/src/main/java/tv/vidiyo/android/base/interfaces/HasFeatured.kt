package tv.vidiyo.android.base.interfaces

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import tv.vidiyo.android.base.fragment.ViewFragment
import tv.vidiyo.android.ui.view.FeaturedVideoView
import tv.vidiyo.android.util.Constant

interface HasFeatured {
    val featuredVideoView: FeaturedVideoView
    fun additionAction()
}

fun <T> T.newReceiver(): BroadcastReceiver where T : ViewFragment, T : HasFeatured = object : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        when (intent?.action) {
            Constant.ACTION_MAXIMIZED -> {
                additionAction()
                featuredVideoView.isMuted = true
                featuredVideoView.player.playWhenReady = false
            }

            Constant.ACTION_MINIMIZED -> featuredVideoView.player.playWhenReady = true
        }
    }
}