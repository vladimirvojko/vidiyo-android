package tv.vidiyo.android.base.fragment

import android.support.v7.widget.RecyclerView
import tv.vidiyo.android.base.adapter.SectionedRecyclerAdapter
import tv.vidiyo.android.base.adapter.section.ItemPosition


abstract class SectionedBaseRecyclerViewFragment<S, I> : BaseRecyclerViewFragment<SectionedRecyclerAdapter<S, I>>() {
    override fun onViewHolderClick(viewHolder: RecyclerView.ViewHolder, position: Int) {
        onViewHolderClick(viewHolder, adapter.provider.getItemPosition(position))
    }

    abstract fun onViewHolderClick(viewHolder: RecyclerView.ViewHolder, position: ItemPosition)
}