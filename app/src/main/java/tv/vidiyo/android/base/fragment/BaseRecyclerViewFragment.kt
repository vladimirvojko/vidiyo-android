package tv.vidiyo.android.base.fragment

import android.content.Context
import android.os.Bundle
import android.support.annotation.CallSuper
import android.support.annotation.IdRes
import android.support.annotation.LayoutRes
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView.LayoutManager
import android.view.View
import tv.vidiyo.android.R
import tv.vidiyo.android.base.adapter.RecyclerAdapter
import tv.vidiyo.android.base.view.UpdatablePresentationView
import tv.vidiyo.android.base.view.holder.ViewHolder
import tv.vidiyo.android.ui.widget.InfiniteScrollRecyclerView

abstract class BaseRecyclerViewFragment<A : RecyclerAdapter> : BaseFragment(), UpdatablePresentationView, ViewHolder.OnViewHolderClickListener {

    @LayoutRes override val layoutResId: Int = R.layout.recycler_view
    @IdRes open val recyclerViewId: Int = R.id.recycler_view

    lateinit var recyclerView: InfiniteScrollRecyclerView
    lateinit var adapter: A

    override fun onCreate(b: Bundle?) {
        super.onCreate(b)
        adapter = onCreateAdapter()
    }

    abstract fun onCreateAdapter(): A

    override fun onViewCreated(view: View, b: Bundle?) {
        super.onViewCreated(view, b)

        recyclerView = view.findViewById(recyclerViewId)

        onRecyclerViewCreated(recyclerView)
    }

    @CallSuper
    protected open fun onRecyclerViewCreated(recyclerView: InfiniteScrollRecyclerView) {
        recyclerView.layoutManager = onCreateLayoutManager(context)
        recyclerView.adapter = adapter
    }

    protected open fun onCreateLayoutManager(context: Context?): LayoutManager = LinearLayoutManager(context)

    //region UpdatablePresentationView

    override fun update() {
        adapter.notifyDataSetChanged()
        recyclerView.finishLoading()
    }

    //endregion
}