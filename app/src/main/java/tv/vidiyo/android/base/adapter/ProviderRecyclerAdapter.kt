package tv.vidiyo.android.base.adapter

import tv.vidiyo.android.base.adapter.delegate.AdapterDelegate
import tv.vidiyo.android.base.adapter.provider.DataProvider


open class ProviderRecyclerAdapter<out T : DataProvider>(
        val provider: T,
        delegates: List<AdapterDelegate>
) : RecyclerAdapter(delegates) {

    override fun getTotalItemCount() = provider.getItemCount()
}