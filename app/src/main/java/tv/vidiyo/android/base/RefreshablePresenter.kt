package tv.vidiyo.android.base

interface RefreshablePresenter {
    fun refresh()
}