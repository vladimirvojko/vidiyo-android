package tv.vidiyo.android.base.view

interface HasPages {
    fun refreshCurrentPage()
}