package tv.vidiyo.android.base.view

interface NeedRegistrationView {
    fun openRegistration()
}