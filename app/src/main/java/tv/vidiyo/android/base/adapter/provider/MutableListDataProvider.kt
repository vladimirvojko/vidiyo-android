package tv.vidiyo.android.base.adapter.provider

open class MutableListDataProvider<T> : ListDataProvider<T> {

    val mutableList: MutableList<T> = mutableListOf()
    override val items: List<T> get() = mutableList

    fun addAll(items: Collection<T>, clear: Boolean = false) {
        if (clear) {
            mutableList.clear()
        }
        mutableList.addAll(items)
    }

    fun clear() = mutableList.clear()
    override fun getItemCount() = items.size
}