package tv.vidiyo.android.domain.contract

import tv.vidiyo.android.api.model.show.Show
import tv.vidiyo.android.api.model.topic.Topic


interface CoverView {
    fun setImage(imageUrl: String?)
    fun setTitle(title: String)
}

fun CoverView.setShow(show: Show) {
    setImage(show.image)
    setTitle(show.name)
}

fun CoverView.setTopic(topic: Topic) {
    setImage(topic.image)
    setTitle(topic.name)
}