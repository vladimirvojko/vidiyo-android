package tv.vidiyo.android.domain.contract

import tv.vidiyo.android.base.view.PresentationView
import tv.vidiyo.android.base.view.RefreshablePresentationView
import tv.vidiyo.android.base.view.ShowMessagePresentationView

interface RestoreContract {

    interface View : PresentationView, ShowMessagePresentationView, RefreshablePresentationView

    interface Presenter {
        fun restore(username: String)
        fun validate(username: String?): Boolean
    }
}