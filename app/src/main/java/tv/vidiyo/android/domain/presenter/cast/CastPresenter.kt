package tv.vidiyo.android.domain.presenter.cast

import tv.vidiyo.android.api.model.Message
import tv.vidiyo.android.base.BasePresenter
import tv.vidiyo.android.base.view.ShowMessagePresentationView
import tv.vidiyo.android.domain.contract.CastContract
import tv.vidiyo.android.extension.accountIsAnonumous
import tv.vidiyo.android.source.BaseDataSource
import tv.vidiyo.android.source.cast.CastRepository
import tv.vidiyo.android.util.SubscriptionManager

class CastPresenter(
        v: CastContract.View,
        val repository: CastRepository,
        private val manager: SubscriptionManager
) : BasePresenter<CastContract.View>(v), CastContract.Presenter, BaseDataSource.Callback {

    init {
        repository.subscribe(this)
    }

    override fun start() {
        if (repository.isEmpty) {
            repository.refresh()

        } else {
            onDataLoaded()
        }
    }

    override fun refresh() {
        repository.refresh()
        repository.getAll().map { it.shouldRefresh = true }
        view?.refreshCurrentPage()
    }

    override fun toggleSubscription() {
        val cast = repository.data ?: return
        if (accountIsAnonumous(view, manager)) return

        view?.setRefreshing(true)
        manager.setSubscription(cast, !manager.isSubscribed(cast)) { _, subscribed, error ->
            view?.setRefreshing(false)
            if (error != null) {
                view?.showMessage(ShowMessagePresentationView.ToastMessage(error))
            } else {
                cast.isSubscribed = subscribed
                view?.updateCastInfo(cast)
            }
        }
    }

    override fun onDataLoaded() {
        view?.setRefreshing(false)
        val info = repository.data ?: return
        view?.updateCastInfo(info)
    }

    override fun onDataNotAvailable(error: Message) {
        // TODO
    }
}