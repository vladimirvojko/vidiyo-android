package tv.vidiyo.android.domain.presenter.profile

import tv.vidiyo.android.api.model.SimpleSubscription
import tv.vidiyo.android.api.model.Subscribable
import tv.vidiyo.android.api.model.channel.Channel
import tv.vidiyo.android.api.model.channel.ChannelItems
import tv.vidiyo.android.api.model.episode.Episode
import tv.vidiyo.android.base.adapter.provider.MutableListDataProvider
import tv.vidiyo.android.base.adapter.provider.SectionedListDataProvider
import tv.vidiyo.android.domain.contract.ProfileContract
import tv.vidiyo.android.domain.presenter.DataSourcePresenter
import tv.vidiyo.android.extension.accountIsAnonumous
import tv.vidiyo.android.extension.toggleSubscriptionInternal
import tv.vidiyo.android.model.InnerPageType
import tv.vidiyo.android.source.profile.InnerProfileRepository
import tv.vidiyo.android.source.profile.ProfileRepository
import tv.vidiyo.android.util.SubscriptionManager

class InnerProfilePresenter(
        v: ProfileContract.Inner.View,
        override val main: ProfileRepository,
        val manager: SubscriptionManager
) : DataSourcePresenter<ProfileContract.Inner.View, InnerProfileRepository<*, *>>(v, main.get(v.type)), ProfileContract.Inner.Presenter {

    private val type = v.type

    init {
        repository.subscribe(this)
    }

    override fun start() {
        if (repository.isEmpty || isDirtyData(manager.getSetFor(type))) {
            repository.refresh()

        } else {
            onDataLoaded()
        }
    }

    override fun refresh() {
        if (repository.shouldRefresh || repository.isEmpty) {
            super.refresh()
        }
    }

    private fun isDirtyData(set: MutableSet<SimpleSubscription>): Boolean {
        val data = repository.data ?: return true
        when (type) {
            InnerPageType.EPISODES -> {
                val items = data.map { it as Episode }
                set
                        .map { (id) -> items.find { it.id == id } != null }
                        .filterNot { it }
                        .forEach { return true }
            }
            else -> {
                val items = data.map { it as ChannelItems<*> }
                        .flatMap { it.items.map { it as Subscribable } }
                set
                        .map { (id) -> items.find { it.id == id } != null }
                        .filterNot { it }
                        .forEach { return true }
            }
        }
        return false
    }

    @Suppress("UNCHECKED_CAST")
    override fun <T> get(at: Int): T? {
        val provider = repository.provider
        return when (provider) {
            is SectionedListDataProvider<*, *> -> provider.getItemForPosition(provider.getItemPosition(at)) as? T
            is MutableListDataProvider<*> -> provider.items[at] as? T
            else -> null
        }
    }

    override fun getSection(at: Int): Channel? {
        val provider = repository.provider
        return when (provider) {
            is SectionedListDataProvider<*, *> -> provider.getSection(provider.getItemPosition(at).section)?.section as? Channel
            else -> null
        }
    }

    override fun toggleSubscription(at: Int) {
        val item = get<Subscribable>(at) ?: return
        if (accountIsAnonumous(view, manager)) return

        toggleSubscriptionInternal(view, item, manager)
    }

    override fun onDataLoaded() {
        view?.let {
            if (repository.data?.size == 0) {
                it.showNoContent()
            } else {
                it.showContent()
            }
            it.update()
        }
    }
}