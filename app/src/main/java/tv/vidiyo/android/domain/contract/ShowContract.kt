package tv.vidiyo.android.domain.contract

import tv.vidiyo.android.api.model.show.Show
import tv.vidiyo.android.api.model.show.ShowInfo
import tv.vidiyo.android.base.RefreshablePresenter
import tv.vidiyo.android.base.adapter.provider.ListDataProvider
import tv.vidiyo.android.base.view.*
import tv.vidiyo.android.source.show.ShowRepository

interface ShowContract {

    interface View : PresentationView, RefreshablePresentationView, ShowMessagePresentationView,
            NeedRegistrationView, HasPages {
        fun updateShowView(show: Show)
        fun updateShowInfo(info: ShowInfo)
    }

    interface Presenter : RefreshablePresenter {
        var show: Show?
        fun toggleSubscription()
    }

    interface Inner {
        interface View : StatePresentationView, UpdatablePresentationView, RefreshablePresentationView,
                ShowMessagePresentationView, NeedRegistrationView, SelectableView {
            var type: Int
        }

        interface Presenter : RefreshablePresenter {
            val main: ShowRepository // TODO
            fun <T> getProvider(): ListDataProvider<T>
            fun <T> get(at: Int): T?
            fun toggleSubscription(at: Int)
        }
    }
}