package tv.vidiyo.android.domain.presenter.channels

import tv.vidiyo.android.api.model.Message
import tv.vidiyo.android.api.model.channel.Channel
import tv.vidiyo.android.api.model.episode.Episode
import tv.vidiyo.android.api.model.topic.Topic
import tv.vidiyo.android.base.BasePresenter
import tv.vidiyo.android.domain.contract.ChannelContract
import tv.vidiyo.android.source.BaseDataSource
import tv.vidiyo.android.source.channels.ChannelRepository

class ChannelPresenter(
        v: ChannelContract.View,
        override val repository: ChannelRepository
) : BasePresenter<ChannelContract.View>(v), ChannelContract.Presenter, BaseDataSource.Callback {

    init {
        repository.subscribe(this)
    }

    override val featured: Episode? get() = repository.featured

    override var channel: Channel
        set(value) { repository.channel = value }
        get() = repository.channel

    override val topics: Array<Topic> get() = repository.topics

    override fun start() {
        if (repository.isEmpty) {
            repository.refresh()

        } else {
            onDataLoaded()
        }
    }

    override fun refresh() {
        repository.refresh()
        view?.refreshCurrentPage()
    }

    override fun onDataLoaded() {
        val featured = repository.featured ?: return
        view?.playEpisode(featured)
        view?.update()
    }

    override fun onDataNotAvailable(error: Message) {
        // TODO
    }
}