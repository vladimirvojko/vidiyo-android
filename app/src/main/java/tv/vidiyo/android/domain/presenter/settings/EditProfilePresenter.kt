package tv.vidiyo.android.domain.presenter.settings

import tv.vidiyo.android.R
import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.api.model.Message
import tv.vidiyo.android.api.model.ServerResponse
import tv.vidiyo.android.api.model.user.UserAccount
import tv.vidiyo.android.api.update
import tv.vidiyo.android.base.BasePresenter
import tv.vidiyo.android.base.view.ShowMessagePresentationView
import tv.vidiyo.android.domain.contract.SettingsContract
import tv.vidiyo.android.extension.ServerCallback
import tv.vidiyo.android.source.profile.ProfileRepository
import tv.vidiyo.android.util.Constant

class EditProfilePresenter(
        v: SettingsContract.Edit.View,
        private val repository: ProfileRepository,
        private val service: VidiyoService
) : BasePresenter<SettingsContract.Edit.View>(v), SettingsContract.Edit.Presenter {

    private var account = repository.data

    override fun start() {
        super.start()
        view?.updateWith(account ?: return)
    }

    override fun save() {
        account?.let {
            view?.setRefreshing(true)
            service.update(it).enqueue(callback)
        }
    }

    override fun updateField(id: Int, value: String?) {
        when (id) {
            R.id.iv_avatar -> account?.imageFile = value
            R.id.et_name -> account?.name = value
            R.id.et_username -> account?.username = value
            R.id.et_bio -> account?.bio = value
            R.id.tv_gender -> return
            R.id.tv_birthday -> account?.birthday = value
            R.id.tv_location -> account?.location = value
            R.id.et_email -> account?.email = value
            R.id.et_phone -> account?.phone = value
            R.id.et_website -> account?.website = value

            else -> return
        }
    }

    private val callback = object : ServerCallback<ServerResponse<UserAccount>>() {
        override fun onSuccess(response: ServerResponse<UserAccount>, url: String?) {
            view?.setRefreshing(false)
            repository.data = response.data ?: return
        }

        override fun onFailure(error: Message, url: String?) {
            view?.setRefreshing(false)

            view?.showMessage(ShowMessagePresentationView.ToastMessage(
                    error.failures?.joinToString(separator = Constant.NEW_LINE) { it } ?: error.msg,
                    error.code
            ))
        }
    }
}