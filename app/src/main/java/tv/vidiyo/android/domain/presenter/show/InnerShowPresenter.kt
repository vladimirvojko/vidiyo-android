package tv.vidiyo.android.domain.presenter.show

import tv.vidiyo.android.base.adapter.provider.ListDataProvider
import tv.vidiyo.android.base.adapter.provider.MutableListDataProvider
import tv.vidiyo.android.domain.contract.ShowContract
import tv.vidiyo.android.domain.presenter.DataSourcePresenter
import tv.vidiyo.android.extension.accountIsAnonumous
import tv.vidiyo.android.extension.toggleSubscriptionInternal
import tv.vidiyo.android.source.show.InnerProvidedRepository
import tv.vidiyo.android.source.show.ShowRepository
import tv.vidiyo.android.util.SubscriptionManager

@Suppress("UNCHECKED_CAST")
class InnerShowPresenter(
        v: ShowContract.Inner.View,
        override val main: ShowRepository,
        val manager: SubscriptionManager
) : DataSourcePresenter<ShowContract.Inner.View, InnerProvidedRepository<*>>(v, main.get(v.type)), ShowContract.Inner.Presenter {

    init {
        repository.subscribe(this)
    }

    override fun <T> getProvider(): ListDataProvider<T> = repository.provider as MutableListDataProvider<T>
    override fun <T> get(at: Int): T? = repository.provider.items[at] as? T

    override fun toggleSubscription(at: Int) {
        val item = get<Any>(at) ?: return
        if (accountIsAnonumous(view, manager)) return

        toggleSubscriptionInternal(view, item, manager)
    }

    override fun refresh() {
        if (repository.shouldRefresh || repository.isEmpty) {
            super.refresh()
        }
    }

    override fun onDataLoaded() {
        view?.let {
            if (repository.data?.size == 0) {
                it.showNoContent()
            } else {
                it.showContent()
            }
            it.update()
        }
    }
}