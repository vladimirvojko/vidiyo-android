package tv.vidiyo.android.domain.presenter.boarding

import tv.vidiyo.android.R
import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.api.model.Message
import tv.vidiyo.android.api.model.ServerResponse
import tv.vidiyo.android.base.BasePresenter
import tv.vidiyo.android.base.view.ShowMessagePresentationView
import tv.vidiyo.android.domain.contract.RestoreContract
import tv.vidiyo.android.extension.ServerCallback
import tv.vidiyo.android.util.Constant

class RestorePresenter(
        v: RestoreContract.View,
        private val service: VidiyoService
) : BasePresenter<RestoreContract.View>(v), RestoreContract.Presenter {

    override fun restore(username: String) {
        view?.setRefreshing(true)
        service.nopassLogin(username).enqueue(callback)
    }

    override fun validate(username: String?) = (username ?: Constant.EMPTY).isNotEmpty()

    private val callback = object : ServerCallback<ServerResponse<Unit>>() {
        override fun onSuccess(response: ServerResponse<Unit>, url: String?) {
            view?.setRefreshing(false)
            view?.showMessage(ShowMessagePresentationView.ToastMessage(R.string.boarding_link_sent))
        }

        override fun onFailure(error: Message, url: String?) {
            view?.setRefreshing(false)
            view?.showMessage(ShowMessagePresentationView.ToastMessage(R.string.boarding_cant_send_link))
        }
    }
}