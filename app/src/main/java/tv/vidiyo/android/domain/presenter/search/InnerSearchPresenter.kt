package tv.vidiyo.android.domain.presenter.search

import tv.vidiyo.android.domain.contract.SearchContract
import tv.vidiyo.android.domain.presenter.DataSourcePresenter
import tv.vidiyo.android.source.search.SearchResultsPageRepository
import tv.vidiyo.android.source.search.SearchResultsRepository
import tv.vidiyo.android.util.Constant


class InnerSearchPresenter(
        v: SearchContract.Inner.View,
        private val main: SearchResultsRepository
) : DataSourcePresenter<SearchContract.Inner.View, SearchResultsPageRepository<*>>(v, main.repositoryForType(v.type)),
        SearchContract.Inner.Presenter {

    init {
        repository.subscribe(this)
    }

    override val totalCount: Int get() = repository.totalCount

    @Suppress("UNCHECKED_CAST")
    override fun <T> get(at: Int): T? = repository.provider.items[at] as? T

    override fun start() {
        view?.query = repository.query
        super.start()
    }

    override fun recent() {
        reset()
        repository.search(Constant.EMPTY, true)
        view?.showContent()
        view?.update()
    }

    override fun refresh() {
        val query = view?.query
        if (query == null || query.isEmpty()) {
            recent()
            return
        }

        if (repository.search(query)) {
            view?.showProgress()
        } else {
            view?.showContent()
        }
    }

    override fun loadMore() = repository.refresh()
    override fun reset() = repository.clear()

    override fun onDataLoaded() {
        view?.let {
            if (repository.data?.size == 0) {
                it.showNoContent()
            } else {
                it.showContent()
            }
            it.update()
        }
    }
}