package tv.vidiyo.android.domain.contract

import tv.vidiyo.android.api.model.social.SocialAuth
import tv.vidiyo.android.api.model.user.Profile
import tv.vidiyo.android.base.adapter.section.ItemPosition
import tv.vidiyo.android.base.view.PresentationView
import tv.vidiyo.android.base.view.RefreshablePresentationView
import tv.vidiyo.android.base.view.ShowMessagePresentationView
import tv.vidiyo.android.base.view.UpdatablePresentationView

interface SignUpContract {

    interface Social {
        interface View : PresentationView, RefreshablePresentationView, SignInPresentationView {
            fun withGoogle()
            fun withFacebook()
            fun startNameEnter(socialAuth: SocialAuth)
        }

        interface Presenter {
            fun process(auth: SocialAuth)
        }
    }

    interface Phone {
        interface View : PresentationView, RefreshablePresentationView {
            fun showError(text: String?)
            fun startCodeEnter()
        }

        interface Presenter {
            fun sendPhone(phone: String)
        }
    }

    interface Code {
        interface View : PresentationView, RefreshablePresentationView {
            fun showError(text: String?)
            fun startNameEnter()
        }

        interface Presenter {
            fun verifyCode(code: String, phone: String)
            fun resend(phone: String)
        }
    }

    interface Email {
        interface View : PresentationView, RefreshablePresentationView {
            fun showError(text: String?)
            fun startNameEnter()
        }

        interface Presenter {
            fun validate(email: String)
        }
    }

    interface UsernameAndPassword {
        interface View : PresentationView, SignInPresentationView, RefreshablePresentationView {
            fun startFindFriends()
            fun startMainAfterAnonymousRegister()
        }

        interface Presenter {
            val profile: Profile
            fun validate(login: String?, password: String?): Boolean
            fun create(login: String?, password: String?)
        }
    }

    interface Name {
        interface View : PresentationView {
            fun startUsernamePasswordEnter()
        }
    }

    interface FindFriends {
        interface BaseView
        interface View : BaseView, PresentationView, RefreshablePresentationView, ShowMessagePresentationView {
            fun startFriendsList()
            fun startSelectChannels()
            fun permissionNotGranted()
        }

        interface Presenter {
            fun findContactFriends()
            fun findFacebookFriends(token: String)
            fun findGoogleFriends()
            fun destroyLoader()
        }
    }

    interface FriendsList {
        interface View : FindFriends.BaseView, PresentationView, RefreshablePresentationView, UpdatablePresentationView {
            fun startNextScreen()
        }

        interface Presenter {
            fun isFriendHasAccount(position: ItemPosition): Boolean
            fun toggleSelection(position: ItemPosition): Boolean
            fun finishSelection()
        }
    }
}