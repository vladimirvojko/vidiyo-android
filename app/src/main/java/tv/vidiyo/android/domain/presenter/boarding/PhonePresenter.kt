package tv.vidiyo.android.domain.presenter.boarding

import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.api.model.Message
import tv.vidiyo.android.api.model.ServerResponse
import tv.vidiyo.android.base.BasePresenter
import tv.vidiyo.android.domain.contract.SignUpContract
import tv.vidiyo.android.extension.ServerCallback

class PhonePresenter(
        v: SignUpContract.Phone.View,
        private val service: VidiyoService
) : BasePresenter<SignUpContract.Phone.View>(v), SignUpContract.Phone.Presenter {

    override fun sendPhone(phone: String) {
        view?.setRefreshing(true)
        service.createPhone(phone).enqueue(callback)
    }

    private val callback = object : ServerCallback<ServerResponse<Unit>>() {
        override fun onSuccess(response: ServerResponse<Unit>, url: String?) {
            view?.setRefreshing(false)
            view?.startCodeEnter()
        }

        override fun onFailure(error: Message, url: String?) {
            view?.setRefreshing(false)
            view?.showError(error.msg)
        }
    }
}