package tv.vidiyo.android.domain.presenter

import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.api.model.Message
import tv.vidiyo.android.api.model.ServerResponse
import tv.vidiyo.android.api.model.cast.Cast
import tv.vidiyo.android.api.model.show.Show
import tv.vidiyo.android.api.model.topic.Topic
import tv.vidiyo.android.base.BasePresenter
import tv.vidiyo.android.base.adapter.provider.ListDataProvider
import tv.vidiyo.android.base.adapter.provider.MutableListDataProvider
import tv.vidiyo.android.domain.contract.ListContract
import tv.vidiyo.android.extension.ServerCallback
import tv.vidiyo.android.extension.accountIsAnonumous
import tv.vidiyo.android.extension.toggleSubscriptionInternal
import tv.vidiyo.android.model.InnerPageType
import tv.vidiyo.android.util.SubscriptionManager

@Suppress("UNCHECKED_CAST")
class ListPresenter(
        v: ListContract.View,
        private val service: VidiyoService,
        private val manager: SubscriptionManager
) : BasePresenter<ListContract.View>(v), ListContract.Presenter {

    private val provider = MutableListDataProvider<Any>()

    override fun start() {
        val v = view ?: return
        val type = v.type
        val userId = v.userId
        val channelId = v.channelId

        if (provider.items.isEmpty()) {
            v.showProgress()
            when (type) {
                InnerPageType.SHOWS -> service.userShowsInChannel(userId, channelId).enqueue(showCallback())
                InnerPageType.CAST -> service.userCastsInChannel(userId, channelId).enqueue(castCallback())
                InnerPageType.TOPICS -> service.userTopicsInChannel(userId, channelId).enqueue(topicCallback())

                else -> v.showNoContent()
            }

        } else {
            v.showContent()
        }
    }

    override fun <T> provider(): ListDataProvider<T> = provider as MutableListDataProvider<T>

    override fun toggleSubscription(at: Int) {
        if (accountIsAnonumous(view, manager)) return

        toggleSubscriptionInternal(view, get(at) ?: return, manager)
    }

    override fun <T> get(at: Int): T? = provider.items[at] as? T

    private fun showCallback() = object : ServerCallback<ServerResponse<Array<Show>>>() {
        override fun onSuccess(response: ServerResponse<Array<Show>>, url: String?) {
            response.data?.let {
                provider.addAll(it.toList(), true)
                view?.showContent()
                view?.update()

            } ?: view?.showNoContent()
        }

        override fun onFailure(error: Message, url: String?) {
            view?.showError(error.msg)
        }
    }

    private fun castCallback() = object : ServerCallback<ServerResponse<Array<Cast>>>() {
        override fun onSuccess(response: ServerResponse<Array<Cast>>, url: String?) {
            response.data?.let {
                provider.addAll(it.toList(), true)
                view?.showContent()
                view?.update()

            } ?: view?.showNoContent()
        }

        override fun onFailure(error: Message, url: String?) {
            view?.showError(error.msg)
        }
    }

    private fun topicCallback() = object : ServerCallback<ServerResponse<Array<Topic>>>() {
        override fun onSuccess(response: ServerResponse<Array<Topic>>, url: String?) {
            response.data?.let {
                provider.addAll(it.toList(), true)
                view?.showContent()
                view?.update()

            } ?: view?.showNoContent()
        }

        override fun onFailure(error: Message, url: String?) {
            view?.showError(error.msg)
        }
    }
}