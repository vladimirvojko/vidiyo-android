package tv.vidiyo.android.domain.contract

import tv.vidiyo.android.api.model.episode.Episode
import tv.vidiyo.android.base.view.PresentationView
import tv.vidiyo.android.base.view.ShowMessagePresentationView

interface ShareContract {
    interface View : PresentationView, ShowMessagePresentationView {
        val episode: Episode?
        fun changeSocialState(id: Int, enabled: Boolean)
        fun dismiss()
    }

    interface Presenter {
        fun connect(id: Int)
        fun send(text: String)
    }
}