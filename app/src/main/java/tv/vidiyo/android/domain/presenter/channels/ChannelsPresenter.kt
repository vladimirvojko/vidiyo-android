package tv.vidiyo.android.domain.presenter.channels

import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.api.assignChannels
import tv.vidiyo.android.api.model.Message
import tv.vidiyo.android.api.model.ServerResponse
import tv.vidiyo.android.base.BasePresenter
import tv.vidiyo.android.domain.contract.ChannelsContract
import tv.vidiyo.android.extension.ServerCallback
import tv.vidiyo.android.source.BaseDataSource
import tv.vidiyo.android.source.channels.ChannelsRepository

class ChannelsPresenter(
        v: ChannelsContract.View,
        override val repository: ChannelsRepository,
        override val provider: ChannelsProvider,
        private val service: VidiyoService
) : BasePresenter<ChannelsContract.View>(v), ChannelsContract.Presenter, BaseDataSource.Callback {

    init {
        repository.subscribe(this)
    }

    override fun start() {
        if (repository.isEmpty) {
            repository.refresh()
        } else {
            onDataLoaded()
        }
    }

    override fun changeSelection(position: Int) {
        provider.changeSelection(position)
    }

    override fun assignChannels() {
        view?.setRefreshing(true)
        service.assignChannels(provider.selected).enqueue(callback)
    }

    override fun onDataLoaded() {
        view?.update()
        provider.updateIds()
    }

    override fun onDataNotAvailable(error: Message) {
        view?.update()
    }

    private val callback = object : ServerCallback<ServerResponse<Array<Int>>>() {
        override fun onSuccess(response: ServerResponse<Array<Int>>, url: String?) {
            view?.setRefreshing(false)
            response.data?.let {
                repository.data?.userChannels = it
                repository.notifyDataLoaded()
                view?.closeEditing()
            }
        }

        override fun onFailure(error: Message, url: String?) {
            view?.setRefreshing(false)
        }
    }
}