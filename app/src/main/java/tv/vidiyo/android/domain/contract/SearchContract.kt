package tv.vidiyo.android.domain.contract

import tv.vidiyo.android.api.model.cast.Cast
import tv.vidiyo.android.api.model.episode.Episode
import tv.vidiyo.android.api.model.show.FriendShow
import tv.vidiyo.android.api.model.show.Show
import tv.vidiyo.android.api.model.topic.Topic
import tv.vidiyo.android.base.RefreshablePresenter
import tv.vidiyo.android.base.view.*
import tv.vidiyo.android.domain.presenter.LoadablePresenter
import tv.vidiyo.android.model.InnerPageType
import tv.vidiyo.android.source.search.SearchMainRepository

interface SearchContract {
    interface View : PresentationView, StatePresentationView, UpdatablePresentationView {
        fun playEpisode(episode: Episode?)
        fun update(sizes: IntArray)
    }

    interface Presenter : RefreshablePresenter {
        val repository: SearchMainRepository
        val sizes: IntArray

        fun getCast(at: Int): Cast?
        fun getFriendShow(at: Int): FriendShow?
        fun getTopPickShow(at: Int): Show?
        fun getTrendingTopic(at: Int): Topic?
    }

    interface Inner {
        interface View : StatePresentationView, UpdatablePresentationView, RefreshablePresentationView, ShowMessagePresentationView {
            @InnerPageType
            val type: Int
            var query: String?
            fun search(q: String?)
        }

        interface Presenter : LoadablePresenter {
            fun <T> get(at: Int): T?
            fun reset()
            fun recent()
        }
    }
}