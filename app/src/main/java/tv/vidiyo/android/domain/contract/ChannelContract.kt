package tv.vidiyo.android.domain.contract

import tv.vidiyo.android.api.model.channel.Channel
import tv.vidiyo.android.api.model.episode.Episode
import tv.vidiyo.android.api.model.topic.Topic
import tv.vidiyo.android.base.RefreshablePresenter
import tv.vidiyo.android.base.adapter.provider.DataProvider
import tv.vidiyo.android.base.view.*
import tv.vidiyo.android.model.InnerPageType
import tv.vidiyo.android.source.channels.ChannelRepository

interface ChannelContract {
    interface View : PresentationView, UpdatablePresentationView, HasPages {
        fun playEpisode(episode: Episode?)
    }

    interface Presenter : RefreshablePresenter {
        val featured: Episode?
        val repository: ChannelRepository
        val channel: Channel
        val topics: Array<Topic>
    }

    interface Inner {
        interface View : StatePresentationView, UpdatablePresentationView, RefreshablePresentationView,
                ShowMessagePresentationView, NeedRegistrationView, SelectableView {
            var type: Int
            var index: Int
        }

        interface Presenter : RefreshablePresenter {
            val repository: ChannelRepository // TODO
            val channel: Channel
            fun getProvider(@InnerPageType type: Int, index: Int): DataProvider
            fun <T> getItem(at: Int): T?
            fun toggleSubscription(at: Int)
        }
    }
}