package tv.vidiyo.android.domain.presenter.settings

import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.api.model.Message
import tv.vidiyo.android.api.model.NotificationsSettings
import tv.vidiyo.android.api.model.ServerResponse
import tv.vidiyo.android.base.BasePresenter
import tv.vidiyo.android.base.view.ShowMessagePresentationView
import tv.vidiyo.android.domain.contract.SettingsContract
import tv.vidiyo.android.extension.ServerCallback
import tv.vidiyo.android.util.Constant

class NotificationsPresenter(
        v: SettingsContract.Notification.View,
        private val service: VidiyoService
) : BasePresenter<SettingsContract.Notification.View>(v), SettingsContract.Notification.Presenter {

    override var data: NotificationsSettings? = null
    override var ignore: Boolean = true

    // saved index and value
    private var gi: Int = Constant.NO_ID
    private var value: Boolean = false

    /**
    INFO: {
    "status": "success",
    "data": {
    "user_id": 808,
    "follower_request": true,
    "accepted_follower_request": true,
    "new_episodes": true,
    "friends_on_vidiyo": true,
    "topic_has_new_video": true,
    "cast_has_new_video": true,
    "comment_liked": true,
    "replied_to_your_comment": true
    }
    }
     */

    override fun start() {
        view?.setRefreshing(true)
        service.notifications().enqueue(callback)
    }

    override fun endIgnoring() {
        ignore = false
    }

    override fun check(groupIndex: Int, index: Int) {
        val d = data ?: return
        view?.setRefreshing(true)
        gi = groupIndex
        value = index == 1
        service.changeNotification(mapOf(d.keyOf(gi) to value)).enqueue(changeCallback)
    }

    private val callback = object : ServerCallback<ServerResponse<NotificationsSettings>>() {
        override fun onSuccess(response: ServerResponse<NotificationsSettings>, url: String?) {
            view?.setRefreshing(false)
            data = response.data ?: return

            ignore = true
            view?.update()
        }

        override fun onFailure(error: Message, url: String?) {
            view?.setRefreshing(false)
            view?.showMessage(ShowMessagePresentationView.ToastMessage(error.msg))
        }
    }

    private val changeCallback = object : ServerCallback<ServerResponse<Unit>>() {
        override fun onSuccess(response: ServerResponse<Unit>, url: String?) {
            view?.setRefreshing(false)
            data?.update(gi, value).also { gi = Constant.NO_ID }
            view?.update()
        }

        override fun onFailure(error: Message, url: String?) {
            view?.setRefreshing(false)
            view?.update()
        }
    }
}