package tv.vidiyo.android.domain.presenter.boarding

import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.api.model.Message
import tv.vidiyo.android.api.model.ServerResponse
import tv.vidiyo.android.base.BasePresenter
import tv.vidiyo.android.domain.contract.SignUpContract
import tv.vidiyo.android.extension.ServerCallback

class EmailPresenter(
        v: SignUpContract.Email.View,
        private val service: VidiyoService
) : BasePresenter<SignUpContract.Email.View>(v), SignUpContract.Email.Presenter {

    override fun validate(email: String) {
        view?.setRefreshing(true)
        service.validate(email).enqueue(callback)
    }

    private val callback = object : ServerCallback<ServerResponse<Unit>>() {
        override fun onSuccess(response: ServerResponse<Unit>, url: String?) {
            view?.setRefreshing(false)
            view?.startNameEnter()
        }

        override fun onFailure(error: Message, url: String?) {
            view?.setRefreshing(false)
            view?.showError(error.msg)
        }
    }
}