package tv.vidiyo.android.domain.contract

import tv.vidiyo.android.base.adapter.provider.ListDataProvider
import tv.vidiyo.android.base.view.*
import tv.vidiyo.android.model.InnerPageType

interface ListContract {
    interface View : UpdatablePresentationView, RefreshablePresentationView, PresentationView,
            StatePresentationView, ShowMessagePresentationView, NeedRegistrationView {
        val userId: Int
        val channelId: Int
        @InnerPageType
        val type: Int
    }

    interface Presenter {
        fun <T> provider(): ListDataProvider<T>
        fun toggleSubscription(at: Int)
        fun <T> get(at: Int): T?
    }
}