package tv.vidiyo.android.domain.contract

import tv.vidiyo.android.api.model.show.FriendShow


interface FriendShowView: CoverView, UserView

fun FriendShowView.setFriendShow(friendShow: FriendShow) {
    setUser(friendShow.user)
    setShow(friendShow.show)
}