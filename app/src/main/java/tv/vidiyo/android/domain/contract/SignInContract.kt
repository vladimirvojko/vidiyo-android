package tv.vidiyo.android.domain.contract

import tv.vidiyo.android.base.view.PresentationView

interface SignInContract {

    interface View : PresentationView, SignInPresentationView {
        fun setRefreshing(block: Boolean)
    }

    interface Presenter {
        fun validate(login: String?, password: String?): Boolean
        fun signIn(login: String?, password: String?)
        fun forgot()
    }
}