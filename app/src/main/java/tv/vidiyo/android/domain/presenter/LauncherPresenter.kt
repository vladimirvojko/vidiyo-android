package tv.vidiyo.android.domain.presenter

import tv.vidiyo.android.R
import tv.vidiyo.android.account.UserAccountManager
import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.api.model.Message
import tv.vidiyo.android.api.model.ServerResponse
import tv.vidiyo.android.api.model.user.UserAccount
import tv.vidiyo.android.base.BasePresenter
import tv.vidiyo.android.domain.contract.LauncherContract
import tv.vidiyo.android.extension.ServerCallback

class LauncherPresenter(
        v: LauncherContract.View,
        private val service: VidiyoService,
        private val manager: UserAccountManager
) : BasePresenter<LauncherContract.View>(v), LauncherContract.Presenter {

    override fun loginWith(token: String) {
        service.nopassConfirm(token).enqueue(callback)
    }

    private val callback = object : ServerCallback<ServerResponse<UserAccount>>() {
        override fun onSuccess(response: ServerResponse<UserAccount>, url: String?) {
            val data = response.data ?: return
            if (manager.addAccount(data)) {
                view?.startMain()
            }
        }

        override fun onFailure(error: Message, url: String?) {
            view?.showError(null, R.string.boarding_error_invalid_token)
        }
    }
}