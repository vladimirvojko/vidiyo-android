package tv.vidiyo.android.domain.presenter.channels

import tv.vidiyo.android.base.adapter.ProviderRecyclerAdapter
import tv.vidiyo.android.base.view.holder.ViewHolder
import tv.vidiyo.android.source.channels.ChannelsMode
import tv.vidiyo.android.ui.adapter.delegate.ChannelsAdapterDelegate

class ChannelsAdapter(
        listener: ViewHolder.OnViewHolderClickListener?,
        provider: ChannelsProvider,
        @ChannelsMode mode: Int
) : ProviderRecyclerAdapter<ChannelsProvider>(provider, listOf(ChannelsAdapterDelegate(provider, listener))) {

    init {
        provider.mode = mode
    }

}