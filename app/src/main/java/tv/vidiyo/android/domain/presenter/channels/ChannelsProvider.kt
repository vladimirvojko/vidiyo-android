package tv.vidiyo.android.domain.presenter.channels

import tv.vidiyo.android.api.model.channel.Channel
import tv.vidiyo.android.base.adapter.provider.DataProvider
import tv.vidiyo.android.source.channels.ChannelsMode
import tv.vidiyo.android.source.channels.ChannelsRepository

class ChannelsProvider(val repository: ChannelsRepository) : DataProvider {

    var mode: Int = ChannelsMode.ALL
        set(value) {
            field = value
            if (value == ChannelsMode.EDIT) {
                selected.clear()
                selected.addAll(repository.userChannelsIds)
            }
        }
    private val map: HashMap<Int, Int> = HashMap(repository.channels.size) // id - position
    val selected = mutableSetOf<Int>()

    fun updateIds() {
        map.clear()
        repository.channels.forEachIndexed { index, channel ->
            map.put(channel.id, index)
        }
    }

    fun getItem(position: Int): Channel? {
        return if (mode == ChannelsMode.ALL || mode == ChannelsMode.EDIT) {
            repository.channels[position]
        } else {
            val id = repository.userChannelsIds[position]
            if (map.size != repository.channels.size) {
                updateIds() // TODO: TEMPORARY SOLUTION
            }
            map[id]?.let { repository.channels[it] }
        }
    }

    fun changeSelection(position: Int) {
        val id = repository.channels[position].id
        selected.apply {
            if (contains(id)) {
                remove(id)
            } else {
                add(id)
            }
        }
    }

    override fun getItemCount() = when (mode) {
        ChannelsMode.MY -> repository.userChannelsIds.size
        else -> repository.size
    }
}