package tv.vidiyo.android.domain.presenter.home

import tv.vidiyo.android.api.model.episode.Episode
import tv.vidiyo.android.api.model.show.Show
import tv.vidiyo.android.api.model.topic.Topic
import tv.vidiyo.android.api.model.user.User
import tv.vidiyo.android.domain.contract.HomeContract
import tv.vidiyo.android.domain.presenter.DataSourcePresenter
import tv.vidiyo.android.source.DataType
import tv.vidiyo.android.source.home.HomeRepository
import tv.vidiyo.android.util.SubscriptionManager

class HomePresenter(
        v: HomeContract.View,
        repository: HomeRepository,
        private val manager: SubscriptionManager
) : DataSourcePresenter<HomeContract.View, HomeRepository>(v, repository), HomeContract.Presenter {

    init {
        repository.subscribe(this)
    }

    override fun getSubscribedShow(at: Int): Show? = repository.showsSubscribedProvider.items?.getOrNull(at)

    override fun getTopPick(at: Int): Show? = repository.showsChannelsProvider.items?.getOrNull(at)

    override fun getFriend(at: Int): User? = repository.showsFriendsProvider.items?.getOrNull(at)?.user

    override fun getTopChannel(at: Int): Topic? = repository.topicsTopProvider.items?.getOrNull(at)

    override fun getTopic(at: Int): Topic? = repository.topicsProvider.items?.getOrNull(at)

    override fun getFriendShow(at: Int): Show? = repository.showsFriendsProvider.items?.getOrNull(at)?.show

    override fun getWatching(at: Int): Episode? = repository.watchingProvider.items?.getOrNull(at)?.episode

    override fun isAnonymous(): Boolean = manager.isAnonymous

    override fun additionalPreparation() {
        view?.setRefreshing(false)
        view?.update(IntArray(DataType.HOME_VALUES.size, {
            repository.getProviderFor(DataType.HOME_VALUES[it]).getItemCount()
        }))
        view?.playEpisode(repository.featured ?: return)
    }
}