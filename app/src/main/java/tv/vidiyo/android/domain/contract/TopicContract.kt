package tv.vidiyo.android.domain.contract

import tv.vidiyo.android.api.model.channel.Channel
import tv.vidiyo.android.api.model.show.Show
import tv.vidiyo.android.api.model.topic.Topic
import tv.vidiyo.android.base.RefreshablePresenter
import tv.vidiyo.android.base.adapter.provider.MutableListDataProvider
import tv.vidiyo.android.base.view.*
import tv.vidiyo.android.source.topic.TopicRepository

interface TopicContract {

    interface View : PresentationView, UpdatablePresentationView, RefreshablePresentationView,
            ShowMessagePresentationView, NeedRegistrationView, HasPages {
        fun updateTopicView(topic: Topic, channel: Channel?)
    }

    interface Presenter : RefreshablePresenter {
        val repository: TopicRepository
        val topic: Topic
        val channel: Channel?
        val related: Array<Topic>

        fun toggleSubscription()
    }

    interface Inner {
        interface View : StatePresentationView, UpdatablePresentationView, RefreshablePresentationView,
                ShowMessagePresentationView, NeedRegistrationView, SelectableView {
            var index: Int
        }

        interface Presenter : RefreshablePresenter {
            val repository: TopicRepository // TODO
            val channel: Channel?
            val provider: MutableListDataProvider<Show>?
            fun getProvider(index: Int): MutableListDataProvider<Show>
            fun get(at: Int): Show?
            fun toggleSubscription(at: Int)
        }
    }
}