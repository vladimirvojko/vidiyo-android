package tv.vidiyo.android.domain.contract

import tv.vidiyo.android.api.model.channel.Channel
import tv.vidiyo.android.api.model.user.PendingRequests
import tv.vidiyo.android.api.model.user.Profile
import tv.vidiyo.android.base.RefreshablePresenter
import tv.vidiyo.android.base.view.*
import tv.vidiyo.android.source.profile.ProfileRepository

interface ProfileContract {

    interface View : PresentationView, RefreshablePresentationView, ShowMessagePresentationView, NeedRegistrationView, HasPages {
        val profileId: Int

        fun updateProfileView(profile: Profile, withPhoto: Boolean = false)
        fun updateConfirmationView(email: String?)
        fun showPendingRequests(requests: PendingRequests)
        fun dialogBlock(block: Boolean, name: String)
        fun dialogBlockInformation(blocked: Boolean, name: String)
    }

    interface Presenter : RefreshablePresenter {
        var isForeign: Boolean
        val ownerId: Int

        fun sendConfirmation()
        fun closeConfirmation()
        fun toggleSubscription()
        fun toggleBlock()
        fun performBlockAction()
    }

    interface Inner {
        interface View : StatePresentationView, UpdatablePresentationView, RefreshablePresentationView,
                ShowMessagePresentationView, NeedRegistrationView, SelectableView {
            var type: Int
        }

        interface Presenter : RefreshablePresenter {
            val main: ProfileRepository

            fun toggleSubscription(at: Int)
            fun <T> get(at: Int): T?
            fun getSection(at: Int): Channel?
        }
    }
}