package tv.vidiyo.android.domain.presenter

import tv.vidiyo.android.account.UserAccountManager
import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.api.model.Message
import tv.vidiyo.android.api.model.ServerResponse
import tv.vidiyo.android.api.model.user.UserAccount
import tv.vidiyo.android.base.BasePresenter
import tv.vidiyo.android.base.view.ShowMessagePresentationView.ToastMessage
import tv.vidiyo.android.domain.contract.SplashContract
import tv.vidiyo.android.domain.contract.checkAndCreateAccount
import tv.vidiyo.android.extension.ServerCallback

class SplashPresenter(
        v: SplashContract.View,
        private val deviceId: String,
        private val manager: UserAccountManager,
        private val service: VidiyoService
) : BasePresenter<SplashContract.View>(v), SplashContract.Presenter {

    override fun registerAnonymous() {
        view?.setRefreshing(true)
        service.createAnonymous(deviceId).enqueue(callback)
    }

    private val callback = object : ServerCallback<ServerResponse<UserAccount>>() {
        override fun onSuccess(response: ServerResponse<UserAccount>, url: String?) {
            view?.setRefreshing(false)
            checkAndCreateAccount(manager, response.data, view)
        }

        override fun onFailure(error: Message, url: String?) {
            view?.setRefreshing(false)
            view?.showMessage(ToastMessage(error.msg))
        }
    }
}