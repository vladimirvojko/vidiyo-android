package tv.vidiyo.android.domain.presenter.search

import tv.vidiyo.android.api.model.Message
import tv.vidiyo.android.api.model.cast.Cast
import tv.vidiyo.android.api.model.show.FriendShow
import tv.vidiyo.android.api.model.show.Show
import tv.vidiyo.android.api.model.topic.Topic
import tv.vidiyo.android.domain.contract.SearchContract
import tv.vidiyo.android.domain.presenter.DataSourcePresenter
import tv.vidiyo.android.source.DataType
import tv.vidiyo.android.source.search.SearchMainRepository

class SearchPresenter(
        v: SearchContract.View,
        repository: SearchMainRepository
) : DataSourcePresenter<SearchContract.View, SearchMainRepository>(v, repository), SearchContract.Presenter {

    init {
        repository.subscribe(this)
    }

    override fun additionalPreparation() {
        view?.playEpisode(repository.featured ?: return)
    }

    override fun getFriendShow(at: Int): FriendShow? = repository.showsFriendsProvider.items?.getOrNull(at)
    override fun getCast(at: Int): Cast? = repository.castsProvider.items?.getOrNull(at)
    override fun getTopPickShow(at: Int): Show? = repository.topPicksProvider.items?.getOrNull(at)
    override fun getTrendingTopic(at: Int): Topic? = repository.trendingChannelsProvider.items?.getOrNull(at)

    override var sizes: IntArray = intArrayOf()
        get() {
            if (field.isEmpty()) {
                field = kotlin.IntArray(DataType.SEARCH_VALUES.size, {
                    repository.getListDataProviderFor(DataType.SEARCH_VALUES[it]).getItemCount()
                })
            }
            return field
        }


    override fun onDataLoaded() {
        super.onDataLoaded()
        sizes = intArrayOf()
        view?.update(sizes)
    }

    override fun onDataNotAvailable(error: Message) {}
}