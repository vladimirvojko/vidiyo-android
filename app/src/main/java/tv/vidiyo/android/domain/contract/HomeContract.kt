package tv.vidiyo.android.domain.contract

import tv.vidiyo.android.api.model.episode.Episode
import tv.vidiyo.android.api.model.show.Show
import tv.vidiyo.android.api.model.topic.Topic
import tv.vidiyo.android.api.model.user.User
import tv.vidiyo.android.base.RefreshablePresenter
import tv.vidiyo.android.base.view.PresentationView
import tv.vidiyo.android.base.view.RefreshablePresentationView
import tv.vidiyo.android.base.view.StatePresentationView
import tv.vidiyo.android.base.view.UpdatablePresentationView

interface HomeContract {
    interface View : PresentationView, StatePresentationView, UpdatablePresentationView, RefreshablePresentationView {
        fun update(sizes: IntArray)

        fun playEpisode(episode: Episode?)
    }

    interface Presenter : RefreshablePresenter {
        fun getFriendShow(at: Int): Show?
        fun getSubscribedShow(at: Int): Show?
        fun getTopPick(at: Int): Show?
        fun getFriend(at: Int): User?
        fun getTopChannel(at: Int): Topic?
        fun getTopic(at: Int): Topic?
        fun getWatching(at: Int): Episode?

        fun isAnonymous(): Boolean
    }
}