package tv.vidiyo.android.domain.presenter.boarding

import tv.vidiyo.android.account.UserAccountManager
import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.api.model.Message
import tv.vidiyo.android.api.model.ServerResponse
import tv.vidiyo.android.api.model.social.SocialAuth
import tv.vidiyo.android.api.model.user.Profile
import tv.vidiyo.android.api.model.user.UserAccount
import tv.vidiyo.android.api.register
import tv.vidiyo.android.api.registerAnonymous
import tv.vidiyo.android.api.socialRegister
import tv.vidiyo.android.base.BasePresenter
import tv.vidiyo.android.domain.contract.SignUpContract
import tv.vidiyo.android.domain.contract.SignUpContract.UsernameAndPassword
import tv.vidiyo.android.domain.contract.checkAndCreateAccount
import tv.vidiyo.android.extension.ServerCallback
import tv.vidiyo.android.util.Constant

class CreateUsernameAndPasswordPresenter(
        v: UsernameAndPassword.View,
        private val service: VidiyoService,
        private val manager: UserAccountManager
) : BasePresenter<SignUpContract.UsernameAndPassword.View>(v), UsernameAndPassword.Presenter {

    override lateinit var profile: Profile
    var social: SocialAuth? = null

    override fun validate(login: String?, password: String?): Boolean {
        val l = login ?: Constant.EMPTY
        val p = password ?: Constant.EMPTY

        return l.isNotEmpty() && p.isNotEmpty()
    }

    override fun create(login: String?, password: String?) {
        login ?: return; password ?: return
        view?.setRefreshing(true)

        profile.username = login
        val acc = manager.account
        val soc = social
        view?.setRefreshing(true)

        when {
            soc != null -> service.socialRegister(soc, profile, password, acc)
            acc != null && acc.isAnonymous -> service.registerAnonymous(profile, password, acc)
            else -> service.register(profile, password)

        }.enqueue(callback)
    }

    private val callback = object : ServerCallback<ServerResponse<UserAccount>>() {
        override fun onSuccess(response: ServerResponse<UserAccount>, url: String?) {
            view?.setRefreshing(false)
            manager.account?.let {
                val data = response.data ?: return
                manager.update(data, completion = {
                    view?.startMainAfterAnonymousRegister()
                })

            } ?: checkAndCreateAccount(manager, response.data, view)
        }

        override fun onFailure(error: Message, url: String?) {
            view?.setRefreshing(false)
            view?.showError(error.msg, 0)
        }
    }
}