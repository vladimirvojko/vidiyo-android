package tv.vidiyo.android.domain.presenter.cast

import tv.vidiyo.android.api.model.Subscribable
import tv.vidiyo.android.domain.contract.CastContract
import tv.vidiyo.android.domain.presenter.DataSourcePresenter
import tv.vidiyo.android.extension.accountIsAnonumous
import tv.vidiyo.android.extension.toggleSubscriptionInternal
import tv.vidiyo.android.source.cast.CastRepository
import tv.vidiyo.android.source.show.InnerProvidedRepository
import tv.vidiyo.android.util.SubscriptionManager

@Suppress("UNCHECKED_CAST")
class InnerCastPresenter(
        v: CastContract.Inner.View,
        override val main: CastRepository,
        private val manager: SubscriptionManager
) : DataSourcePresenter<CastContract.Inner.View, InnerProvidedRepository<*>>(v, main.get(v.type)), CastContract.Inner.Presenter {

    init {
        repository.subscribe(this)
    }

    override val totalCount: Int get() = repository.totalCount

    override fun <T> get(at: Int): T? = repository.provider.items[at] as? T
    override fun loadMore() = repository.refresh()

    override fun refresh() {
        if (repository.shouldRefresh || repository.isEmpty) {
            super.refresh()
        }
    }

    override fun toggleSubscription(at: Int) {
        val item = get<Subscribable>(at) ?: return
        if (accountIsAnonumous(view, manager)) return

        toggleSubscriptionInternal(view, item, manager)
    }

    override fun onDataLoaded() {
        view?.let {
            if (repository.data?.size == 0) {
                it.showNoContent()
            } else {
                it.showContent()
            }
            it.update()
        }
    }
}