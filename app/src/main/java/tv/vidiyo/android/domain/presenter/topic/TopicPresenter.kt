package tv.vidiyo.android.domain.presenter.topic

import tv.vidiyo.android.api.model.Message
import tv.vidiyo.android.api.model.channel.Channel
import tv.vidiyo.android.api.model.topic.Topic
import tv.vidiyo.android.base.BasePresenter
import tv.vidiyo.android.base.view.ShowMessagePresentationView
import tv.vidiyo.android.domain.contract.TopicContract
import tv.vidiyo.android.extension.accountIsAnonumous
import tv.vidiyo.android.source.BaseDataSource
import tv.vidiyo.android.source.topic.TopicRepository
import tv.vidiyo.android.util.SubscriptionManager

class TopicPresenter(
        v: TopicContract.View,
        override val repository: TopicRepository,
        private val manager: SubscriptionManager
) : BasePresenter<TopicContract.View>(v), TopicContract.Presenter, BaseDataSource.Callback {

    init {
        repository.subscribe(this)
    }

    override val channel: Channel? get() = repository.channel
    override val related: Array<Topic> get() = repository.related
    override var topic: Topic
        set(value) {
            repository.topic = value
        }
        get() = repository.topic

    override fun start() {
        if (repository.isEmpty) {
            repository.refresh()

        } else {
            onDataLoaded()
        }
    }

    override fun refresh() {
        repository.refresh()
        view?.refreshCurrentPage()
    }

    override fun toggleSubscription() {
        if (accountIsAnonumous(view, manager)) return

        view?.setRefreshing(true)
        manager.setSubscription(topic, !manager.isSubscribed(topic)) { _, subscribed, error ->
            view?.setRefreshing(false)
            if (error != null) {
                view?.showMessage(ShowMessagePresentationView.ToastMessage(error))
            } else {
                topic.subscribers += if (subscribed) 1 else -1
                topic.isSubscribed = subscribed
                view?.updateTopicView(topic, channel)
            }
        }
    }

    override fun onDataLoaded() {
        view?.setRefreshing(false)
        view?.update()
    }

    override fun onDataNotAvailable(error: Message) {
        // TODO
    }
}