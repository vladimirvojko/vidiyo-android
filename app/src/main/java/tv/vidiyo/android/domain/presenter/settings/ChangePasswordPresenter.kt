package tv.vidiyo.android.domain.presenter.settings

import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.api.model.Message
import tv.vidiyo.android.api.model.ServerResponse
import tv.vidiyo.android.base.BasePresenter
import tv.vidiyo.android.base.view.ShowMessagePresentationView
import tv.vidiyo.android.domain.contract.SettingsContract
import tv.vidiyo.android.extension.ServerCallback
import tv.vidiyo.android.util.Constant

class ChangePasswordPresenter(
        v: SettingsContract.Password.View,
        private val service: VidiyoService
) : BasePresenter<SettingsContract.Password.View>(v), SettingsContract.Password.Presenter {

    override fun validate(current: String, new: String, confirm: String)
            = current.isNotEmpty() && new.isNotEmpty() && new == confirm

    override fun change(current: String, new: String, confirm: String) {
        view?.setRefreshing(true)
        service.changePassword(current, new, confirm).enqueue(callback)
    }

    private val callback = object : ServerCallback<ServerResponse<Unit>>() {
        override fun onSuccess(response: ServerResponse<Unit>, url: String?) {
            view?.setRefreshing(false)
        }

        override fun onFailure(error: Message, url: String?) {
            view?.setRefreshing(false)

            view?.showMessage(ShowMessagePresentationView.ToastMessage(
                    error.failures?.joinToString(separator = Constant.NEW_LINE) { it } ?: error.msg,
                    error.code
            ))
        }
    }
}