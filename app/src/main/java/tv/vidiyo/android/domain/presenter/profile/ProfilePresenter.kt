package tv.vidiyo.android.domain.presenter.profile

import tv.vidiyo.android.account.UserAccountManager
import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.api.model.Message
import tv.vidiyo.android.api.model.ServerResponse
import tv.vidiyo.android.api.model.user.SubscriptionRequest
import tv.vidiyo.android.api.model.user.SubscriptionStatus
import tv.vidiyo.android.api.model.user.toFollowingUser
import tv.vidiyo.android.base.BasePresenter
import tv.vidiyo.android.base.view.ShowMessagePresentationView
import tv.vidiyo.android.domain.contract.ProfileContract
import tv.vidiyo.android.extension.ServerCallback
import tv.vidiyo.android.extension.accountIsAnonumous
import tv.vidiyo.android.model.NotificationType
import tv.vidiyo.android.source.BaseDataSource
import tv.vidiyo.android.source.profile.ProfileRepository
import tv.vidiyo.android.util.Constant
import tv.vidiyo.android.util.NotificationsCenter
import tv.vidiyo.android.util.SubscriptionManager

class ProfilePresenter(
        v: ProfileContract.View,
        val repository: ProfileRepository,
        private val service: VidiyoService,
        accountManager: UserAccountManager,
        private val manager: SubscriptionManager,
        private val center: NotificationsCenter? = null
) : BasePresenter<ProfileContract.View>(v), ProfileContract.Presenter, BaseDataSource.Callback, NotificationsCenter.Listener {

    override val ownerId: Int = accountManager.account?.id ?: Constant.NO_ID
    override var isForeign = repository.id != ownerId

    init {
        repository.subscribe(this)
    }

    override fun start() {
        center?.apply {
            addListener(this@ProfilePresenter)
            refreshPendingStatus()
        }

        if (repository.isEmpty) {
            repository.refresh()

        } else {
            onDataLoaded()
        }
    }

    override fun refresh() {
        repository.refresh()
        repository.getAll().map { it.shouldRefresh = true }
        view?.refreshCurrentPage()
    }

    override fun onDestroy() {
        center?.removeListener(this)
    }

    override fun closeConfirmation() {
        repository.confirmationWasClosed = true
    }

    override fun toggleSubscription() {
        val profile = repository.data ?: return
        if (accountIsAnonumous(view, manager)) return

        val item = profile.toFollowingUser()
        val status = manager.subscriptionStatus(item)
        view?.setRefreshing(true)
        manager.setUserSubscription(if (status != SubscriptionStatus.accepted && status != SubscriptionStatus.pending) {
            SubscriptionRequest.follow
        } else {
            SubscriptionRequest.unfollow
        }, item) { subscriptions, error ->
            view?.setRefreshing(false)
            if (error != null) {
                view?.showMessage(ShowMessagePresentationView.ToastMessage(error))

            } else if (subscriptions != null && subscriptions.isNotEmpty()) {
                val first = subscriptions.first()
                profile.followersCount += when (first.status) {
                    SubscriptionStatus.accepted -> 1
                    SubscriptionStatus.deleted -> if (profile.status != SubscriptionStatus.pending) -1 else 0
                    else -> 0
                }
                profile.status = first.status
                view?.updateProfileView(profile)
            }
        }
    }

    override fun toggleBlock() {
        val user = repository.data ?: return
        if (accountIsAnonumous(view, manager)) return
        val name = user.name ?: return

        view?.dialogBlock(user.backprop != SubscriptionStatus.blocked, name)
    }

    override fun performBlockAction() {
        val user = repository.data ?: return
        val name = user.name ?: return
        val request = if (user.backprop == SubscriptionStatus.blocked) SubscriptionRequest.unblock else SubscriptionRequest.block

        view?.setRefreshing(true)
        manager.setUserSubscription(request, user.toFollowingUser()) { subscriptions, error ->
            view?.setRefreshing(false)
            val first = subscriptions?.firstOrNull()
            if (first != null) {
                user.backprop = first.status
                view?.dialogBlockInformation(request == SubscriptionRequest.block, name)

            } else view?.showMessage(ShowMessagePresentationView.ToastMessage(error))
        }
    }

    override fun sendConfirmation() {
        repository.confirmationWasClosed = true
        service.resendEmailConfirmation().enqueue(callback)
    }

    override fun onAction(action: Int, @NotificationType type: String) {
        if (action == NotificationsCenter.ACTION_LOADED && type == NotificationType.REQUEST_PENDING) {
            center?.pendingRequests?.let { if (it.count > 0) view?.showPendingRequests(it) }
        }
    }

    override fun onDataLoaded() {
        val profile = repository.data ?: return

        view?.setRefreshing(false)
        view?.updateProfileView(profile, true)
        view?.updateConfirmationView(if (profile.isEmailVerified || repository.confirmationWasClosed) null else profile.email)
    }

    override fun onDataChanged() {
        view?.updateProfileView(repository.data ?: return, true)
    }

    override fun onDataNotAvailable(error: Message) {
        // TODO
    }

    private val callback = object : ServerCallback<ServerResponse<Unit>>() {}
}