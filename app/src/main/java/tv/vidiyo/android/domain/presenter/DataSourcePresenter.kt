package tv.vidiyo.android.domain.presenter

import tv.vidiyo.android.api.model.Message
import tv.vidiyo.android.base.BasePresenter
import tv.vidiyo.android.base.RefreshablePresenter
import tv.vidiyo.android.base.view.PresentationView
import tv.vidiyo.android.base.view.StatePresentationView
import tv.vidiyo.android.base.view.UpdatablePresentationView
import tv.vidiyo.android.source.BaseDataSource
import tv.vidiyo.android.source.DataSource

abstract class DataSourcePresenter<out V, out D>(
        v: V,
        val repository: D
) : BasePresenter<V>(v), BaseDataSource.Callback, RefreshablePresenter
        where V : PresentationView, V : UpdatablePresentationView, D : DataSource<*> {

    protected val stateView: StatePresentationView? get() = view as? StatePresentationView

    override fun start() {
        if (repository.isEmpty) {
            refresh()

        } else {
            stateView?.let {
                if (repository.size == 0) {
                    it.showNoContent()
                } else {
                    it.showContent()
                }
            }
            additionalPreparation()
        }
    }

    override fun refresh() {
        stateView?.showProgress()
        repository.refresh()
    }

    protected open fun additionalPreparation() {}

    override fun onDataLoaded() {
        stateView?.showContent()
        view?.update()
        additionalPreparation()
    }

    override fun onDataNotAvailable(error: Message) {
        stateView?.showError(error.msg, false)
    }
}