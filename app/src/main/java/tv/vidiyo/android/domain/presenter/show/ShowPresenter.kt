package tv.vidiyo.android.domain.presenter.show

import tv.vidiyo.android.api.model.Message
import tv.vidiyo.android.api.model.show.Show
import tv.vidiyo.android.base.BasePresenter
import tv.vidiyo.android.base.view.ShowMessagePresentationView
import tv.vidiyo.android.domain.contract.ShowContract
import tv.vidiyo.android.extension.accountIsAnonumous
import tv.vidiyo.android.source.BaseDataSource
import tv.vidiyo.android.source.show.ShowRepository
import tv.vidiyo.android.util.SubscriptionManager

class ShowPresenter(
        v: ShowContract.View,
        val repository: ShowRepository,
        private val manager: SubscriptionManager
) : BasePresenter<ShowContract.View>(v), ShowContract.Presenter, BaseDataSource.Callback {

    init {
        repository.subscribe(this)
    }

    override var show: Show? = null
        get() {
            if (field == null) {
                field = repository.data
            }
            return field
        }

    override fun toggleSubscription() {
        val show = repository.data ?: return
        if (accountIsAnonumous(view, manager)) return
        
        view?.setRefreshing(true)
        manager.setSubscription(show, !manager.isSubscribed(show)) { _, subscribed, error ->
            view?.setRefreshing(false)
            if (error != null) {
                view?.showMessage(ShowMessagePresentationView.ToastMessage(error))
            } else {
                show.subscribers += if (subscribed) 1 else -1
                show.isSubscribed = subscribed
                view?.updateShowView(show)
            }
        }
    }

    override fun refresh() {
        repository.refresh()
        repository.getAll().map { it.shouldRefresh = true }
        view?.refreshCurrentPage()
    }

    override fun start() {
        if (repository.isEmpty) {
            repository.refresh()

        } else {
            onDataLoaded()
        }
    }

    override fun onDataLoaded() {
        view?.setRefreshing(false)
        val info = repository.data ?: return
        view?.updateShowView(info)
        view?.updateShowInfo(info)
    }

    override fun onDataNotAvailable(error: Message) {
        // TODO
    }
}