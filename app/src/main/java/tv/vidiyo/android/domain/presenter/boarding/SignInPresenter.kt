package tv.vidiyo.android.domain.presenter.boarding

import tv.vidiyo.android.account.UserAccountManager
import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.api.model.Message
import tv.vidiyo.android.api.model.ServerResponse
import tv.vidiyo.android.api.model.user.UserAccount
import tv.vidiyo.android.base.BasePresenter
import tv.vidiyo.android.domain.contract.SignInContract
import tv.vidiyo.android.domain.contract.checkAndCreateAccount
import tv.vidiyo.android.extension.ServerCallback
import tv.vidiyo.android.util.Constant

class SignInPresenter(
        v: SignInContract.View,
        private val service: VidiyoService,
        private val manager: UserAccountManager
) : BasePresenter<SignInContract.View>(v), SignInContract.Presenter {

    override fun validate(login: String?, password: String?): Boolean {
        val l = login ?: Constant.EMPTY
        val p = password ?: Constant.EMPTY

        return l.isNotEmpty() && p.isNotEmpty()
    }

    override fun signIn(login: String?, password: String?) {
        val l = login ?: Constant.EMPTY
        val p = password ?: Constant.EMPTY

        if (l.isNotEmpty() && p.isNotEmpty()) {
            view?.setRefreshing(true)
            service.login(l, p).enqueue(callback)
        }
    }

    override fun forgot() {
        // TODO "not implemented"
    }

    private val callback = object : ServerCallback<ServerResponse<UserAccount>>() {
        override fun onSuccess(response: ServerResponse<UserAccount>, url: String?) {
            view?.setRefreshing(false)
            manager.account?.let {
                val data = response.data ?: return
                manager.update(data, completion = {
                    view?.startMain()
                })

            } ?: checkAndCreateAccount(manager, response.data, view)
        }

        override fun onFailure(error: Message, url: String?) {
            view?.setRefreshing(false)
            view?.showError(error.msg, error.code)
        }
    }
}