package tv.vidiyo.android.domain.contract

import tv.vidiyo.android.api.model.comment.Comment
import tv.vidiyo.android.api.model.episode.EpisodeInfo
import tv.vidiyo.android.api.model.episode.Product
import tv.vidiyo.android.api.model.episode.ViewInfo
import tv.vidiyo.android.api.model.show.Show
import tv.vidiyo.android.base.adapter.provider.ListDataProvider
import tv.vidiyo.android.base.adapter.provider.SectionedListDataProvider
import tv.vidiyo.android.base.view.*
import tv.vidiyo.android.domain.presenter.LoadablePresenter

interface EpisodeContract {

    interface Player {
        interface View : PresentationView, ShowMessagePresentationView, NeedRegistrationView {
            fun load(info: EpisodeInfo)
            fun showLikeViewAndUpdate(liked: Boolean, withAnimation: Boolean = true)
        }

        interface Presenter {
            val episode: EpisodeInfo?
            fun canShare(): Boolean
            fun updateWithActualState()
            fun getProduct(at: Int): Product?
            fun toggleLike()
            fun setViewTime(position: Long)
        }

    }

    interface Info {
        interface View : PresentationView, RefreshablePresentationView, ShowMessagePresentationView, NeedRegistrationView {
            fun updateDescription(info: EpisodeInfo)
            fun updateShowView(show: Show)
            fun changeProductsVisibility(visibility: Boolean)
            fun forceUpdateInnerViews()
        }

        interface Presenter {
            fun getProduct(at: Int): Product?
            fun toggleShowSubscription()
        }
    }

    interface Inner {
        interface View : StatePresentationView, UpdatablePresentationView, ShowMessagePresentationView, RefreshablePresentationView, NeedRegistrationView {
            val type: Int
        }

        interface Presenter : LoadablePresenter {
            val commentsProvider: SectionedListDataProvider<Comment, Comment>
            val commentsCount: Int
            fun notAnonymous(): Boolean
            fun changeCurrentEpisodeTo(episodeId: Int)
            fun createComment(episodeId: Int, parentId: Int, text: String)
            fun <T> getProvider(): ListDataProvider<T>
            fun <T> get(at: Int): T?
            fun toggleSubscription(at: Int)
        }
    }

    interface Notifier {
        fun send(viewInfo: ViewInfo)
    }
}