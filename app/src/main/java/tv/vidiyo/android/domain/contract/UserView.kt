package tv.vidiyo.android.domain.contract

import android.support.annotation.DrawableRes
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.user.User


interface UserView {
    fun <T> setAvatar(avatar: T?, @DrawableRes fallbackImageResId: Int)
    fun setUsername(username: String?)
    fun setName(name: String?)
}

fun UserView.setUser(user: User) {
    setAvatar(user.image, R.drawable.ic_default_profile)
    user.name?.let { setName(it) }
    user.username?.let { setUsername(it) }
}