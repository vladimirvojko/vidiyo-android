package tv.vidiyo.android.domain.presenter.boarding

import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.api.connect
import tv.vidiyo.android.api.model.Message
import tv.vidiyo.android.api.model.ServerResponse
import tv.vidiyo.android.base.BasePresenter
import tv.vidiyo.android.base.adapter.section.ItemPosition
import tv.vidiyo.android.domain.contract.SignUpContract.FriendsList
import tv.vidiyo.android.extension.ServerCallback
import tv.vidiyo.android.source.FriendsRepository

class FriendsListPresenter(
        v: FriendsList.View,
        val repository: FriendsRepository,
        private val service: VidiyoService
) : BasePresenter<FriendsList.View>(v), FriendsList.Presenter {

    override fun isFriendHasAccount(position: ItemPosition) = repository.provider.getItemForPosition(position)?.username != null

    override fun toggleSelection(position: ItemPosition): Boolean {
        repository.provider.toggleSelection(position)
        return repository.provider.selected.contains(position)
    }

    override fun finishSelection() {
        val selectedFriends = repository.provider.selectedItems

        view?.setRefreshing(true)
        service.connect(selectedFriends.filter {
            it.username != null

        }, selectedFriends.filter {
            it.username == null && (it.email != null || it.phone != null)

        }).enqueue(callback)
    }

    private val callback = object : ServerCallback<ServerResponse<Unit>>() {
        override fun onSuccess(response: ServerResponse<Unit>, url: String?) {
            view?.setRefreshing(false)
            view?.startNextScreen()
        }

        override fun onFailure(error: Message, url: String?) {
            view?.setRefreshing(false)
        }
    }
}