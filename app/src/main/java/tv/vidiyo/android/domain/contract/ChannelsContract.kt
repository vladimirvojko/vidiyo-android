package tv.vidiyo.android.domain.contract

import tv.vidiyo.android.base.view.PresentationView
import tv.vidiyo.android.base.view.RefreshablePresentationView
import tv.vidiyo.android.base.view.ShowMessagePresentationView
import tv.vidiyo.android.base.view.UpdatablePresentationView
import tv.vidiyo.android.domain.presenter.channels.ChannelsProvider
import tv.vidiyo.android.source.channels.ChannelsRepository

interface ChannelsContract {

    interface View : PresentationView, UpdatablePresentationView, RefreshablePresentationView, ShowMessagePresentationView {
        fun closeEditing() {}
    }

    interface BoardingView : View, SignInPresentationView {
        fun updateMessage(count: Int)
        fun changeMessageVisibility(visible: Boolean)
        fun changeButtonVisibility(visible: Boolean)
        fun showGathering()
        fun showIntro()
    }

    interface Presenter {
        val repository: ChannelsRepository
        val provider: ChannelsProvider
        fun changeSelection(position: Int)
        fun assignChannels()
    }
}