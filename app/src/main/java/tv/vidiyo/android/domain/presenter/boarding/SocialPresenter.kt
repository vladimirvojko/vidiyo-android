package tv.vidiyo.android.domain.presenter.boarding

import tv.vidiyo.android.account.UserAccountManager
import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.api.model.Message
import tv.vidiyo.android.api.model.ServerResponse
import tv.vidiyo.android.api.model.social.SocialAuth
import tv.vidiyo.android.api.model.user.UserAccount
import tv.vidiyo.android.api.socialAuth
import tv.vidiyo.android.base.BasePresenter
import tv.vidiyo.android.domain.contract.SignUpContract
import tv.vidiyo.android.domain.contract.checkAndCreateAccount
import tv.vidiyo.android.extension.ServerCallback

class SocialPresenter(
        v: SignUpContract.Social.View,
        private val service: VidiyoService,
        private val manager: UserAccountManager
) : BasePresenter<SignUpContract.Social.View>(v), SignUpContract.Social.Presenter {

    lateinit var auth: SocialAuth

    override fun process(auth: SocialAuth) {
        this.auth = auth
        view?.setRefreshing(true)
        service.socialAuth(auth).enqueue(callback)
    }

    private val callback = object : ServerCallback<ServerResponse<UserAccount>>() {
        override fun onSuccess(response: ServerResponse<UserAccount>, url: String?) {
            view?.setRefreshing(false)
            manager.account?.let {
                val data = response.data ?: return
                manager.update(data, completion = {
                    view?.startMain()
                })

            } ?: checkAndCreateAccount(manager, response.data, view)
        }

        override fun onFailure(error: Message, url: String?) {
            view?.setRefreshing(false)
            view?.startNameEnter(auth)
        }
    }
}