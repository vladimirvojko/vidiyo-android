package tv.vidiyo.android.domain.presenter.episode

import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.api.model.Message
import tv.vidiyo.android.api.model.ServerResponse
import tv.vidiyo.android.api.model.comment.Comment
import tv.vidiyo.android.base.adapter.provider.ListDataProvider
import tv.vidiyo.android.base.adapter.provider.MutableListDataProvider
import tv.vidiyo.android.base.adapter.provider.SectionedListDataProvider
import tv.vidiyo.android.base.adapter.section.Section
import tv.vidiyo.android.domain.contract.EpisodeContract
import tv.vidiyo.android.domain.presenter.DataSourcePresenter
import tv.vidiyo.android.extension.ServerCallback
import tv.vidiyo.android.extension.accountIsAnonumous
import tv.vidiyo.android.extension.toggleSubscriptionInternal
import tv.vidiyo.android.source.episode.BaseInnerEpisodeRepository
import tv.vidiyo.android.source.episode.CommentsRepository
import tv.vidiyo.android.source.episode.EpisodeRepository
import tv.vidiyo.android.util.SubscriptionManager

@Suppress("UNCHECKED_CAST")
class EpisodeInnerPresenter(
        v: EpisodeContract.Inner.View,
        private val main: EpisodeRepository,
        private val service: VidiyoService,
        val manager: SubscriptionManager
) : DataSourcePresenter<EpisodeContract.Inner.View, BaseInnerEpisodeRepository<*, *>>(v, main.get(v.type)), EpisodeContract.Inner.Presenter {

    init {
        repository.subscribe(this)
    }

    override val totalCount: Int get() = main.commentsRepository.totalCount
    override val commentsCount: Int get() = main.commentsRepository.size

    override fun start() {
        if (repository.isEmpty || repository.shouldRefresh) {
            refresh()

        } else {
            view?.let {
                if (repository.size == 0) {
                    it.showNoContent()
                } else {
                    it.showContent()
                }
            }
            additionalPreparation()
        }
    }

    override fun loadMore() = main.commentsRepository.refresh()
    override val commentsProvider = main.commentsRepository.provider
    override fun <T> getProvider(): ListDataProvider<T> = repository.provider as MutableListDataProvider<T>

    override fun <T> get(at: Int): T? {
        val p = repository.provider
        return when (p) {
            is MutableListDataProvider<*> -> p.items[at] as? T
            is SectionedListDataProvider<*, *> -> {
                val position = p.getItemPosition(at)
                if (position.isHeader) {
                    p.getSection(position.section)?.section
                } else {
                    p.getItemForPosition(position)
                } as? T
            }
            else -> null
        }
    }

    override fun notAnonymous(): Boolean {
        return !accountIsAnonumous(view, manager)
    }

    override fun toggleSubscription(at: Int) {
        val item = get<Any>(at) ?: return
        if (accountIsAnonumous(view, manager)) return

        toggleSubscriptionInternal(view, item, manager)
    }

    override fun changeCurrentEpisodeTo(episodeId: Int) {
        main.id = episodeId
    }

    override fun createComment(episodeId: Int, parentId: Int, text: String) {
        service.comment(episodeId, text, if (parentId != -1) parentId else null).enqueue(callback)
    }

    override fun onDataLoaded() {
        view?.let {
            if (repository.data?.size == 0 && repository !is CommentsRepository) {
                it.showNoContent()
            } else {
                it.showContent()
            }
            it.update()
        }
    }

    private val callback = object : ServerCallback<ServerResponse<Comment>>() {
        override fun onSuccess(response: ServerResponse<Comment>, url: String?) {
            val comment = response.data ?: return
            with(commentsProvider) {
                if (comment.parentId == 0) {
                    insertSection(1, Section(comment, true))
                } else {
                    sections.firstOrNull {
                        it.section.section?.id == comment.parentId
                    }?.apply {
                        section.hasFooter = true
                        if (items.size > 0) {
                            items[0] = comment
                        } else {
                            items.add(comment)
                        }
                    }
                }
            }
            view?.update()
        }

        override fun onFailure(error: Message, url: String?) {
            view?.showError(error.msg, true)
        }
    }
}