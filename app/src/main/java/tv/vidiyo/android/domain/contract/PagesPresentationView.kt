package tv.vidiyo.android.domain.contract

import tv.vidiyo.android.base.view.StatePresentationView
import tv.vidiyo.android.base.view.UpdatablePresentationView


interface PagesPresentationView : StatePresentationView, UpdatablePresentationView