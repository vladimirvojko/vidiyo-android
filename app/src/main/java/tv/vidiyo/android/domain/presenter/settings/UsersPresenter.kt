package tv.vidiyo.android.domain.presenter.settings

import tv.vidiyo.android.api.model.user.FollowingUser
import tv.vidiyo.android.api.model.user.SubscriptionRequest
import tv.vidiyo.android.api.model.user.SubscriptionStatus
import tv.vidiyo.android.base.adapter.provider.FilterableDataProvider
import tv.vidiyo.android.base.view.ShowMessagePresentationView
import tv.vidiyo.android.domain.contract.SettingsContract
import tv.vidiyo.android.domain.presenter.DataSourcePresenter
import tv.vidiyo.android.model.NotificationType
import tv.vidiyo.android.source.settings.UsersRepository
import tv.vidiyo.android.util.NotificationsCenter
import tv.vidiyo.android.util.SubscriptionManager

class UsersPresenter(
        v: SettingsContract.Users.View,
        r: UsersRepository,
        private val manager: SubscriptionManager,
        private val center: NotificationsCenter
) : DataSourcePresenter<SettingsContract.Users.View, UsersRepository>(v, r), SettingsContract.Users.Presenter {

    override val provider = FilterableDataProvider<FollowingUser>()
    override val totalCount: Int get() = repository.totalCount

    init {
        r.subscribe(this)
    }

    override fun start() {
        if (repository.isEmpty) {
            view?.setRefreshing(true)
            refresh()

        } else {
            stateView?.let {
                if (repository.size == 0) {
                    it.showNoContent()
                } else {
                    it.showContent()
                }
            }
            additionalPreparation()
        }
    }

    override fun loadMore() = repository.refresh()
    override fun get(at: Int) = provider.items.getOrNull(at)
    override fun accept(at: Int) = action(SubscriptionRequest.accept, at)
    override fun delete(at: Int) = action(SubscriptionRequest.delete, at)

    private fun action(request: SubscriptionRequest, at: Int) {
        val item = get(at) ?: return
        view?.setRefreshing(true)
        manager.setUserSubscription(request, item) { subscriptions, error ->
            view?.setRefreshing(false)
            if (error != null) {
                view?.showMessage(ShowMessagePresentationView.ToastMessage(error))
            } else {
                val first = subscriptions?.first() ?: return@setUserSubscription
                item.backprop = first.status
                if (first.status == SubscriptionStatus.deleted) {
                    provider.remove(item)
                }
                center.refreshPendingStatus(true)
                view?.update()
            }
        }
    }

    override fun onDataLoaded() {
        super.onDataLoaded()
        view?.setRefreshing(false)
        val data = repository.data
        if (data == null || data.isEmpty()) {
            center.pendingRequests = null
            center.notify(NotificationsCenter.ACTION_LOADED, NotificationType.REQUEST_PENDING)

        } else {
            provider.addAll(data.toList(), true)
        }
    }
}