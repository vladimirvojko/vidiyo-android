package tv.vidiyo.android.domain.contract

import android.content.Context
import android.support.annotation.DrawableRes
import tv.vidiyo.android.R
import tv.vidiyo.android.api.model.cast.Cast
import tv.vidiyo.android.api.model.show.SearchingShow
import tv.vidiyo.android.api.model.topic.Topic

interface SearchResultView {
    fun <T> setImage(image: T?, @DrawableRes fallbackResId: Int? = null)
    fun setTitle(title: String?)
    fun setSubtitle(subtitle: String?)
    fun setSubscribersCount(count: Int?)
}

fun SearchResultView.setSearchingShow(show: SearchingShow) {
    setImage(show.image)
    setTitle(show.name)
    setSubtitle(show.channel)
    setSubscribersCount(show.subscribers)
}

fun SearchResultView.setCast(cast: Cast, context: Context) {
    setImage(cast.image, R.drawable.ic_default_profile)
    setTitle(cast.name)
    setSubtitle(context.getString(cast.type.stringId))
    setSubscribersCount(cast.subscribers)
}

fun SearchResultView.setTopic(topic: Topic) {
    setImage(topic.image)
    setTitle(topic.name)
    setSubtitle(topic.channel)
    setSubscribersCount(topic.subscribers)
}