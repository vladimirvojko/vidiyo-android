package tv.vidiyo.android.domain.presenter.channels

import android.annotation.SuppressLint
import tv.vidiyo.android.api.model.Subscribable
import tv.vidiyo.android.api.model.channel.Channel
import tv.vidiyo.android.api.model.show.Show
import tv.vidiyo.android.api.model.topic.Topic
import tv.vidiyo.android.base.adapter.provider.DataProvider
import tv.vidiyo.android.base.adapter.provider.MutableListDataProvider
import tv.vidiyo.android.base.adapter.provider.SectionedListDataProvider
import tv.vidiyo.android.domain.contract.ChannelContract
import tv.vidiyo.android.domain.presenter.DataSourcePresenter
import tv.vidiyo.android.extension.accountIsAnonumous
import tv.vidiyo.android.extension.toggleSubscriptionInternal
import tv.vidiyo.android.model.InnerPageType
import tv.vidiyo.android.source.channels.ChannelRepository
import tv.vidiyo.android.util.SubscriptionManager

class InnerChannelPresenter(
        v: ChannelContract.Inner.View,
        r: ChannelRepository,
        private val manager: SubscriptionManager
) : DataSourcePresenter<ChannelContract.Inner.View, ChannelRepository>(v, r), ChannelContract.Inner.Presenter {

    override val channel: Channel get() = repository.channel
    private lateinit var provider: DataProvider

    @Suppress("UNCHECKED_CAST")
    override fun <T> getItem(at: Int): T? {
        val p = provider
        return when (p) {
            is MutableListDataProvider<*> -> p.items.getOrNull(at)
            is SectionedListDataProvider<*, *> -> {
                val position = p.getItemPosition(at)
                if (position.isHeader) {
                    p.getSection(position.section)?.section
                } else {
                    p.getItemForPosition(position)
                }
            }

            else -> throw IllegalArgumentException("Unsupported provider type")
        } as T?
    }

    @Suppress("UNCHECKED_CAST")
    override fun refresh() {
        val index = view?.index ?: return
        val type = view?.type ?: return

        val p = provider
        when (type) {
            InnerPageType.ALL -> (p as? MutableListDataProvider<Show>)?.addAll(repository.shows.asList(), true)
            InnerPageType.TOPICS -> (p as? SectionedListDataProvider<Topic, Show>)?.let { sp ->
                val p2 = repository.getProvider(index)
                p2.sections.forEachIndexed { index, (section, items) ->
                    sp.setSection(index, section)
                    sp.setSectionItems(index, items)
                }
            }

            InnerPageType.MORE -> (p as? MutableListDataProvider<Topic>)?.addAll(repository.topics.asList(), true)
            else -> throw IllegalArgumentException()
        }
        view?.update()
    }

    @SuppressLint("SwitchIntDef")
    override fun getProvider(@InnerPageType type: Int, index: Int): DataProvider {
        provider = when (type) {
            InnerPageType.ALL -> MutableListDataProvider<Show>().apply {
                addAll(repository.shows.asList(), true)
            }
            InnerPageType.TOPICS -> repository.getProvider(index)
            InnerPageType.MORE -> MutableListDataProvider<Topic>().apply {
                addAll(repository.topics.asList(), true)
            }
            else -> throw IllegalArgumentException()
        }
        return provider
    }

    override fun toggleSubscription(at: Int) {
        val item = getItem<Subscribable>(at) ?: return
        if (accountIsAnonumous(view, manager)) return

        toggleSubscriptionInternal(view, item, manager)
    }
}