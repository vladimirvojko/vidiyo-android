package tv.vidiyo.android.domain.presenter.discover

import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.api.model.Message
import tv.vidiyo.android.api.model.ServerResponse
import tv.vidiyo.android.api.model.social.SocialAccount
import tv.vidiyo.android.api.model.social.SocialType
import tv.vidiyo.android.api.model.user.FollowingUser
import tv.vidiyo.android.api.model.user.SubscriptionRequest
import tv.vidiyo.android.base.BasePresenter
import tv.vidiyo.android.base.adapter.provider.MutableListDataProvider
import tv.vidiyo.android.base.view.ShowMessagePresentationView
import tv.vidiyo.android.domain.contract.DiscoverContract
import tv.vidiyo.android.extension.ServerCallback
import tv.vidiyo.android.model.Contact
import tv.vidiyo.android.model.uploadContacts
import tv.vidiyo.android.util.SocialManager
import tv.vidiyo.android.util.SubscriptionManager

class DiscoveredPresenter(
        v: DiscoverContract.Discovered.View,
        private val service: VidiyoService,
        val subscriptionManager: SubscriptionManager,
        private val socialManager: SocialManager
) : BasePresenter<DiscoverContract.Discovered.View>(v), DiscoverContract.Discovered.Presenter {

    companion object {
        private const val TYPE_FACEBOOK = 0
        private const val TYPE_CONTACTS = 1
    }

    override val provider = MutableListDataProvider<FollowingUser>()

    override fun start() {
        val v = view ?: return
        val type = v.type

        if (v.connected && type == TYPE_CONTACTS) {
            service.contactFriends().enqueue(usersCallback)

        } else when (type) {
            TYPE_FACEBOOK -> socialManager.requestToken(SocialAccount(SocialType.facebook), false, completion = { _, t ->
                service.fbFriends(t).enqueue(usersCallback)

            }, failure = { error -> view?.showMessage(ShowMessagePresentationView.ToastMessage(error)) })

            TYPE_CONTACTS -> view?.apply {
                if (permissionGranted()) initLoader() else requestPermissions()
            }
        }
    }

    override fun get(at: Int): FollowingUser? = provider.items.getOrNull(at)

    override fun follow(at: Int) {
        val user = get(at) ?: return
        view?.setRefreshing(true)
        subscriptionManager.setUserSubscription(SubscriptionRequest.follow, user) { subscriptions, error ->
            view?.setRefreshing(false)
            if (error == null) {
                val first = subscriptions?.first() ?: return@setUserSubscription
                user.status = first.status
                view?.update()

            } else {
                view?.showMessage(ShowMessagePresentationView.ToastMessage(error))
            }
        }
    }

    override fun followAll() {
        val list = provider.items.let { it.subList(1, it.size) }
        view?.setRefreshing(true)
        subscriptionManager.setUsersSubscription(SubscriptionRequest.follow, list.toTypedArray()) { subscriptions, error ->
            view?.setRefreshing(false)
            if (error == null && subscriptions != null) {
                for (item in provider.items) {
                    val subscription = subscriptions.find { it.followingId == item.id } ?: continue
                    item.status = subscription.status
                }
                view?.update()

            } else view?.showMessage(ShowMessagePresentationView.ToastMessage(error))
        }
    }

    override fun disconnect() {
        val v = view ?: return
        v.setRefreshing(true)
        val call = if (v.type == TYPE_CONTACTS) service.disconnectContact() else service.disconnectFacebook()
        call.enqueue(disconnectCallback)
    }

    override fun onContactsLoaded(list: List<Contact>) = service.uploadContacts(list).enqueue(usersCallback)

    private val usersCallback = object : ServerCallback<ServerResponse<Array<FollowingUser>>>() {
        override fun onSuccess(response: ServerResponse<Array<FollowingUser>>, url: String?) {
            val data = response.data
            if (data != null && data.isNotEmpty()) {
                val list = mutableListOf(FollowingUser()).also { it.addAll(data) }
                provider.addAll(list, true)
                view?.showContent()

            } else {
                view?.showNoContent()
            }
        }

        override fun onFailure(error: Message, url: String?) {
            view?.showError(error.msg)
        }
    }

    private val disconnectCallback = object : ServerCallback<ServerResponse<Message>>() {
        override fun onSuccess(response: ServerResponse<Message>, url: String?) {
            view?.setRefreshing(false)
            view?.onBackPressed()
        }

        override fun onFailure(error: Message, url: String?) {
            view?.setRefreshing(false)
            view?.showMessage(ShowMessagePresentationView.ToastMessage(error.msg))
        }
    }
}