package tv.vidiyo.android.domain.presenter.settings

import com.google.firebase.iid.FirebaseInstanceId
import tv.vidiyo.android.App
import tv.vidiyo.android.account.UserAccountManager
import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.api.model.Message
import tv.vidiyo.android.api.model.ServerResponse
import tv.vidiyo.android.api.model.user.UserAccount
import tv.vidiyo.android.base.BasePresenter
import tv.vidiyo.android.domain.contract.SettingsContract
import tv.vidiyo.android.extension.ServerCallback
import tv.vidiyo.android.util.SearchManager

class MainSettingsPresenter(
        v: SettingsContract.Main.View,
        private val manager: UserAccountManager,
        private val service: VidiyoService,
        private val searchManager: SearchManager
) : BasePresenter<SettingsContract.Main.View>(v), SettingsContract.Main.Presenter {

    private var tempPrivacy = false
    override var account: UserAccount = manager.account ?: throw IllegalStateException("Account can't be null")

    override fun togglePrivacy(isPrivate: Boolean) {
        tempPrivacy = isPrivate
        service.togglePrivacy(isPrivate.toString()).enqueue(callback)
        view?.setRefreshing(true)
    }

    override fun logout() {
        manager.remove(object : UserAccountManager.AccountRemoveCallback {
            override fun onSuccess() {
                with(App.graph) {
                    destroyDataComponent()
                    applicationComponent.provideSubscriptionManager().reset()
                }

                Thread(Runnable {
                    FirebaseInstanceId.getInstance().deleteInstanceId()
                    view?.startSplashActivity()
                }).start()
            }

            override fun onFailure() {
                // TODO("not implemented")
            }
        })
    }

    override fun clearSearch() = searchManager.clear()

    private val callback = object : ServerCallback<ServerResponse<Boolean>>() {
        override fun onSuccess(response: ServerResponse<Boolean>, url: String?) {
            view?.setRefreshing(false)

            account.isPrivate = tempPrivacy
            manager.updateAccountData(account)
        }

        override fun onFailure(error: Message, url: String?) {
            view?.setRefreshing(false)
        }
    }
}