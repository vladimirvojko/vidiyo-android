package tv.vidiyo.android.domain.presenter.boarding

import android.content.Context
import android.os.Bundle
import android.support.v4.app.LoaderManager
import android.support.v4.content.Loader
import retrofit2.Call
import tv.vidiyo.android.R
import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.api.model.Message
import tv.vidiyo.android.api.model.ServerResponse
import tv.vidiyo.android.api.model.user.ContactFriends
import tv.vidiyo.android.api.model.user.Friend
import tv.vidiyo.android.base.BasePresenter
import tv.vidiyo.android.base.view.ShowMessagePresentationView.ToastMessage
import tv.vidiyo.android.domain.contract.SignUpContract.FindFriends
import tv.vidiyo.android.extension.ServerCallback
import tv.vidiyo.android.loader.ContactsLoader
import tv.vidiyo.android.model.Contact
import tv.vidiyo.android.model.findContacts
import tv.vidiyo.android.model.groupContacts
import tv.vidiyo.android.source.FriendsRepository

class FindFriendsPresenter(
        v: FindFriends.View,
        private val repository: FriendsRepository,
        private val service: VidiyoService,
        private val loaderManager: LoaderManager,
        context: Context
) : BasePresenter<FindFriends.View>(v), FindFriends.Presenter, LoaderManager.LoaderCallbacks<List<Contact>> {

    private val loader: Loader<List<Contact>> = ContactsLoader(context)
    private var call: Call<ServerResponse<ContactFriends>>? = null

    override fun findContactFriends() {
        if (repository.contacts == null) {
            loaderManager.initLoader(0, null, this).forceLoad()
        } else {
            loaderManager.getLoader<Any>(0)?.let { loaderManager.destroyLoader(0) }
            loaderManager.restartLoader(0, null, this).forceLoad()
        }
    }

    override fun findFacebookFriends(token: String) {
        view?.setRefreshing(true)
        service.facebookFriends(token).enqueue(facebookCallback)
    }

    override fun findGoogleFriends() {
        view?.showMessage(ToastMessage("not implemented yet"))
    }

    override fun onLoadFinished(loader: Loader<List<Contact>>?, data: List<Contact>?) {
        repository.contacts = data

        if (data != null && !data.isEmpty()) {
            call?.apply { if (!isExecuted) cancel() }
            call = service.findContacts(data).apply { enqueue(contactsCallback) }
        } else {
            view?.setRefreshing(false)
            view?.showMessage(ToastMessage(R.string.message_no_contacts_found))
        }
    }

    override fun onLoaderReset(loader: Loader<List<Contact>>?) {
        repository.contacts = null

        view?.setRefreshing(false)
    }

    override fun onCreateLoader(id: Int, args: Bundle?) = loader.apply { view?.setRefreshing(true) }

    override fun destroyLoader() {
        loaderManager.destroyLoader(0)
    }

    private val contactsCallback = object : ServerCallback<ServerResponse<ContactFriends>>() {
        override fun onSuccess(response: ServerResponse<ContactFriends>, url: String?) {
            view?.setRefreshing(false)

            val contacts = repository.contacts ?: return; response.data ?: return

            repository.friends = groupContacts(contacts, response.data).also {
                view?.startFriendsList()
            }
        }

        override fun onFailure(error: Message, url: String?) {
            view?.setRefreshing(false)
            view?.showMessage(ToastMessage(error.msg))
        }
    }

    private val facebookCallback = object : ServerCallback<ServerResponse<Array<Friend>>>() {
        override fun onSuccess(response: ServerResponse<Array<Friend>>, url: String?) {
            view?.setRefreshing(false)
            val data = response.data

            repository.friends = data?.toList()
            if (data != null && data.isNotEmpty()) {
                view?.startFriendsList()

            } else {
                view?.showMessage(ToastMessage(R.string.message_no_friends_found))
            }
        }

        override fun onFailure(error: Message, url: String?) {
            view?.setRefreshing(false)
            view?.showMessage(ToastMessage(error.msg))
        }
    }
}