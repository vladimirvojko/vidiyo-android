package tv.vidiyo.android.domain.presenter.episode

import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.api.model.Message
import tv.vidiyo.android.api.model.ServerResponse
import tv.vidiyo.android.api.model.episode.EpisodeInfo
import tv.vidiyo.android.api.model.episode.ViewInfo
import tv.vidiyo.android.base.BasePresenter
import tv.vidiyo.android.base.view.ShowMessagePresentationView
import tv.vidiyo.android.domain.contract.EpisodeContract
import tv.vidiyo.android.extension.ServerCallback
import tv.vidiyo.android.extension.accountIsAnonumous
import tv.vidiyo.android.source.BaseDataSource
import tv.vidiyo.android.source.episode.EpisodeRepository
import tv.vidiyo.android.util.SubscriptionManager

class EpisodePlayerPresenter(
        v: EpisodeContract.Player.View,
        val repository: EpisodeRepository,
        private val service: VidiyoService,
        private val manager: SubscriptionManager,
        private val notifier: EpisodeContract.Notifier
) : BasePresenter<EpisodeContract.Player.View>(v), EpisodeContract.Player.Presenter, BaseDataSource.Callback {

    init {
        repository.subscribe(this)
    }

    override val episode: EpisodeInfo? get() = repository.data

    override fun start() {
        if (repository.isEmpty) {
            repository.refresh()

        } else {
            onDataLoaded()
        }
    }

    override fun getProduct(at: Int) = repository.productsProvider.items.getOrNull(at)

    override fun updateWithActualState() {
        view?.showLikeViewAndUpdate(repository.data?.isFavorite ?: return, false)
    }

    override fun canShare(): Boolean {
        return !accountIsAnonumous(view, manager)
    }

    override fun toggleLike() {
        val info = repository.data ?: return
        if (accountIsAnonumous(view, manager)) return

        val favorite = !manager.isLiked(info)
        view?.showLikeViewAndUpdate(favorite)
        manager.favorite(info, favorite) { _, subscribed, error ->
            if (error != null) {
                view?.showMessage(ShowMessagePresentationView.ToastMessage(error))
            } else {
                info.isFavorite = subscribed
            }
            view?.showLikeViewAndUpdate(info.isFavorite, false)
        }
    }

    override fun setViewTime(position: Long) {
        service.setEpisode(episode?.id ?: return, position / 1000f).enqueue(callback)
    }

    override fun onDataLoaded() {
        val info = episode ?: return

        view?.load(info)
        view?.showLikeViewAndUpdate(info.isFavorite, false)
    }

    override fun onDataNotAvailable(error: Message) {
        // TODO
    }

    private val callback = object : ServerCallback<ServerResponse<ViewInfo>>() {
        override fun onSuccess(response: ServerResponse<ViewInfo>, url: String?) {
            val viewInfo = response.data ?: return
            notifier.send(viewInfo)
        }
    }
}