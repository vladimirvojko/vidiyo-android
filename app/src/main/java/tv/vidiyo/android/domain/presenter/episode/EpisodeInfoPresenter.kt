package tv.vidiyo.android.domain.presenter.episode

import tv.vidiyo.android.api.model.Message
import tv.vidiyo.android.api.model.episode.Product
import tv.vidiyo.android.base.BasePresenter
import tv.vidiyo.android.base.view.ShowMessagePresentationView
import tv.vidiyo.android.domain.contract.EpisodeContract
import tv.vidiyo.android.extension.accountIsAnonumous
import tv.vidiyo.android.source.BaseDataSource
import tv.vidiyo.android.source.episode.EpisodeRepository
import tv.vidiyo.android.util.SubscriptionManager

class EpisodeInfoPresenter(
        v: EpisodeContract.Info.View,
        val repository: EpisodeRepository,
        private val manager: SubscriptionManager
) : BasePresenter<EpisodeContract.Info.View>(v), EpisodeContract.Info.Presenter, BaseDataSource.Callback {

    private var needForceUpdate = false

    init {
        repository.subscribe(this)
    }

    override fun getProduct(at: Int): Product? = repository.productsProvider.items.getOrNull(at)

    override fun start() {
        if (repository.isEmpty) {
            repository.refresh()

        } else {
            onDataLoaded()
        }
    }

    override fun toggleShowSubscription() {
        val show = repository.data?.showInfo ?: return
        if (accountIsAnonumous(view, manager)) return

        view?.setRefreshing(true)
        manager.setSubscription(show, !manager.isSubscribed(show)) { _, subscribed, error ->
            view?.setRefreshing(false)
            if (error != null) {
                view?.showMessage(ShowMessagePresentationView.ToastMessage(error))
            } else {
                show.subscribers += if (subscribed) 1 else -1
                show.isSubscribed = subscribed
                view?.updateShowView(show)
            }
        }
    }

    override fun onDataLoaded() {
        val info = repository.data ?: return

        view?.updateShowView(info.showInfo)
        view?.updateDescription(info)
        if (needForceUpdate) {
            view?.forceUpdateInnerViews()
        }
        needForceUpdate = true
    }

    override fun onDataNotAvailable(error: Message) {
        // TODO
    }
}