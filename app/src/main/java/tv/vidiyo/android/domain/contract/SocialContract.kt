package tv.vidiyo.android.domain.contract

import tv.vidiyo.android.api.Dateable
import tv.vidiyo.android.api.Openable
import tv.vidiyo.android.base.adapter.provider.MutableListDataProvider
import tv.vidiyo.android.base.view.ShowMessagePresentationView
import tv.vidiyo.android.base.view.StatePresentationView
import tv.vidiyo.android.base.view.UpdatablePresentationView

interface SocialContract {
    interface View : StatePresentationView, ShowMessagePresentationView, UpdatablePresentationView

    interface Presenter {
        val provider: MutableListDataProvider<Dateable>
        fun get(at: Int): Openable?
        fun load()
    }
}