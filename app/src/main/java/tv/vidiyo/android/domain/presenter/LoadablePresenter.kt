package tv.vidiyo.android.domain.presenter

import tv.vidiyo.android.source.Loadable

interface LoadablePresenter : Loadable {
    fun loadMore()
}