package tv.vidiyo.android.domain.presenter.settings

import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.api.model.Message
import tv.vidiyo.android.api.model.ServerResponse
import tv.vidiyo.android.api.model.social.SocialAccount
import tv.vidiyo.android.api.model.social.SocialStatus
import tv.vidiyo.android.api.model.social.SocialType
import tv.vidiyo.android.base.BasePresenter
import tv.vidiyo.android.base.adapter.provider.MutableListDataProvider
import tv.vidiyo.android.base.view.ShowMessagePresentationView
import tv.vidiyo.android.domain.contract.SettingsContract
import tv.vidiyo.android.extension.ServerCallback
import tv.vidiyo.android.source.profile.ProfileRepository
import tv.vidiyo.android.util.SocialManager

class SocialAccountPresenter(
        v: SettingsContract.Socials.View,
        private val manager: SocialManager,
        private val service: VidiyoService,
        private val repository: ProfileRepository
) : BasePresenter<SettingsContract.Socials.View>(v), SettingsContract.Socials.Presenter {

    override val provider = MutableListDataProvider<SocialAccount>().apply {
        val socials = repository.data?.socials
        val block: (SocialAccount) -> Unit = { account ->
            socials?.find { it.socialType == account.socialType }?.let { account.apply(it) }
        }

        addAll(listOf(
                SocialAccount(0, 0, null, SocialType.facebook, SocialStatus.none).also(block),
                SocialAccount(0, 0, null, SocialType.twitter, SocialStatus.none).also(block),
                SocialAccount(0, 0, null, SocialType.google, SocialStatus.none).also(block)
        ), true)
    }

    override fun start() {
        view?.setRefreshing(true)
        service.socials().enqueue(callback)
    }

    override fun connect(at: Int) {
        val account = provider.items[at]
        view?.setRefreshing(true)
        manager.connect(account, !account.isConnected, completion = { a, e ->
            view?.setRefreshing(false)
            if (a != null) {
                replace(a)
                repository.data?.socials = provider.items.toTypedArray()
                view?.update()
            } else if (e != null) {
                view?.showMessage(ShowMessagePresentationView.ToastMessage(e.msg))
            }

        }, failure = { error ->
            view?.setRefreshing(false)
            error?.let { view?.showMessage(ShowMessagePresentationView.ToastMessage(it)) }
        })
    }

    private fun replace(account: SocialAccount) {
        provider.items.firstOrNull {
            it.socialType == account.socialType
        }?.apply(account)
    }

    private val callback = object : ServerCallback<ServerResponse<Array<SocialAccount>>>() {
        override fun onSuccess(response: ServerResponse<Array<SocialAccount>>, url: String?) {
            view?.setRefreshing(false)
            val array = response.data ?: return
            array.forEach(this@SocialAccountPresenter::replace)

            repository.data?.socials = array
            view?.update()
        }

        override fun onFailure(error: Message, url: String?) {
            view?.setRefreshing(false)
            view?.showMessage(ShowMessagePresentationView.ToastMessage(error.msg))
        }
    }
}