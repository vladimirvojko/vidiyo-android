package tv.vidiyo.android.domain.contract

import tv.vidiyo.android.api.model.episode.Episode
import tv.vidiyo.android.api.model.episode.ViewInfo
import tv.vidiyo.android.api.model.show.Show
import tv.vidiyo.android.base.view.StatePresentationView
import tv.vidiyo.android.base.view.UpdatablePresentationView
import tv.vidiyo.android.domain.presenter.LoadablePresenter
import tv.vidiyo.android.source.watching.InnerWatchingRepository

interface WatchingContract {

    interface Inner {
        interface View : StatePresentationView, UpdatablePresentationView {
            val isResume: Boolean
        }

        interface Presenter : LoadablePresenter {
            val repository: InnerWatchingRepository<*>

            fun getShow(at: Int): Show?
            fun getEpisode(at: Int): Episode?
            fun findInfo(id: Int): ViewInfo?
            fun removeById(id: Int): Boolean

            fun remove(position: Int)
            fun share(position: Int)
        }
    }
}