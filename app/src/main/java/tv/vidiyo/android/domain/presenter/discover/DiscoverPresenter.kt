package tv.vidiyo.android.domain.presenter.discover

import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.api.model.DiscoveredPeople
import tv.vidiyo.android.api.model.Message
import tv.vidiyo.android.api.model.ServerResponse
import tv.vidiyo.android.api.model.user.SubscriptionRequest
import tv.vidiyo.android.api.model.user.SubscriptionStatus
import tv.vidiyo.android.api.model.user.SuggestedUser
import tv.vidiyo.android.base.BasePresenter
import tv.vidiyo.android.base.adapter.provider.FilterableDataProvider
import tv.vidiyo.android.base.view.ShowMessagePresentationView
import tv.vidiyo.android.domain.contract.DiscoverContract
import tv.vidiyo.android.extension.ServerCallback
import tv.vidiyo.android.util.SocialManager
import tv.vidiyo.android.util.SubscriptionManager

@Suppress("UNCHECKED_CAST")
class DiscoverPresenter(
        v: DiscoverContract.View,
        private val service: VidiyoService,
        private val subscriptionManager: SubscriptionManager,
        private val socialManager: SocialManager
) : BasePresenter<DiscoverContract.View>(v), DiscoverContract.Presenter {

    override val provider = FilterableDataProvider<Any>()

    override fun start() {
        view?.showProgress()
        service.discoverPeople(socialManager.facebookToken).enqueue(callback)
    }

    override fun <T> get(at: Int) = provider.items[at] as? T

    override fun hide(at: Int) = action(SubscriptionRequest.hide, at)
    override fun follow(at: Int) = action(SubscriptionRequest.follow, at)

    private fun action(request: SubscriptionRequest, at: Int) {
        val suggested = get<SuggestedUser>(at) ?: return
        val item = suggested.toFollowingUser()

        view?.setRefreshing(true)
        subscriptionManager.setUserSubscription(request, item) { subscriptions, error ->
            view?.setRefreshing(false)
            if (error != null) {
                view?.showMessage(ShowMessagePresentationView.ToastMessage(error))
            } else {
                val first = subscriptions?.first() ?: return@setUserSubscription
                suggested.status = first.status
                if (first.status == SubscriptionStatus.hidden) {
                    provider.remove(item)
                }

                view?.update()
            }
        }
    }

    private val callback = object : ServerCallback<ServerResponse<DiscoveredPeople>>() {
        override fun onSuccess(response: ServerResponse<DiscoveredPeople>, url: String?) {
            view?.showContent()
            val data = response.data ?: return
            val items = listOf<Any>(data.facebook, data.contacts) + data.suggestions
            provider.addAll(items, true)
            view?.update()
        }

        override fun onFailure(error: Message, url: String?) {
            view?.showError(error.msg)
        }
    }
}