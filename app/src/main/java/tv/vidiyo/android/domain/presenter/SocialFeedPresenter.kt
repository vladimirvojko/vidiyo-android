package tv.vidiyo.android.domain.presenter

import tv.vidiyo.android.api.Dateable
import tv.vidiyo.android.api.Openable
import tv.vidiyo.android.api.model.Message
import tv.vidiyo.android.base.BasePresenter
import tv.vidiyo.android.base.adapter.provider.MutableListDataProvider
import tv.vidiyo.android.domain.contract.SocialContract
import tv.vidiyo.android.extension.weak
import tv.vidiyo.android.source.BaseDataSource
import tv.vidiyo.android.source.social.SocialFeedRepository

class SocialFeedPresenter(v: SocialContract.View, private val repository: SocialFeedRepository)
    : BasePresenter<SocialContract.View>(v), SocialContract.Presenter, BaseDataSource.Callback {

    override val provider: MutableListDataProvider<Dateable> get() = repository.mainProvider

    init {
        repository.listener = this.weak()
    }

    override fun start() {
        repository.start()
    }

    override fun get(at: Int): Openable? = provider.items.getOrNull(at) as? Openable

    override fun load() {
        repository.load()
    }

    override fun onDataLoaded() {
        if (provider.items.isEmpty()) {
            view?.showNoContent()

        } else {
            view?.showContent()
            view?.update()
        }
    }

    override fun onDataNotAvailable(error: Message) {
        // TODO("not implemented")
    }
}