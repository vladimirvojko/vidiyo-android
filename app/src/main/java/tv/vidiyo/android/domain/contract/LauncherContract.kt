package tv.vidiyo.android.domain.contract

import tv.vidiyo.android.base.view.PresentationView

interface LauncherContract {
    interface View : PresentationView, SignInPresentationView {
        fun startSplash()
    }

    interface Presenter {
        fun loginWith(token: String)
    }
}