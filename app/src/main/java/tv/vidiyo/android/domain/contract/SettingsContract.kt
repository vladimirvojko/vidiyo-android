package tv.vidiyo.android.domain.contract

import tv.vidiyo.android.api.model.NotificationsSettings
import tv.vidiyo.android.api.model.social.SocialAccount
import tv.vidiyo.android.api.model.user.FollowingType
import tv.vidiyo.android.api.model.user.FollowingUser
import tv.vidiyo.android.api.model.user.UserAccount
import tv.vidiyo.android.base.adapter.provider.FilterableDataProvider
import tv.vidiyo.android.base.adapter.provider.MutableListDataProvider
import tv.vidiyo.android.base.view.PresentationView
import tv.vidiyo.android.base.view.RefreshablePresentationView
import tv.vidiyo.android.base.view.ShowMessagePresentationView
import tv.vidiyo.android.base.view.UpdatablePresentationView
import tv.vidiyo.android.domain.presenter.LoadablePresenter

interface SettingsContract {

    interface Main {
        interface View : PresentationView, RefreshablePresentationView {
            fun startSplashActivity()
        }

        interface Presenter {
            var account: UserAccount

            fun togglePrivacy(isPrivate: Boolean)
            fun clearSearch()
            fun logout()
        }
    }

    interface Edit {
        interface View : PresentationView, RefreshablePresentationView, ShowMessagePresentationView {
            fun updateWith(account: UserAccount)
            fun updateLocation(with: String)
        }

        interface Presenter {
            fun save()
            fun updateField(id: Int, value: String?)
        }
    }

    interface Password {
        interface View : PresentationView, RefreshablePresentationView, ShowMessagePresentationView
        interface Presenter {
            fun validate(current: String, new: String, confirm: String): Boolean
            fun change(current: String, new: String, confirm: String)
        }
    }

    interface Users {
        interface View : PresentationView, UpdatablePresentationView, RefreshablePresentationView, ShowMessagePresentationView {
            val type: FollowingType
            val userId: Int
        }

        interface Presenter : LoadablePresenter {
            val provider: FilterableDataProvider<FollowingUser>
            fun get(at: Int): FollowingUser?
            fun accept(at: Int)
            fun delete(at: Int)
        }
    }

    interface Socials {
        interface View : PresentationView, UpdatablePresentationView, RefreshablePresentationView, ShowMessagePresentationView
        interface Presenter {
            val provider: MutableListDataProvider<SocialAccount>
            fun connect(at: Int)
        }
    }

    interface Notification {
        interface View : PresentationView, RefreshablePresentationView, ShowMessagePresentationView, UpdatablePresentationView
        interface Presenter {
            val data: NotificationsSettings?
            val ignore: Boolean
            fun check(groupIndex: Int, index: Int)
            fun endIgnoring()
        }
    }
}