package tv.vidiyo.android.domain.contract

import android.content.Intent
import tv.vidiyo.android.App
import tv.vidiyo.android.R
import tv.vidiyo.android.account.UserAccountManager
import tv.vidiyo.android.api.model.user.UserAccount
import tv.vidiyo.android.base.fragment.BaseFragment
import tv.vidiyo.android.extension.startActivity
import tv.vidiyo.android.ui.MainActivity

interface SignInPresentationView {
    fun showError(text: String?, code: Int)
    fun startMain()
}

fun <T> T.startMainImpl() where T : BaseFragment, T : SignInPresentationView {
    App.graph.destroyDataComponent()
    App.graph.applicationComponent.provideSubscriptionManager().refreshUser()
    activity.startActivity<MainActivity>(flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
}

fun checkAndCreateAccount(manager: UserAccountManager, data: UserAccount?, view: SignInPresentationView?) {
    if (data != null && manager.addAccount(data)) {
        view?.startMain()

    } else {
        view?.showError(null, R.string.error_cant_create_account)
    }
}