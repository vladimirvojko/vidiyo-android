package tv.vidiyo.android.domain.contract

import tv.vidiyo.android.api.model.cast.Cast
import tv.vidiyo.android.api.model.cast.CastInfo
import tv.vidiyo.android.base.RefreshablePresenter
import tv.vidiyo.android.base.view.*
import tv.vidiyo.android.domain.presenter.LoadablePresenter
import tv.vidiyo.android.source.cast.CastRepository

interface CastContract {

    interface View : PresentationView, RefreshablePresentationView, ShowMessagePresentationView, NeedRegistrationView, HasPages {
        val cast: Cast
        fun updateCastInfo(info: CastInfo)
    }

    interface Presenter : RefreshablePresenter {
        fun toggleSubscription()
    }

    interface Inner {
        interface View : StatePresentationView, UpdatablePresentationView, RefreshablePresentationView,
                ShowMessagePresentationView, NeedRegistrationView, SelectableView {
            var type: Int
        }

        interface Presenter : RefreshablePresenter, LoadablePresenter {
            val main: CastRepository

            fun toggleSubscription(at: Int)
            fun <T> get(at: Int): T?
        }
    }
}