package tv.vidiyo.android.domain.contract

import tv.vidiyo.android.api.model.user.FollowingUser
import tv.vidiyo.android.base.adapter.provider.MutableListDataProvider
import tv.vidiyo.android.base.view.*
import tv.vidiyo.android.model.Contact

interface DiscoverContract {
    interface View : PresentationView, UpdatablePresentationView, RefreshablePresentationView, ShowMessagePresentationView, StatePresentationView
    interface Presenter {
        val provider: MutableListDataProvider<Any>

        fun <T> get(at: Int): T?
        fun hide(at: Int)
        fun follow(at: Int)
    }

    interface Discovered {
        interface View : PresentationView, UpdatablePresentationView, RefreshablePresentationView,
                ShowMessagePresentationView, StatePresentationView {
            val type: Int
            val connected: Boolean

            fun onBackPressed()
            fun initLoader()
            fun permissionGranted(): Boolean
            fun requestPermissions()
        }

        interface Presenter {
            val provider: MutableListDataProvider<FollowingUser>

            fun disconnect()
            fun onContactsLoaded(list: List<Contact>)
            fun follow(at: Int)
            fun followAll()
            fun get(at: Int): FollowingUser?
        }
    }
}