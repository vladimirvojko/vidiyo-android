package tv.vidiyo.android.domain.presenter

import retrofit2.Call
import retrofit2.Response
import tv.vidiyo.android.R
import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.api.model.Message
import tv.vidiyo.android.api.model.ServerResponse
import tv.vidiyo.android.api.model.ShareEpisode
import tv.vidiyo.android.api.model.social.SocialToken
import tv.vidiyo.android.api.model.social.SocialType
import tv.vidiyo.android.base.BasePresenter
import tv.vidiyo.android.base.view.ShowMessagePresentationView
import tv.vidiyo.android.domain.contract.ShareContract
import tv.vidiyo.android.extension.ServerCallback
import tv.vidiyo.android.util.SocialManager

class SharePresenter(
        v: ShareContract.View,
        private val service: VidiyoService,
        private val manager: SocialManager
) : BasePresenter<ShareContract.View>(v), ShareContract.Presenter {

    private val tokens = hashSetOf<SocialToken>()

    override fun connect(id: Int) {
        val type = getSocialType(id)
        tokens.find { it.type == type }?.let {
            tokens.remove(it)
            view?.changeSocialState(id, false)

        } ?: manager.requestSocialToken(getSocialType(id), completion = { token ->
            tokens.remove(token)
            tokens.add(token)
            view?.changeSocialState(id, true)

        }, failure = { view?.showMessage(ShowMessagePresentationView.ToastMessage(it)) })
    }

    private fun getSocialType(id: Int) = when (id) {
        R.id.action_facebook -> SocialType.facebook
        R.id.action_twitter -> SocialType.twitter

        else -> throw IllegalArgumentException("Unknown type for id $id")
    }

    override fun send(text: String) {
        val id = view?.episode?.id ?: return
        val share = ShareEpisode(text, tokens.toTypedArray())
        service.shareEpisode(id, share).enqueue(callback)
    }

    private val callback = object : ServerCallback<ServerResponse<Unit>>() {
        override fun onResponse(call: Call<ServerResponse<Unit>>?, response: Response<ServerResponse<Unit>>?) {
            view?.dismiss()
        }

        override fun onFailure(error: Message, url: String?) {
            view?.showMessage(ShowMessagePresentationView.ToastMessage(error.msg))
        }
    }
}