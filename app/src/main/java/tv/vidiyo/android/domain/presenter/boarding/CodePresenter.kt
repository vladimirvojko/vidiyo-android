package tv.vidiyo.android.domain.presenter.boarding

import tv.vidiyo.android.api.ApiKey
import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.api.model.Message
import tv.vidiyo.android.api.model.ServerResponse
import tv.vidiyo.android.base.BasePresenter
import tv.vidiyo.android.domain.contract.SignUpContract
import tv.vidiyo.android.extension.ServerCallback
import tv.vidiyo.android.extension.has

class CodePresenter(
        v: SignUpContract.Code.View,
        private val service: VidiyoService
) : BasePresenter<SignUpContract.Code.View>(v), SignUpContract.Code.Presenter {

    override fun verifyCode(code: String, phone: String) {
        view?.setRefreshing(true)
        service.validateCode(code, phone).enqueue(callback)
    }

    override fun resend(phone: String) {
        view?.setRefreshing(true)
        service.createPhone(phone).enqueue(callback)
    }

    private val callback = object : ServerCallback<ServerResponse<Unit>>() {
        override fun onSuccess(response: ServerResponse<Unit>, url: String?) {
            view?.setRefreshing(false)
            if (url.has(ApiKey.code)) {
                view?.startNameEnter()
            }
        }

        override fun onFailure(error: Message, url: String?) {
            view?.setRefreshing(false)
            if (url.has(ApiKey.code)) {
                view?.showError(error.msg)
            }
        }
    }
}