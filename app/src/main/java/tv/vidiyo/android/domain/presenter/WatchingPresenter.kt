package tv.vidiyo.android.domain.presenter

import tv.vidiyo.android.api.model.episode.Episode
import tv.vidiyo.android.api.model.episode.ViewInfo
import tv.vidiyo.android.api.model.show.NewShowEpisode
import tv.vidiyo.android.api.model.show.Show
import tv.vidiyo.android.domain.contract.WatchingContract
import tv.vidiyo.android.source.watching.InnerWatchingRepository
import tv.vidiyo.android.source.watching.WatchingRepository
import tv.vidiyo.android.util.ALog

class WatchingPresenter(
        v: WatchingContract.Inner.View,
        val main: WatchingRepository
) : DataSourcePresenter<WatchingContract.Inner.View, InnerWatchingRepository<*>>(v, main.getRepository(v.isResume)), WatchingContract.Inner.Presenter {

    init {
        repository.subscribe(this)
    }

    override val totalCount: Int get() = repository.totalCount

    override fun getShow(at: Int): Show? {
        val any = repository.provider.items.getOrNull(at)
        return when (any) {
            is ViewInfo -> any.episode?.show
            is NewShowEpisode -> any.show
            else -> null
        }
    }

    override fun loadMore() = repository.refresh()

    override fun getEpisode(at: Int): Episode? {
        val any = repository.provider.items.getOrNull(at)
        return when (any) {
            is ViewInfo -> any.episode
            is NewShowEpisode -> any.episode
            else -> null
        }
    }

    override fun findInfo(id: Int): ViewInfo? = repository.provider.items.find {
        it is ViewInfo && it.id == id
    } as? ViewInfo

    override fun removeById(id: Int): Boolean = repository.provider.mutableList.remove(findInfo(id))

    override fun remove(position: Int) {
        // TODO("not implemented")
        ALog.d("not implemented")
    }

    override fun share(position: Int) {
        // TODO("not implemented")
        ALog.d("not implemented")
    }
}