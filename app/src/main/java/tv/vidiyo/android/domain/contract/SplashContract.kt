package tv.vidiyo.android.domain.contract

import tv.vidiyo.android.base.view.PresentationView
import tv.vidiyo.android.base.view.RefreshablePresentationView
import tv.vidiyo.android.base.view.ShowMessagePresentationView

interface SplashContract {
    interface View : PresentationView, SignInPresentationView, RefreshablePresentationView, ShowMessagePresentationView
    interface Presenter {
        fun registerAnonymous()
    }
}