package tv.vidiyo.android.domain.presenter.topic

import tv.vidiyo.android.api.model.channel.Channel
import tv.vidiyo.android.api.model.show.Show
import tv.vidiyo.android.base.adapter.provider.MutableListDataProvider
import tv.vidiyo.android.domain.contract.TopicContract
import tv.vidiyo.android.domain.presenter.DataSourcePresenter
import tv.vidiyo.android.extension.accountIsAnonumous
import tv.vidiyo.android.extension.toggleSubscriptionInternal
import tv.vidiyo.android.source.topic.TopicRepository
import tv.vidiyo.android.util.SubscriptionManager

class InnerTopicPresenter(
        v: TopicContract.Inner.View,
        r: TopicRepository,
        private val manager: SubscriptionManager
) : DataSourcePresenter<TopicContract.Inner.View, TopicRepository>(v, r), TopicContract.Inner.Presenter {

    override var provider: MutableListDataProvider<Show>? = null
    override val channel: Channel? get() = repository.channel

    override fun get(at: Int) = provider?.items?.getOrNull(at)

    override fun toggleSubscription(at: Int) {
        val item = get(at) ?: return
        if (accountIsAnonumous(view, manager)) return

        toggleSubscriptionInternal(view, item, manager)
    }

    override fun refresh() {
        val index = view?.index ?: return
        provider?.addAll(repository.getProvider(index).items, true)
        view?.update()
    }

    override fun getProvider(index: Int): MutableListDataProvider<Show>
            = repository.getProvider(index).also { provider = it }
}