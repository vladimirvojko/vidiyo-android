package tv.vidiyo.android.domain.presenter.channels

import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.api.model.Message
import tv.vidiyo.android.api.model.ServerResponse
import tv.vidiyo.android.api.model.user.UserAccount
import tv.vidiyo.android.api.registerAssignChannels
import tv.vidiyo.android.base.BasePresenter
import tv.vidiyo.android.domain.contract.ChannelsContract
import tv.vidiyo.android.extension.ServerCallback
import tv.vidiyo.android.source.BaseDataSource
import tv.vidiyo.android.source.channels.ChannelsRepository

class BoardingChannelsPresenter(
        v: ChannelsContract.BoardingView,
        override val repository: ChannelsRepository,
        override val provider: ChannelsProvider,
        private val service: VidiyoService
) : BasePresenter<ChannelsContract.BoardingView>(v), ChannelsContract.Presenter, BaseDataSource.Callback {

    val minimumTopics = 3

    init {
        repository.subscribe(this)
        repository.refresh()
    }

    override fun changeSelection(position: Int) {
        provider.changeSelection(position)

        val count = minimumTopics - provider.selected.size
        if (count > 0) {
            view?.updateMessage(count)
            view?.changeButtonVisibility(false)

        } else {
            view?.changeMessageVisibility(false)
            view?.changeButtonVisibility(true)
        }
    }

    override fun assignChannels() {
        view?.setRefreshing(true)
        service.registerAssignChannels(provider.selected).enqueue(callback)
    }

    override fun onDataLoaded() {
        view?.update()
    }

    override fun onDataNotAvailable(error: Message) {
        view?.update()
    }

    private val callback = object : ServerCallback<ServerResponse<UserAccount>>() {
        override fun onSuccess(response: ServerResponse<UserAccount>, url: String?) {
            view?.setRefreshing(false)
            view?.showGathering()
            repository.data?.userChannels = provider.selected.toTypedArray()
        }

        override fun onFailure(error: Message, url: String?) {
            view?.setRefreshing(false)
        }
    }
}