package tv.vidiyo.android.api.twitter

import com.google.gson.annotations.SerializedName
import tv.vidiyo.android.api.Social

class TWUser(
        val id: Long,
        @SerializedName(Social.Key.ID_STR) val idStr: String,
        val name: String,
        @SerializedName(Social.Key.SCREEN_NAME) val screenName: String,
        val url: String,
        @SerializedName(Social.Key.PROFILE_IMAGE_URL) val imageUrl: String,
        @SerializedName(Social.Key.PROFILE_IMAGE_URL_HTTPS) val imageUrlHttps: String
)