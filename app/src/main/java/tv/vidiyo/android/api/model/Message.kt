package tv.vidiyo.android.api.model

import com.google.gson.annotations.SerializedName
import tv.vidiyo.android.api.ApiKey
import java.util.*

data class Message(
        val msg: String?,
        val failures: Array<String>?,
        @SerializedName(ApiKey.errorCode) val code: Int
) {
    val errorCode: ErrorCode get() = ErrorCode.parse(code)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Message

        if (msg != other.msg) return false
        if (!Arrays.equals(failures, other.failures)) return false
        if (code != other.code) return false

        return true
    }

    override fun hashCode(): Int {
        var result = msg?.hashCode() ?: 0
        result = 31 * result + (failures?.let { Arrays.hashCode(it) } ?: 0)
        result = 31 * result + code
        return result
    }
}