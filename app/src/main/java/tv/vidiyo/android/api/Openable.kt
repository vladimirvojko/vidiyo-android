package tv.vidiyo.android.api

interface Openable {
    val url: String
}