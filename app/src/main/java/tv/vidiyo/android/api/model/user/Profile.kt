package tv.vidiyo.android.api.model.user

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import tv.vidiyo.android.api.ApiKey

open class Profile : User {

    var email: String? = null
    var bio: String? = null
    var birthday: String? = null
    var location: String? = null
    var phone: String? = null
    var website: String? = null
    @SerializedName(ApiKey.isPrivate) var isPrivate: Boolean = false
    @SerializedName(ApiKey.isEmailVerified) var isEmailVerified: Boolean = false

    var imageFile: String? = null

    @SerializedName(ApiKey.subscribersCount) var followersCount: Int = 0
    @SerializedName(ApiKey.followingCount) var followingCount: Int = 0
    @SerializedName(ApiKey.backpropStatus) var backprop: SubscriptionStatus = SubscriptionStatus.none
    @SerializedName(ApiKey.status) var status: SubscriptionStatus = SubscriptionStatus.none

    constructor() : super()

    constructor(id: Int, username: String?, name: String?, image: String?) : super(id, username, name, image)

    constructor(parcel: Parcel) : super(parcel) {
        email = parcel.readString()
        bio = parcel.readString()
        birthday = parcel.readString()
        location = parcel.readString()
        phone = parcel.readString()
        website = parcel.readString()
        isPrivate = parcel.readByte() != 0.toByte()
        isEmailVerified = parcel.readByte() != 0.toByte()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        super.writeToParcel(parcel, flags)
        parcel.writeString(email)
        parcel.writeString(bio)
        parcel.writeString(birthday)
        parcel.writeString(location)
        parcel.writeString(phone)
        parcel.writeString(website)
        parcel.writeByte(if (isPrivate) 1 else 0)
        parcel.writeByte(if (isEmailVerified) 1 else 0)
    }

    override fun describeContents(): Int = 0

    companion object CREATOR : Parcelable.Creator<Profile> {
        override fun createFromParcel(parcel: Parcel): Profile {
            return Profile(parcel)
        }

        override fun newArray(size: Int): Array<Profile?> {
            return arrayOfNulls(size)
        }
    }
}