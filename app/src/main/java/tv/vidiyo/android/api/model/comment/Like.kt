package tv.vidiyo.android.api.model.comment

import com.google.gson.annotations.SerializedName
import tv.vidiyo.android.api.ApiKey

class Like(
        var id: Int = 0,
        var liked: Boolean = false,
        @SerializedName(ApiKey.userId) var userId: Int = 0
)