package tv.vidiyo.android.api.twitter

import com.google.gson.annotations.SerializedName
import tv.vidiyo.android.api.Social

class TWAccessToken(@SerializedName(Social.Key.ACCESS_TOKEN) val token: String)