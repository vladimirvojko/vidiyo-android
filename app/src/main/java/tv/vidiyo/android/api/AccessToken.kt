package tv.vidiyo.android.api

import com.google.gson.annotations.SerializedName

data class AccessToken(
        @SerializedName(Social.Key.ACCESS_TOKEN) val token: String,
        @SerializedName(Social.Key.TOKEN_TYPE) val type: String
)