package tv.vidiyo.android.api.model.user

import tv.vidiyo.android.R

enum class Source {
    vidiyo,
    contacts,
    facebook;

    val labelId: Int? get() = when (this) {
        contacts -> R.string.source_contacts
        facebook -> R.string.source_facebook
        vidiyo -> null
    }
}