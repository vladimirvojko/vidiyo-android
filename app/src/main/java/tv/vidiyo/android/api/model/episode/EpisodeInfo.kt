package tv.vidiyo.android.api.model.episode

import android.net.Uri
import com.google.gson.annotations.SerializedName
import tv.vidiyo.android.api.ApiKey
import tv.vidiyo.android.api.model.show.ShowInfo
import tv.vidiyo.android.api.model.social.SocialAccount

class EpisodeInfo(
        @SerializedName(ApiKey.inFavorite) var isFavorite: Boolean,
        @SerializedName(ApiKey.showInfo) var showInfo: ShowInfo,
        @SerializedName(ApiKey.totalViews) var totalViews: Int,
        val view: ViewInfo,
        var products: Array<Product>,
        @SerializedName(ApiKey.userSocialAccounts) var socials: Array<SocialAccount>,
        @SerializedName(ApiKey.instreamAds) var ads: Array<String>?
) : Episode() {
    val adsUri: Array<Uri>?
        get() = ads?.map {
            Uri.parse(it)
        }?.toTypedArray()
}