package tv.vidiyo.android.api.model.topic

import com.google.gson.annotations.SerializedName
import tv.vidiyo.android.api.ApiKey
import tv.vidiyo.android.api.model.channel.Channel
import tv.vidiyo.android.api.model.show.Show

class TopicInfo(
        var topic: Topic? = null,
        var channel: Channel? = null,
        @SerializedName(ApiKey.relatedTopics) var topics: Array<Topic>?,
        var shows: Array<Show>?,
        @SerializedName(ApiKey.topicShows) var topicShows: Array<TopicShow>?
)