package tv.vidiyo.android.api.model

import com.google.gson.annotations.SerializedName
import tv.vidiyo.android.api.ApiKey
import tv.vidiyo.android.api.model.user.SuggestedUser

class DiscoveredPeople(
        @SerializedName(ApiKey.facebook) val facebook: ConnectionCount,
        @SerializedName(ApiKey.contacts) val contacts: ConnectionCount,
        @SerializedName(ApiKey.users) val suggestions: Array<SuggestedUser>
)