package tv.vidiyo.android.api.model.social

data class SocialConnection(val url: String, val type: SocialType)