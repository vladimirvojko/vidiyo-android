package tv.vidiyo.android.api.model.show

import android.os.Parcel
import android.os.Parcelable
import tv.vidiyo.android.api.model.Subscribable

open class Show(
        id: Int = 0,
        isSubscribed: Boolean = false,
        var name: String = "",
        var image: String? = null,
        var subscribers: Int = 0,
        var category: String? = null
) : Subscribable(id, isSubscribed), Parcelable {

    companion object CREATOR : Parcelable.Creator<Show> {
        override fun createFromParcel(parcel: Parcel): Show {
            return Show(parcel)
        }

        override fun newArray(size: Int): Array<Show?> {
            return arrayOfNulls(size)
        }
    }

    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readByte() == 1.toByte(),
            parcel.readString(),
            parcel.readString(),
            parcel.readInt(),
            parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeByte(if (isSubscribed) 1 else 0)
        parcel.writeString(name)
        parcel.writeString(image)
        parcel.writeInt(subscribers)
        parcel.writeString(category)
    }

    override fun describeContents(): Int {
        return 0
    }
}