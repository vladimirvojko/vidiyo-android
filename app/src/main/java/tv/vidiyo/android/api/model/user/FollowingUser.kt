package tv.vidiyo.android.api.model.user

import com.google.gson.annotations.SerializedName
import tv.vidiyo.android.api.ApiKey

class FollowingUser(
        @SerializedName(ApiKey.followingId) override var followingId: Int = 0,
        @SerializedName(ApiKey.subscriberId) override var subscriberId: Int = 0,
        @SerializedName(ApiKey.status) override var status: SubscriptionStatus = SubscriptionStatus.none,
        @SerializedName(ApiKey.backpropStatus) var backprop: SubscriptionStatus = SubscriptionStatus.none,
        @SerializedName(ApiKey.beforeDate) var beforeDate: Long = 0
) : User(), UserSubscription