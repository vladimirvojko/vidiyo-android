package tv.vidiyo.android.api.instagram

import com.google.gson.annotations.SerializedName
import tv.vidiyo.android.api.Dateable
import tv.vidiyo.android.api.Openable
import tv.vidiyo.android.api.Social
import tv.vidiyo.android.api.model.Count
import java.util.*

class IGMedia(
        val id: String,
        @SerializedName(Social.Key.CREATED_TIME) val created: String,
        val link: String,
        val caption: IGCaption?,
        val user: IGUser,
        val comments: Count,
        val likes: Count,
        val images: IGImages
) : Dateable, Openable {
    override val date: Date get() = Date(created.toLong() * 1000)
    override val url: String get() = link
}