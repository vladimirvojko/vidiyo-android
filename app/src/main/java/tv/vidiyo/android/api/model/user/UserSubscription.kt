package tv.vidiyo.android.api.model.user

import com.google.gson.annotations.SerializedName
import tv.vidiyo.android.R

enum class SubscriptionStatus {
    @SerializedName("accepted") accepted,
    @SerializedName("pending") pending,
    @SerializedName("deleted") deleted,
    @SerializedName("blocked") blocked,
    @SerializedName("hidden") hidden,
    @SerializedName("none") none
}

enum class SubscriptionRequest {
    follow, unfollow, accept, delete, block, unblock, hide;

    val path: String get() = when (this) {
            SubscriptionRequest.follow -> "follow-request"
            SubscriptionRequest.unfollow -> "unfollow-request"
            SubscriptionRequest.accept -> "accept-followers"
            SubscriptionRequest.delete -> "remove-followers"
            SubscriptionRequest.block -> "block-request"
            SubscriptionRequest.unblock -> "unblock-request"
            SubscriptionRequest.hide -> "hide"
        }

    fun isEquivalentTo(status: SubscriptionStatus): Boolean {
        return when (status) {
            SubscriptionStatus.accepted -> this == accept
            SubscriptionStatus.pending -> this == follow
            SubscriptionStatus.deleted -> this == delete
            SubscriptionStatus.blocked -> this == block
            SubscriptionStatus.hidden -> this == hide
            else -> false
        }
    }
}

interface UserSubscription {
    var followingId: Int
    var subscriberId: Int
    var status: SubscriptionStatus
}

enum class FollowingType {
    followers, following, pending, discover, blocked;

    val titleRes: Int
        get() = when (this) {
            followers -> R.string.followers
            following -> R.string.following
            pending -> R.string.follow_requests
            discover -> R.string.discover_new_people
            blocked -> R.string.blocked_users
        }
}