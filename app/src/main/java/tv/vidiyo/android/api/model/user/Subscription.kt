package tv.vidiyo.android.api.model.user

import com.google.gson.annotations.SerializedName
import tv.vidiyo.android.api.ApiKey

class Subscription(
        @SerializedName(ApiKey.followingId) val followingId: Int,
        @SerializedName(ApiKey.subscriberId) val subscriberId: Int,
        @SerializedName(ApiKey.status) val status: SubscriptionStatus
) {
    override fun hashCode() = followingId
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as Subscription
        return followingId == other.followingId
                && subscriberId == other.subscriberId
                && status == other.status
    }
}