package tv.vidiyo.android.api.instagram

import com.google.gson.annotations.SerializedName
import tv.vidiyo.android.api.ApiKey
import tv.vidiyo.android.api.Social

class IGUser(
        val id: String,
        val username: String,
        @SerializedName(Social.Key.PROFILE_PICTURE) val picture: String,
        @SerializedName(ApiKey.fullName) val fullName: String
)