package tv.vidiyo.android.api.model.show

import tv.vidiyo.android.api.model.user.Profile

class FriendShow(
        var user: Profile,
        var show: Show
)