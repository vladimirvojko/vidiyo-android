package tv.vidiyo.android.api

object Social {
    object Facebook {
        const val BASE_URL = "https://graph.facebook.com"
        const val API_PATH = "v2.8"
        const val URL = "$BASE_URL/$API_PATH/"

        const val CLIENT_ID = "328670407533804"
        const val CLIENT_SECRET = "626c2c7d5558cd1282adbfdccf4ca848"
        const val GRANT_TYPE = "client_credentials"

        const val POST_FIELDS = "id,story,message,caption,description,full_picture,from{id,name,picture},created_time"
    }

    object Instagram {
        const val BASE_URL = "https://api.instagram.com"
        const val API_PATH = "v1"
        const val URL = "$BASE_URL/$API_PATH/"
    }

    object Twitter {
        const val BASE_URL = "https://api.twitter.com"
        const val API_PATH = "1.1"
        const val URL = "$BASE_URL/$API_PATH/"

        const val CONSUMER_KEY = "Kikge6qAwN4NSyPnOFMPXawIf"
        const val CONSUMER_SECRET = "fSYZIa5mSEGHBk7mCqnMiC6kALDwa3VXuwyyuidSC9AJ09zl5M"
        const val TOKEN_CREDENTIALS = "$CONSUMER_KEY:$CONSUMER_SECRET"

        // TODO: update if key/secret changed
        const val TOKEN_CREDENTIALS_BASE64 = "S2lrZ2U2cUF3TjROU3lQbk9GTVBYYXdJZjpmU1laSWE1bVNFR0hCazdtQ3FuTWlDNmtBTER3YTNWWHV3eXl1aWRTQzlBSjA5emw1TQ=="
    }

    object Key {
        const val ACCESS_TOKEN = "access_token"
        const val TOKEN_TYPE = "token_type"
        const val CLIENT_ID = "client_id"
        const val CLIENT_SECRET = "client_secret"
        const val GRANT_TYPE = "grant_type"

        const val ID_STR = "id_str"
        const val SCREEN_NAME = "screen_name"

        const val PROFILE_IMAGE_URL = "profile_image_url"
        const val PROFILE_IMAGE_URL_HTTPS = "profile_image_url_https"
        const val PROFILE_PICTURE = "profile_picture"

        const val LIMIT = "limit"
        const val OFFSET = "offset"
        const val FIELDS = "fields"

        const val FULL_PICTURE = "full_picture"
        const val CREATED_TIME = "created_time"
        const val STANDARD_RESOLUTION = "standard_resolution"
        const val LOW_RESOLUTION = "low_resolution"
        const val Q = "q"
    }
}