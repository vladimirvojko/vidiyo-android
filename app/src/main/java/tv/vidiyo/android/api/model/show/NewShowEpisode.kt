package tv.vidiyo.android.api.model.show

import com.google.gson.annotations.SerializedName
import tv.vidiyo.android.api.ApiKey
import tv.vidiyo.android.api.model.episode.Episode

class NewShowEpisode(
        var show: Show,
        var episode: Episode,
        @SerializedName(ApiKey.newEpisodesCount) var count: Int = 0
)