package tv.vidiyo.android.api.instagram

class IGResponse<T>(
        val data: Array<T>,
        val meta: IGMeta
)

class IGMeta(val code: Int)