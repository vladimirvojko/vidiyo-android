package tv.vidiyo.android.api.model

import tv.vidiyo.android.R

enum class ErrorCode {
    incorrectUsername,
    incorrectPassword,
    serverError,
    unknown;

    companion object {
        fun parse(code: Int) = try {
            ErrorCode.values()[code - 1]
        } catch (e: IndexOutOfBoundsException) {
            ErrorCode.unknown
        }
    }

    val stringId: Int get() = when (this) {
        ErrorCode.incorrectUsername -> R.string.error_incorrect_username
        ErrorCode.incorrectPassword -> R.string.error_incorrect_password
        ErrorCode.serverError -> R.string.error_server_error
        else -> R.string.error_unknown
    }
}