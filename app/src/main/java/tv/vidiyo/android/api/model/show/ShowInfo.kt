package tv.vidiyo.android.api.model.show

import com.google.gson.annotations.SerializedName
import tv.vidiyo.android.api.ApiKey
import tv.vidiyo.android.api.model.channel.Channel
import tv.vidiyo.android.api.model.episode.Episode
import tv.vidiyo.android.api.model.social.Socials

class ShowInfo(
        var channel: Channel,
        var description: String,
        @SerializedName(ApiKey.lastEpisode) var lastEpisode: Episode,
        var episodes: Array<Episode>,
        var socials: Socials,
        @SerializedName(ApiKey.showBanner) var banner: String?
) : Show()