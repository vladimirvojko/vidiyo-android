package tv.vidiyo.android.api.model.channel

import com.google.gson.annotations.SerializedName
import tv.vidiyo.android.api.ApiKey

class ChannelItems<T>(
        id: Int,
        name: String,
        image: String?,
        @SerializedName(ApiKey.data) val items: Array<T>
) : Channel(id, name, image)