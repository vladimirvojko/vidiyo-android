package tv.vidiyo.android.api.model.episode

import android.net.Uri
import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import tv.vidiyo.android.api.ApiKey

class Media(
        @SerializedName(ApiKey.mediaId) val id: Int,
        @SerializedName(ApiKey.mediaUid) val uid: String?,
        @SerializedName(ApiKey.videoRef) val reference: String?,
        @SerializedName(ApiKey.duration) val duration: Float
) : Parcelable {

    companion object CREATOR : Parcelable.Creator<Media> {
        override fun createFromParcel(parcel: Parcel): Media {
            return Media(parcel)
        }

        override fun newArray(size: Int): Array<Media?> {
            return arrayOfNulls(size)
        }
    }

    val uri: Uri?
        get() = if (reference != null) Uri.parse(reference) else {
            null
        }

    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString(),
            parcel.readString(),
            parcel.readFloat()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(uid)
        parcel.writeString(reference)
        parcel.writeFloat(duration)
    }

    override fun describeContents() = 0
}