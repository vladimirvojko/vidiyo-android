package tv.vidiyo.android.api

import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*
import retrofit2.http.Header
import tv.vidiyo.android.App
import tv.vidiyo.android.api.graph.FBPost
import tv.vidiyo.android.api.graph.FBResponse
import tv.vidiyo.android.api.instagram.IGAccessToken
import tv.vidiyo.android.api.instagram.IGMedia
import tv.vidiyo.android.api.instagram.IGResponse
import tv.vidiyo.android.api.instagram.IGUser
import tv.vidiyo.android.api.model.*
import tv.vidiyo.android.api.model.cast.Cast
import tv.vidiyo.android.api.model.cast.CastInfo
import tv.vidiyo.android.api.model.channel.Channel
import tv.vidiyo.android.api.model.channel.ChannelInfo
import tv.vidiyo.android.api.model.channel.ChannelItems
import tv.vidiyo.android.api.model.comment.Comment
import tv.vidiyo.android.api.model.comment.Like
import tv.vidiyo.android.api.model.episode.Episode
import tv.vidiyo.android.api.model.episode.EpisodeInfo
import tv.vidiyo.android.api.model.episode.ViewInfo
import tv.vidiyo.android.api.model.show.NewShowEpisode
import tv.vidiyo.android.api.model.show.SearchingShow
import tv.vidiyo.android.api.model.show.Show
import tv.vidiyo.android.api.model.show.ShowInfo
import tv.vidiyo.android.api.model.social.SocialAccount
import tv.vidiyo.android.api.model.social.SocialAuth
import tv.vidiyo.android.api.model.topic.Topic
import tv.vidiyo.android.api.model.topic.TopicInfo
import tv.vidiyo.android.api.model.user.*
import tv.vidiyo.android.api.twitter.Tweet
import tv.vidiyo.android.extension.*
import tv.vidiyo.android.util.Constant
import tv.vidiyo.android.util.LocationWatcher
import java.io.File

interface VidiyoService {

    //region Auth
    @POST("user/login")
    fun login(@Query(ApiKey.login) login: String,
              @Query(ApiKey.password) password: String): Call<ServerResponse<UserAccount>>

    @[POST("{path}") FormUrlEncoded]
    fun socialAuth(@Path(ApiKey.path) path: String,
                   @Field(ApiKey.userId) userId: String,
                   @Field(ApiKey.idToken) idToken: String): Call<ServerResponse<UserAccount>>

    @GET("user/validate-email")
    fun validate(@Query(ApiKey.email) email: String): Call<ServerResponse<Unit>>

    @[POST("user/register") Multipart]
    fun register(@PartMap map: Map<String, @JvmSuppressWildcards RequestBody>,
                 @Part part: MultipartBody.Part?): Call<ServerResponse<UserAccount>>

    @[POST("{path}") Multipart]
    fun socialRegister(@Path(ApiKey.path) path: String,
                       @PartMap map: Map<String, @JvmSuppressWildcards RequestBody>,
                       @Part part: MultipartBody.Part?): Call<ServerResponse<UserAccount>>

    @[POST("user/register-anonymous") Multipart]
    fun registerAnonymous(@PartMap map: Map<String, @JvmSuppressWildcards RequestBody>,
                          @Part part: MultipartBody.Part?): Call<ServerResponse<UserAccount>>

    @POST("create-phone")
    fun createPhone(@Query(ApiKey.phone) phone: String): Call<ServerResponse<Unit>>

    @[POST("validate-code") FormUrlEncoded]
    fun validateCode(@Field(ApiKey.code) code: String,
                     @Field(ApiKey.phone) phone: String): Call<ServerResponse<Unit>>

    @POST("user/create-anonymous")
    fun createAnonymous(@Query("device_id") deviceId: String? = null): Call<ServerResponse<UserAccount>>

    @POST("nopass-login")
    fun nopassLogin(@Query(ApiKey.payload) login: String): Call<ServerResponse<Unit>>

    @GET("nopass-confirm")
    fun nopassConfirm(@Query(ApiKey.token) token: String): Call<ServerResponse<UserAccount>>
    //endregion

    //region Content
    @GET("me")
    fun me(): Call<ServerResponse<UserAccount>>

    @GET("channels")
    fun channels(): Call<ServerResponse<Array<Channel>>>

    @GET("home/data")
    fun home(): Call<ServerResponse<HomeData>>

    @GET("user/channels")
    fun userChannels(): Call<ServerResponse<UserChannels>>

    @GET("channel/{id}/info")
    fun channelInfo(@Path(ApiKey.id) id: Int): Call<ServerResponse<ChannelInfo>>

    @GET("topic/{id}/info")
    fun topicInfo(@Path(ApiKey.id) id: Int): Call<ServerResponse<TopicInfo>>
    //endregion

    //region Show
    @GET("show/{id}/info")
    fun showInfo(@Path(ApiKey.id) id: Int): Call<ServerResponse<ShowInfo>>

    @GET("show/{id}/casts")
    fun showCasts(@Path(ApiKey.id) id: Int): Call<ServerResponse<Array<Cast>>>

    @GET("show/{id}/topics")
    fun showTopics(@Path(ApiKey.id) id: Int): Call<ServerResponse<Array<Topic>>>

    @GET("show/{id}/subscribers")
    fun showSubscribers(@Path(ApiKey.id) id: Int): Call<ServerResponse<Array<FollowingUser>>>

    @GET("show/{id}/episodes")
    fun showEpisodes(@Path(ApiKey.id) id: Int): Call<ServerResponse<Array<Episode>>>
    //endregion

    //region Episode
    @GET("episode/{id}/info")
    fun episodeInfo(@Path(ApiKey.id) id: Int,
                    @Query(ApiKey.lat) lat: Double = LocationWatcher.latLng.latitude,
                    @Query(ApiKey.lng) lng: Double = LocationWatcher.latLng.longitude,
                    @Query(ApiKey.language) language: String = "en"): Call<ServerResponse<EpisodeInfo>>

    @GET("episode/{id}/casts")
    fun episodeCasts(@Path(ApiKey.id) id: Int): Call<ServerResponse<Array<Cast>>>

    @GET("episode/{id}/comments")
    fun episodeComments(@Path(ApiKey.id) id: Int,
                        @Query(ApiKey.limit) limit: Int?,
                        @Query(ApiKey.beforeDate) before: Long?): Call<ServerResponse<Array<Comment>>>

    @POST("episode/{id}/comments")
    fun comment(@Path(ApiKey.id) id: Int,
                @Query(ApiKey.text) text: String,
                @Query(ApiKey.parentId) parentId: Int?): Call<ServerResponse<Comment>>

    @GET("episode/{id}/topics")
    fun episodeTopics(@Path(ApiKey.id) id: Int): Call<ServerResponse<Array<Topic>>>

    @GET("episode/{id}/subscribers")
    fun episodeSubscribers(@Path(ApiKey.id) id: Int): Call<ServerResponse<Array<FollowingUser>>>

    @GET("episode/{id}/related")
    fun episodeRelated(@Path(ApiKey.id) id: Int): Call<ServerResponse<Array<Episode>>>

    @POST("{path}-episodes")
    fun episodeFavorite(@Path(ApiKey.path) path: String,
                        @Query(ApiKey.episodes) ids: String): Call<ServerResponse<IntArray>>

    @POST("episode/{id}/share")
    fun shareEpisode(@Path(ApiKey.id) id: Int, @Body share: ShareEpisode): Call<ServerResponse<Unit>>
    //endregion

    //region Cast
    @GET("cast/{id}/info")
    fun castInfo(@Path(ApiKey.id) id: Int): Call<ServerResponse<CastInfo>>

    @GET("cast/{id}/episodes")
    fun castEpisodes(@Path(ApiKey.id) id: Int,
                     @Query(ApiKey.limit) limit: Int? = null,
                     @Query(ApiKey.offset) offset: Int? = null): Call<ServerResponse<Array<Episode>>>

    @GET("cast/{id}/shows")
    fun castShows(@Path(ApiKey.id) id: Int,
                  @Query(ApiKey.limit) limit: Int? = null,
                  @Query(ApiKey.offset) offset: Int? = null): Call<ServerResponse<Array<Show>>>
    //endregion

    //region Connect
    @GET("find-contacts")
    fun findContacts(@Query(ApiKey.emails) emails: String,
                     @Query(ApiKey.phones) phones: String): Call<ServerResponse<ContactFriends>>

    @GET("facebook-friends")
    fun facebookFriends(@Query(ApiKey.token) token: String): Call<ServerResponse<Array<Friend>>>
    //endregion

    //region Subscription
    @POST("user/assign-channels")
    fun assignChannels(@Query(ApiKey.channels) ids: String): Call<ServerResponse<Array<Int>>>

    @POST("user/register-assign-channels")
    fun registerAssignChannels(@Query(ApiKey.channels) ids: String): Call<ServerResponse<UserAccount>>

    @POST("follow-users")
    fun connect(@Query(ApiKey.ids) ids: String,
                @Query(ApiKey.phones) phones: String?,
                @Query(ApiKey.emails) emails: String?): Call<ServerResponse<Unit>>

    @POST("user/{path}-shows")
    fun subscribeShows(@Path(ApiKey.path) path: String,
                       @Query(ApiKey.shows) ids: String): Call<ServerResponse<IntArray>>

    @POST("user/{path}-users")
    fun subscribeUsers(@Path(ApiKey.path) path: String,
                       @Query(ApiKey.users) ids: String): Call<ServerResponse<IntArray>>

    @POST("user/{path}-casts")
    fun subscribeCasts(@Path(ApiKey.path) path: String,
                       @Query(ApiKey.casts) ids: String): Call<ServerResponse<IntArray>>

    @POST("user/{path}-topics")
    fun subscribeTopics(@Path(ApiKey.path) path: String,
                        @Query(ApiKey.topics) ids: String): Call<ServerResponse<IntArray>>
    //endregion

    //region Comment
    //TODO
    //endregion

    //region Like
    @POST("{target}/{id}/like")

    fun like(@Path(ApiKey.target) target: String,
             @Path(ApiKey.id) id: Int,
             @Query(ApiKey.liked) liked: Boolean): Call<ServerResponse<Like>>

    @POST("{target}/{id}/like/remove")
    fun likeRemove(@Path(ApiKey.target) target: String,
                   @Path(ApiKey.id) id: Int): Call<ServerResponse<Unit>>
    //endregion

    //region Watching
    @POST("episode/{id}/set-view")
    fun setEpisode(@Path(ApiKey.id) id: Int, @Query(ApiKey.viewTime) viewTime: Float): Call<ServerResponse<ViewInfo>>

    @GET("viewed-info")
    fun viewedInfo(@Query(ApiKey.limit) limit: Int?,
                   @Query(ApiKey.beforeDate) before: Long?): Call<ServerResponse<Array<ViewInfo>>>

    @GET("new-episodes")
    fun newEpisodes(): Call<ServerResponse<Array<NewShowEpisode>>>
    //endregion

    //region Notification
    @POST("user/register-device")
    fun registerFcmToken(@Query(ApiKey.notificationToken) token: String,
                         @Query(ApiKey.platform) platform: String = "android",
                         @Query(ApiKey.deviceToken) deviceId: String = App.deviceId): Call<ServerResponse<Unit>>

    @GET("notifications/{target}/count")
    fun notificationCount(@Path(ApiKey.target) target: String): Call<ServerResponse<Count>>
    //endregion

    //region User
    @POST("resend-email")
    fun resendEmailConfirmation(): Call<ServerResponse<Unit>>

    @GET("user/{id}/profile")
    fun userProfile(@Path(ApiKey.id) id: Int): Call<ServerResponse<UserAccount>>

    @GET("user/{id}/profile/shows")
    fun userShows(@Path(ApiKey.id) id: Int): Call<ServerResponse<Array<ChannelItems<Show>>>>

    @GET("user/{id}/profile/episodes")
    fun userEpisodes(@Path(ApiKey.id) id: Int): Call<ServerResponse<Array<Episode>>>

    @GET("user/{id}/profile/casts")
    fun userCasts(@Path(ApiKey.id) id: Int): Call<ServerResponse<Array<ChannelItems<Cast>>>>

    @GET("user/{id}/profile/topics")
    fun userTopics(@Path(ApiKey.id) id: Int): Call<ServerResponse<Array<ChannelItems<Topic>>>>

    @GET("user/{id}/profile/channel/{channel_id}/shows")
    fun userShowsInChannel(@Path(ApiKey.id) id: Int,
                           @Path(ApiKey.channelId) channelId: Int): Call<ServerResponse<Array<Show>>>

    @GET("user/{id}/profile/channel/{channel_id}/topics")
    fun userTopicsInChannel(@Path(ApiKey.id) id: Int,
                            @Path(ApiKey.channelId) channelId: Int): Call<ServerResponse<Array<Topic>>>

    @GET("user/{id}/profile/channel/{channel_id}/casts")
    fun userCastsInChannel(@Path(ApiKey.id) id: Int,
                           @Path(ApiKey.channelId) channelId: Int): Call<ServerResponse<Array<Cast>>>

    @POST("user/toggle-privacy")
    fun togglePrivacy(@Query(ApiKey.isPrivate) isPrivate: String): Call<ServerResponse<Boolean>>

    @GET("user/pending-status")
    fun pendingStatus(): Call<ServerResponse<PendingRequests>>

    @GET("user/pendings")
    fun pendingRequests(@Query(ApiKey.limit) limit: Int,
                        @Query(ApiKey.before) before: Long? = null,
                        @Query(ApiKey.query) query: String? = null): Call<ServerResponse<Array<FollowingUser>>>

    @GET("user/blocked")
    fun blocked(@Query(ApiKey.limit) limit: Int,
                @Query(ApiKey.before) before: Long? = null): Call<ServerResponse<Array<FollowingUser>>>

    @GET("user/{id}/following")
    fun userFollowing(@Path(ApiKey.id) id: Int,
                      @Query(ApiKey.limit) limit: Int,
                      @Query(ApiKey.before) before: Long? = null,
                      @Query(ApiKey.query) query: String? = null): Call<ServerResponse<Array<FollowingUser>>>

    @GET("user/{id}/followers")
    fun userFollowers(@Path(ApiKey.id) id: Int,
                      @Query(ApiKey.limit) limit: Int,
                      @Query(ApiKey.before) before: Long? = null,
                      @Query(ApiKey.query) query: String? = null): Call<ServerResponse<Array<FollowingUser>>>

    @POST("user/{path}")
    fun respondTo(@Path(ApiKey.path) path: String, @Query(ApiKey.ids) ids: String): Call<ServerResponse<Unit>>

    @[POST("user/update") Multipart]
    fun update(@PartMap map: Map<String, @JvmSuppressWildcards RequestBody>,
               @Part part: MultipartBody.Part?): Call<ServerResponse<UserAccount>>

    @POST("user/profile/change-password")
    fun changePassword(@Query(ApiKey.old) current: String,
                       @Query(ApiKey.new) new: String,
                       @Query(ApiKey.confirm) confirm: String): Call<ServerResponse<Unit>>

    @GET("user/socials")
    fun socials(): Call<ServerResponse<Array<SocialAccount>>>

    @POST("user/socials/connect-social")
    fun connectSocial(@Query(ApiKey.type) type: String, @Query(ApiKey.token) token: String): Call<ServerResponse<SocialAccount>>

    @POST("user/socials/disconnect-social")
    fun disconnectSocial(@Query(ApiKey.id) id: Int): Call<ServerResponse<SocialAccount>>

    @POST("user/{target}")
    fun specificSubscriptionAction(@Path(ApiKey.target) action: String, @Query(ApiKey.ids) ids: String): Call<ServerResponse<Array<Subscription>>>
    //endregion

    //region Discover
    @GET("user/discover")
    fun discoverPeople(@Query(ApiKey.token) token: String?): Call<ServerResponse<DiscoveredPeople>>

    @POST("upload-contacts")
    fun uploadContacts(@Query(ApiKey.emails) emails: String,
                       @Query(ApiKey.phones) phones: String): Call<ServerResponse<Array<FollowingUser>>>

    @GET("contact-friends")
    fun contactFriends(): Call<ServerResponse<Array<FollowingUser>>>

    @GET("facebook-friends")
    fun fbFriends(@Query(ApiKey.token) token: String): Call<ServerResponse<Array<FollowingUser>>>

    @POST("disconnect-contacts")
    fun disconnectContact(): Call<ServerResponse<Message>>

    @POST("disconnect-facebook")
    fun disconnectFacebook(): Call<ServerResponse<Message>>
    //endregion

    //region Search
    @GET("search/main")
    fun searchMain(): Call<ServerResponse<SearchMain>>

    @GET("search/people")
    fun searchPeople(@Query(ApiKey.query) query: String?,
                     @Query(ApiKey.offset) offset: Int?,
                     @Query(ApiKey.limit) limit: Int? = null): Call<ServerResponse<Array<User>>>

    @GET("search/shows")
    fun searchShows(@Query(ApiKey.query) query: String?,
                    @Query(ApiKey.offset) offset: Int?,
                    @Query(ApiKey.limit) limit: Int? = null): Call<ServerResponse<Array<SearchingShow>>>

    @GET("search/casts")
    fun searchCast(@Query(ApiKey.query) query: String?,
                   @Query(ApiKey.offset) offset: Int?,
                   @Query(ApiKey.limit) limit: Int? = null): Call<ServerResponse<Array<Cast>>>

    @GET("search/topics")
    fun searchTopics(@Query(ApiKey.query) query: String?,
                     @Query(ApiKey.offset) offset: Int?,
                     @Query(ApiKey.limit) limit: Int? = null): Call<ServerResponse<Array<Topic>>>
    //endregion

    //region Settings > Notification

    @GET("settings/notifications")
    fun notifications(): Call<ServerResponse<NotificationsSettings>>

    @[POST("settings/notifications") FormUrlEncoded]
    fun changeNotification(@FieldMap map: Map<String, Boolean>): Call<ServerResponse<Unit>>

    //endregion

    //region Social Feed
    @GET
    fun facebookAccessToken(@Url url: String = "${Social.Facebook.URL}oauth/${ApiKey.accessToken}",
                            @Query(Social.Key.CLIENT_ID) clientId: String = Social.Facebook.CLIENT_ID,
                            @Query(Social.Key.CLIENT_SECRET) secret: String = Social.Facebook.CLIENT_SECRET,
                            @Query(Social.Key.GRANT_TYPE) type: String = Social.Facebook.GRANT_TYPE): Call<AccessToken>

    @GET
    fun facebookPageFeed(@Url url: String,
                         @Query(Social.Key.ACCESS_TOKEN) token: String,
                         @Query(Social.Key.LIMIT) limit: Int? = null,
                         @Query(Social.Key.OFFSET) offset: Int? = null,
                         @Query(Social.Key.FIELDS) fields: String = Social.Facebook.POST_FIELDS): Call<FBResponse<FBPost>>

    @GET("check-social/instagram")
    fun instagramAccessToken(): Call<ServerResponse<IGAccessToken>>

    @GET
    fun instagramUserSearch(@Url url: String,
                            @Query(Social.Key.Q) query: String,
                            @Query(Social.Key.ACCESS_TOKEN) token: String): Call<IGResponse<IGUser>>

    @GET
    fun instagramRecentMedia(@Url url: String,
                             @Query(Social.Key.ACCESS_TOKEN) token: String,
                             @Query(ApiKey.count) count: Int? = null,
                             @Query("min_id") minId: String? = null,
                             @Query("max_id") maxId: String? = null): Call<IGResponse<IGMedia>>

    @[POST FormUrlEncoded Headers("authorization: Basic ${Social.Twitter.TOKEN_CREDENTIALS_BASE64}")]
    fun twitterAccessToken(@Url url: String = "${Social.Twitter.BASE_URL}/oauth2/token",
                           @Field(Social.Key.GRANT_TYPE) type: String = Social.Facebook.GRANT_TYPE): Call<AccessToken>

    @GET
    fun twitterStatuses(@Url url: String,
                        @Header("Authorization") header: String,
                        @Query(Social.Key.SCREEN_NAME) screenName: String?,
                        @Query(ApiKey.id) id: String? = null,
                        @Query(ApiKey.count) count: Int? = null,
                        @Query("since_id") sinceId: Int? = null,
                        @Query("max_id") maxId: Int? = null): Call<Array<Tweet>>

    @GET
    fun ipInfo(@Url url: String = "https://ipinfo.io/json"): Call<IpInfo>
    //endregion
}

//region Extension
fun VidiyoService.respondTo(request: SubscriptionRequest, users: Array<User>): Call<ServerResponse<Unit>> {
    val ids = users.joinToString(separator = Constant.COMMA) { "${it.id}" }
    return respondTo(request.path, ids)
}

fun VidiyoService.episodeFavorite(favorite: Boolean, vararg episodes: Episode): Call<ServerResponse<IntArray>> {
    val ids = episodes.joinToString(separator = Constant.COMMA) { "${it.id}" }
    return episodeFavorite(if (favorite) "favorite" else "unfavorite", ids)
}

fun VidiyoService.register(profile: Profile, password: String): Call<ServerResponse<UserAccount>> {
    val file = profile.imageFile?.let { File(it) }
    return register(profile.toMultipartMap().apply {
        put(ApiKey.password, password.requestBody)
    }, file?.imagePartBody)
}

fun VidiyoService.registerAnonymous(profile: Profile, password: String, account: UserAccount): Call<ServerResponse<UserAccount>> {
    val file = profile.imageFile?.let { File(it) }
    return registerAnonymous(profile.toMultipartMap().apply {
        put(ApiKey.password, password.requestBody)
        put(ApiKey.token, account.token.requestBody)

    }, file?.imagePartBody)
}

fun VidiyoService.socialAuth(social: SocialAuth): Call<ServerResponse<UserAccount>> {
    return socialAuth(social.type.authEndpoint, social.userId, social.token)
}

fun VidiyoService.socialRegister(social: SocialAuth, profile: Profile, password: String, account: UserAccount?): Call<ServerResponse<UserAccount>> {
    val file = profile.imageFile?.let { File(it) }
    return socialRegister(social.type.registerEndpoint, linkedMapOf(
            ApiKey.password to password.requestBody,
            ApiKey.idToken to social.token.requestBody,
            ApiKey.userId to social.userId.requestBody
    ) + profile.toMultipartMap().apply {
        account?.token?.let { put(ApiKey.token, it.requestBody) }
    }, file?.imagePartBody)
}

fun VidiyoService.assignChannels(channels: Set<Int>): Call<ServerResponse<Array<Int>>> {
    val ids = channels.joinToString(separator = Constant.COMMA) { "$it" }
    return assignChannels(ids)
}

fun VidiyoService.registerAssignChannels(channels: Set<Int>): Call<ServerResponse<UserAccount>> {
    val ids = channels.joinToString(separator = Constant.COMMA) { "$it" }
    return registerAssignChannels(ids)
}

fun VidiyoService.connect(toFollow: List<Friend>, toInvite: List<Friend>? = null): Call<ServerResponse<Unit>> {
    val ids = toFollow.joinToString(separator = Constant.COMMA) { "${it.id}" }
    var phones: String? = null
    var emails: String? = null

    toInvite?.let {
        val sbEmails = StringBuilder()
        val sbPhones = StringBuilder()
        for (item in it) {
            if (item.email != null) {
                sbEmails.append(item.email).append(Constant.COMMA)
            } else {
                sbPhones.append(item.phone).append(Constant.COMMA)
            }
        }

        phones = sbPhones.ifNotEmpty()?.deleteLastCharacter()?.toString()
        emails = sbEmails.ifNotEmpty()?.deleteLastCharacter()?.toString()
    }

    return connect(ids, phones, emails)
}

fun VidiyoService.subscribe(any: Any, subscribe: Boolean): Call<ServerResponse<IntArray>> {
    val path = if (subscribe) ApiKey.assign else ApiKey.unbind
    return when (any) {
        is Show -> subscribeShows(path, any.id.toString())
        is Cast -> subscribeCasts(path, any.id.toString())
        is Topic -> subscribeTopics(path, any.id.toString())
        is User -> subscribeUsers(if (subscribe) ApiKey.subscribe else ApiKey.unsubscribe, any.id.toString())
        else -> throw IllegalArgumentException("Unknown type")
    }
}

fun VidiyoService.update(account: UserAccount): Call<ServerResponse<UserAccount>> {
    val file = account.imageFile?.let { File(it) }
    return update(account.toMultipartMap(), file?.imagePartBody)
}

fun VidiyoService.requestFor(type: FollowingType, id: Int, limit: Int, before: Long?): Call<ServerResponse<Array<FollowingUser>>> = when (type) {
    FollowingType.followers -> userFollowers(id, limit, before)
    FollowingType.following -> userFollowing(id, limit, before)
    FollowingType.pending -> pendingRequests(limit, before)
    FollowingType.blocked -> blocked(limit, before)

    else -> throw IllegalStateException("Can't find request for type $type")
}

fun VidiyoService.subscription(request: SubscriptionRequest, user: FollowingUser)
        = specificSubscriptionAction(request.path, "${user.id}")

fun VidiyoService.subscription(request: SubscriptionRequest, users: Array<FollowingUser>)
        = specificSubscriptionAction(request.path, users.joinToString(separator = Constant.COMMA) { "${it.id}" })

fun VidiyoService.disconnectSocial(account: SocialAccount) = disconnectSocial(account.id)
fun VidiyoService.connectSocial(account: SocialAccount, token: String) = connectSocial(account.socialType.name.toUpperCase(), token)
fun VidiyoService.newEpisodesCount() = notificationCount("episode")

fun VidiyoService.facebookFeed(pageId: String, token: String, limit: Int? = null, offset: Int? = null)
        = facebookPageFeed(Social.Facebook.URL + pageId + "/posts", token, limit, offset)

fun VidiyoService.twitterTweets(token: String, screenName: String?, id: String? = null, count: Int? = null, sinceId: Int? = null, maxId: Int? = null)
        = twitterStatuses("${Social.Twitter.URL}statuses/user_timeline.json", "Bearer $token", screenName, id, count, sinceId, maxId)

fun VidiyoService.instagramUserSearch(query: String, token: String) = instagramUserSearch(Social.Instagram.URL + "users/search", query, token)
fun VidiyoService.instagramRecents(userId: String, token: String, count: Int? = null, minId: String? = null, maxId: String? = null)
        = instagramRecentMedia(Social.Instagram.URL + "users/$userId/media/recent", token, count, minId, maxId)
//endregion