package tv.vidiyo.android.api.model

data class SimpleSubscription(
        val id: Int,
        val subscribed: Boolean
) {
    override fun hashCode() = id
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other is SimpleSubscription) {
            return this.id == other.id
        }
        return false
    }
}