package tv.vidiyo.android.api.model

import android.os.Parcel
import android.os.Parcelable

class City(val name: String, val state: String) : Parcelable {

    companion object CREATOR : Parcelable.Creator<City> {
        override fun createFromParcel(parcel: Parcel) = City(parcel)
        override fun newArray(size: Int): Array<City?> = arrayOfNulls(size)
    }

    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(state)
    }

    override fun describeContents() = 0

    val address: String get() = "$name, $state"
}