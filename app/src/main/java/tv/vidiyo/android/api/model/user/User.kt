package tv.vidiyo.android.api.model.user

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import tv.vidiyo.android.api.ApiKey
import tv.vidiyo.android.util.Constant

open class User(
        var id: Int,
        @SerializedName(ApiKey.username) var username: String?,
        var name: String?,
        var image: String?
) : Parcelable {

    constructor() : this(Constant.NO_ID, null, null, null)

    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(username)
        parcel.writeString(name)
        parcel.writeString(image)
    }

    override fun describeContents(): Int = 0

    companion object {
        val CREATOR = object : Parcelable.Creator<User> {
            override fun createFromParcel(parcel: Parcel): User {
                return User(parcel)
            }

            override fun newArray(size: Int): Array<User?> {
                return arrayOfNulls(size)
            }
        }
    }
}