package tv.vidiyo.android.api.model.comment

class Comments(
        var start: Int = 0,
        var count: Int = 0,
        var total: Int = 0,
        var comments: Array<Comment> = emptyArray()
)