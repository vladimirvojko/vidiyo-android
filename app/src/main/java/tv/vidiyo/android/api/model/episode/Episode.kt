package tv.vidiyo.android.api.model.episode

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import tv.vidiyo.android.api.ApiKey
import tv.vidiyo.android.api.model.show.Show

open class Episode(
        var id: Int = 0,
        var name: String = "",
        var image: String? = null,
        @SerializedName(ApiKey.seasonNumber) var seasonsNumber: Int = 0,
        @SerializedName(ApiKey.episodeNumber) var episodesNumber: Int = 0,
        @SerializedName(ApiKey.publishDate) var publishDate: Long = 0L,
        var media: Media? = null,
        var show: Show? = null,
        @SerializedName(ApiKey.showName) var showName: String? = null,
        var description: String? = null
) : Parcelable {

    companion object CREATOR : Parcelable.Creator<Episode> {
        override fun createFromParcel(parcel: Parcel): Episode {
            return Episode(parcel)
        }

        override fun newArray(size: Int): Array<Episode?> {
            return arrayOfNulls(size)
        }
    }

    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString(),
            parcel.readString(),
            parcel.readInt(),
            parcel.readInt(),
            parcel.readLong(),
            parcel.readParcelable(Media::class.java.classLoader),
            parcel.readParcelable(Show::class.java.classLoader),
            parcel.readString(),
            parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(name)
        parcel.writeString(image)
        parcel.writeInt(seasonsNumber)
        parcel.writeInt(episodesNumber)
        parcel.writeLong(publishDate)
        parcel.writeParcelable(media, flags)
        parcel.writeParcelable(show, flags)
        parcel.writeString(showName)
        parcel.writeString(description)
    }

    override fun describeContents() = 0
}