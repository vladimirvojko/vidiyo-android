package tv.vidiyo.android.api.model.user

import com.google.gson.annotations.SerializedName
import tv.vidiyo.android.api.ApiKey

class PendingRequests(
        @SerializedName(ApiKey.pendingsCount) var count: Int,
        @SerializedName(ApiKey.lastPending) var last: User? = null
)