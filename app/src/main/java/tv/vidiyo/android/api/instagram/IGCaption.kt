package tv.vidiyo.android.api.instagram

import com.google.gson.annotations.SerializedName
import tv.vidiyo.android.api.Social

class IGCaption(
        val id: String,
        val text: String,
        val from: IGUser,
        @SerializedName(Social.Key.CREATED_TIME) val created: String
)