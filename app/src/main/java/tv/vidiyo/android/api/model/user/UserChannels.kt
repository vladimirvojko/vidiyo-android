package tv.vidiyo.android.api.model.user

import com.google.gson.annotations.SerializedName
import tv.vidiyo.android.api.ApiKey
import tv.vidiyo.android.api.model.channel.Channel

class UserChannels(
        var channels: Array<Channel>,
        @SerializedName(ApiKey.userChannels) var userChannels: Array<Int>
)