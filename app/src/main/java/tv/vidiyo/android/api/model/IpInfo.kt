package tv.vidiyo.android.api.model

import tv.vidiyo.android.util.Constant

//{
//    "ip": "91.228.236.139",
//    "city": "Kichkas",
//    "region": "Zaporiz'ka Oblast'",
//    "country": "UA",
//    "loc": "47.8833,35.0833",
//    "org": "AS198030 Private Entrepreneur Shantyr Yuriy"
//}
class IpInfo(
        val ip: String,
        val city: String?,
        val region: String?,
        val country: String,
        val loc: String,
        val org: String
) {
    val latLng: LatLng
        get() {
            val l = loc.split(Constant.COMMA)
            return LatLng(l[0].toDouble(), l[1].toDouble())
        }
}