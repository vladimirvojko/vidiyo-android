package tv.vidiyo.android.api.model.user

import com.google.gson.annotations.SerializedName
import tv.vidiyo.android.api.ApiKey


class ContactFriends(
        @SerializedName(ApiKey.followingIds) var followingIds: Array<Int>,
        val users: Array<Friend>
)