package tv.vidiyo.android.api.model.social

import com.google.gson.annotations.SerializedName
import tv.vidiyo.android.api.ApiKey
import tv.vidiyo.android.util.Constant

class SocialAccount(
        var id: Int,
        @SerializedName(ApiKey.userId) var userId: Int,
        @SerializedName(ApiKey.socialId) var socialId: String?,
        @SerializedName(ApiKey.socialType) var socialType: SocialType,
        var status: SocialStatus?,
        var withPublicAccess: Boolean = false
) {

    constructor(type: SocialType) : this(0, 0, Constant.EMPTY, type, null)

    val isConnected: Boolean get() = status == SocialStatus.connected

    fun apply(account: SocialAccount) {
        id = account.id
        userId = account.userId
        socialId = account.socialId
        socialType = account.socialType
        status = account.status ?: if (socialId != null) SocialStatus.connected else SocialStatus.none
        withPublicAccess = account.withPublicAccess
    }
}