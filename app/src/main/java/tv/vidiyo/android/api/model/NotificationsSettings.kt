package tv.vidiyo.android.api.model

import com.google.gson.annotations.SerializedName
import tv.vidiyo.android.api.ApiKey
import tv.vidiyo.android.extension.toInt

class NotificationsSettings(
        @SerializedName(ApiKey.followerRequest) var followerRequest: Boolean,
        @SerializedName(ApiKey.acceptedFollowerRequest) var acceptedRequest: Boolean,
        @SerializedName(ApiKey.newEpisodes) var newEpisodes: Boolean,
        @SerializedName(ApiKey.friendsOnVidiyo) var friendsOnVidiyo: Boolean,
        @SerializedName(ApiKey.topicHasNewVideo) var topics: Boolean,
        @SerializedName(ApiKey.castHasNewVideo) var cast: Boolean,
        @SerializedName(ApiKey.commentLiked) var commentLiked: Boolean,
        @SerializedName(ApiKey.repliedToYourComment) var commentReplied: Boolean
) {
    fun update(index: Int, value: Boolean) {
        when (index) {
            0 -> followerRequest = value
            1 -> acceptedRequest = value
            2 -> newEpisodes = value
            3 -> friendsOnVidiyo = value
            4 -> topics = value
            5 -> cast = value
            6 -> commentLiked = value
            7 -> commentReplied = value
        }
    }

    fun valueOf(index: Int): Int {
        return (when (index) {
            0 -> followerRequest
            1 -> acceptedRequest
            2 -> newEpisodes
            3 -> friendsOnVidiyo
            4 -> topics
            5 -> cast
            6 -> commentLiked
            7 -> commentReplied

            else -> throw IllegalArgumentException("Unknown index")
        }).toInt()
    }

    fun keyOf(index: Int): String {
        return when (index) {
            0 -> ApiKey.followerRequest
            1 -> ApiKey.acceptedFollowerRequest
            2 -> ApiKey.newEpisodes
            3 -> ApiKey.friendsOnVidiyo
            4 -> ApiKey.topicHasNewVideo
            5 -> ApiKey.castHasNewVideo
            6 -> ApiKey.commentLiked
            7 -> ApiKey.repliedToYourComment

            else -> throw IllegalArgumentException("Unknown index")
        }
    }
}