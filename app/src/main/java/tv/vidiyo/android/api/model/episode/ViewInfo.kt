package tv.vidiyo.android.api.model.episode

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import tv.vidiyo.android.api.ApiKey

class ViewInfo(
        var id: Int,
        @SerializedName(ApiKey.viewTime) var time: Float,
        var duration: Float,
        var finished: Boolean,
        var episode: Episode?,
        @SerializedName(ApiKey.beforeDate) var beforeDate: Long?
) : Parcelable {

    companion object CREATOR : Parcelable.Creator<ViewInfo> {
        override fun createFromParcel(parcel: Parcel) = ViewInfo(parcel)
        override fun newArray(size: Int): Array<ViewInfo?> = arrayOfNulls(size)
    }

    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readFloat(),
            parcel.readFloat(),
            parcel.readByte() != 0.toByte(),
            parcel.readParcelable(Episode::class.java.classLoader),
            parcel.readValue(Long::class.java.classLoader) as? Long
    )

    fun update(viewInfo: ViewInfo) {
        if (id == viewInfo.id) {
            time = viewInfo.time
            duration = viewInfo.duration
            finished = viewInfo.finished
        }
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeFloat(time)
        parcel.writeFloat(duration)
        parcel.writeByte(if (finished) 1 else 0)
        parcel.writeParcelable(episode, flags)
        parcel.writeValue(beforeDate)
    }

    override fun describeContents() = 0
}