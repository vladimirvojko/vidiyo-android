package tv.vidiyo.android.api.model.episode

import com.google.gson.annotations.SerializedName
import tv.vidiyo.android.api.ApiKey

class Product(
        val id: Int,
        var name: String,
        @SerializedName(ApiKey.brandImage) var brandImage: String,
        @SerializedName(ApiKey.productImage) var productImage: String,
        var price: Float,
        var url: String,
        @SerializedName(ApiKey.episodeId) var episodeId: Int
)