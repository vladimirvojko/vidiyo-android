package tv.vidiyo.android.api.model

import java.lang.Double.doubleToLongBits

class LatLng(
        val latitude: Double,
        val longitude: Double
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as LatLng
        return latitude == other.latitude && longitude == other.longitude
    }

    override fun hashCode(): Int {
        val lat = doubleToLongBits(latitude)
        val i = 31 + (lat xor lat.ushr(32)).toInt()
        val lng = doubleToLongBits(longitude)
        return i * 31 + (lng xor lng.ushr(32)).toInt()
    }

    override fun toString() = "latLng [$latitude, $longitude]"
}