package tv.vidiyo.android.api.model.social

import android.os.Parcel
import android.os.Parcelable
import com.facebook.AccessToken
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import org.json.JSONObject
import tv.vidiyo.android.util.Constant

class SocialAuth(
        val type: SocialAuthType,
        val token: String,
        val userId: String,
        var imageUrl: String?,

        var name: String?,
        var email: String?
) : Parcelable {

    constructor(accessToken: AccessToken, obj: JSONObject) : this(
            SocialAuthType.facebook,
            accessToken.token,
            accessToken.userId,
            obj.getJSONObject("picture")?.getJSONObject("data")?.getString("url"),
            obj.getString("name"),
            obj.getString("email")
    )

    constructor(account: GoogleSignInAccount) : this(
            SocialAuthType.google,
            account.idToken ?: Constant.EMPTY,
            account.id ?: Constant.EMPTY,
            account.photoUrl.toString(),
            account.displayName,
            account.email
    )

    constructor(parcel: Parcel) : this(
            SocialAuthType.values()[parcel.readInt()],
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(type.ordinal)
        parcel.writeString(token)
        parcel.writeString(userId)
        parcel.writeString(imageUrl)
        parcel.writeString(name)
        parcel.writeString(email)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<SocialAuth> {
        override fun createFromParcel(parcel: Parcel): SocialAuth {
            return SocialAuth(parcel)
        }

        override fun newArray(size: Int): Array<SocialAuth?> {
            return arrayOfNulls(size)
        }
    }
}