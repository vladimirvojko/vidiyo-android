package tv.vidiyo.android.api.model.cast

import tv.vidiyo.android.api.model.social.Socials

class CastInfo(
        var bio: String? = null,
        var socials: Socials
) : Cast()