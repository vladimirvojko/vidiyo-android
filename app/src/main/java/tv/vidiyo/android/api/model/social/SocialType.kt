package tv.vidiyo.android.api.model.social

import com.google.gson.annotations.SerializedName
import tv.vidiyo.android.R

enum class SocialType {
    instagram,
    @SerializedName("FACEBOOK") facebook,
    @SerializedName("TWITTER") twitter,
    snapchat,
    pinterest,
    imdb,
    @SerializedName("GOOGLE") google;

    val title: Int
        get() = when (this) {
            instagram -> R.string.instagram
            facebook -> R.string.facebook
            twitter -> R.string.twitter
            snapchat -> R.string.snapchat
            pinterest -> R.string.pinterest
            imdb -> R.string.imdb
            google -> R.string.google_plus
        }

    val icon24: Int
        get() = when (this) {
            instagram -> R.drawable.svg_instagram
            facebook -> R.drawable.svg_facebook
            twitter -> R.drawable.svg_twitter
            snapchat -> R.drawable.svg_snapchat
            pinterest -> R.drawable.svg_pinterest
            imdb -> R.drawable.svg_imdb
            google -> R.drawable.svg_google
        }

    val icon16: Int
        get() = when (this) {
            facebook -> R.drawable.svg_facebook_16_blue
            twitter -> R.drawable.svg_twitter_16
            google -> R.drawable.svg_google_16

            else -> icon24
        }
}