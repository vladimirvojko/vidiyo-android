package tv.vidiyo.android.api.model.social

import com.google.gson.annotations.SerializedName

enum class SocialStatus {
    @SerializedName("connected") connected,
    @SerializedName("deleted") deleted,
    @SerializedName("none") none
}