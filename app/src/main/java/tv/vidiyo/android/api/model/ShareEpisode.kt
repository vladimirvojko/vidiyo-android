package tv.vidiyo.android.api.model

import com.google.gson.annotations.SerializedName
import tv.vidiyo.android.api.ApiKey
import tv.vidiyo.android.api.model.social.SocialToken

class ShareEpisode(
        @SerializedName(ApiKey.caption) val caption: String,
        @SerializedName(ApiKey.accessTokens) val tokens: Array<SocialToken>
)