package tv.vidiyo.android.api.model.user

import android.net.Uri
import android.os.Parcel
import android.os.Parcelable
import tv.vidiyo.android.extension.readBoolean
import tv.vidiyo.android.extension.writeBoolean
import tv.vidiyo.android.model.Contact
import tv.vidiyo.android.util.Constant

class Friend : User {
    val photoUri: Uri?
    var phone: String?
    var email: String?
    var isSelected: Boolean

    constructor(
            id: Int,
            username: String?,
            name: String?,
            image: String?,
            photoUri: Uri?,
            phone: String? = null,
            email: String? = null,
            isSelected: Boolean = false
    ) : super(id, username, name, image) {
        this.photoUri = photoUri
        this.phone = phone
        this.email = email
        this.isSelected = isSelected
    }

    constructor(contact: Contact) : this(
            Constant.NO_ID,
            null,
            contact.name,
            null,
            contact.displayPhotoUri ?: contact.thumbnailPhotoUri,
            contact.phones.firstOrNull(),
            contact.emails.firstOrNull()
    )

    constructor(parcel: Parcel) : super(parcel) {
        photoUri = if (parcel.readBoolean()) {
            parcel.readParcelable(Uri::class.java.classLoader)
        } else {
            null
        }
        phone = parcel.readString()
        email = parcel.readString()
        isSelected = parcel.readBoolean()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        super.writeToParcel(parcel, flags)

        parcel.writeBoolean(photoUri != null)
        photoUri?.let { parcel.writeParcelable(it, flags) }
        parcel.writeString(phone)
        parcel.writeString(email)
        parcel.writeBoolean(isSelected)
    }

    companion object CREATOR : Parcelable.Creator<Friend> {
        override fun createFromParcel(parcel: Parcel): Friend {
            return Friend(parcel)
        }

        override fun newArray(size: Int): Array<Friend?> {
            return arrayOfNulls(size)
        }
    }
}