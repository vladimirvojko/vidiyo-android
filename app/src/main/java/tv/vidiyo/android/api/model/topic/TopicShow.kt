package tv.vidiyo.android.api.model.topic

import com.google.gson.annotations.SerializedName
import tv.vidiyo.android.api.ApiKey

class TopicShow(
        @SerializedName(ApiKey.topicId) var id: Int = 0,
        @SerializedName(ApiKey.showId) var showId: Int = 0
)