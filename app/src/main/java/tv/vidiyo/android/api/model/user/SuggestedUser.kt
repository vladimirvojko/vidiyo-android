package tv.vidiyo.android.api.model.user

class SuggestedUser(
        var source: Source = Source.vidiyo,
        var status: SubscriptionStatus = SubscriptionStatus.none
) : User() {
    fun toFollowingUser() = FollowingUser(0, 0, status, SubscriptionStatus.none, 0).also {
        it.id = id
        it.name = name
        it.username = username
        it.image = image
    }
}