package tv.vidiyo.android.api.model

data class Header(val textResId: Int)
data class Item(
        val iconResId: Int,
        val titleResId: Int,
        val subtitleResId: Int
)