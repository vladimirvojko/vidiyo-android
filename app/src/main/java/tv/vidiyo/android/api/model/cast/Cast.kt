package tv.vidiyo.android.api.model.cast

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import tv.vidiyo.android.api.ApiKey
import tv.vidiyo.android.api.model.Subscribable

open class Cast(
        var name: String? = null,
        @SerializedName(ApiKey.type) var typeInt: Int = 0,
        var image: String? = null,
        var subscribers: Int = 0
) : Subscribable(), Parcelable {

    companion object CREATOR : Parcelable.Creator<Cast> {
        override fun createFromParcel(parcel: Parcel): Cast {
            return Cast(parcel)
        }

        override fun newArray(size: Int): Array<Cast?> {
            return arrayOfNulls(size)
        }
    }

    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readInt(),
            parcel.readString(),
            parcel.readInt()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeInt(typeInt)
        parcel.writeString(image)
        parcel.writeInt(subscribers)
    }

    val type: CastType get() = try {
        CastType.values()[typeInt]
    } catch (e: IndexOutOfBoundsException) {
        CastType.unknown
    }

    override fun describeContents() = 0
}