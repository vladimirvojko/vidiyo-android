package tv.vidiyo.android.api

import android.content.Context
import okhttp3.Interceptor
import okhttp3.Response
import tv.vidiyo.android.BuildConfig
import tv.vidiyo.android.R
import tv.vidiyo.android.account.UserAccountManager
import tv.vidiyo.android.ui.boarding.SplashActivity
import java.io.IOException

class AuthInterceptor(
        private val context: Context,
        private val manager: UserAccountManager
) : Interceptor {

    private val token: String? get() = manager.token

    override fun intercept(chain: Interceptor.Chain): Response? {
        val original = chain.request()

        val t = token
        if (t != null) {
            val requestBuilder = original.newBuilder().apply {
                if (original.url().toString().contains(BuildConfig.SERVER_URL)) {
                    header("Authorization", "Bearer " + t)
                }
            }.method(original.method(), original.body())

            val response = chain.proceed(requestBuilder.build())
            if (response.code() == 403) {
                manager.remove(null)
                SplashActivity.start(context, R.string.error_invalid_token)

                throw IOException("Invalid token")
            }

            return response
        }

        return chain.proceed(original)
    }
}