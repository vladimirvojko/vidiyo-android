package tv.vidiyo.android.api.model.social

class Socials(
        var instagram: String? = null,
        var facebook: String? = null,
        var twitter: String? = null,
        var snapchat: String? = null,
        var pinterest: String? = null,
        var imdb: String? = null
)

val Socials.connections: List<SocialConnection>?
    get() {
        val list = mutableListOf<SocialConnection>()
        instagram?.let { list.add(SocialConnection(it, SocialType.instagram)) }
        facebook?.let { list.add(SocialConnection(it, SocialType.facebook)) }
        twitter?.let { list.add(SocialConnection(it, SocialType.twitter)) }
        snapchat?.let { list.add(SocialConnection(it, SocialType.snapchat)) }
        pinterest?.let { list.add(SocialConnection(it, SocialType.pinterest)) }
        imdb?.let { list.add(SocialConnection(it, SocialType.imdb)) }
        return list
    }