package tv.vidiyo.android.api.model.social

import com.google.gson.annotations.SerializedName
import tv.vidiyo.android.api.ApiKey

class Connection(
        @SerializedName(ApiKey.isConnected) var isConnected: Boolean = false,
        var count: Int = 0
)