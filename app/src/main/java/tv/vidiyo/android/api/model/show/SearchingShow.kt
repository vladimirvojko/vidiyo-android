package tv.vidiyo.android.api.model.show

class SearchingShow(
        id: Int,
        name: String,
        image: String?,
        subscribers: Int,
        var channel: String?
) : Show(id, false, name, image, subscribers, null)