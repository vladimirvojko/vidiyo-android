package tv.vidiyo.android.api.instagram

import com.google.gson.annotations.SerializedName
import tv.vidiyo.android.api.Social

class IGImage(
        val url: String,
        val width: Int,
        val height: Int
)

class IGImages(
        @SerializedName(Social.Key.STANDARD_RESOLUTION) val standard: IGImage,
        @SerializedName(Social.Key.LOW_RESOLUTION) val low: IGImage
)