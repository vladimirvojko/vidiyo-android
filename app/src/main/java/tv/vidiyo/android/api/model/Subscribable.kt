package tv.vidiyo.android.api.model

import com.google.gson.annotations.SerializedName
import tv.vidiyo.android.api.ApiKey

open class Subscribable(
        var id: Int = 0,
        @SerializedName(ApiKey.isSubscribed) var isSubscribed: Boolean = false
)