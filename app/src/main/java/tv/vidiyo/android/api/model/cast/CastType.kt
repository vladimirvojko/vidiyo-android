package tv.vidiyo.android.api.model.cast

import tv.vidiyo.android.R

enum class CastType {
    unknown, host, coHost, specialGuest, star,
    guestStar, judges, anchor, coAnchor, narrator;

    val stringId: Int get() = when (this) {
        CastType.host -> R.string.cast_host
        CastType.coHost -> R.string.cast_co_host
        CastType.specialGuest -> R.string.cast_special_guest
        CastType.star -> R.string.cast_star
        CastType.guestStar -> R.string.cast_guest_star
        CastType.judges -> R.string.cast_judges
        CastType.anchor -> R.string.cast_anchor
        CastType.coAnchor -> R.string.cast_co_anchor
        CastType.narrator -> R.string.cast_narrator
        CastType.unknown -> R.string.unknown
    }
}