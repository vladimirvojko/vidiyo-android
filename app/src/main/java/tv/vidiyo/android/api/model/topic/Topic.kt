package tv.vidiyo.android.api.model.topic

import android.os.Parcel
import android.os.Parcelable
import tv.vidiyo.android.api.model.Subscribable

class Topic(
        var name: String = "",
        var image: String? = null,
        var channel: String? = null,
        var subscribers: Int = 0
) : Subscribable(), Parcelable {

    companion object CREATOR : Parcelable.Creator<Topic> {
        override fun createFromParcel(parcel: Parcel): Topic {
            return Topic(parcel)
        }

        override fun newArray(size: Int): Array<Topic?> {
            return arrayOfNulls(size)
        }
    }

    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readInt()
    ) {
        id = parcel.readInt()
        isSubscribed = parcel.readByte() == 1.toByte()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(image)
        parcel.writeString(channel)
        parcel.writeInt(subscribers)
        parcel.writeInt(id)
        parcel.writeByte(if (isSubscribed) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }
}