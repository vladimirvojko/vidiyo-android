package tv.vidiyo.android.api.model.channel

import com.google.gson.annotations.SerializedName
import tv.vidiyo.android.api.ApiKey
import tv.vidiyo.android.api.model.episode.Episode
import tv.vidiyo.android.api.model.episode.Media
import tv.vidiyo.android.api.model.show.Show
import tv.vidiyo.android.api.model.topic.Topic
import tv.vidiyo.android.api.model.topic.TopicShow

class ChannelInfo(
        var media: Media? = null,
        var shows: Array<Show>,
        var topics: Array<Topic>,
        @SerializedName(ApiKey.topicShows) var topicShows: Array<TopicShow>,
        @SerializedName(ApiKey.featuredVideo) var featured: Episode? = null
)