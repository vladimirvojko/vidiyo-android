package tv.vidiyo.android.api.model.social

enum class SocialAuthType {
    none, facebook, google;

    val authEndpoint: String
        get() = when (this) {
            google -> "google-auth"
            facebook -> "facebook-auth"
            else -> throw IllegalStateException("Auth endpoint provided only for Facebook and Google")
        }

    val registerEndpoint: String
        get() = when (this) {
            google -> "google-register"
            facebook -> "facebook-register"
            else -> throw IllegalStateException("Register endpoint provided only for Facebook and Google")
        }
}