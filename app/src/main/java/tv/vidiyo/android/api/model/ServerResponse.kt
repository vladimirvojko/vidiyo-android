package tv.vidiyo.android.api.model

class ServerResponse<out T>(
        val status: String,
        val data: T?,
        val error: Message?,
        val meta: Count?
)