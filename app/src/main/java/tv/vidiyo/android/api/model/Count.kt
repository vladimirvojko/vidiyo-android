package tv.vidiyo.android.api.model

import com.google.gson.annotations.SerializedName
import tv.vidiyo.android.api.ApiKey

class Count(@SerializedName(ApiKey.count, alternate = arrayOf(ApiKey.updated, ApiKey.totalCount)) val count: Int)