package tv.vidiyo.android.api.model.user

import android.accounts.Account
import com.google.gson.annotations.SerializedName
import tv.vidiyo.android.account.AccountAuthenticator
import tv.vidiyo.android.api.ApiKey
import tv.vidiyo.android.api.model.social.SocialAccount

class UserAccount(
        id: Int,
        username: String?,
        name: String?,
        image: String?,
        account: Account?,
        var gender: String?,
        var password: String? = null,
        var token: String,
        @SerializedName(ApiKey.social) var socials: Array<SocialAccount>?
) : Profile(id, username, name, image) {

    var account: Account = account ?: Account(username, AccountAuthenticator.ACCOUNT_TYPE)
    val isAnonymous: Boolean get() = username == null
}

fun UserAccount.toFollowingUser() = FollowingUser(0, 0, status, backprop, 0).also {
    it.id = id
    it.name = name
    it.username = username
    it.image = image
}