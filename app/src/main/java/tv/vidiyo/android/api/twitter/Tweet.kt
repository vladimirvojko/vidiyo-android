package tv.vidiyo.android.api.twitter

import com.google.gson.annotations.SerializedName
import tv.vidiyo.android.api.ApiKey
import tv.vidiyo.android.api.Dateable
import tv.vidiyo.android.api.Openable
import tv.vidiyo.android.api.Social
import tv.vidiyo.android.extension.CREATED_TIME_FORMAT_TWITTER
import java.util.*

class Tweet(
        val id: String,
        @SerializedName(Social.Key.ID_STR) val idStr: String,
        val text: String,
        val user: TWUser,
        @SerializedName(ApiKey.createdAt) val created: String
) : Dateable, Openable {
    override val date: Date get() = CREATED_TIME_FORMAT_TWITTER.parse(created)
    override val url: String get() = "https://twitter.com/${user.screenName}/status/$idStr"
}