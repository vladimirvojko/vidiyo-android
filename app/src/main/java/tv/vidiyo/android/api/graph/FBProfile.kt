package tv.vidiyo.android.api.graph

class FBProfile(
        val id: String,
        val name: String,
        val picture: FBPicture?
)