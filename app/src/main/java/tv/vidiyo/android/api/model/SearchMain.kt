package tv.vidiyo.android.api.model

import com.google.gson.annotations.SerializedName
import tv.vidiyo.android.api.ApiKey
import tv.vidiyo.android.api.model.cast.Cast
import tv.vidiyo.android.api.model.episode.Episode
import tv.vidiyo.android.api.model.show.FriendShow
import tv.vidiyo.android.api.model.show.Show
import tv.vidiyo.android.api.model.topic.Topic

class SearchMain(
        @SerializedName(ApiKey.casts) var casts: Array<Cast>,
        @SerializedName(ApiKey.showsFriends) var showsFriends: Array<FriendShow>,
        @SerializedName(ApiKey.topicsTop) var topicsTop: Array<Topic>,
        @SerializedName(ApiKey.showsTop) var showsTop: Array<Show>,
        @SerializedName(ApiKey.featuredVideo) var featured: Episode
)