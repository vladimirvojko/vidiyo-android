package tv.vidiyo.android.api.model.social

import com.google.gson.annotations.SerializedName
import tv.vidiyo.android.api.ApiKey

class SocialToken(
        @SerializedName(ApiKey.socialType) val type: SocialType,
        @SerializedName(ApiKey.accessToken) val token: String,
        @SerializedName(ApiKey.accessTokenSecret) val secret: String?
) {
    constructor(account: SocialAccount, token: String, secret: String?)
            : this(account.socialType, token, secret)

    override fun hashCode() = type.hashCode()
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other is SocialToken) {
            return this.type == other.type
        }
        return false
    }
}