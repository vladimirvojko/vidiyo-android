package tv.vidiyo.android.api.model

import android.os.Parcel
import android.os.Parcelable

class Country(
        val name: String,
        val iso2: String,
        val iso3: String,
        val dial: String
) : Parcelable {

    constructor(parcel: Parcel) : this(parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(iso2)
        parcel.writeString(iso3)
        parcel.writeString(dial)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Country> {
        override fun createFromParcel(parcel: Parcel): Country {
            return Country(parcel)
        }

        override fun newArray(size: Int): Array<Country?> {
            return arrayOfNulls(size)
        }
    }
}

val Country.CREATOR.default get() = Country("US", "US", "USA", "1")
val Country.CREATOR.debug get() = Country("Ukraine", "UA", "UKR", "380")