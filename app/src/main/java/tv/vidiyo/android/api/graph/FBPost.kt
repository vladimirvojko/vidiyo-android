package tv.vidiyo.android.api.graph

import com.google.gson.annotations.SerializedName
import tv.vidiyo.android.api.Dateable
import tv.vidiyo.android.api.Openable
import tv.vidiyo.android.api.Social
import tv.vidiyo.android.extension.CREATED_TIME_FORMAT_FACEBOOK
import java.util.*

class FBPost(
        val id: String,
        val message: String?,
        val story: String?,
        val caption: String?,
        val description: String?,
        @SerializedName(Social.Key.FULL_PICTURE) val picture: String?,
        val from: FBProfile?,
        @SerializedName(Social.Key.CREATED_TIME) val created: String?
) : Dateable, Openable {
    override val date: Date get() = CREATED_TIME_FORMAT_FACEBOOK.parse(created)
    override val url: String get() = "https://facebook.com/$id"
}