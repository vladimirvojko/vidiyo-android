package tv.vidiyo.android.api.model.comment

import com.google.gson.annotations.SerializedName
import tv.vidiyo.android.api.ApiKey
import tv.vidiyo.android.api.model.user.User

class Comment(
        var id: Int,
        var text: String,
        @SerializedName(ApiKey.parentId) var parentId: Int,
        @SerializedName(ApiKey.createdAt) var created: Long,
        var user: User,
        var comments: Replies? = null,
        var likes: Array<Like> = emptyArray()
)