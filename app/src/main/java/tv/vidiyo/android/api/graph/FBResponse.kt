package tv.vidiyo.android.api.graph

class FBResponse<T>(
        val data: Array<T>,
        val paging: FBPaging
)

class FBPaging(
        val previous: String?,
        val next: String?
)