package tv.vidiyo.android.api.model

import com.google.gson.annotations.SerializedName
import tv.vidiyo.android.api.ApiKey

class ConnectionCount(
        @SerializedName(ApiKey.isConnected) val connected: Boolean,
        @SerializedName(ApiKey.count) val count: Int
)