package tv.vidiyo.android.api.model

import com.google.gson.annotations.SerializedName
import tv.vidiyo.android.api.ApiKey
import tv.vidiyo.android.api.model.episode.Episode
import tv.vidiyo.android.api.model.episode.ViewInfo
import tv.vidiyo.android.api.model.show.FriendShow
import tv.vidiyo.android.api.model.show.Show
import tv.vidiyo.android.api.model.topic.Topic

class HomeData(
        @SerializedName(ApiKey.showsFriends) var showsFriends: Array<FriendShow>,
        @SerializedName(ApiKey.showsInChannels) var showsChannels: Array<Show>,
        @SerializedName(ApiKey.showsSubscribed) var showsSubscribed: Array<Show>,
        @SerializedName(ApiKey.topicsTop) var topicsTop: Array<Topic>,
        var topics: Array<Topic>,
        @SerializedName(ApiKey.continueWatching) var watching: Array<ViewInfo>,
        @SerializedName(ApiKey.featuredVideo) var featured: Episode
)