package tv.vidiyo.android.source

import tv.vidiyo.android.api.model.user.Friend
import tv.vidiyo.android.model.Contact
import tv.vidiyo.android.model.FriendsListType

interface FriendsDataSource {
    var type: FriendsListType
    var contacts: List<Contact>?
    var friends: List<Friend>?
}