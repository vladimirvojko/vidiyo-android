package tv.vidiyo.android.source.watching

import retrofit2.Call
import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.api.model.ServerResponse
import tv.vidiyo.android.api.model.episode.ViewInfo
import tv.vidiyo.android.api.model.show.NewShowEpisode
import tv.vidiyo.android.base.adapter.provider.MutableListDataProvider
import tv.vidiyo.android.model.NotificationType
import tv.vidiyo.android.source.LoadableDataSource
import tv.vidiyo.android.util.Constant
import tv.vidiyo.android.util.NotificationsCenter

class WatchingRepository(service: VidiyoService, center: NotificationsCenter) : NotificationsCenter.Listener {

    init {
        center.addListener(this)
    }

    val resumeRepository = object : InnerWatchingRepository<ViewInfo>(service) {
        override fun createRequest(limit: Int): Call<ServerResponse<Array<ViewInfo>>>
                = service.viewedInfo(limit, provider.items.lastOrNull()?.beforeDate)
    }

    val newEpisodesRepository = object : InnerWatchingRepository<NewShowEpisode>(service) {
        override fun createRequest(limit: Int): Call<ServerResponse<Array<NewShowEpisode>>> = service.newEpisodes()
    }

    fun getRepository(forResume: Boolean) = if (forResume) resumeRepository else newEpisodesRepository

    override fun onAction(action: Int, type: String) {
        if (type == NotificationType.EPISODE_NEW) {
            newEpisodesRepository.refresh()
        }
    }
}

abstract class InnerWatchingRepository<T>(
        protected val service: VidiyoService,
        private val limit: Int = Constant.LIMIT
) : LoadableDataSource<T>(), WatchingDataSource<T> {

    override val provider: MutableListDataProvider<T> = MutableListDataProvider()
    override val apiRequest: Call<ServerResponse<Array<T>>> get() = createRequest(limit)

    abstract fun createRequest(limit: Int): Call<ServerResponse<Array<T>>>

    override fun additionalPreparation(array: Array<T>) {
        provider.addAll(array.asList(), true)
    }
}