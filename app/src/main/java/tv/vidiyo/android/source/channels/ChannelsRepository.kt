package tv.vidiyo.android.source.channels

import retrofit2.Call
import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.api.model.ServerResponse
import tv.vidiyo.android.api.model.channel.Channel
import tv.vidiyo.android.api.model.user.UserChannels
import tv.vidiyo.android.source.DataSource

class ChannelsRepository(private val service: VidiyoService) : DataSource<UserChannels>(), ChannelsDataSource {

    override val apiRequest: Call<ServerResponse<UserChannels>> get() = service.userChannels()
    override val channels: Array<Channel> get() = data?.channels ?: emptyArray()
    override val userChannelsIds: Array<Int> get() = data?.userChannels ?: emptyArray()
    override val size get() = channels.size

    override fun getItem(position: Int): Channel = channels[position]
}