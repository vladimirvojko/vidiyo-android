package tv.vidiyo.android.source.search

import tv.vidiyo.android.base.adapter.provider.MutableListDataProvider


interface SearchResultsPageDataSource<T> {
    var query: String?
    val provider: MutableListDataProvider<T>
    val totalCount: Int

    fun search(query: String, force: Boolean = false): Boolean
}