package tv.vidiyo.android.source.home

import retrofit2.Call
import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.api.model.HomeData
import tv.vidiyo.android.api.model.ServerResponse
import tv.vidiyo.android.api.model.episode.Episode
import tv.vidiyo.android.api.model.episode.ViewInfo
import tv.vidiyo.android.api.model.show.FriendShow
import tv.vidiyo.android.api.model.show.Show
import tv.vidiyo.android.api.model.topic.Topic
import tv.vidiyo.android.base.adapter.provider.ListDataProvider
import tv.vidiyo.android.source.DataSource
import tv.vidiyo.android.source.DataType

class HomeRepository(private val service: VidiyoService) : DataSource<HomeData>(), HomeDataSource {

    override val apiRequest: Call<ServerResponse<HomeData>> get() = service.home()

    val showsFriendsProvider: ListDataProvider<FriendShow> = object : ListDataProvider<FriendShow> {
        override val items: List<FriendShow>? get() = data?.showsFriends?.asList()
    }
    val showsChannelsProvider: ListDataProvider<Show> = object : ListDataProvider<Show> {
        override val items: List<Show>? get() = data?.showsChannels?.asList()
    }
    val showsSubscribedProvider: ListDataProvider<Show> = object : ListDataProvider<Show> {
        override val items: List<Show>? get() = data?.showsSubscribed?.asList()
    }
    val topicsTopProvider: ListDataProvider<Topic> = object : ListDataProvider<Topic> {
        override val items: List<Topic>? get() = data?.topicsTop?.asList()
    }
    val topicsProvider: ListDataProvider<Topic> = object : ListDataProvider<Topic> {
        override val items: List<Topic>? get() = data?.topics?.asList()
    }
    val watchingProvider: ListDataProvider<ViewInfo> = object : ListDataProvider<ViewInfo> {
        override val items: List<ViewInfo>? get() = data?.watching?.asList()
    }

    override val featured: Episode? get() = data?.featured
    override val size get() = 1

    override fun getProviderFor(@DataType type: Int) = when (type) {
        DataType.TYPE_WATCHING -> watchingProvider
        DataType.TYPE_FRIENDS_WATCHING -> showsFriendsProvider
        DataType.TYPE_TOP_CHANNELS -> topicsTopProvider
        DataType.TYPE_TOP_PICKS -> showsChannelsProvider
        DataType.TYPE_SUBSCRIPTION -> showsSubscribedProvider
        DataType.TYPE_YOUR_CHANNELS -> topicsProvider
        else -> throw IllegalArgumentException()
    }
}