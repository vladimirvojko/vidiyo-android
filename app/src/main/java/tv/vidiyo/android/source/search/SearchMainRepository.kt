package tv.vidiyo.android.source.search

import android.annotation.SuppressLint
import retrofit2.Call
import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.api.model.SearchMain
import tv.vidiyo.android.api.model.ServerResponse
import tv.vidiyo.android.api.model.cast.Cast
import tv.vidiyo.android.api.model.episode.Episode
import tv.vidiyo.android.api.model.show.FriendShow
import tv.vidiyo.android.api.model.show.Show
import tv.vidiyo.android.api.model.topic.Topic
import tv.vidiyo.android.base.adapter.provider.ListDataProvider
import tv.vidiyo.android.source.DataSource
import tv.vidiyo.android.source.DataType

class SearchMainRepository(private val service: VidiyoService) : DataSource<SearchMain>(), SearchMainDataSource {

    override val apiRequest: Call<ServerResponse<SearchMain>> get() = service.searchMain()
    override var lastQuery: String? = null

    val castsProvider = object : ListDataProvider<Cast> {
        override val items: List<Cast>? get() = data?.casts?.asList()
    }
    val showsFriendsProvider: ListDataProvider<FriendShow> = object : ListDataProvider<FriendShow> {
        override val items: List<FriendShow>? get() = data?.showsFriends?.asList()
    }
    val topPicksProvider: ListDataProvider<Show> = object : ListDataProvider<Show> {
        override val items: List<Show>? get() = data?.showsTop?.asList()
    }
    val trendingChannelsProvider: ListDataProvider<Topic> = object : ListDataProvider<Topic> {
        override val items: List<Topic>? get() = data?.topicsTop?.asList()
    }

    override val featured: Episode? get() = data?.featured
    override val size: Int get() = 1

    @SuppressLint("SwitchIntDef")
    override fun getListDataProviderFor(@DataType type: Int): ListDataProvider<*> = when (type) {
        DataType.TYPE_CASTS_SMALL -> castsProvider
        DataType.TYPE_FRIENDS_WATCHING -> showsFriendsProvider
        DataType.TYPE_TOP_PICKS -> topPicksProvider
        DataType.TYPE_TOP_CHANNELS -> trendingChannelsProvider
        else -> throw IllegalArgumentException()
    }
}