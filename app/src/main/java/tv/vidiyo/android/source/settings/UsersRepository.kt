package tv.vidiyo.android.source.settings

import retrofit2.Call
import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.api.model.ServerResponse
import tv.vidiyo.android.api.model.user.FollowingType
import tv.vidiyo.android.api.model.user.FollowingUser
import tv.vidiyo.android.api.requestFor
import tv.vidiyo.android.source.LoadableDataSource
import tv.vidiyo.android.util.Constant

class UsersRepository(
        private val service: VidiyoService,
        private val type: FollowingType,
        private val id: Int = 0,
        private val limit: Int = Constant.LIMIT
) : LoadableDataSource<FollowingUser>(), UsersDataSource {

    override val users: Array<FollowingUser> get() = data ?: emptyArray()
    override val apiRequest: Call<ServerResponse<Array<FollowingUser>>> get() = service.requestFor(type, id, limit, users.lastOrNull()?.beforeDate)
    override val size: Int get() = users.size

    override fun handleResponse(response: ServerResponse<Array<FollowingUser>>) {
        super.handleResponse(response)
        val data = response.data
        totalCount = users.size + if (data == null || data.isEmpty() || data.size < Constant.LIMIT) 0 else 5
    }
}