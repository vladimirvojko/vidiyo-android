package tv.vidiyo.android.source

import tv.vidiyo.android.base.adapter.provider.DataProvider

interface ProvidedDataSource<out P : DataProvider> {
    val provider: P
}