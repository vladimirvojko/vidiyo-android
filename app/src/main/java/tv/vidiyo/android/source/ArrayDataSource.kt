package tv.vidiyo.android.source

abstract class ArrayDataSource<T> : DataSource<Array<T>>() {
    override val size get() = data?.size ?: 0
}