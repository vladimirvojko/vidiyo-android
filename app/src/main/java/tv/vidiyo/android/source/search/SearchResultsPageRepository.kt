package tv.vidiyo.android.source.search

import retrofit2.Call
import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.api.model.ServerResponse
import tv.vidiyo.android.api.model.cast.Cast
import tv.vidiyo.android.api.model.show.SearchingShow
import tv.vidiyo.android.api.model.topic.Topic
import tv.vidiyo.android.api.model.user.User
import tv.vidiyo.android.base.adapter.provider.MutableListDataProvider
import tv.vidiyo.android.model.InnerPageType
import tv.vidiyo.android.source.LoadableDataSource
import tv.vidiyo.android.util.Constant
import tv.vidiyo.android.util.SearchManager

abstract class SearchResultsPageRepository<T>(
        protected val service: VidiyoService,
        protected val manager: SearchManager,
        private val limit: Int = Constant.LIMIT
) : LoadableDataSource<T>(), SearchResultsPageDataSource<T> {

    override val provider: MutableListDataProvider<T> = MutableListDataProvider()
    override val apiRequest get() = createRequest(query, provider.getItemCount(), limit)
    override var query: String? = null

    abstract val recent: Array<T>
    abstract fun createRequest(query: String?, offset: Int? = null, limit: Int): Call<ServerResponse<Array<T>>>

    override fun search(query: String, force: Boolean): Boolean {
        if (query != this.query || force) {
            this.query = query
            totalCount = 0
            provider.clear()

            if (query.isEmpty()) {
                provider.addAll(recent.toList(), true)

            } else refresh()
            return true
        }
        return false
    }

    fun clear() {
        data = null
        provider.clear()
    }

    protected fun <T> slice(array: Array<T>): List<T>? {
        if (array.isNotEmpty()) {
            val slice = minOf(1, array.size - 1)
            return array.slice(IntRange(0, slice))
        }
        return null
    }

    override fun additionalPreparation(array: Array<T>) {
        provider.addAll(array.asList(), true)
    }
}

class SearchPeopleRepository(service: VidiyoService, manager: SearchManager) : SearchResultsPageRepository<User>(service, manager) {
    override val recent: Array<User> get() = manager.people
    override fun createRequest(query: String?, offset: Int?, limit: Int) = service.searchPeople(query, offset, limit)
    override fun additionalPreparation(array: Array<User>) {
        super.additionalPreparation(array)
        manager.set(InnerPageType.PEOPLE, slice(array)?.toTypedArray() ?: return)
    }
}

class SearchShowsRepository(service: VidiyoService, manager: SearchManager) : SearchResultsPageRepository<SearchingShow>(service, manager) {
    override val recent: Array<SearchingShow> get() = manager.shows
    override fun createRequest(query: String?, offset: Int?, limit: Int) = service.searchShows(query, offset, limit)
    override fun additionalPreparation(array: Array<SearchingShow>) {
        super.additionalPreparation(array)
        manager.set(InnerPageType.SHOWS, slice(array)?.toTypedArray() ?: return)
    }
}

class SearchCastRepository(service: VidiyoService, manager: SearchManager) : SearchResultsPageRepository<Cast>(service, manager) {
    override val recent: Array<Cast> get() = manager.casts
    override fun createRequest(query: String?, offset: Int?, limit: Int) = service.searchCast(query, offset, limit)
    override fun additionalPreparation(array: Array<Cast>) {
        super.additionalPreparation(array)
        manager.set(InnerPageType.CAST, slice(array)?.toTypedArray() ?: return)
    }
}

class SearchTopicsRepository(service: VidiyoService, manager: SearchManager) : SearchResultsPageRepository<Topic>(service, manager) {
    override val recent: Array<Topic> get() = manager.topics
    override fun createRequest(query: String?, offset: Int?, limit: Int) = service.searchTopics(query, offset, limit)
    override fun additionalPreparation(array: Array<Topic>) {
        super.additionalPreparation(array)
        manager.set(InnerPageType.TOPICS, slice(array)?.toTypedArray() ?: return)
    }
}