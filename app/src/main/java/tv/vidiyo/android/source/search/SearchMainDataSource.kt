package tv.vidiyo.android.source.search

import tv.vidiyo.android.api.model.episode.Episode
import tv.vidiyo.android.base.adapter.provider.ListDataProvider
import tv.vidiyo.android.source.DataType

interface SearchMainDataSource {
    val featured: Episode?
    var lastQuery: String?
    fun getListDataProviderFor(@DataType type: Int): ListDataProvider<*>
}