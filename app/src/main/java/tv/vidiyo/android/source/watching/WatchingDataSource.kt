package tv.vidiyo.android.source.watching

import tv.vidiyo.android.base.adapter.provider.MutableListDataProvider

interface WatchingDataSource<T> {
    val totalCount: Int
    val provider: MutableListDataProvider<T>
}