package tv.vidiyo.android.source.home

import tv.vidiyo.android.api.model.episode.Episode
import tv.vidiyo.android.base.adapter.provider.ListDataProvider
import tv.vidiyo.android.source.DataType

interface HomeDataSource {
    val featured: Episode?
    fun getProviderFor(@DataType type: Int): ListDataProvider<*>
}