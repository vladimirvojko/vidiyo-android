package tv.vidiyo.android.source.channels;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
@IntDef({ChannelsMode.ALL, ChannelsMode.MY, ChannelsMode.EDIT})
public @interface ChannelsMode {
    int ALL = 1;
    int MY = 2;
    int EDIT = 4;
}
