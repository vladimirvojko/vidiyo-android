package tv.vidiyo.android.source.topic

import tv.vidiyo.android.api.model.channel.Channel
import tv.vidiyo.android.api.model.show.Show
import tv.vidiyo.android.api.model.topic.Topic
import tv.vidiyo.android.api.model.topic.TopicShow
import tv.vidiyo.android.base.adapter.provider.MutableListDataProvider

interface TopicDataSource {
    val topic: Topic
    val channel: Channel?
    val shows: Array<Show>
    val related: Array<Topic>
    val topicShows: Array<TopicShow>

    fun getProvider(index: Int): MutableListDataProvider<Show>
}