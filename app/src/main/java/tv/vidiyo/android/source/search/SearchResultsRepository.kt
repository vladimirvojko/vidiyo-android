package tv.vidiyo.android.source.search

import android.annotation.SuppressLint
import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.model.InnerPageType
import tv.vidiyo.android.util.SearchManager

class SearchResultsRepository(service: VidiyoService, manager: SearchManager) {

    private val peopleRepository = SearchPeopleRepository(service, manager)
    private val showsRepository = SearchShowsRepository(service, manager)
    private val castRepository = SearchCastRepository(service, manager)
    private val topicsRepository = SearchTopicsRepository(service, manager)

    @SuppressLint("SwitchIntDef")
    fun repositoryForType(@InnerPageType type: Int) = when (type) {
        InnerPageType.PEOPLE -> peopleRepository
        InnerPageType.SHOWS -> showsRepository
        InnerPageType.CAST -> castRepository
        InnerPageType.TOPICS -> topicsRepository
        else -> throw IllegalArgumentException()
    }

    fun getAll() = arrayOf(peopleRepository, showsRepository, castRepository, topicsRepository)
}