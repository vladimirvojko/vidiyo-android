package tv.vidiyo.android.source.episode

import android.annotation.SuppressLint
import retrofit2.Call
import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.api.model.ServerResponse
import tv.vidiyo.android.api.model.cast.Cast
import tv.vidiyo.android.api.model.comment.Comment
import tv.vidiyo.android.api.model.episode.Episode
import tv.vidiyo.android.api.model.episode.EpisodeInfo
import tv.vidiyo.android.api.model.episode.Product
import tv.vidiyo.android.api.model.topic.Topic
import tv.vidiyo.android.api.model.user.FollowingUser
import tv.vidiyo.android.base.adapter.provider.DataProvider
import tv.vidiyo.android.base.adapter.provider.MutableListDataProvider
import tv.vidiyo.android.base.adapter.provider.SectionedListDataProvider
import tv.vidiyo.android.base.adapter.section.Section
import tv.vidiyo.android.model.InnerPageType
import tv.vidiyo.android.source.ArrayDataSource
import tv.vidiyo.android.source.DataSource
import tv.vidiyo.android.source.Loadable
import tv.vidiyo.android.source.ProvidedDataSource
import tv.vidiyo.android.util.Constant

class EpisodeRepository(
        initialId: Int,
        private val service: VidiyoService
) : DataSource<EpisodeInfo>() {

    var id: Int = initialId
        set(value) {
            call?.cancel().also { call = null }
            field = value
            arrayOf(castRepository, commentsRepository, episodesRepository, topicsRepository, subscribersRepository).forEach {
                it.shouldRefresh = true
            }
            refresh()
        }

    override val size: Int = 1
    override val apiRequest: Call<ServerResponse<EpisodeInfo>> get() = service.episodeInfo(id)

    // TODO: commentsRepository
    val productsProvider = MutableListDataProvider<Product>()
    val castRepository = object : InnerEpisodeRepository<Cast>(service) {
        override val apiRequest: Call<ServerResponse<Array<Cast>>> get() = service.episodeCasts(id)
    }
    val commentsRepository = object : CommentsRepository(service) {
        override val provider = SectionedListDataProvider<Comment, Comment>()
        override val apiRequest: Call<ServerResponse<Array<Comment>>> get() = service.episodeComments(id, Constant.LIMIT, data?.lastOrNull()?.created)

        override fun handleResponse(response: ServerResponse<Array<Comment>>) {
            totalCount = response.meta?.count ?: 0

            val items = response.data ?: return
            val array = (data?.plus(items) ?: items).also { data = it }

            totalCount = array.size + if (items.isEmpty() || items.size < Constant.LIMIT) 0 else 5

            provider.sections.clear()
            provider.setSection(0, Section(hasHeader = true))
            for ((index, item) in array.withIndex()) {
                val reply = item.comments?.first
                provider.setSection(index + 1, Section(item, true, reply != null))
                reply?.let {
                    provider.setSectionItems(index + 1, listOf(it))
                }
            }
        }
    }
    val episodesRepository = object : InnerEpisodeRepository<Episode>(service) {
        override val apiRequest: Call<ServerResponse<Array<Episode>>> get() = service.episodeRelated(id)
    }
    val topicsRepository = object : InnerEpisodeRepository<Topic>(service) {
        override val apiRequest: Call<ServerResponse<Array<Topic>>> get() = service.episodeTopics(id)
    }
    val subscribersRepository = object : InnerEpisodeRepository<FollowingUser>(service) {
        override val apiRequest: Call<ServerResponse<Array<FollowingUser>>> get() = service.episodeSubscribers(id)
    }

    @SuppressLint("SwitchIntDef")
    fun get(@InnerPageType type: Int) = when (type) {
        InnerPageType.EPISODES -> episodesRepository
        InnerPageType.CAST -> castRepository
        InnerPageType.TOPICS -> topicsRepository
        InnerPageType.SUBSCRIBERS -> subscribersRepository
        InnerPageType.COMMENTS -> commentsRepository

        else -> throw IllegalArgumentException("Can't find inner repository for type $type")
    }

    override fun handleResponse(response: ServerResponse<EpisodeInfo>) {
        super.handleResponse(response)
        val products = data?.products ?: return
        productsProvider.addAll(products.toList(), true)
    }
}

abstract class BaseInnerEpisodeRepository<T, out P : DataProvider>(
        protected val service: VidiyoService
) : ArrayDataSource<T>(), ProvidedDataSource<P> {
    var shouldRefresh: Boolean = false

    override fun refresh() {
        super.refresh()
        shouldRefresh = false
    }
}

abstract class InnerEpisodeRepository<T>(
        service: VidiyoService
) : BaseInnerEpisodeRepository<T, MutableListDataProvider<T>>(service) {
    override val provider: MutableListDataProvider<T> = MutableListDataProvider()
    override fun handleResponse(response: ServerResponse<Array<T>>) {
        super.handleResponse(response)
        provider.addAll(data?.asList() ?: return, true)
    }
}

abstract class CommentsRepository(
        service: VidiyoService
) : BaseInnerEpisodeRepository<Comment, SectionedListDataProvider<Comment, Comment>>(service), Loadable {
    override var totalCount: Int = 0
}