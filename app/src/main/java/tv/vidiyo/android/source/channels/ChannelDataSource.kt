package tv.vidiyo.android.source.channels

import tv.vidiyo.android.api.model.channel.Channel
import tv.vidiyo.android.api.model.episode.Episode
import tv.vidiyo.android.api.model.show.Show
import tv.vidiyo.android.api.model.topic.Topic
import tv.vidiyo.android.api.model.topic.TopicShow
import tv.vidiyo.android.base.adapter.provider.SectionedListDataProvider

interface ChannelDataSource {
    var channel: Channel

    val shows: Array<Show>
    val topics: Array<Topic>
    val topicShows: Array<TopicShow>
    val featured: Episode?

    fun getProvider(index: Int): SectionedListDataProvider<Topic, Show>
}