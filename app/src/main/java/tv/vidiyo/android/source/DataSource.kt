package tv.vidiyo.android.source

import android.support.annotation.CallSuper
import tv.vidiyo.android.api.model.ServerResponse

abstract class DataSource<DATA> : BaseDataSource<DATA, ServerResponse<DATA>>() {

    @CallSuper
    override fun handleResponse(response: ServerResponse<DATA>) {
        data = response.data
    }
}