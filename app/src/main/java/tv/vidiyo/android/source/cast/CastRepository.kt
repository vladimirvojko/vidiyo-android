package tv.vidiyo.android.source.cast

import android.annotation.SuppressLint
import retrofit2.Call
import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.api.model.ServerResponse
import tv.vidiyo.android.api.model.cast.CastInfo
import tv.vidiyo.android.api.model.episode.Episode
import tv.vidiyo.android.api.model.show.Show
import tv.vidiyo.android.api.model.social.SocialConnection
import tv.vidiyo.android.api.model.social.connections
import tv.vidiyo.android.model.InnerPageType
import tv.vidiyo.android.source.DataSource
import tv.vidiyo.android.source.show.InnerProvidedRepository
import tv.vidiyo.android.util.Constant

class CastRepository(
        private val id: Int,
        private val service: VidiyoService,
        private val limit: Int = Constant.LIMIT
) : DataSource<CastInfo>() {

    override val apiRequest: Call<ServerResponse<CastInfo>> get() = service.castInfo(id)
    override val size: Int = 0

    val showsRepository = object : InnerProvidedRepository<Show>(service) {
        override val apiRequest: Call<ServerResponse<Array<Show>>> get() = service.castShows(id, limit, provider.items.size)
    }
    val episodesRepository = object : InnerProvidedRepository<Episode>(service) {
        override val apiRequest: Call<ServerResponse<Array<Episode>>> get() = service.castEpisodes(id, limit, provider.items.size)
    }
    val socialsRepository = object : InnerProvidedRepository<SocialConnection>(service) {
        override val apiRequest: Call<ServerResponse<Array<SocialConnection>>> get() = throw IllegalArgumentException("Useless api call")
        override val size: Int = 1
        override val isEmpty: Boolean = false

        override fun refresh() {}
    }

    @SuppressLint("SwitchIntDef")
    fun get(@InnerPageType type: Int) = when (type) {
        InnerPageType.SHOWS -> showsRepository
        InnerPageType.EPISODES -> episodesRepository
        InnerPageType.CONNECT_AND_SHARE -> socialsRepository

        else -> throw IllegalArgumentException("Can't find inner repository for type $type")
    }

    fun getAll() = arrayOf(showsRepository, episodesRepository)

    override fun handleResponse(response: ServerResponse<CastInfo>) {
        super.handleResponse(response)
        data?.let {
            socialsRepository.provider.addAll(it.socials.connections ?: return, true)
            socialsRepository.notifyDataLoaded()
        }
    }
}