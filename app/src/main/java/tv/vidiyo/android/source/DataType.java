package tv.vidiyo.android.source;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
@IntDef({DataType.TYPE_WATCHING, DataType.TYPE_FRIENDS_WATCHING, DataType.TYPE_TOP_CHANNELS,
        DataType.TYPE_TOP_PICKS, DataType.TYPE_SUBSCRIPTION, DataType.TYPE_YOUR_CHANNELS, })
public @interface DataType {
    int TYPE_WATCHING = 0;
    int TYPE_FRIENDS_WATCHING = 1;
    int TYPE_TOP_CHANNELS = 2;
    int TYPE_TOP_PICKS = 3;
    int TYPE_SUBSCRIPTION = 4;
    int TYPE_YOUR_CHANNELS = 5;
    int TYPE_CASTS_SMALL = 6;

    int[] HOME_VALUES = new int[]{
            TYPE_WATCHING, TYPE_FRIENDS_WATCHING, TYPE_TOP_CHANNELS, TYPE_TOP_PICKS, TYPE_SUBSCRIPTION, TYPE_YOUR_CHANNELS
    };

    int[] SEARCH_VALUES = new int[]{
            TYPE_CASTS_SMALL, TYPE_FRIENDS_WATCHING, TYPE_TOP_PICKS, TYPE_TOP_CHANNELS
    };
}
