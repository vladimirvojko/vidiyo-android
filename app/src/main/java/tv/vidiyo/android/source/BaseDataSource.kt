package tv.vidiyo.android.source

import retrofit2.Call
import tv.vidiyo.android.api.model.Message
import tv.vidiyo.android.extension.ServerCallback
import java.util.*

abstract class BaseDataSource<DATA, RESPONSE> : ServerCallback<RESPONSE>() {

    interface Callback {
        fun onDataLoaded()
        fun onDataNotAvailable(error: Message)
        fun onDataChanged() {}
    }

    // TODO: when unSubscribe?
    abstract val size: Int
    abstract val apiRequest: Call<RESPONSE> get
    open val isEmpty: Boolean get() = data == null

    protected var call: Call<RESPONSE>? = null
    protected val isForceLoad: Boolean get() = call != null

    private val subscribers: WeakHashMap<Callback, Boolean> = WeakHashMap(1)

    open var data: DATA? = null

    open fun refresh() {
        if (call == null) {
            call = apiRequest.also { it.enqueue(this) }
        }
    }

    fun subscribe(callback: Callback) = subscribers.put(callback, true)
    fun unSubscribe(callback: Callback) = subscribers.remove(callback)

    override fun onSuccess(response: RESPONSE, url: String?) {
        handleResponse(response)
        notifyDataLoaded()
        call = null
    }

    abstract fun handleResponse(response: RESPONSE)

    fun notifyDataLoaded() {
        subscribers.keys.forEach { it?.onDataLoaded() }
    }

    fun notifyDataChanged() {
        subscribers.keys.forEach { it?.onDataChanged() }
    }

    override fun onFailure(error: Message, url: String?) {
        call = null
        subscribers.keys.forEach { it?.onDataNotAvailable(error) }
    }
}