package tv.vidiyo.android.source.channels

import tv.vidiyo.android.api.model.channel.Channel

interface ChannelsDataSource {
    val channels: Array<Channel>
    val userChannelsIds: Array<Int>
    fun getItem(position: Int): Channel
}