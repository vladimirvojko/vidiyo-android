package tv.vidiyo.android.source.profile

import android.annotation.SuppressLint
import retrofit2.Call
import tv.vidiyo.android.account.UserAccountManager
import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.api.model.ServerResponse
import tv.vidiyo.android.api.model.cast.Cast
import tv.vidiyo.android.api.model.channel.Channel
import tv.vidiyo.android.api.model.channel.ChannelItems
import tv.vidiyo.android.api.model.episode.Episode
import tv.vidiyo.android.api.model.show.Show
import tv.vidiyo.android.api.model.topic.Topic
import tv.vidiyo.android.api.model.user.UserAccount
import tv.vidiyo.android.base.adapter.provider.DataProvider
import tv.vidiyo.android.base.adapter.provider.MutableListDataProvider
import tv.vidiyo.android.base.adapter.provider.SectionedListDataProvider
import tv.vidiyo.android.base.adapter.section.Section
import tv.vidiyo.android.model.InnerPageType
import tv.vidiyo.android.source.ArrayDataSource
import tv.vidiyo.android.source.DataSource
import tv.vidiyo.android.source.ProvidedDataSource

class ProfileRepository(
        val id: Int,
        private val service: VidiyoService,
        private val manager: UserAccountManager? = null
) : DataSource<UserAccount>() {

    override val size: Int = 0
    override val apiRequest: Call<ServerResponse<UserAccount>>
        get() = if (manager != null) service.me() else service.userProfile(id)

    override var data: UserAccount? = null
        set(value) {
            field = value
            if (!isForceLoad) {
                value?.let { manager?.updateAccountData(it) }
                notifyDataChanged()
            }
        }

    var confirmationWasClosed = false

    val showsRepository = object : InnerProfileRepository<ChannelItems<Show>, SectionedListDataProvider<Channel, Show>>(service) {
        override val provider: SectionedListDataProvider<Channel, Show> = SectionedListDataProvider()
        override val apiRequest: Call<ServerResponse<Array<ChannelItems<Show>>>> get() = service.userShows(id)

        override fun handleResponse(response: ServerResponse<Array<ChannelItems<Show>>>) {
            super.handleResponse(response)
            val data = data ?: return
            for ((index, item) in data.withIndex()) {
                provider.setSection(index, Section(item as Channel, true, false))
                provider.setSectionItems(index, item.items.asList())
            }
        }
    }
    val episodesRepository = object : InnerProfileRepository<Episode, MutableListDataProvider<Episode>>(service) {
        override val provider: MutableListDataProvider<Episode> = MutableListDataProvider()
        override val apiRequest: Call<ServerResponse<Array<Episode>>> get() = service.userEpisodes(id)

        override fun handleResponse(response: ServerResponse<Array<Episode>>) {
            super.handleResponse(response)
            provider.addAll(data?.asList() ?: return, true)
        }
    }
    val castRepository = object : InnerProfileRepository<ChannelItems<Cast>, SectionedListDataProvider<Channel, Cast>>(service) {
        override val provider: SectionedListDataProvider<Channel, Cast> = SectionedListDataProvider()
        override val apiRequest: Call<ServerResponse<Array<ChannelItems<Cast>>>> get() = service.userCasts(id)

        override fun handleResponse(response: ServerResponse<Array<ChannelItems<Cast>>>) {
            super.handleResponse(response)
            val data = data ?: return
            for ((index, item) in data.withIndex()) {
                provider.setSection(index, Section(item as Channel, true, false))
                provider.setSectionItems(index, item.items.asList())
            }
        }
    }
    val topicsRepository = object : InnerProfileRepository<ChannelItems<Topic>, SectionedListDataProvider<Channel, Topic>>(service) {
        override val provider: SectionedListDataProvider<Channel, Topic> = SectionedListDataProvider()
        override val apiRequest: Call<ServerResponse<Array<ChannelItems<Topic>>>> get() = service.userTopics(id)

        override fun handleResponse(response: ServerResponse<Array<ChannelItems<Topic>>>) {
            super.handleResponse(response)
            val data = data ?: return
            for ((index, item) in data.withIndex()) {
                provider.setSection(index, Section(item as Channel, true, false))
                provider.setSectionItems(index, item.items.asList())
            }
        }
    }

    @SuppressLint("SwitchIntDef")
    fun get(@InnerPageType type: Int) = when (type) {
        InnerPageType.SHOWS -> showsRepository
        InnerPageType.EPISODES -> episodesRepository
        InnerPageType.CAST -> castRepository
        InnerPageType.TOPICS -> topicsRepository

        else -> throw IllegalArgumentException("Can't find inner repository for type $type")
    }

    fun getAll() = arrayOf(showsRepository, episodesRepository, castRepository, topicsRepository)
}

abstract class InnerProfileRepository<T, out P : DataProvider>(
        protected val service: VidiyoService
) : ArrayDataSource<T>(), ProvidedDataSource<P> {
    var shouldRefresh: Boolean = false

    override fun handleResponse(response: ServerResponse<Array<T>>) {
        super.handleResponse(response)
        shouldRefresh = false
    }
}