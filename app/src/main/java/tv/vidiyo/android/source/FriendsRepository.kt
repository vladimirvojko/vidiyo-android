package tv.vidiyo.android.source

import retrofit2.Call
import tv.vidiyo.android.api.model.ServerResponse
import tv.vidiyo.android.api.model.user.Friend
import tv.vidiyo.android.base.adapter.provider.SectionedSelectableDataProvider
import tv.vidiyo.android.base.adapter.section.Section
import tv.vidiyo.android.model.Contact
import tv.vidiyo.android.model.FriendsListType

class FriendsRepository : DataSource<List<Friend>>(), FriendsDataSource {
    override val apiRequest: Call<ServerResponse<List<Friend>>> get() = throw NoSuchFieldException()

    override var type = FriendsListType.CONNECT_CONTACTS
    override var contacts: List<Contact>? = null
    override var friends: List<Friend>? get() = data
        set(value) {
            data = value
            data?.let {
                provider.setSection(0, Section(type, true, true))
                provider.setSectionItems(0, it)
            }
        }

    override fun refresh() {}
    override val size: Int get() = friends?.size ?: 0

    val provider: SectionedSelectableDataProvider<FriendsListType, Friend> = SectionedSelectableDataProvider()
}