package tv.vidiyo.android.source

import tv.vidiyo.android.api.model.ServerResponse

abstract class LoadableDataSource<T> : ArrayDataSource<T>(), Loadable {
    override var totalCount = 0

    override fun handleResponse(response: ServerResponse<Array<T>>) {
        val items = response.data ?: return
        val array = (data?.plus(items) ?: items).also { data = it }
        totalCount = response.meta?.count ?: 0
        additionalPreparation(array)
    }

    open protected fun additionalPreparation(array: Array<T>) {}
}