package tv.vidiyo.android.source.topic

import retrofit2.Call
import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.api.model.ServerResponse
import tv.vidiyo.android.api.model.channel.Channel
import tv.vidiyo.android.api.model.show.Show
import tv.vidiyo.android.api.model.topic.Topic
import tv.vidiyo.android.api.model.topic.TopicInfo
import tv.vidiyo.android.api.model.topic.TopicShow
import tv.vidiyo.android.base.adapter.provider.MutableListDataProvider
import tv.vidiyo.android.source.DataSource

class TopicRepository(override var topic: Topic, private val service: VidiyoService) : DataSource<TopicInfo>(), TopicDataSource {

    override val apiRequest: Call<ServerResponse<TopicInfo>> get() = service.topicInfo(topic.id)
    override val size: Int get() = 1

    override val channel: Channel? get() = data?.channel
    override val shows: Array<Show> get() = data?.shows ?: emptyArray()
    override val related: Array<Topic> get() = data?.topics ?: emptyArray()
    override val topicShows: Array<TopicShow> get() = data?.topicShows ?: emptyArray()

    override fun handleResponse(response: ServerResponse<TopicInfo>) {
        super.handleResponse(response)
        data?.topic?.let { topic = it }
    }

    override fun getProvider(index: Int) = MutableListDataProvider<Show>().apply {
        addAll(when (index) {
            -1 -> shows.asList()
            else -> {
                // TODO: update when download data
                if (index >= related.size) return@apply

                val topic = related[index]
                topicShows.filter { topic.id == it.id }.flatMap {
                    val id = it.showId
                    shows.filter { it.id == id }
                }
            }
        }, true)
    }
}