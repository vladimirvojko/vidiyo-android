package tv.vidiyo.android.source.show

import android.annotation.SuppressLint
import retrofit2.Call
import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.api.model.ServerResponse
import tv.vidiyo.android.api.model.cast.Cast
import tv.vidiyo.android.api.model.episode.Episode
import tv.vidiyo.android.api.model.show.ShowInfo
import tv.vidiyo.android.api.model.topic.Topic
import tv.vidiyo.android.api.model.user.FollowingUser
import tv.vidiyo.android.base.adapter.provider.MutableListDataProvider
import tv.vidiyo.android.model.InnerPageType
import tv.vidiyo.android.source.DataSource
import tv.vidiyo.android.source.LoadableDataSource
import tv.vidiyo.android.source.ProvidedDataSource
import tv.vidiyo.android.util.Constant

class ShowRepository(
        private val id: Int,
        private val service: VidiyoService
) : DataSource<ShowInfo>() {

    override val apiRequest: Call<ServerResponse<ShowInfo>> get() = service.showInfo(id)
    override val size: Int = 0

    val episodesRepository = object : InnerProvidedRepository<Episode>(service) {
        override val apiRequest: Call<ServerResponse<Array<Episode>>> get() = service.showEpisodes(id)
    }
    val castRepository = object : InnerProvidedRepository<Cast>(service) {
        override val apiRequest: Call<ServerResponse<Array<Cast>>> get() = service.showCasts(id)
    }
    // TODO: val socialRepository
    val topicsRepository = object : InnerProvidedRepository<Topic>(service) {
        override val apiRequest: Call<ServerResponse<Array<Topic>>> get() = service.showTopics(id)
    }
    val subscribersRepository = object : InnerProvidedRepository<FollowingUser>(service) {
        override val apiRequest: Call<ServerResponse<Array<FollowingUser>>> get() = service.showSubscribers(id)
    }

    @SuppressLint("SwitchIntDef")
    fun get(@InnerPageType type: Int) = when (type) {
        InnerPageType.EPISODES -> episodesRepository
        InnerPageType.CAST -> castRepository
        InnerPageType.TOPICS -> topicsRepository
        InnerPageType.SUBSCRIBERS -> subscribersRepository

        else -> throw IllegalArgumentException("Can't find inner repository for type $type")
    }

    fun getAll() = arrayOf(episodesRepository, castRepository, topicsRepository, subscribersRepository)
}

abstract class InnerProvidedRepository<T>(
        protected val service: VidiyoService
) : LoadableDataSource<T>(), ProvidedDataSource<MutableListDataProvider<T>> {

    override val provider: MutableListDataProvider<T> = MutableListDataProvider()
    var shouldRefresh = false

    override fun additionalPreparation(array: Array<T>) {
        provider.addAll(data?.asList() ?: return, true)
    }

    override fun handleResponse(response: ServerResponse<Array<T>>) {
        super.handleResponse(response)
        val data = response.data
        totalCount = provider.items.size + if (data == null || data.isEmpty() || data.size < Constant.LIMIT) 0 else 5
        shouldRefresh = false
    }
}