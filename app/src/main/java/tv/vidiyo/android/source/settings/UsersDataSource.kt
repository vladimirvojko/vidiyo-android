package tv.vidiyo.android.source.settings

import tv.vidiyo.android.api.model.user.FollowingUser

interface UsersDataSource {
    val users: Array<FollowingUser>
}