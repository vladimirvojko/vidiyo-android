package tv.vidiyo.android.source.social

import android.support.annotation.CallSuper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import tv.vidiyo.android.api.*
import tv.vidiyo.android.api.graph.FBPost
import tv.vidiyo.android.api.graph.FBResponse
import tv.vidiyo.android.api.instagram.IGAccessToken
import tv.vidiyo.android.api.instagram.IGMedia
import tv.vidiyo.android.api.instagram.IGResponse
import tv.vidiyo.android.api.instagram.IGUser
import tv.vidiyo.android.api.model.Message
import tv.vidiyo.android.api.model.ServerResponse
import tv.vidiyo.android.api.model.social.Socials
import tv.vidiyo.android.api.twitter.Tweet
import tv.vidiyo.android.base.adapter.provider.MutableListDataProvider
import tv.vidiyo.android.extension.ServerCallback
import tv.vidiyo.android.extension.lastSegment
import tv.vidiyo.android.source.BaseDataSource
import tv.vidiyo.android.source.ProvidedDataSource
import tv.vidiyo.android.util.FeedHandler
import java.lang.ref.WeakReference

class SocialFeedRepository(val socials: Socials?, private val service: VidiyoService) {

    private val handler = FeedHandler(this)
    var listener: WeakReference<BaseDataSource.Callback>? = null

    var facebookRepository: InnerSocialRepository<FBResponse<FBPost>>? = null
    var instagramRepository: InnerSocialRepository<IGResponse<IGMedia>>? = null
    var twitterRepository: InnerSocialRepository<Array<Tweet>>? = null

    val mainProvider = MutableListDataProvider<Dateable>()

    fun start() {
        if (mainProvider.items.isEmpty()) {
            val socials = socials
            if (socials == null) {
                notifyDataLoaded()
                return
            }

            socials.facebook?.lastSegment?.let {
                handler.preparation()
                service.facebookAccessToken().enqueue(initFbCallback(it))
            }
            socials.instagram?.lastSegment?.let {
                handler.preparation()
                service.instagramAccessToken().enqueue(initInstagramCallback(it))
            }
            socials.twitter?.lastSegment?.let {
                handler.preparation()
                service.twitterAccessToken().enqueue(initTwitterCallback(it))
            }

        } else notifyDataLoaded()
    }

    fun load() {
        facebookRepository?.let {
            handler.refreshing()
            it.refresh()
        }
        instagramRepository?.let {
            handler.refreshing()
            it.refresh()
        }
        twitterRepository?.let {
            handler.refreshing()
            it.refresh()
        }
    }

    fun notifyDataLoaded() {
        val items = mainProvider.items.sortedByDescending { it.date }
        mainProvider.addAll(items, true)
        listener?.get()?.onDataLoaded()
    }

    //region initialize
    private fun initFbCallback(path: String) = object : Callback<AccessToken> {
        override fun onResponse(call: Call<AccessToken>, r: Response<AccessToken>) {
            handler.preparationEnd()
            val t = r.body()?.token ?: return
            facebookRepository = object : InnerSocialRepository<FBResponse<FBPost>>(t, service, handler) {
                override val apiRequest: Call<FBResponse<FBPost>> get() = service.facebookFeed(path, token)

                override fun handleResponse(response: FBResponse<FBPost>) {
                    mainProvider.addAll(response.data.toList())
                    super.handleResponse(response)
                }
            }
        }

        override fun onFailure(call: Call<AccessToken>?, t: Throwable?) = handler.preparationEnd()
    }

    private fun initInstagramCallback(path: String) = object : ServerCallback<ServerResponse<IGAccessToken>>() {
        override fun onSuccess(response: ServerResponse<IGAccessToken>, url: String?) {
            response.data?.let {
                if (!it.active) return
                service.instagramUserSearch(path, it.token).enqueue(instagramSearchCallback(it.token))
            } ?: handler.preparationEnd()
        }

        override fun onFailure(error: Message, url: String?) = handler.preparationEnd()
    }

    private fun instagramSearchCallback(accessToken: String) = object : Callback<IGResponse<IGUser>> {
        override fun onResponse(call: Call<IGResponse<IGUser>>?, r: Response<IGResponse<IGUser>>) {
            handler.preparationEnd()
            val body = r.body() ?: return
            val user = body.data.firstOrNull() ?: return
            instagramRepository = object : InnerSocialRepository<IGResponse<IGMedia>>(accessToken, service, handler) {
                override val apiRequest: Call<IGResponse<IGMedia>> get() = service.instagramRecents(user.id, token)

                override fun handleResponse(response: IGResponse<IGMedia>) {
                    mainProvider.addAll(response.data.toList())
                    super.handleResponse(response)
                }
            }
        }

        override fun onFailure(call: Call<IGResponse<IGUser>>?, t: Throwable?) = handler.preparationEnd()
    }

    private fun initTwitterCallback(path: String) = object : Callback<AccessToken> {
        override fun onResponse(call: Call<AccessToken>?, r: Response<AccessToken>) {
            handler.preparationEnd()
            val accessToken = r.body() ?: return
            twitterRepository = object : InnerSocialRepository<Array<Tweet>>(accessToken.token, service, handler) {
                override val apiRequest: Call<Array<Tweet>> get() = service.twitterTweets(token, path)

                override fun handleResponse(response: Array<Tweet>) {
                    mainProvider.addAll(response.toList())
                    super.handleResponse(response)
                }
            }
        }

        override fun onFailure(call: Call<AccessToken>?, t: Throwable?) = handler.preparationEnd()
    }
    //endregion
}

abstract class InnerSocialRepository<T>(protected val token: String, protected val service: VidiyoService, private val handler: FeedHandler)
    : BaseDataSource<T, T>(), ProvidedDataSource<MutableListDataProvider<T>> {

    override val provider = MutableListDataProvider<T>()
    override val size: Int get() = provider.items.size

    @CallSuper
    override fun handleResponse(response: T) {
        handler.refreshingEnd()
    }

    override fun onFailure(error: Message, url: String?) {
        call = null
        handler.refreshingEnd()
    }
}