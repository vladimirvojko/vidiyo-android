package tv.vidiyo.android.source.channels

import retrofit2.Call
import tv.vidiyo.android.api.VidiyoService
import tv.vidiyo.android.api.model.ServerResponse
import tv.vidiyo.android.api.model.channel.Channel
import tv.vidiyo.android.api.model.channel.ChannelInfo
import tv.vidiyo.android.api.model.episode.Episode
import tv.vidiyo.android.api.model.show.Show
import tv.vidiyo.android.api.model.topic.Topic
import tv.vidiyo.android.api.model.topic.TopicShow
import tv.vidiyo.android.base.adapter.provider.SectionedListDataProvider
import tv.vidiyo.android.base.adapter.section.Section
import tv.vidiyo.android.source.DataSource

class ChannelRepository(override var channel: Channel, private val service: VidiyoService) : DataSource<ChannelInfo>(), ChannelDataSource {

    override val apiRequest: Call<ServerResponse<ChannelInfo>> get() = service.channelInfo(channel.id)
    override val size: Int get() = 1

    override val shows: Array<Show> get() = data?.shows ?: emptyArray()
    override val topics: Array<Topic> get() = data?.topics ?: emptyArray()
    override val topicShows: Array<TopicShow> get() = data?.topicShows ?: emptyArray()
    override val featured: Episode? get() = data?.featured

    override fun getProvider(index: Int) = SectionedListDataProvider<Topic, Show>().apply {
        val topic = topics[index]
        setSection(0, Section(topic, true))
        setSectionItems(0, topicShows.filter { topic.id == it.id }.flatMap {
            val id = it.showId
            shows.filter { it.id == id }
        })
    }
}