package tv.vidiyo.android.os

import android.content.Context
import android.graphics.Bitmap
import android.media.ThumbnailUtils
import android.os.AsyncTask
import tv.vidiyo.android.extension.cacheBitmap
import tv.vidiyo.android.extension.weak
import java.lang.ref.WeakReference


class CropAndSave(context: Context, listener: AsyncTaskListener<String>,
        val bitmap: Bitmap,
        val maxSize: Int
) : AsyncTask<Unit, Unit, String>() {

    val context: WeakReference<Context> = context.weak()
    val listener: WeakReference<AsyncTaskListener<String>> = listener.weak()

    private fun clearCacheDir() {
        val cacheDir = context.get()?.cacheDir ?: return
        cacheDir.listFiles().filter { it.isFile }.forEach { it.delete() }
    }

    override fun doInBackground(vararg params: Unit?): String? {
        clearCacheDir()

        val size = Math.min(Math.min(bitmap.width, bitmap.height), maxSize)
        return context.get()?.cacheBitmap(ThumbnailUtils.extractThumbnail(bitmap, size, size))
    }

    override fun onPostExecute(result: String?) {
        super.onPostExecute(result)
        listener.get()?.onTaskComplete(result)
    }
}