package tv.vidiyo.android.os

interface AsyncTaskListener<in RESULT> {
    fun onTaskComplete(result: RESULT?)
}