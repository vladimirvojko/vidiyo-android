# Kotlin
-assumenosideeffects class kotlin.jvm.toggleSubscriptionInternal.Intrinsics {
    static void checkParameterIsNotNull(java.lang.Object, java.lang.String);
}

# AppCompat
-keep public class android.support.v7.widget.** { *; }
-keep public class android.support.v7.toggleSubscriptionInternal.widget.** { *; }
-keep public class android.support.v7.toggleSubscriptionInternal.view.menu.** { *; }
-keep public class * extends android.support.v4.view.ActionProvider {
    public <init>(android.content.Context);
}

# Google
-keep public class com.google.android.gms.* { public *; }
-dontwarn com.google.android.gms.**
-keep class * extends java.util.ListResourceBundle {
    protected Object[][] getContents();
}
-keep public class com.google.android.gms.common.toggleSubscriptionInternal.safeparcel.SafeParcelable {
    public static final *** NULL;
}
-keepnames @com.google.android.gms.common.annotation.KeepName class *
-keepclassmembernames class * {
    @com.google.android.gms.common.annotation.KeepName *;
}
-keepnames class * implements android.os.Parcelable {
    public static final ** CREATOR;
}

# Fabric
-keep public class * extends java.lang.Exception
-keepattributes *Annotation*, SourceFile, LineNumberTable
-keep class com.crashlytics.** { *; }
-dontwarn com.crashlytics.**

# Glide
-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public class * extends com.bumptech.glide.AppGlideModule
-keep public enum com.bumptech.glide.load.resource.bitmap.ImageHeaderParser$** {
    **[] $VALUES;
    public *;
}

# OkHttp
-dontwarn okio.**
-dontwarn okhttp3.**
-dontwarn javax.annotation.*

# Retrofit2
-dontnote retrofit2.Platform
-dontwarn retrofit2.Platform$Java8
-keepattributes Signature
-keepattributes Exceptions

# LeakCanary
-dontwarn com.squareup.haha.guava.**
-dontwarn com.squareup.haha.perflib.**
-dontwarn com.squareup.haha.trove.**
-dontwarn com.squareup.leakcanary.**
-keep class com.squareup.haha.** { *; }
-keep class com.squareup.leakcanary.** { *; }
-dontwarn android.app.Notification

# Facebook
-keep class com.facebook.** {
   *;
}

# Gson
-keep class tv.vidiyo.android.api.model.** { *; }

# BottomBar
-dontwarn com.roughike.bottombar.**