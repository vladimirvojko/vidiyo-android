package com.google.android.exoplayer2.castdemo;

import android.view.View;

public interface OnAdPlayingListener {
    void onCurrentProgressChanged(long current, long duration);

    View getView();
}
