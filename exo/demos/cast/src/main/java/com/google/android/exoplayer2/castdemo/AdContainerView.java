package com.google.android.exoplayer2.castdemo;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

public class AdContainerView extends FrameLayout implements OnAdPlayingListener {

    private final TextView tvDuration;

    public AdContainerView(@NonNull Context context) {
        this(context, null);
    }

    public AdContainerView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AdContainerView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate(context, R.layout.view_advertisement, this);
        tvDuration = findViewById(R.id.exo_ad_duration);
    }

    @Override
    public void onCurrentProgressChanged(long current, long duration) {
        tvDuration.setText("" + current);
    }

    @Override
    public View getView() {
        return this;
    }
}
